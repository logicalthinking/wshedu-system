package com.logicalthinking.manage.system.annotation;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.alibaba.fastjson.JSONObject;
import com.hellooop.odj.token.manager.TokenManager;
import com.logicalthinking.manage.system.controller.BaseController;
import com.logicalthinking.manage.system.entity.SysLogInfo;
import com.logicalthinking.manage.system.service.SysLogInfoService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DateUtil;

@Component
@Aspect
public class LogAspect {
	
	private Logger logger = LoggerFactory.getLogger(LogAspect.class);
	
	@Autowired
	private SysLogInfoService sysLogInfoService;
	
	@Around(value = "@annotation(sysLog)") // aspect增强注解，对存在该注解方法的前后做拦截
	public Object logAroud(ProceedingJoinPoint joinPoint, SysLog sysLog)
			throws Throwable {
		// 执行方法体
		Object result = joinPoint.proceed();
		
		SysLogInfo sysLogInfo = new SysLogInfo();
		sysLogInfo.setLogId(UUID.randomUUID().toString());
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		Map<String, String[]> params = request.getParameterMap();
		sysLogInfo.setOperateParam(JSONObject.toJSONString(params));
		
		String ip  = getIpAddress(request);
		sysLogInfo.setIp(ip);
		
		Integer port = request.getRemotePort();
		sysLogInfo.setPort(port);
		
		// 获取切点处的签名
		MethodSignature methodSignature = (MethodSignature) joinPoint
				.getSignature();
		// 获取当前的方法
		Method method = methodSignature.getMethod();
		RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);
		
		String operateUrl = requestMapping.value()[0];
		sysLogInfo.setOperateUrl(operateUrl);
		
		String operateType = sysLog.operateType();
		sysLogInfo.setOperateName(operateType);
		
		String sysCode = StringUtils.isNotBlank(sysLog.sysCode())?sysLog.sysCode():ConstantUtil.SYS_CODE_PORTAL;
		sysLogInfo.setSysCode(sysCode);
		
		String logCustType = request.getAttribute("logCustType")!=null?(String)request.getAttribute("logCustType"):ConstantUtil.CUST_TYPE_PC;
		sysLogInfo.setLogCustType(logCustType);
		
		String operateDesc = (String) request.getAttribute("operateDesc");
		sysLogInfo.setOperateDesc(operateDesc);
		
		String token = BaseController.getToken(request);
		sysLogInfo.setUserToken(token);
		
		TokenManager tokenManager = new TokenManager();
		String userId = "";
		try {
			userId = tokenManager.getTokenData(token, "userId");
		} catch (Exception e) {
			e.printStackTrace();
		}
		userId = StringUtils.isNotBlank(userId)?userId:(String)request.getAttribute("userId");
		sysLogInfo.setUserId(userId);
		
		sysLogInfo.setCreateTime(DateUtil.formatDate(new Date(), DateUtil.DATE_TIME_PATTERN));
		sysLogInfo.setState(ConstantUtil.IS_VALID_Y);
		
		sysLogInfoService.insertSysLogInfo(sysLogInfo);
		
		logger.info("operateType:{},operateUrl:{},param:{}",
				operateType,operateUrl,JSONObject.toJSONString(params));
		
		return result;
	}

	/**
	 * 获取IP地址的方法
	 * 
	 * @param request
	 *            传一个request对象下来
	 * @return
	 */
	private static String getIpAddress(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
}
