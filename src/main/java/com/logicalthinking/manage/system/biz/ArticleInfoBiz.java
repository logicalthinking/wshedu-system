package com.logicalthinking.manage.system.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.ArticleInfo;
import com.logicalthinking.manage.system.service.ArticleInfoService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.DateUtil;
import com.logicalthinking.manage.system.utils.RegexUtil;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.utils.StringUtil;
import com.logicalthinking.manage.system.utils.ValidateException;

/**
 * 文案信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Service
public class ArticleInfoBiz {

	@Resource
	private ArticleInfoService articleInfoService;

	/**
	 * 查询ArticleInfo集合
	 */
	public ResultEntity<List<ArticleInfo>> articleInfoList(Map<String, Object> map) throws DaoException, ValidateException{
		List<ArticleInfo> articleInfos = articleInfoService.selectArticleInfoList(map);
		int total = articleInfoService.selectArticleInfoListCount(map);
		return new ResultEntity<List<ArticleInfo>>(ConstantUtil.CODE_200, articleInfos, total);
	}
	
	
	/**
	 * 查询ArticleInfo集合
	 */
	public List<ArticleInfo> selectAllArticleInfoList(Map<String, Object> map) throws DaoException{
		return  articleInfoService.selectAllArticleInfoList(map);
	}
	
	
	/**
	 * 查询ArticleInfo
	 * @throws DaoException 
	 */
	public List<ArticleInfo> selectArticleListByType(String articleType) throws DaoException{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("articleType", articleType);
		map.put("sortName", "rank");
		map.put("sortOrder", "asc");
		map.put("state", ConstantUtil.IS_VALID_Y);
		List<ArticleInfo> articleInfos = articleInfoService.selectAllArticleInfoList(map);
		return articleInfos;
	}
	
	
	/**
	 * 查询ArticleInfo
	 * @throws DaoException 
	 */
	public ArticleInfo selectArticleInfoByType(String articleType) throws DaoException{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("articleType", articleType);
		map.put("sortName", "create_time");
		map.put("sortOrder", "desc");
		map.put("state", ConstantUtil.IS_VALID_Y);
		List<ArticleInfo> articleInfos = articleInfoService.selectAllArticleInfoList(map);
		if(articleInfos!=null){
			return articleInfos.get(0);
		}
		return null;
	}

	/**
	 * 根据articleId查询ArticleInfo
	 */
	public ResultEntity<ArticleInfo> articleInfoByArticleId(String articleId) throws DaoException{
		if (StringUtils.isNotBlank(articleId)) {
			ArticleInfo articleInfo = articleInfoService.selectArticleInfoByArticleId(articleId);
			return new ResultEntity<ArticleInfo>(ConstantUtil.CODE_200, articleInfo);
		} else {
			return new ResultEntity<ArticleInfo>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增ArticleInfo
	 */
	public ResultEntity<ArticleInfo> insertArticleInfo(ArticleInfo articleInfo) throws DaoException{
		articleInfo.setArticleId(UUID.randomUUID().toString());
		articleInfo.setCreateTime(DateUtil.getNowDate(DateUtil.DATE_TIME_PATTERN));
		articleInfo.setState(ConstantUtil.IS_VALID_Y);
		articleInfoService.insertArticleInfo(articleInfo);
		return new ResultEntity<ArticleInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改ArticleInfo
	 */
	public ResultEntity<ArticleInfo> updateArticleInfo(ArticleInfo articleInfo) throws DaoException{
		articleInfoService.updateArticleInfo(articleInfo);
		return new ResultEntity<ArticleInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除ArticleInfo
	 */
	public ResultEntity<ArticleInfo> deleteArticleInfo(Map<String, Object> map) throws DaoException{
		String articleIds = (String) map.get("articleIds");
		if (StringUtil.isNotNull(articleIds) && !RegexUtil.isCharAndQuot(articleIds)) {
			String newIds = StringUtil.strToCharAndQuot(articleIds);
			map.put("articleIds",newIds);
		}
		articleInfoService.deleteArticleInfo(map);
		return new ResultEntity<ArticleInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}


	/**
	 * 排序
	 * @param articleIds
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public ResultEntity<ArticleInfo> articleSort(String[] articleIds) throws DaoException, ValidateException {
		if(articleIds==null || articleIds.length == 0){
			throw new ValidateException("参数错误！"); 
		}
		int rank = 1;
		for(String articleId: articleIds){
			ArticleInfo articleInfo = new ArticleInfo();
			articleInfo.setArticleId(articleId);
			articleInfo.setRank(rank);
			articleInfoService.updateArticleInfo(articleInfo);
			rank++;
		}
		return new ResultEntity<ArticleInfo>(ConstantUtil.CODE_200, "排序成功");
	}

}