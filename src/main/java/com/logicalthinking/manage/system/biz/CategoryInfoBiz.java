package com.logicalthinking.manage.system.biz;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import com.logicalthinking.manage.system.entity.CategoryInfo;
import com.logicalthinking.manage.system.service.CategoryInfoService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.RegexUtil;
import com.logicalthinking.manage.system.utils.StringUtil;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 分类管理业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Service
public class CategoryInfoBiz {

	@Resource
	private CategoryInfoService categoryInfoService;

	/**
	 * 查询CategoryInfo集合
	 */
	public ResultEntity<List<CategoryInfo>> categoryInfoList(Map<String, Object> map) throws DaoException, ValidateException{
		List<CategoryInfo> categoryInfos = categoryInfoService.selectCategoryInfoList(map);
		int total = categoryInfoService.selectCategoryInfoListCount(map);
		return new ResultEntity<List<CategoryInfo>>(ConstantUtil.CODE_200, categoryInfos, total);
	}

	/**
	 * 查询CategoryInfo集合
	 */
	public List<CategoryInfo> categoryInfoAllList(Map<String, Object> map) throws DaoException, ValidateException{
		List<CategoryInfo> categoryInfos = categoryInfoService.selectAllCategoryInfoList(map);
		return categoryInfos;
	}

	
	/**
	 * 根据typeId查询CategoryInfo
	 */
	public ResultEntity<CategoryInfo> categoryInfoByTypeId(String typeId) throws DaoException{
		if (StringUtils.isNotBlank(typeId)) {
			CategoryInfo categoryInfo = categoryInfoService.selectCategoryInfoByTypeId(typeId);
			return new ResultEntity<CategoryInfo>(ConstantUtil.CODE_200, categoryInfo);
		} else {
			return new ResultEntity<CategoryInfo>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增CategoryInfo
	 */
	public ResultEntity<CategoryInfo> insertCategoryInfo(CategoryInfo categoryInfo) throws DaoException{
		categoryInfo.setTypeId(UUID.randomUUID().toString());
		categoryInfo.setState(ConstantUtil.IS_VALID_Y);
		categoryInfoService.insertCategoryInfo(categoryInfo);
		return new ResultEntity<CategoryInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改CategoryInfo
	 */
	public ResultEntity<CategoryInfo> updateCategoryInfo(CategoryInfo categoryInfo) throws DaoException{
		categoryInfoService.updateCategoryInfo(categoryInfo);
		return new ResultEntity<CategoryInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除CategoryInfo
	 */
	public ResultEntity<CategoryInfo> deleteCategoryInfo(Map<String, Object> map) throws DaoException{
		String typeIds = (String) map.get("typeIds");
		if (StringUtil.isNotNull(typeIds) && !RegexUtil.isCharAndQuot(typeIds)) {
			String newIds = StringUtil.strToCharAndQuot(typeIds);
			map.put("typeIds",newIds);
		}
		categoryInfoService.deleteCategoryInfo(map);
		return new ResultEntity<CategoryInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}

}