package com.logicalthinking.manage.system.biz;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import com.logicalthinking.manage.system.entity.ContactRecord;
import com.logicalthinking.manage.system.service.ContactRecordService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 学员联系记录业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class ContactRecordBiz {

	@Resource
	private ContactRecordService contactRecordService;

	/**
	 * 查询ContactRecord集合
	 */
	public ResultEntity<List<ContactRecord>> contactRecordList(Map<String, Object> map) throws DaoException, ValidateException{
		List<ContactRecord> contactRecords = contactRecordService.selectContactRecordList(map);
		int total = contactRecordService.selectContactRecordListCount(map);
		return new ResultEntity<List<ContactRecord>>(ConstantUtil.CODE_200, contactRecords, total);
	}

	/**
	 * 根据contactId查询ContactRecord
	 */
	public ResultEntity<ContactRecord> contactRecordByContactId(String contactId) throws DaoException{
		if (StringUtils.isNotBlank(contactId)) {
			ContactRecord contactRecord = contactRecordService.selectContactRecordByContactId(contactId);
			return new ResultEntity<ContactRecord>(ConstantUtil.CODE_200, contactRecord);
		} else {
			return new ResultEntity<ContactRecord>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增ContactRecord
	 */
	public ResultEntity<ContactRecord> insertContactRecord(ContactRecord contactRecord) throws DaoException{
		contactRecord.setContactId(UUID.randomUUID().toString());
		contactRecordService.insertContactRecord(contactRecord);
		return new ResultEntity<ContactRecord>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改ContactRecord
	 */
	public ResultEntity<ContactRecord> updateContactRecord(ContactRecord contactRecord) throws DaoException{
		contactRecordService.updateContactRecord(contactRecord);
		return new ResultEntity<ContactRecord>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除ContactRecord
	 */
	public ResultEntity<ContactRecord> deleteContactRecord(Map<String, Object> map) throws DaoException{
		contactRecordService.deleteContactRecord(map);
		return new ResultEntity<ContactRecord>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}

}