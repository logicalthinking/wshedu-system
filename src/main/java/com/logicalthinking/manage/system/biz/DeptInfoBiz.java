package com.logicalthinking.manage.system.biz;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import com.logicalthinking.manage.system.entity.DeptInfo;
import com.logicalthinking.manage.system.service.DeptInfoService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.RegexUtil;
import com.logicalthinking.manage.system.utils.StringUtil;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.vo.DeptTreeVo;

/**
 * 部门信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class DeptInfoBiz {

	@Resource
	private DeptInfoService deptInfoService;

	/**
	 * 查询DeptInfo集合
	 */
	public ResultEntity<List<DeptInfo>> deptInfoList(Map<String, Object> map) throws DaoException, ValidateException{
		List<DeptInfo> deptInfos = deptInfoService.selectDeptInfoList(map);
		int total = deptInfoService.selectDeptInfoListCount(map);
		return new ResultEntity<List<DeptInfo>>(ConstantUtil.CODE_200, deptInfos, total);
	}
	
	
	public List<DeptInfo> deptInfoAllList(Map<String, Object> map) throws DaoException{
		return deptInfoService.selectAllDeptInfoList(map);
	}
	
	/**
	 * 查询组织结构树
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<DeptTreeVo> selectDeptInfoTreeList(Map<String, Object> map) throws DaoException{
		return deptInfoService.selectDeptInfoTreeList(map);
	}
	

	/**
	 * 根据deptId查询DeptInfo
	 */
	public ResultEntity<DeptInfo> deptInfoByDeptId(String deptId) throws DaoException{
		if (StringUtils.isNotBlank(deptId)) {
			DeptInfo deptInfo = deptInfoService.selectDeptInfoByDeptId(deptId);
			return new ResultEntity<DeptInfo>(ConstantUtil.CODE_200, deptInfo);
		} else {
			return new ResultEntity<DeptInfo>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增DeptInfo
	 */
	public ResultEntity<DeptInfo> insertDeptInfo(DeptInfo deptInfo) throws DaoException{
		deptInfo.setDeptId(UUID.randomUUID().toString());
		deptInfoService.insertDeptInfo(deptInfo);
		return new ResultEntity<DeptInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改DeptInfo
	 */
	public ResultEntity<DeptInfo> updateDeptInfo(DeptInfo deptInfo) throws DaoException{
		deptInfoService.updateDeptInfo(deptInfo);
		return new ResultEntity<DeptInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除DeptInfo
	 */
	public ResultEntity<DeptInfo> deleteDeptInfo(Map<String, Object> map) throws DaoException{
		String deptIds = (String) map.get("deptIds");
		if (StringUtil.isNotNull(deptIds) && !RegexUtil.isCharAndQuot(deptIds)) {
			String newIds = StringUtil.strToCharAndQuot(deptIds);
			map.put("deptIds",newIds);
		}
		deptInfoService.deleteDeptInfo(map);
		return new ResultEntity<DeptInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}

}