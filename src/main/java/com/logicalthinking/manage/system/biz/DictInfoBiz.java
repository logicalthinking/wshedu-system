package com.logicalthinking.manage.system.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.logicalthinking.manage.system.entity.DictInfo;
import com.logicalthinking.manage.system.service.CacheService;
import com.logicalthinking.manage.system.service.DictInfoService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.DictConstant;
import com.logicalthinking.manage.system.utils.RedisUtils;
import com.logicalthinking.manage.system.utils.RegexUtil;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.utils.StringUtil;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.vo.DictTreeVo;
import com.logicalthinking.manage.system.vo.KeyValue;

/**
 * 字典信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Service
public class DictInfoBiz {

	@Resource
	private DictInfoService dictInfoService;

	@Autowired
	private CacheService cacheService;
	
	@Autowired
	private RedisUtils redisUtils;
	
	/**
	 * 查询DictInfo集合
	 */
	public ResultEntity<List<DictInfo>> dictInfoList(Map<String, Object> map) throws DaoException, ValidateException{
		List<DictInfo> dictInfos = dictInfoService.selectDictInfoList(map);
		int total = dictInfoService.selectDictInfoListCount(map);
		return new ResultEntity<List<DictInfo>>(ConstantUtil.CODE_200, dictInfos, total);
	}
	
	
	/**
	 * 查询所有DictInfo集合
	 */
	public List<DictInfo> dictInfoAllList(Map<String, Object> map) throws DaoException{
		List<DictInfo> dictInfos = dictInfoService.selectAllDictInfoList(map);
		return dictInfos;
	}
	
	/**
	 *  从字典缓存读取值
	 *  
	 * @param dictType
	 * @param dictValue
	 * @return
	 */
	public String getDictCacheValue(String dictType,String dictValue){
		String key = dictType+"_"+dictValue;
		return (String) redisUtils.hget(ConstantUtil.REDIS_KEY_DICT_MAP,key);
	}

	/**
	 * 根据dictId查询DictInfo
	 */
	public ResultEntity<DictInfo> dictInfoByDictId(String dictId) throws DaoException{
		if (StringUtils.isNotBlank(dictId)) {
			DictInfo dictInfo = dictInfoService.selectDictInfoByDictId(dictId);
			return new ResultEntity<DictInfo>(ConstantUtil.CODE_200, dictInfo);
		} else {
			return new ResultEntity<DictInfo>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}
	
	/**
	 * 根据dictId查询DictInfo
	 */
	public DictInfo selectDictInfoByDictId(String dictId) throws DaoException{
		DictInfo dictInfo = dictInfoService.selectDictInfoByDictId(dictId==null?"":dictId);
		return dictInfo;
	}
	
	/**
	 * 查询单个对象
	 * @param dictType
	 * @param dictValue
	 * @return
	 * @throws DaoException
	 */
	public DictInfo selectDictInfoByTypeValue(String dictType,String dictValue) throws DaoException{
		DictInfo dictInfo = dictInfoService.selectDictInfoByTypeValue(dictType, dictValue);
		return dictInfo;
	}
	
	/**
	 * 查询集合对象
	 * @param dictType
	 * @param dictValue
	 * @return
	 * @throws DaoException
	 */
	public List<KeyValue> selectDictChidren(String dictType,String dictValue) throws DaoException{
		return dictInfoService.selectDictChildren(dictType,dictValue);
	}
	
	/**
	 * 查询集合对象
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<KeyValue> selectDictChidren(Map<String, Object> map) throws DaoException{
		return dictInfoService.selectDictChildren(map);
	}
	
	/**
	 * 查询集合对象
	 * @param dictType
	 * @return
	 * @throws DaoException
	 */
	public List<KeyValue> selectDictChidren(String dictType) throws DaoException{
		return dictInfoService.selectDictChildren(dictType);
	}
	
	
	/**
	 * 清除字典缓存
	 * @throws Exception 
	 */
	public void clearDictsCache() throws Exception{
		cacheService.clearDictCache();
		cacheService.initDictCache();
	}
	
	/**
	 * 查询字典tree集合
	 * @throws Exception 
	 */
	public List<DictTreeVo> getDictTreeVoList(Map<String, Object> map) throws DaoException{
		
		List<DictTreeVo> dictTreeVos = null;
		boolean flag = redisUtils.hasKey(ConstantUtil.REDIS_KEY_DICT_TREE_LIST);
		if(flag == true){
			String json = (String) redisUtils.get(ConstantUtil.REDIS_KEY_DICT_TREE_LIST);
			if(StringUtil.isNotNull(json)){
				dictTreeVos = JSONObject.parseArray(json, DictTreeVo.class);
			}
		}else{
			dictTreeVos = dictInfoService.selectDictTreeVoList(map);
			redisUtils.set(ConstantUtil.REDIS_KEY_DICT_TREE_LIST, JSONArray.toJSONString(dictTreeVos));
		}
		return dictTreeVos;
	}

	/**
	 * 新增DictInfo
	 */
	public ResultEntity<DictInfo> insertDictInfo(DictInfo dictInfo) throws DaoException{
		
		//校验dictValue是否存在
		String dictType = dictInfo.getDictType();
		String dictValue = dictInfo.getDictValue();
		//判断是否存在
		DictInfo oldDict = dictInfoService.selectDictInfoByTypeValue(dictType, dictValue);
		if(oldDict!=null){
			return new ResultEntity<DictInfo>(ConstantUtil.CODE_407, "该字典值已经存在！");
		}
		dictInfo.setDictId(UUID.randomUUID().toString());
		dictInfo.setState(ConstantUtil.IS_VALID_Y);
		dictInfoService.insertDictInfo(dictInfo);
		
		try {
			cacheService.clearDictCache();
			cacheService.initDictCache();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResultEntity<DictInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改DictInfo
	 */
	public ResultEntity<DictInfo> updateDictInfo(DictInfo dictInfo) throws DaoException{
		
		//校验dictValue是否存在
		String dictType = dictInfo.getDictType();
		String dictValue = dictInfo.getDictValue();
		//判断是否存在
		DictInfo checkDict = dictInfoService.selectDictInfoByTypeValue(dictType, dictValue);
		if(checkDict!=null && !checkDict.getDictId().equals(dictInfo.getDictId())){
			return new ResultEntity<DictInfo>(ConstantUtil.CODE_407, "该字典值已经存在！");
		}
		
		DictInfo oldDict = dictInfoService.selectDictInfoByDictId(dictInfo.getDictId());
		//判断上级是否是root
		if(oldDict!=null){
			DictInfo parentDict = dictInfoService.selectDictInfoByDictId(oldDict.getDictPid());
			if(parentDict!=null && DictConstant.DICT_ROOT_VALUE.equals(parentDict.getDictValue())){
				//是则更新子集的dictType
				if(StringUtil.isNotNull(dictInfo.getDictValue())){
					//修改子集的dictType
					Map<String,Object> map = new HashMap<String, Object>();
					map.put("dictTypeNew", dictInfo.getDictValue());
					map.put("dictType", oldDict.getDictValue());
					map.put("notDictValue", oldDict.getDictValue());
					dictInfoService.updateDictInfo(map);
				}
			}
		}
		
		dictInfoService.updateDictInfo(dictInfo);
		
		try {
			cacheService.clearDictCache();
			cacheService.initDictCache();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResultEntity<DictInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除DictInfo
	 */
	public ResultEntity<DictInfo> deleteDictInfo(Map<String, Object> map) throws DaoException{
		String dictIds = (String) map.get("dictIds");
		String dictId = (String) map.get("dictId");
		
		//查询是否有下级子节点
		if(StringUtil.isNotNull(dictId)){
			List<DictTreeVo> dictInfos = dictInfoService.selectDictTreeVoByDictPid(dictId);
			if(dictInfos!=null && dictInfos.size()>0){
				return new ResultEntity<DictInfo>(ConstantUtil.CODE_407, "该字典有下级子节点,暂无法删除!");
			}
		}
		
		if(StringUtil.isNotNull(dictIds) && !RegexUtil.isCharAndQuot(dictIds)){
			String [] dictIdsArr = dictIds.split(",");
			boolean flag = false;
			for(String dicId: dictIdsArr){
				List<DictTreeVo> dictInfos = dictInfoService.selectDictTreeVoByDictPid(dicId);
				if(dictInfos!=null && dictInfos.size()>0){
					flag = true;
				}
			}
			if(flag == true){
				return new ResultEntity<DictInfo>(ConstantUtil.CODE_407, "您选择的字典中有下级子节点,暂无法删除!");
			}
		}else if(StringUtil.isNotNull(dictIds) && RegexUtil.isCharAndQuot(dictIds)
				&& !"''".equals(dictIds)){
			String [] dictIdsArr = dictIds.split(",");
			boolean flag = false;
			for(String dicId: dictIdsArr){
				List<DictTreeVo> dictInfos = dictInfoService.selectDictTreeVoByDictPid(dicId.substring(1,dicId.length()-1));
				if(dictInfos!=null && dictInfos.size()>0){
					flag = true;
				}
			}
			if(flag == true){
				return new ResultEntity<DictInfo>(ConstantUtil.CODE_407, "您选择的字典有下级子节点,暂无法删除!");
			}
		}
		
		if(StringUtils.isNotBlank(dictIds) && !RegexUtil.isCharAndQuot(dictIds)) {
			String newIds = StringUtil.strToCharAndQuot(dictIds);
			map.put("dictIds",newIds);
		}
		dictInfoService.deleteDictInfo(map);
		
		try {
			cacheService.clearDictCache();
			cacheService.initDictCache();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResultEntity<DictInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}


	public ResultEntity<DictInfo> sortDictInfos(String[] dictIds) throws ValidateException, DaoException {
		if(dictIds==null || dictIds.length == 0){
			throw new ValidateException("字典参数错误！"); 
		}
		int rank = 1;
		for(String dictId: dictIds){
			DictInfo dictInfo = new DictInfo();
			dictInfo.setDictId(dictId);
			dictInfo.setRank(rank);
			dictInfoService.updateDictInfo(dictInfo);
			rank++;
		}
		
		try {
			cacheService.clearDictCache();
			cacheService.initDictCache();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResultEntity<DictInfo>(ConstantUtil.CODE_200, "排序成功");
	}

}