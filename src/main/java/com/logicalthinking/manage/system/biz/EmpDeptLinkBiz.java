package com.logicalthinking.manage.system.biz;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import com.logicalthinking.manage.system.entity.EmpDeptLink;
import com.logicalthinking.manage.system.service.EmpDeptLinkService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 员工部门关联信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class EmpDeptLinkBiz {

	@Resource
	private EmpDeptLinkService empDeptLinkService;

	/**
	 * 查询EmpDeptLink集合
	 */
	public ResultEntity<List<EmpDeptLink>> empDeptLinkList(Map<String, Object> map) throws DaoException, ValidateException{
		List<EmpDeptLink> empDeptLinks = empDeptLinkService.selectEmpDeptLinkList(map);
		int total = empDeptLinkService.selectEmpDeptLinkListCount(map);
		return new ResultEntity<List<EmpDeptLink>>(ConstantUtil.CODE_200, empDeptLinks, total);
	}

	/**
	 * 根据linkId查询EmpDeptLink
	 */
	public ResultEntity<EmpDeptLink> empDeptLinkByLinkId(String linkId) throws DaoException{
		if (StringUtils.isNotBlank(linkId)) {
			EmpDeptLink empDeptLink = empDeptLinkService.selectEmpDeptLinkByLinkId(linkId);
			return new ResultEntity<EmpDeptLink>(ConstantUtil.CODE_200, empDeptLink);
		} else {
			return new ResultEntity<EmpDeptLink>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增EmpDeptLink
	 */
	public ResultEntity<EmpDeptLink> insertEmpDeptLink(EmpDeptLink empDeptLink) throws DaoException{
		empDeptLink.setLinkId(UUID.randomUUID().toString());
		empDeptLinkService.insertEmpDeptLink(empDeptLink);
		return new ResultEntity<EmpDeptLink>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改EmpDeptLink
	 */
	public ResultEntity<EmpDeptLink> updateEmpDeptLink(EmpDeptLink empDeptLink) throws DaoException{
		empDeptLinkService.updateEmpDeptLink(empDeptLink);
		return new ResultEntity<EmpDeptLink>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除EmpDeptLink
	 */
	public ResultEntity<EmpDeptLink> deleteEmpDeptLink(Map<String, Object> map) throws DaoException{
		empDeptLinkService.deleteEmpDeptLink(map);
		return new ResultEntity<EmpDeptLink>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}

}