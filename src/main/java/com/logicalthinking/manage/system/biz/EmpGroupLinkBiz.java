package com.logicalthinking.manage.system.biz;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import com.logicalthinking.manage.system.entity.EmpGroupLink;
import com.logicalthinking.manage.system.service.EmpGroupLinkService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 员工分组关联信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class EmpGroupLinkBiz {

	@Resource
	private EmpGroupLinkService empGroupLinkService;

	/**
	 * 查询EmpGroupLink集合
	 */
	public ResultEntity<List<EmpGroupLink>> empGroupLinkList(Map<String, Object> map) throws DaoException, ValidateException{
		List<EmpGroupLink> empGroupLinks = empGroupLinkService.selectEmpGroupLinkList(map);
		int total = empGroupLinkService.selectEmpGroupLinkListCount(map);
		return new ResultEntity<List<EmpGroupLink>>(ConstantUtil.CODE_200, empGroupLinks, total);
	}

	/**
	 * 根据linkId查询EmpGroupLink
	 */
	public ResultEntity<EmpGroupLink> empGroupLinkByLinkId(String linkId) throws DaoException{
		if (StringUtils.isNotBlank(linkId)) {
			EmpGroupLink empGroupLink = empGroupLinkService.selectEmpGroupLinkByLinkId(linkId);
			return new ResultEntity<EmpGroupLink>(ConstantUtil.CODE_200, empGroupLink);
		} else {
			return new ResultEntity<EmpGroupLink>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增EmpGroupLink
	 */
	public ResultEntity<EmpGroupLink> insertEmpGroupLink(EmpGroupLink empGroupLink) throws DaoException{
		empGroupLink.setLinkId(UUID.randomUUID().toString());
		empGroupLinkService.insertEmpGroupLink(empGroupLink);
		return new ResultEntity<EmpGroupLink>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改EmpGroupLink
	 */
	public ResultEntity<EmpGroupLink> updateEmpGroupLink(EmpGroupLink empGroupLink) throws DaoException{
		empGroupLinkService.updateEmpGroupLink(empGroupLink);
		return new ResultEntity<EmpGroupLink>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除EmpGroupLink
	 */
	public ResultEntity<EmpGroupLink> deleteEmpGroupLink(Map<String, Object> map) throws DaoException{
		empGroupLinkService.deleteEmpGroupLink(map);
		return new ResultEntity<EmpGroupLink>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}

}