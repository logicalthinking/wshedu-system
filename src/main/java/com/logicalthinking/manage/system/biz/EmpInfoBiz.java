package com.logicalthinking.manage.system.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.EmpDeptLink;
import com.logicalthinking.manage.system.entity.EmpInfo;
import com.logicalthinking.manage.system.service.EmpDeptLinkService;
import com.logicalthinking.manage.system.service.EmpInfoService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.DateUtil;
import com.logicalthinking.manage.system.utils.RegexUtil;
import com.logicalthinking.manage.system.utils.StringUtil;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 员工信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class EmpInfoBiz {

	@Resource
	private EmpInfoService empInfoService;
	
	@Autowired
	private EmpDeptLinkService empDeptLinkService;

	/**
	 * 查询EmpInfo集合
	 */
	public ResultEntity<List<EmpInfo>> empInfoList(Map<String, Object> map) throws DaoException, ValidateException{
		List<EmpInfo> empInfos = empInfoService.selectEmpInfoList(map);
		int total = empInfoService.selectEmpInfoListCount(map);
		return new ResultEntity<List<EmpInfo>>(ConstantUtil.CODE_200, empInfos, total);
	}

	/**
	 * 根据empId查询EmpInfo
	 */
	public ResultEntity<EmpInfo> empInfoByEmpId(String empId) throws DaoException{
		if (StringUtils.isNotBlank(empId)) {
			EmpInfo empInfo = empInfoService.selectEmpInfoByEmpId(empId);
			return new ResultEntity<EmpInfo>(ConstantUtil.CODE_200, empInfo);
		} else {
			return new ResultEntity<EmpInfo>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增EmpInfo
	 */
	public ResultEntity<EmpInfo> insertEmpInfo(EmpInfo empInfo) throws DaoException{
		empInfo.setEmpId(UUID.randomUUID().toString());
		empInfo.setCreateTime(DateUtil.getNowDate(DateUtil.DATE_TIME_PATTERN));
		empInfo.setUpdateTime(DateUtil.getNowDate(DateUtil.DATE_TIME_PATTERN));
		empInfoService.insertEmpInfo(empInfo);
		return new ResultEntity<EmpInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}
	
	/**
	 * 新增EmpInfo
	 */
	public ResultEntity<EmpInfo> insertEmpInfo(EmpInfo empInfo,String[] deptIds) throws DaoException{
		empInfo.setEmpId(UUID.randomUUID().toString());
		empInfo.setCreateTime(DateUtil.getNowDate(DateUtil.DATE_TIME_PATTERN));
		empInfo.setUpdateTime(DateUtil.getNowDate(DateUtil.DATE_TIME_PATTERN));
		if(StringUtil.isNull(empInfo.getJoinTime())){
			empInfo.setJoinTime(null);
		}
		empInfoService.insertEmpInfo(empInfo);
		
		if(deptIds!=null && deptIds.length>0){
			for(String deptId:deptIds){
				EmpDeptLink empDeptLink = new EmpDeptLink();
				empDeptLink.setDeptId(deptId);
				empDeptLink.setEmpId(empInfo.getEmpId());
				empDeptLink.setLinkId(UUID.randomUUID().toString());
				empDeptLink.setState(ConstantUtil.IS_VALID_Y);
				empDeptLinkService.insertEmpDeptLink(empDeptLink);
			}
		}
		
		return new ResultEntity<EmpInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改EmpInfo
	 */
	public ResultEntity<EmpInfo> updateEmpInfo(EmpInfo empInfo,String[] deptIds) throws DaoException{
		
		//删除部门
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("empId", empInfo.getEmpId());
		empDeptLinkService.deleteEmpDeptLink(map);
		
		if(deptIds!=null && deptIds.length>0){
			for(String deptId:deptIds){
				EmpDeptLink empDeptLink = new EmpDeptLink();
				empDeptLink.setDeptId(deptId);
				empDeptLink.setEmpId(empInfo.getEmpId());
				empDeptLink.setLinkId(UUID.randomUUID().toString());
				empDeptLink.setState(ConstantUtil.IS_VALID_Y);
				empDeptLinkService.insertEmpDeptLink(empDeptLink);
			}
		}
		
		if(StringUtil.isNull(empInfo.getJoinTime())){
			empInfo.setJoinTime(null);
		}
		empInfo.setUpdateTime(DateUtil.getNowDate(DateUtil.DATE_TIME_PATTERN));
		empInfoService.updateEmpInfo(empInfo);
		
		return new ResultEntity<EmpInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}
	
	/**
	 * 修改EmpInfo
	 */
	public ResultEntity<EmpInfo> updateEmpInfo(EmpInfo empInfo) throws DaoException{
		
		if(StringUtil.isNull(empInfo.getJoinTime())){
			empInfo.setJoinTime(null);
		}
		empInfo.setUpdateTime(DateUtil.getNowDate(DateUtil.DATE_TIME_PATTERN));
		empInfoService.updateEmpInfo(empInfo);
		return new ResultEntity<EmpInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除EmpInfo
	 */
	public ResultEntity<EmpInfo> deleteEmpInfo(Map<String, Object> map) throws DaoException{
		String empIds = (String) map.get("empIds");
		if (StringUtil.isNotNull(empIds) && !RegexUtil.isCharAndQuot(empIds)) {
			
			//删除关联信息
			for(String empId:empIds.split(",")){
				Map<String,Object> linkMap = new HashMap<String, Object>();
				linkMap.put("empId", empId);
				empDeptLinkService.deleteEmpDeptLink(linkMap);
			}
			
			String newIds = StringUtil.strToCharAndQuot(empIds);
			map.put("empIds",newIds);
		}
		
		empInfoService.deleteEmpInfo(map);
		return new ResultEntity<EmpInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}

}