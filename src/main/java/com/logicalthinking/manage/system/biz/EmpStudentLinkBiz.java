package com.logicalthinking.manage.system.biz;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import com.logicalthinking.manage.system.entity.EmpStudentLink;
import com.logicalthinking.manage.system.service.EmpStudentLinkService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 员工学员关联信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class EmpStudentLinkBiz {

	@Resource
	private EmpStudentLinkService empStudentLinkService;

	/**
	 * 查询EmpStudentLink集合
	 */
	public ResultEntity<List<EmpStudentLink>> empStudentLinkList(Map<String, Object> map) throws DaoException, ValidateException{
		List<EmpStudentLink> empStudentLinks = empStudentLinkService.selectEmpStudentLinkList(map);
		int total = empStudentLinkService.selectEmpStudentLinkListCount(map);
		return new ResultEntity<List<EmpStudentLink>>(ConstantUtil.CODE_200, empStudentLinks, total);
	}

	/**
	 * 根据linkId查询EmpStudentLink
	 */
	public ResultEntity<EmpStudentLink> empStudentLinkByLinkId(String linkId) throws DaoException{
		if (StringUtils.isNotBlank(linkId)) {
			EmpStudentLink empStudentLink = empStudentLinkService.selectEmpStudentLinkByLinkId(linkId);
			return new ResultEntity<EmpStudentLink>(ConstantUtil.CODE_200, empStudentLink);
		} else {
			return new ResultEntity<EmpStudentLink>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增EmpStudentLink
	 */
	public ResultEntity<EmpStudentLink> insertEmpStudentLink(EmpStudentLink empStudentLink) throws DaoException{
		empStudentLink.setLinkId(UUID.randomUUID().toString());
		empStudentLinkService.insertEmpStudentLink(empStudentLink);
		return new ResultEntity<EmpStudentLink>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改EmpStudentLink
	 */
	public ResultEntity<EmpStudentLink> updateEmpStudentLink(EmpStudentLink empStudentLink) throws DaoException{
		empStudentLinkService.updateEmpStudentLink(empStudentLink);
		return new ResultEntity<EmpStudentLink>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除EmpStudentLink
	 */
	public ResultEntity<EmpStudentLink> deleteEmpStudentLink(Map<String, Object> map) throws DaoException{
		empStudentLinkService.deleteEmpStudentLink(map);
		return new ResultEntity<EmpStudentLink>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}

}