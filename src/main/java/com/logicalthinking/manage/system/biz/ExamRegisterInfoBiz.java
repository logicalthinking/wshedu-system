package com.logicalthinking.manage.system.biz;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import com.logicalthinking.manage.system.entity.ExamRegisterInfo;
import com.logicalthinking.manage.system.service.ExamRegisterInfoService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 报考信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class ExamRegisterInfoBiz {

	@Resource
	private ExamRegisterInfoService examRegisterInfoService;

	/**
	 * 查询ExamRegisterInfo集合
	 */
	public ResultEntity<List<ExamRegisterInfo>> examRegisterInfoList(Map<String, Object> map) throws DaoException, ValidateException{
		List<ExamRegisterInfo> examRegisterInfos = examRegisterInfoService.selectExamRegisterInfoList(map);
		int total = examRegisterInfoService.selectExamRegisterInfoListCount(map);
		return new ResultEntity<List<ExamRegisterInfo>>(ConstantUtil.CODE_200, examRegisterInfos, total);
	}

	/**
	 * 根据regId查询ExamRegisterInfo
	 */
	public ResultEntity<ExamRegisterInfo> examRegisterInfoByRegId(String regId) throws DaoException{
		if (StringUtils.isNotBlank(regId)) {
			ExamRegisterInfo examRegisterInfo = examRegisterInfoService.selectExamRegisterInfoByRegId(regId);
			return new ResultEntity<ExamRegisterInfo>(ConstantUtil.CODE_200, examRegisterInfo);
		} else {
			return new ResultEntity<ExamRegisterInfo>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增ExamRegisterInfo
	 */
	public ResultEntity<ExamRegisterInfo> insertExamRegisterInfo(ExamRegisterInfo examRegisterInfo) throws DaoException{
		examRegisterInfo.setRegId(UUID.randomUUID().toString());
		examRegisterInfoService.insertExamRegisterInfo(examRegisterInfo);
		return new ResultEntity<ExamRegisterInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改ExamRegisterInfo
	 */
	public ResultEntity<ExamRegisterInfo> updateExamRegisterInfo(ExamRegisterInfo examRegisterInfo) throws DaoException{
		examRegisterInfoService.updateExamRegisterInfo(examRegisterInfo);
		return new ResultEntity<ExamRegisterInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除ExamRegisterInfo
	 */
	public ResultEntity<ExamRegisterInfo> deleteExamRegisterInfo(Map<String, Object> map) throws DaoException{
		examRegisterInfoService.deleteExamRegisterInfo(map);
		return new ResultEntity<ExamRegisterInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}

}