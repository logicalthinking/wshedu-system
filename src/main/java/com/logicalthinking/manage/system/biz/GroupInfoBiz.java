package com.logicalthinking.manage.system.biz;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import com.logicalthinking.manage.system.entity.GroupInfo;
import com.logicalthinking.manage.system.service.GroupInfoService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 分组信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class GroupInfoBiz {

	@Resource
	private GroupInfoService groupInfoService;

	/**
	 * 查询GroupInfo集合
	 */
	public ResultEntity<List<GroupInfo>> groupInfoList(Map<String, Object> map) throws DaoException, ValidateException{
		List<GroupInfo> groupInfos = groupInfoService.selectGroupInfoList(map);
		int total = groupInfoService.selectGroupInfoListCount(map);
		return new ResultEntity<List<GroupInfo>>(ConstantUtil.CODE_200, groupInfos, total);
	}

	/**
	 * 根据groupId查询GroupInfo
	 */
	public ResultEntity<GroupInfo> groupInfoByGroupId(String groupId) throws DaoException{
		if (StringUtils.isNotBlank(groupId)) {
			GroupInfo groupInfo = groupInfoService.selectGroupInfoByGroupId(groupId);
			return new ResultEntity<GroupInfo>(ConstantUtil.CODE_200, groupInfo);
		} else {
			return new ResultEntity<GroupInfo>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增GroupInfo
	 */
	public ResultEntity<GroupInfo> insertGroupInfo(GroupInfo groupInfo) throws DaoException{
		groupInfo.setGroupId(UUID.randomUUID().toString());
		groupInfoService.insertGroupInfo(groupInfo);
		return new ResultEntity<GroupInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改GroupInfo
	 */
	public ResultEntity<GroupInfo> updateGroupInfo(GroupInfo groupInfo) throws DaoException{
		groupInfoService.updateGroupInfo(groupInfo);
		return new ResultEntity<GroupInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除GroupInfo
	 */
	public ResultEntity<GroupInfo> deleteGroupInfo(Map<String, Object> map) throws DaoException{
		groupInfoService.deleteGroupInfo(map);
		return new ResultEntity<GroupInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}

}