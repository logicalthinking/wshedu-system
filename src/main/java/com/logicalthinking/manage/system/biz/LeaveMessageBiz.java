package com.logicalthinking.manage.system.biz;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import com.logicalthinking.manage.system.entity.LeaveMessage;
import com.logicalthinking.manage.system.service.LeaveMessageService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.DateUtil;
import com.logicalthinking.manage.system.utils.RegexUtil;
import com.logicalthinking.manage.system.utils.StringUtil;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 留言信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Service
public class LeaveMessageBiz {

	@Resource
	private LeaveMessageService leaveMessageService;

	/**
	 * 查询LeaveMessage集合
	 */
	public ResultEntity<List<LeaveMessage>> leaveMessageList(Map<String, Object> map) throws DaoException, ValidateException{
		List<LeaveMessage> leaveMessages = leaveMessageService.selectLeaveMessageList(map);
		int total = leaveMessageService.selectLeaveMessageListCount(map);
		return new ResultEntity<List<LeaveMessage>>(ConstantUtil.CODE_200, leaveMessages, total);
	}

	/**
	 * 根据msgId查询LeaveMessage
	 */
	public ResultEntity<LeaveMessage> leaveMessageByMsgId(String msgId) throws DaoException{
		if (StringUtils.isNotBlank(msgId)) {
			LeaveMessage leaveMessage = leaveMessageService.selectLeaveMessageByMsgId(msgId);
			return new ResultEntity<LeaveMessage>(ConstantUtil.CODE_200, leaveMessage);
		} else {
			return new ResultEntity<LeaveMessage>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增LeaveMessage
	 */
	public ResultEntity<LeaveMessage> insertLeaveMessage(LeaveMessage leaveMessage) throws DaoException{
		leaveMessage.setMsgId(UUID.randomUUID().toString());
		leaveMessage.setCreateTime(DateUtil.getNowDate(DateUtil.DATE_TIME_PATTERN));
		leaveMessageService.insertLeaveMessage(leaveMessage);
		return new ResultEntity<LeaveMessage>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改LeaveMessage
	 */
	public ResultEntity<LeaveMessage> updateLeaveMessage(LeaveMessage leaveMessage) throws DaoException{
		leaveMessageService.updateLeaveMessage(leaveMessage);
		return new ResultEntity<LeaveMessage>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除LeaveMessage
	 */
	public ResultEntity<LeaveMessage> deleteLeaveMessage(Map<String, Object> map) throws DaoException{
		String msgIds = (String) map.get("msgIds");
		if (StringUtil.isNotNull(msgIds) && !RegexUtil.isCharAndQuot(msgIds)) {
			String newIds = StringUtil.strToCharAndQuot(msgIds);
			map.put("msgIds",newIds);
		}
		leaveMessageService.deleteLeaveMessage(map);
		return new ResultEntity<LeaveMessage>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}

}