package com.logicalthinking.manage.system.biz;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import com.logicalthinking.manage.system.entity.StudentInfo;
import com.logicalthinking.manage.system.service.StudentInfoService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 学员信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class StudentInfoBiz {

	@Resource
	private StudentInfoService studentInfoService;

	/**
	 * 查询StudentInfo集合
	 */
	public ResultEntity<List<StudentInfo>> studentInfoList(Map<String, Object> map) throws DaoException, ValidateException{
		List<StudentInfo> studentInfos = studentInfoService.selectStudentInfoList(map);
		int total = studentInfoService.selectStudentInfoListCount(map);
		return new ResultEntity<List<StudentInfo>>(ConstantUtil.CODE_200, studentInfos, total);
	}

	/**
	 * 根据stuId查询StudentInfo
	 */
	public ResultEntity<StudentInfo> studentInfoByStuId(String stuId) throws DaoException{
		if (StringUtils.isNotBlank(stuId)) {
			StudentInfo studentInfo = studentInfoService.selectStudentInfoByStuId(stuId);
			return new ResultEntity<StudentInfo>(ConstantUtil.CODE_200, studentInfo);
		} else {
			return new ResultEntity<StudentInfo>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增StudentInfo
	 */
	public ResultEntity<StudentInfo> insertStudentInfo(StudentInfo studentInfo) throws DaoException{
		studentInfo.setStuId(UUID.randomUUID().toString());
		studentInfoService.insertStudentInfo(studentInfo);
		return new ResultEntity<StudentInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改StudentInfo
	 */
	public ResultEntity<StudentInfo> updateStudentInfo(StudentInfo studentInfo) throws DaoException{
		studentInfoService.updateStudentInfo(studentInfo);
		return new ResultEntity<StudentInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除StudentInfo
	 */
	public ResultEntity<StudentInfo> deleteStudentInfo(Map<String, Object> map) throws DaoException{
		studentInfoService.deleteStudentInfo(map);
		return new ResultEntity<StudentInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}

}