package com.logicalthinking.manage.system.biz;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import com.logicalthinking.manage.system.entity.SysLogInfo;
import com.logicalthinking.manage.system.service.SysLogInfoService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.DateUtil;
import com.logicalthinking.manage.system.utils.RegexUtil;
import com.logicalthinking.manage.system.utils.StringUtil;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 系统日志信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
@Service
public class SysLogInfoBiz {

	@Resource
	private SysLogInfoService sysLogInfoService;

	/**
	 * 查询SysLogInfo集合
	 */
	public ResultEntity<List<SysLogInfo>> sysLogInfoList(Map<String, Object> map) throws DaoException, ValidateException{
		List<SysLogInfo> sysLogInfos = sysLogInfoService.selectSysLogInfoList(map);
		int total = sysLogInfoService.selectSysLogInfoListCount(map);
		return new ResultEntity<List<SysLogInfo>>(ConstantUtil.CODE_200, sysLogInfos, total);
	}

	/**
	 * 根据logId查询SysLogInfo
	 */
	public ResultEntity<SysLogInfo> sysLogInfoByLogId(String logId) throws DaoException{
		if (StringUtils.isNotBlank(logId)) {
			SysLogInfo sysLogInfo = sysLogInfoService.selectSysLogInfoByLogId(logId);
			return new ResultEntity<SysLogInfo>(ConstantUtil.CODE_200, sysLogInfo);
		} else {
			return new ResultEntity<SysLogInfo>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增SysLogInfo
	 */
	public ResultEntity<SysLogInfo> insertSysLogInfo(SysLogInfo sysLogInfo) throws DaoException{
		sysLogInfo.setLogId(UUID.randomUUID().toString());
		sysLogInfo.setCreateTime(DateUtil.getNowDate(DateUtil.DATE_TIME_PATTERN));
		sysLogInfo.setState(ConstantUtil.IS_VALID_Y);
		sysLogInfoService.insertSysLogInfo(sysLogInfo);
		return new ResultEntity<SysLogInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改SysLogInfo
	 */
	public ResultEntity<SysLogInfo> updateSysLogInfo(SysLogInfo sysLogInfo) throws DaoException{
		sysLogInfoService.updateSysLogInfo(sysLogInfo);
		return new ResultEntity<SysLogInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除SysLogInfo
	 */
	public ResultEntity<SysLogInfo> deleteSysLogInfo(Map<String, Object> map) throws DaoException{
		String logIds = (String) map.get("logIds");
		if (StringUtil.isNotNull(logIds) && !RegexUtil.isCharAndQuot(logIds)) {
			String newIds = StringUtil.strToCharAndQuot(logIds);
			map.put("logIds",newIds);
		}
		sysLogInfoService.deleteSysLogInfo(map);
		return new ResultEntity<SysLogInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}

}