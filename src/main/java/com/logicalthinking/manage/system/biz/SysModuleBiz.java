package com.logicalthinking.manage.system.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hellooop.odj.token.manager.TokenManager;
import com.logicalthinking.manage.system.entity.SysModule;
import com.logicalthinking.manage.system.entity.SysUserRole;
import com.logicalthinking.manage.system.entity.SysUsers;
import com.logicalthinking.manage.system.service.CacheService;
import com.logicalthinking.manage.system.service.SysModuleService;
import com.logicalthinking.manage.system.service.SysUsersService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.RedisUtils;
import com.logicalthinking.manage.system.utils.RegexUtil;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.utils.StringUtil;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.vo.ModuleTreeVo;

/**
 * 系统模块信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
@Service("sysModuleBiz")
public class SysModuleBiz {

	@Resource
	private SysModuleService sysModuleService;
	
	@Autowired
	private RedisUtils redisUtils;
	
	@Autowired
	private CacheService cacheService;
	
	@Autowired
	private SysUsersService sysUsersService;

	/**
	 * 查询SysModule集合
	 */
	public ResultEntity<List<SysModule>> sysModuleList(Map<String, Object> map) throws DaoException, ValidateException{
		List<SysModule> sysModules = sysModuleService.selectSysModuleList(map);
		int total = sysModuleService.selectSysModuleListCount(map);
		return new ResultEntity<List<SysModule>>(ConstantUtil.CODE_200, sysModules, total);
	}
	
	
	/**
	 * 查询SysModule集合
	 */
	public List<SysModule> sysModuleAllList(Map<String, Object> map) throws DaoException{
		List<SysModule> sysModules = sysModuleService.selectAllSysModuleList(map);
		return sysModules;
	}
	
	/**
	 * 校验访问页面是否有权限
	 * @throws ValidateException 
	 */
	public boolean selectModuleListByValid(String moduleUrl,String token) throws DaoException, ValidateException{
		boolean flag = true;
		
		if(StringUtils.isBlank(moduleUrl)){
			throw new ValidateException("moduleUrl参数错误");
		}
		
		TokenManager tokenManager = new TokenManager();
		try {
			String  userId = tokenManager.getTokenData(token,"userId");
			if(StringUtils.isBlank(userId)){
				return false;
			}
			
			//查询用户
			SysUsers sysUsers = sysUsersService.selectSysUsersByUserId(userId);
			if(sysUsers==null || sysUsers.getSysUserRoles()==null || 
					sysUsers.getSysUserRoles().size()<=0){
				return false;
			}
			
			List<SysUserRole> sysUserRoles = sysUsers.getSysUserRoles();
			String roleCode = ConstantUtil.ROLE_CODE_SYS_MANAGER;
			boolean isDev = false;
			for(int i=0;i<sysUserRoles.size();i++){
				if(sysUserRoles.get(i).getSysRole()!=null){
					if(roleCode.equals(sysUserRoles.get(i).getSysRole().getRoleCode())){
						isDev = true;
					}
				}
			}
			
			if(isDev == false){
				List<SysModule> sysModules = sysModuleService.selectModuleListByValid(moduleUrl, userId);
				
				if(sysModules!=null && sysModules.size()>0){
					String moduleId = sysModules.get(0).getModuleId();
					String moduleName = sysModules.get(0).getModuleName();
					if(StringUtil.isNotNull(moduleId)
							&& StringUtil.isNull(moduleName)){
						flag = false;
					}
				}
			}
		} catch (Exception e) {
			throw new ValidateException(e,e.getMessage());
		}
		
		return flag;
	}
	
	
	/**
	 * 查询SysModule集合
	 */
	public List<ModuleTreeVo> selectModuleTreeList(Map<String, Object> map) throws DaoException{
		List<ModuleTreeVo> moduleTreeVos = null;
		boolean flag = redisUtils.hasKey(ConstantUtil.REDIS_KEY_PERMISSION_MODULE_MENUS);
		if(flag == true){
			String json = (String) redisUtils.get(ConstantUtil.REDIS_KEY_PERMISSION_MODULE_MENUS);
			if(StringUtil.isNotNull(json)){
				moduleTreeVos = JSONObject.parseArray(json, ModuleTreeVo.class);
			}
		}else{
			moduleTreeVos = sysModuleService.selectModuleTreeVoList(map);
			redisUtils.set(ConstantUtil.REDIS_KEY_PERMISSION_MODULE_MENUS, JSONArray.toJSONString(moduleTreeVos));
		}
		return moduleTreeVos;
	}
	
	
	/**
	 * 清除模块缓存
	 */
	public void clearModulesCache() throws DaoException{
		try {
			cacheService.clearModuleCache();
			cacheService.initModuleCache();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 查询所有SysModule树形数据集合
	 */
	public List<ModuleTreeVo> selectAllModuleMenuTreeList(Map<String, Object> map) throws DaoException{
		List<ModuleTreeVo> moduleTreeVos = null;
		boolean flag = redisUtils.hasKey(ConstantUtil.REDIS_KEY_ALL_MODULE_MENUS);
		if(flag == true){
			String json = (String) redisUtils.get(ConstantUtil.REDIS_KEY_ALL_MODULE_MENUS);
			if(StringUtil.isNotNull(json)){
				moduleTreeVos = JSONObject.parseArray(json, ModuleTreeVo.class);
			}
		}else{
			moduleTreeVos = sysModuleService.selectAllModuleMenuTreeList(map);
			redisUtils.set(ConstantUtil.REDIS_KEY_ALL_MODULE_MENUS, JSONArray.toJSONString(moduleTreeVos));
		}
		
		return moduleTreeVos;
	}
	
	//获取特定页面的操作权限
	public List<ModuleTreeVo> selectModuleByOnePage(SysUsers sysUsers,String moduleCode,String parentModuleCode) throws DaoException, ValidateException{
		
		if(StringUtils.isBlank(moduleCode)){
			throw new ValidateException("参数错误");
		}
		
		if(StringUtils.isBlank(parentModuleCode)){
			throw new ValidateException("参数错误");
		}
		
		List<ModuleTreeVo> moduleTreeVos = null;
		
		List<SysUserRole> sysUserRoles = sysUsers.getSysUserRoles();
		if(sysUserRoles!=null && sysUserRoles.size()>0){
			String roleId = sysUserRoles.get(0).getRoleId();
			boolean flag = redisUtils.hasKey(roleId+ConstantUtil.REDIS_KEY_USER_MODULES_SUFFX);
			if(flag == true){
				String json = (String) redisUtils.get(roleId+ConstantUtil.REDIS_KEY_USER_MODULES_SUFFX);
				if(StringUtil.isNotNull(json)){
					List<ModuleTreeVo> list = JSONObject.parseArray(json, ModuleTreeVo.class);
					if(list!=null && list.size()>0){
						for(ModuleTreeVo moduleTreeVo:list){
							
							//比较上级
							if(parentModuleCode.equals(moduleTreeVo.getField())){
								if(moduleTreeVo.getChildren()!=null && moduleTreeVo.getChildren().size()>0){
									for(ModuleTreeVo childrenVo:moduleTreeVo.getChildren()){
										//比较当前级
										if(moduleCode.equals(childrenVo.getField())){
											moduleTreeVos = childrenVo.getChildren();
										}
									}
								}
							}
						}
					}
				}
			}else{
				//查询用户角色信息
				String busiIds = "";
				String roleCode = ConstantUtil.ROLE_CODE_SYS_MANAGER;
				boolean isDev = false;
				for(int i=0;i<sysUserRoles.size();i++){
					if(i==0){
						busiIds+=sysUserRoles.get(i).getRoleId();
					}else{
						busiIds+=","+sysUserRoles.get(i).getRoleId();
					}
					if(sysUserRoles.get(i).getSysRole()!=null){
						if(roleCode.equals(sysUserRoles.get(i).getSysRole().getRoleCode())){
							isDev = true;
						}
					}
				}
				
				if(isDev == true){
					moduleTreeVos = sysModuleService.selectModuleTreeListByOnePage(moduleCode, parentModuleCode, null);
				}else{
					moduleTreeVos = sysModuleService.selectModuleTreeListByOnePage(moduleCode, parentModuleCode, busiIds);
				}
			}
		}
		
		return moduleTreeVos;
	}
	
	/**
	 * 查询当前用户模块菜单
	 * @throws ValidateException 
	 */
	public List<ModuleTreeVo> selectUserModuleMenuTreeList(SysUsers sysUsers,Map<String, Object> map) throws DaoException, ValidateException{
		List<ModuleTreeVo> moduleTreeVos = null;
		
		List<SysUserRole> sysUserRoles = sysUsers.getSysUserRoles();
		if(sysUserRoles!=null && sysUserRoles.size()>0){
			String roleId = sysUserRoles.get(0).getRoleId();
			boolean flag = redisUtils.hasKey(roleId+ConstantUtil.REDIS_KEY_USER_MODULES_SUFFX);
			if(flag == true){
				String json = (String) redisUtils.get(roleId+ConstantUtil.REDIS_KEY_USER_MODULES_SUFFX);
				if(StringUtil.isNotNull(json)){
					moduleTreeVos = JSONObject.parseArray(json, ModuleTreeVo.class);
				}
			}else{
				//查询用户角色信息
				String busiIds = "";
				String roleCode = ConstantUtil.ROLE_CODE_SYS_MANAGER;
				boolean isDev = false;
				for(int i=0;i<sysUserRoles.size();i++){
					if(i==0){
						busiIds+=sysUserRoles.get(i).getRoleId();
					}else{
						busiIds+=","+sysUserRoles.get(i).getRoleId();
					}
					if(sysUserRoles.get(i).getSysRole()!=null){
						if(roleCode.equals(sysUserRoles.get(i).getSysRole().getRoleCode())){
							isDev = true;
						}
					}
				}
				
				if(isDev == true){
					//排除公共模块
					map.put("excludeModules", ConstantUtil.public_modules);
					moduleTreeVos = sysModuleService.selectAllModuleMenuTreeList(map);
				}else{
					map.put("busiIds", busiIds);
					moduleTreeVos = sysModuleService.selectUserModuleMenuTreeList(map);
				}
				redisUtils.set(roleId+ConstantUtil.REDIS_KEY_USER_MODULES_SUFFX, JSONArray.toJSONString(moduleTreeVos));
			}
		}
		
		return moduleTreeVos;
	}
	
	/**
	 * 根据moduleId查询SysModule
	 */
	public ResultEntity<SysModule> sysModuleByModuleId(String moduleId) throws DaoException{
		if (StringUtils.isNotBlank(moduleId)) {
			SysModule sysModule = sysModuleService.selectSysModuleByModuleId(moduleId);
			return new ResultEntity<SysModule>(ConstantUtil.CODE_200, sysModule);
		} else {
			return new ResultEntity<SysModule>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增SysModule
	 */
	public ResultEntity<SysModule> insertSysModule(SysModule sysModule) throws DaoException{
		sysModule.setModuleId(UUID.randomUUID().toString());
		sysModule.setState(ConstantUtil.IS_VALID_Y);
		sysModuleService.insertSysModule(sysModule);
		
		try {
			cacheService.clearModuleCache();
			cacheService.initModuleCache();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResultEntity<SysModule>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改SysModule
	 */
	public ResultEntity<SysModule> updateSysModule(SysModule sysModule) throws DaoException{
		sysModuleService.updateSysModule(sysModule);
		
		try {
			cacheService.clearModuleCache();
			cacheService.initModuleCache();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResultEntity<SysModule>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除SysModule
	 */
	public ResultEntity<SysModule> deleteSysModule(Map<String, Object> map) throws DaoException{
		
		String moduleIds = (String) map.get("moduleIds");
		String moduleId = (String) map.get("moduleId");
		
		//查询是否有下级子节点
		if(StringUtil.isNotNull(moduleId)){
			Map<String, Object> validMap = new HashMap<String, Object>();
			validMap.put("parentId", moduleId);
			validMap.put("state", ConstantUtil.IS_VALID_Y);
			List<SysModule> sysModules = sysModuleService.selectAllSysModuleList(validMap);
			if(sysModules!=null && sysModules.size()>0){
				return new ResultEntity<SysModule>(ConstantUtil.CODE_407, "该模块有下级子模块,暂无法删除!");
			}
		}
		
		if(StringUtil.isNotNull(moduleIds) && !RegexUtil.isCharAndQuot(moduleIds)){
			String [] moduleIdsArr = moduleIds.split(",");
			boolean flag = false;
			for(String modId: moduleIdsArr){
				Map<String, Object> validMap = new HashMap<String, Object>();
				validMap.put("parentId", modId);
				validMap.put("state", ConstantUtil.IS_VALID_Y);
				List<SysModule> sysModules = sysModuleService.selectAllSysModuleList(validMap);
				if(sysModules!=null && sysModules.size()>0){
					flag = true;
				}
			}
			if(flag == true){
				return new ResultEntity<SysModule>(ConstantUtil.CODE_407, "您选择的模块下有子模块,暂无法删除!");
			}
		}else if(StringUtil.isNotNull(moduleIds) && RegexUtil.isCharAndQuot(moduleIds)
				&& !"''".equals(moduleIds)){
			String [] moduleIdsArr = moduleIds.split(",");
			boolean flag = false;
			for(String modId: moduleIdsArr){
				Map<String, Object> validMap = new HashMap<String, Object>();
				validMap.put("parentId", modId);
				validMap.put("state", ConstantUtil.IS_VALID_Y);
				List<SysModule> sysModules = sysModuleService.selectAllSysModuleList(validMap);
				if(sysModules!=null && sysModules.size()>0){
					flag = true;
				}
			}
			if(flag == true){
				return new ResultEntity<SysModule>(ConstantUtil.CODE_407, "您选择的模块下有子模块,暂无法删除!");
			}
		}
		
		if (StringUtil.isNotNull(moduleIds) && !RegexUtil.isCharAndQuot(moduleIds)) {
			String newIds = StringUtil.strToCharAndQuot(moduleIds);
			map.put("moduleIds",newIds);
		}
		sysModuleService.deleteSysModule(map);
		
		try {
			cacheService.clearModuleCache();
			cacheService.initModuleCache();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResultEntity<SysModule>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}


	public ResultEntity<SysModule> sortModules(String[] moduleIds) throws ValidateException, DaoException {
		if(moduleIds==null || moduleIds.length == 0){
			throw new ValidateException("模块参数错误！"); 
		}
		int rank = 1;
		for(String moduleId: moduleIds){
			SysModule sysModule = new SysModule();
			sysModule.setModuleId(moduleId);
			sysModule.setRank(rank);
			sysModuleService.updateSysModule(sysModule);
			rank++;
		}
		
		try {
			cacheService.clearModuleCache();
			cacheService.initModuleCache();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResultEntity<SysModule>(ConstantUtil.CODE_200, "排序成功");
	}

}