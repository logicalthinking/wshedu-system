package com.logicalthinking.manage.system.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.SysModuleLink;
import com.logicalthinking.manage.system.service.SysModuleLinkService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.RedisUtils;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.utils.ValidateException;

/**
 * 模块关联业务信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Service
public class SysModuleLinkBiz {

	@Resource
	private SysModuleLinkService sysModuleLinkService;

	@Autowired
	private RedisUtils redisUtils;
	
	/**
	 * 查询SysModuleLink集合
	 */
	public ResultEntity<List<SysModuleLink>> sysModuleLinkList(Map<String, Object> map) throws DaoException, ValidateException{
		List<SysModuleLink> sysModuleLinks = sysModuleLinkService.selectSysModuleLinkList(map);
		int total = sysModuleLinkService.selectSysModuleLinkListCount(map);
		return new ResultEntity<List<SysModuleLink>>(ConstantUtil.CODE_200, sysModuleLinks, total);
	}

	/**
	 * 查询SysModuleLink集合
	 */
	public List<SysModuleLink> selectModuleByRoleList(String busiId) throws DaoException, ValidateException{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("busiId", busiId);
		map.put("state", ConstantUtil.IS_VALID_Y);
		List<SysModuleLink> sysModuleLinks = sysModuleLinkService.selectSysModuleLinkList(map);
		return sysModuleLinks;
	}

	/**
	 * 根据linkId查询SysModuleLink
	 */
	public ResultEntity<SysModuleLink> sysModuleLinkByLinkId(String linkId) throws DaoException{
		if (StringUtils.isNotBlank(linkId)) {
			SysModuleLink sysModuleLink = sysModuleLinkService.selectSysModuleLinkByLinkId(linkId);
			return new ResultEntity<SysModuleLink>(ConstantUtil.CODE_200, sysModuleLink);
		} else {
			return new ResultEntity<SysModuleLink>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增SysModuleLink
	 */
	public ResultEntity<SysModuleLink> insertSysModuleLink(SysModuleLink sysModuleLink) throws DaoException{
		sysModuleLink.setLinkId(UUID.randomUUID().toString());
		sysModuleLink.setState(ConstantUtil.IS_VALID_Y);
		sysModuleLinkService.insertSysModuleLink(sysModuleLink);
		return new ResultEntity<SysModuleLink>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}
	
	/**
	 * 新增SysModuleLink
	 */
	public ResultEntity<SysModuleLink> insertSysModuleLink(String busiId,String [] moduleIds) throws DaoException{
		Map<String,Object> map = new HashMap<String, Object>();
		redisUtils.del(busiId+ConstantUtil.REDIS_KEY_USER_MODULES_SUFFX);
		
		//删除之前的记录
		map.clear();
		map.put("busiId", busiId);
		sysModuleLinkService.deleteSysModuleLink(map);
		
		if(moduleIds!=null && moduleIds.length>0){
			for(String moduleId:moduleIds){
				SysModuleLink sysModuleLink = new SysModuleLink();
				sysModuleLink.setLinkId(UUID.randomUUID().toString());
				sysModuleLink.setBusiId(busiId);
				sysModuleLink.setModuleId(moduleId);
				sysModuleLink.setState(ConstantUtil.IS_VALID_Y);
				sysModuleLinkService.insertSysModuleLink(sysModuleLink);
			}
		}
		return new ResultEntity<SysModuleLink>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改SysModuleLink
	 */
	public ResultEntity<SysModuleLink> updateSysModuleLink(SysModuleLink sysModuleLink) throws DaoException{
		sysModuleLinkService.updateSysModuleLink(sysModuleLink);
		return new ResultEntity<SysModuleLink>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除SysModuleLink
	 */
	public ResultEntity<SysModuleLink> deleteSysModuleLink(Map<String, Object> map) throws DaoException{
		sysModuleLinkService.deleteSysModuleLink(map);
		return new ResultEntity<SysModuleLink>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}

}