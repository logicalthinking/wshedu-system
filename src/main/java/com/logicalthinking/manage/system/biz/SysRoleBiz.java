package com.logicalthinking.manage.system.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.SysRole;
import com.logicalthinking.manage.system.service.CacheService;
import com.logicalthinking.manage.system.service.SysRoleService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.RegexUtil;
import com.logicalthinking.manage.system.utils.StringUtil;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 角色信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
@Service
public class SysRoleBiz {

	@Resource
	private SysRoleService sysRoleService;
	
	@Autowired
	private CacheService cacheService;

	/**
	 * 查询SysRole集合
	 */
	public ResultEntity<List<SysRole>> sysRoleList(Map<String, Object> map) throws DaoException, ValidateException{
		List<SysRole> sysRoles = sysRoleService.selectSysRoleList(map);
		int total = sysRoleService.selectSysRoleListCount(map);
		return new ResultEntity<List<SysRole>>(ConstantUtil.CODE_200, sysRoles, total);
	}
	
	/**
	 * 查询所有SysRole集合
	 */
	public List<SysRole> sysRoleAllList(Map<String, Object> map) throws DaoException{
		List<SysRole> sysRoles = sysRoleService.selectAllSysRoleList(map);
		return sysRoles;
	}

	/**
	 * 根据roleId查询SysRole
	 */
	public ResultEntity<SysRole> sysRoleByRoleId(String roleId) throws DaoException{
		if (StringUtils.isNotBlank(roleId)) {
			SysRole sysRole = sysRoleService.selectSysRoleByRoleId(roleId);
			return new ResultEntity<SysRole>(ConstantUtil.CODE_200, sysRole);
		} else {
			return new ResultEntity<SysRole>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增SysRole
	 */
	public ResultEntity<SysRole> insertSysRole(SysRole sysRole) throws DaoException{
		sysRole.setRoleId(UUID.randomUUID().toString());
		sysRole.setState(ConstantUtil.IS_VALID_Y);
		
		//校验重复
		if(StringUtil.isNotNull(sysRole.getRoleCode())){
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("roleCode", sysRole.getRoleCode());
			int total = sysRoleService.selectSysRoleListCount(map);
			if(total>0){
				return new ResultEntity<SysRole>(ConstantUtil.CODE_407, "该角色编号已存在！");
			}
		}
		
		sysRoleService.insertSysRole(sysRole);
		
		try {
			cacheService.clearModuleCache();
			cacheService.initModuleCache();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResultEntity<SysRole>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改SysRole
	 * @throws ValidateException 
	 */
	public ResultEntity<SysRole> updateSysRole(SysRole sysRole) throws DaoException{
		//校验重复
		if(StringUtil.isNotNull(sysRole.getRoleCode())){
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("roleCode", sysRole.getRoleCode());
			List<SysRole> sysRoles = sysRoleService.selectAllSysRoleList(map);
			if(sysRoles!=null && sysRoles.size()>0){
				if(sysRoles.size() == 1 && sysRoles.get(0).getRoleId().equals(sysRole.getRoleId())){
					//说明是当前本身
				}else{
					return new ResultEntity<SysRole>(ConstantUtil.CODE_407, "该角色编号已存在！");
				}
			}
		}
		
		sysRoleService.updateSysRole(sysRole);
		
		try {
			cacheService.clearModuleCache();
			cacheService.initModuleCache();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResultEntity<SysRole>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除SysRole
	 */
	public ResultEntity<SysRole> deleteSysRole(Map<String, Object> map) throws DaoException{
		String roleIds = (String) map.get("roleIds");
		if (StringUtil.isNotNull(roleIds) && !RegexUtil.isCharAndQuot(roleIds)) {
			String newIds = StringUtil.strToCharAndQuot(roleIds);
			map.put("roleIds",newIds);
		}
		sysRoleService.deleteSysRole(map);
		
		try {
			cacheService.clearModuleCache();
			cacheService.initModuleCache();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResultEntity<SysRole>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}

}