package com.logicalthinking.manage.system.biz;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import com.logicalthinking.manage.system.entity.SysUserRole;
import com.logicalthinking.manage.system.service.SysUserRoleService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 用户关联角色信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-16
 */
@Service
public class SysUserRoleBiz {

	@Resource
	private SysUserRoleService sysUserRoleService;

	/**
	 * 查询SysUserRole集合
	 */
	public ResultEntity<List<SysUserRole>> sysUserRoleList(Map<String, Object> map) throws DaoException, ValidateException{
		List<SysUserRole> sysUserRoles = sysUserRoleService.selectSysUserRoleList(map);
		int total = sysUserRoleService.selectSysUserRoleListCount(map);
		return new ResultEntity<List<SysUserRole>>(ConstantUtil.CODE_200, sysUserRoles, total);
	}

	/**
	 * 根据userRoleId查询SysUserRole
	 */
	public ResultEntity<SysUserRole> sysUserRoleByUserRoleId(String userRoleId) throws DaoException{
		if (StringUtils.isNotBlank(userRoleId)) {
			SysUserRole sysUserRole = sysUserRoleService.selectSysUserRoleByUserRoleId(userRoleId);
			return new ResultEntity<SysUserRole>(ConstantUtil.CODE_200, sysUserRole);
		} else {
			return new ResultEntity<SysUserRole>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增SysUserRole
	 */
	public ResultEntity<SysUserRole> insertSysUserRole(SysUserRole sysUserRole) throws DaoException{
		sysUserRoleService.insertSysUserRole(sysUserRole);
		return new ResultEntity<SysUserRole>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改SysUserRole
	 */
	public ResultEntity<SysUserRole> updateSysUserRole(SysUserRole sysUserRole) throws DaoException{
		sysUserRoleService.updateSysUserRole(sysUserRole);
		return new ResultEntity<SysUserRole>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除SysUserRole
	 */
	public ResultEntity<SysUserRole> deleteSysUserRole(Map<String, Object> map) throws DaoException{
		sysUserRoleService.deleteSysUserRole(map);
		return new ResultEntity<SysUserRole>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}

}