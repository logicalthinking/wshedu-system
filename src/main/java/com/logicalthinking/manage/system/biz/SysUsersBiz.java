package com.logicalthinking.manage.system.biz;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.hellooop.odj.token.manager.TokenManager;
import com.logicalthinking.manage.system.entity.SysUserRole;
import com.logicalthinking.manage.system.entity.SysUsers;
import com.logicalthinking.manage.system.service.SysUserRoleService;
import com.logicalthinking.manage.system.service.SysUsersService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.DateUtil;
import com.logicalthinking.manage.system.utils.MD5Utils;
import com.logicalthinking.manage.system.utils.RedisUtils;
import com.logicalthinking.manage.system.utils.RegexUtil;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.utils.StringUtil;
import com.logicalthinking.manage.system.utils.ValidateException;

/**
 * 用户信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-12
 */
@Service
public class SysUsersBiz {

	@Resource
	private SysUsersService sysUsersService;
	
	@Resource
	private SysUserRoleService sysUserRoleService;
	
	@Autowired
	private RedisUtils redisUtils;

	/**
	 * 查询SysUsers集合
	 */
	public ResultEntity<List<SysUsers>> sysUsersList(Map<String, Object> map) throws DaoException, ValidateException {
		List<SysUsers> sysUsers = sysUsersService.selectSysUsersList(map);
		int total = sysUsersService.selectSysUsersListCount(map);
		return new ResultEntity<List<SysUsers>>(ConstantUtil.CODE_200, sysUsers, total);
	}

	/**
	 * 根据userId查询SysUsers
	 */
	public ResultEntity<SysUsers> sysUsersByUserId(String userId) throws DaoException {
		if (StringUtils.isNotBlank(userId)) {
			SysUsers sysUsers = sysUsersService.selectSysUsersByUserId(userId);
			return new ResultEntity<SysUsers>(ConstantUtil.CODE_200, sysUsers);
		} else {
			return new ResultEntity<SysUsers>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增SysUsers
	 */
	public ResultEntity<SysUsers> insertSysUsers(SysUsers sysUsers) throws DaoException {
		
		//校验用户名重复
		if(StringUtil.isNotNull(sysUsers.getUserName())){
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("userName", sysUsers.getUserName());
			int total = sysUsersService.selectSysUsersListCount(map);
			if(total>0){
				return new ResultEntity<SysUsers>(ConstantUtil.CODE_407, "该用户名已存在！");
			}
		}
		
		//校验手机号重复
		if(StringUtil.isNotNull(sysUsers.getTelephone())){
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("telephone", sysUsers.getTelephone());
			int total = sysUsersService.selectSysUsersListCount(map);
			if(total>0){
				return new ResultEntity<SysUsers>(ConstantUtil.CODE_407, "该手机号已存在！");
			}
		}
		
		//校验邮箱重复
		if(StringUtil.isNotNull(sysUsers.getEmail())){
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("email", sysUsers.getEmail());
			int total = sysUsersService.selectSysUsersListCount(map);
			if(total>0){
				return new ResultEntity<SysUsers>(ConstantUtil.CODE_407, "该邮箱已存在！");
			}
		}
		
		sysUsers.setUserId(UUID.randomUUID().toString());
		String userPass = sysUsers.getUserPass();
		if(StringUtils.isNotBlank(userPass)){
			sysUsers.setUserPass(MD5Utils.getMD5(userPass));
		}
		
		sysUsers.setCreateTime(DateUtil.formatDate(new Date(), DateUtil.DATE_TIME_PATTERN));
		sysUsers.setUpdateTime(DateUtil.formatDate(new Date(), DateUtil.DATE_TIME_PATTERN));
		sysUsers.setState(ConstantUtil.IS_VALID_Y);
		sysUsersService.insertSysUsers(sysUsers);
		return new ResultEntity<SysUsers>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}
	
	/**
	 * 新增SysUsers
	 */
	public ResultEntity<SysUsers> insertSysUsers(SysUsers sysUsers,String roleIds) throws DaoException {
		
		//校验用户名重复
		if(StringUtil.isNotNull(sysUsers.getUserName())){
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("userName", sysUsers.getUserName());
			int total = sysUsersService.selectSysUsersListCount(map);
			if(total>0){
				return new ResultEntity<SysUsers>(ConstantUtil.CODE_407, "该用户名已存在！");
			}
		}
		
		//校验手机号重复
		if(StringUtil.isNotNull(sysUsers.getTelephone())){
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("telephone", sysUsers.getTelephone());
			int total = sysUsersService.selectSysUsersListCount(map);
			if(total>0){
				return new ResultEntity<SysUsers>(ConstantUtil.CODE_407, "该手机号已存在！");
			}
		}
		
		//校验邮箱重复
		if(StringUtil.isNotNull(sysUsers.getEmail())){
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("email", sysUsers.getEmail());
			int total = sysUsersService.selectSysUsersListCount(map);
			if(total>0){
				return new ResultEntity<SysUsers>(ConstantUtil.CODE_407, "该邮箱已存在！");
			}
		}
		
		String userId = UUID.randomUUID().toString();
		sysUsers.setUserId(userId);
		String userPass = sysUsers.getUserPass();
		if(StringUtils.isNotBlank(userPass)){
			sysUsers.setUserPass(MD5Utils.getMD5(userPass));
		}
		sysUsers.setCreateTime(DateUtil.formatDate(new Date(), DateUtil.DATE_TIME_PATTERN));
		sysUsers.setUpdateTime(DateUtil.formatDate(new Date(), DateUtil.DATE_TIME_PATTERN));
		sysUsers.setState(ConstantUtil.IS_VALID_Y);
		sysUsersService.insertSysUsers(sysUsers);
		
		//新增用户角色关联信息表
		if(StringUtil.isNotNull(roleIds)){
			String [] roleIdArr = roleIds.split(",");
			for(String roleId: roleIdArr){
				SysUserRole sysUserRole = new SysUserRole();
				sysUserRole.setUserRoleId(UUID.randomUUID().toString());
				sysUserRole.setRoleId(roleId);
				sysUserRole.setUserId(userId);
				sysUserRole.setState(ConstantUtil.IS_VALID_Y);
				sysUserRoleService.insertSysUserRole(sysUserRole);
			}
		}
		
		return new ResultEntity<SysUsers>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改SysUsers
	 * @throws ValidateException 
	 */
	public ResultEntity<SysUsers> updateSysUsers(SysUsers sysUsers) throws DaoException {
		//校验用户名重复
		if(StringUtil.isNotNull(sysUsers.getUserName())){
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("userName", sysUsers.getUserName());
			List<SysUsers> list = sysUsersService.selectAllSysUsersList(map);
			if(list!=null && list.size()>0){
				if(list.size() == 1 && list.get(0).getUserId().equals(sysUsers.getUserId())){
					//说明是当前本身
				}else{
					return new ResultEntity<SysUsers>(ConstantUtil.CODE_407, "该用户名已存在！");
				}
			}
		}
		
		//校验手机号重复
		if(StringUtil.isNotNull(sysUsers.getTelephone())){
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("telephone", sysUsers.getTelephone());
			List<SysUsers> list = sysUsersService.selectAllSysUsersList(map);
			if(list!=null && list.size()>0){
				if(list.size() == 1 && list.get(0).getUserId().equals(sysUsers.getUserId())){
					//说明是当前本身
				}else{
					return new ResultEntity<SysUsers>(ConstantUtil.CODE_407, "该手机号已存在！");
				}
			}
		}
		
		//校验邮箱重复
		if(StringUtil.isNotNull(sysUsers.getEmail())){
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("email", sysUsers.getEmail());
			List<SysUsers> list = sysUsersService.selectAllSysUsersList(map);
			if(list!=null && list.size()>0){
				if(list.size() == 1 && list.get(0).getUserId().equals(sysUsers.getUserId())){
					//说明是当前本身
				}else{
					return new ResultEntity<SysUsers>(ConstantUtil.CODE_407, "该邮箱已存在！");
				}
			}
		}
		
		sysUsers.setUpdateTime(DateUtil.formatDate(new Date(), DateUtil.DATE_TIME_PATTERN));
		String userPass = sysUsers.getUserPass();
		if(StringUtils.isNotBlank(userPass)){
			sysUsers.setUserPass(MD5Utils.getMD5(userPass));
		}
		sysUsersService.updateSysUsers(sysUsers);
		return new ResultEntity<SysUsers>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}
	
	/**
	 * 修改SysUsers
	 */
	public ResultEntity<SysUsers> updateSysUsers(SysUsers sysUsers,String roleIds) throws DaoException {
		//校验用户名重复
		if(StringUtil.isNotNull(sysUsers.getUserName())){
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("userName", sysUsers.getUserName());
			List<SysUsers> list = sysUsersService.selectAllSysUsersList(map);
			if(list!=null && list.size()>0){
				if(list.size() == 1 && list.get(0).getUserId().equals(sysUsers.getUserId())){
					//说明是当前本身
				}else{
					return new ResultEntity<SysUsers>(ConstantUtil.CODE_407, "该用户名已存在！");
				}
			}
		}
		
		//校验手机号重复
		if(StringUtil.isNotNull(sysUsers.getTelephone())){
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("telephone", sysUsers.getTelephone());
			List<SysUsers> list = sysUsersService.selectAllSysUsersList(map);
			if(list!=null && list.size()>0){
				if(list.size() == 1 && list.get(0).getUserId().equals(sysUsers.getUserId())){
					//说明是当前本身
				}else{
					return new ResultEntity<SysUsers>(ConstantUtil.CODE_407, "该手机号已存在！");
				}
			}
		}
		
		//校验邮箱重复
		if(StringUtil.isNotNull(sysUsers.getEmail())){
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("email", sysUsers.getEmail());
			List<SysUsers> list = sysUsersService.selectAllSysUsersList(map);
			if(list!=null && list.size()>0){
				if(list.size() == 1 && list.get(0).getUserId().equals(sysUsers.getUserId())){
					//说明是当前本身
				}else{
					return new ResultEntity<SysUsers>(ConstantUtil.CODE_407, "该邮箱已存在！");
				}
			}
		}
		
		sysUsers.setUpdateTime(DateUtil.formatDate(new Date(), DateUtil.DATE_TIME_PATTERN));
		String userPass = sysUsers.getUserPass();
		if(StringUtils.isNotBlank(userPass)){
			sysUsers.setUserPass(MD5Utils.getMD5(userPass));
		}
		sysUsersService.updateSysUsers(sysUsers);
		
		//删除用户关联角色信息
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userId", sysUsers.getUserId());
		sysUserRoleService.deleteSysUserRole(map);
		
		//新增用户角色关联信息表
		if(StringUtil.isNotNull(roleIds)){
			String [] roleIdArr = roleIds.split(",");
			for(String roleId: roleIdArr){
				SysUserRole sysUserRole = new SysUserRole();
				sysUserRole.setUserRoleId(UUID.randomUUID().toString());
				sysUserRole.setRoleId(roleId);
				sysUserRole.setUserId(sysUsers.getUserId());
				sysUserRole.setState(ConstantUtil.IS_VALID_Y);
				sysUserRoleService.insertSysUserRole(sysUserRole);
			}
		}
		
		return new ResultEntity<SysUsers>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除SysUsers
	 */
	public ResultEntity<SysUsers> deleteSysUsers(Map<String, Object> map) throws DaoException {
		String userIds = (String) map.get("userIds");
		if (StringUtil.isNotNull(userIds) && !RegexUtil.isCharAndQuot(userIds)) {
			String newIds = StringUtil.strToCharAndQuot(userIds);
			map.put("userIds",newIds);
		}
		sysUsersService.deleteSysUsers(map);
		return new ResultEntity<SysUsers>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}

	public ResultEntity<JSONObject> dologin(Map<String, Object> map) throws DaoException, ValidateException {
		/**
		 * 校验用户名 密码
		 */
		if(map==null || !map.containsKey("userName")
		 || StringUtils.isBlank((String)map.get("userName"))){
			throw new ValidateException("用户名不能为空");
		}

		if(map==null || !map.containsKey("userPass")
				|| StringUtils.isBlank((String)map.get("userPass"))){
			throw new ValidateException("密码不能为空");
		}
		
		//md5加密
		String userPass = (String) map.get("userPass");
		userPass = MD5Utils.getMD5(userPass);
		map.put("userPass", userPass);
		
		List<SysUsers> sysUsersList = sysUsersService.selectSysUsersList(map);
		if(sysUsersList==null || sysUsersList.size()<=0){
			return new ResultEntity<JSONObject>(ConstantUtil.CODE_402, ConstantUtil.MSG_402);
		}

		SysUsers sysUsers = sysUsersList.get(0);
		String accessKey = (String) redisUtils.get(ConstantUtil.REDIS_KEY_ACCESS_KEY);
		if(StringUtils.isBlank(accessKey)){
			return new ResultEntity<JSONObject>(ConstantUtil.CODE_500,"accessKey为空，无法创建token");
		}

		TokenManager tokenManager = new TokenManager();
		long relativeTime = 7200L;
		JSONObject tokenData = new JSONObject();
		tokenData.put("userId",sysUsers.getUserId());

		String token;
		try {
			token = tokenManager.createToken(accessKey,relativeTime,tokenData.toJSONString(),"","");
		} catch (Exception e) {
			throw new DaoException(e,e.getMessage());
		}
		JSONObject result = new JSONObject();
		result.put("token",token);
		result.put("userId",sysUsers.getUserId());
		return new ResultEntity<JSONObject>(ConstantUtil.CODE_200,result);
	}

	public ResultEntity<JSONObject> loginOut(String token) throws ValidateException, DaoException {

		TokenManager tokenManager = new TokenManager();
		String accessKey = (String) redisUtils.get(ConstantUtil.REDIS_KEY_ACCESS_KEY);
		try {
			tokenManager.cleanToken(token,accessKey,"");
		} catch (Exception e) {
			throw new DaoException(e,e.getMessage());
		}
		return new ResultEntity<JSONObject>(ConstantUtil.CODE_200,"清除token成功");
	}

	/**
	 * 修改密码
	 * @param map
	 * @param sysUsers
	 * @return
	 * @throws ValidateException
	 * @throws DaoException
	 */
	public ResultEntity<SysUsers> updateUserPass(Map<String, Object> map,SysUsers sysUsers) throws ValidateException, DaoException {
		String password = (String) map.get("password");
		String newUserPass = (String) map.get("newUserPass");
		
		if(StringUtil.isNull(password)){
			throw new ValidateException("原密码不能为空");
		}
		
		if(StringUtil.isNull(newUserPass)){
			throw new ValidateException("新密码不能为空");
		}
		
		String oldPass = sysUsers.getUserPass();
		String checkPass = MD5Utils.getMD5(password);
		if(!checkPass.equals(oldPass)){
			return new ResultEntity<SysUsers>(ConstantUtil.CODE_404,"原密码错误");
		}
		
		//修改用户
		SysUsers updateUser = new SysUsers();
		updateUser.setUserId(sysUsers.getUserId());
		String userPass = MD5Utils.getMD5(newUserPass);
		updateUser.setUserPass(userPass);
		sysUsersService.updateSysUsers(updateUser);
		
		return new ResultEntity<SysUsers>(ConstantUtil.CODE_200,"密码修改成功");
	}
}