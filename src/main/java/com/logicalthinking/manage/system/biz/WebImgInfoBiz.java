	package com.logicalthinking.manage.system.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.WebImgInfo;
import com.logicalthinking.manage.system.service.WebImgInfoService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.DateUtil;
import com.logicalthinking.manage.system.utils.RegexUtil;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.utils.StringUtil;
import com.logicalthinking.manage.system.utils.ValidateException;

/**
 * 网站图片信息业务处理类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Service
public class WebImgInfoBiz {

	@Resource
	private WebImgInfoService webImgInfoService;

	/**
	 * 查询WebImgInfo集合
	 */
	public ResultEntity<List<WebImgInfo>> webImgInfoList(Map<String, Object> map) throws DaoException, ValidateException{
		List<WebImgInfo> webImgInfos = webImgInfoService.selectWebImgInfoList(map);
		int total = webImgInfoService.selectWebImgInfoListCount(map);
		return new ResultEntity<List<WebImgInfo>>(ConstantUtil.CODE_200, webImgInfos, total);
	}
	
	
	/**
	 * 查询WebImgInfo集合
	 */
	public List<WebImgInfo> webImgInfoAllList(Map<String, Object> map) throws DaoException{
		List<WebImgInfo> webImgInfos = webImgInfoService.selectAllWebImgInfoList(map);
		return webImgInfos;
	}
	
	/**
	 * 查询WebImgInfo集合
	 * @throws DaoException 
	 */
	public List<WebImgInfo> selectTypeImages(String imgType) throws DaoException{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("imgType", imgType);
		map.put("state", ConstantUtil.IS_VALID_Y);
		map.put("sortName", "rank");
		map.put("sortOrder", "asc");
		List<WebImgInfo> webImgInfos = webImgInfoService.selectAllWebImgInfoList(map);
		return webImgInfos;
	}
	
	
	/**
	 * 查询WebImgInfo集合
	 * @throws DaoException 
	 */
	public WebImgInfo selectBackgroundImage(String imgType) throws DaoException{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("imgType", imgType);
		map.put("state", ConstantUtil.IS_VALID_Y);
		map.put("sortName", "create_time");
		map.put("sortOrder", "desc");
		List<WebImgInfo> webImgInfos = webImgInfoService.selectAllWebImgInfoList(map);
		if(webImgInfos!=null && webImgInfos.size()>0){
			return webImgInfos.get(0);
		}
		return null;
	}
	

	/**
	 * 根据imgId查询WebImgInfo
	 */
	public ResultEntity<WebImgInfo> webImgInfoByImgId(String imgId) throws DaoException{
		if (StringUtils.isNotBlank(imgId)) {
			WebImgInfo webImgInfo = webImgInfoService.selectWebImgInfoByImgId(imgId);
			return new ResultEntity<WebImgInfo>(ConstantUtil.CODE_200, webImgInfo);
		} else {
			return new ResultEntity<WebImgInfo>(ConstantUtil.CODE_404, ConstantUtil.MSG_404);
		}
	}

	/**
	 * 新增WebImgInfo
	 */
	public ResultEntity<WebImgInfo> insertWebImgInfo(WebImgInfo webImgInfo) throws DaoException{
		webImgInfo.setImgId(UUID.randomUUID().toString());
		webImgInfo.setCreateTime(DateUtil.getNowDate(DateUtil.DATE_TIME_PATTERN));
		webImgInfo.setState(ConstantUtil.IS_VALID_Y);
		String imgPath = webImgInfo.getImgPath();
		if(StringUtil.isNotNull(imgPath)
				&& imgPath.indexOf(".")!=-1){
			String suffix = imgPath.substring(imgPath.lastIndexOf("."));
			webImgInfo.setSuffix(suffix);
		}
		webImgInfoService.insertWebImgInfo(webImgInfo);
		return new ResultEntity<WebImgInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改WebImgInfo
	 */
	public ResultEntity<WebImgInfo> updateWebImgInfo(WebImgInfo webImgInfo) throws DaoException{
		
		String imgPath = webImgInfo.getImgPath();
		if(StringUtil.isNotNull(imgPath)
				&& imgPath.indexOf(".")!=-1){
			String suffix = imgPath.substring(imgPath.lastIndexOf("."));
			webImgInfo.setSuffix(suffix);
		}
		
		webImgInfoService.updateWebImgInfo(webImgInfo);
		return new ResultEntity<WebImgInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除WebImgInfo
	 */
	public ResultEntity<WebImgInfo> deleteWebImgInfo(Map<String, Object> map) throws DaoException{
		String imgIds = (String) map.get("imgIds");
		if (StringUtil.isNotNull(imgIds) && !RegexUtil.isCharAndQuot(imgIds)) {
			String newIds = StringUtil.strToCharAndQuot(imgIds);
			map.put("imgIds",newIds);
		}
		webImgInfoService.deleteWebImgInfo(map);
		return new ResultEntity<WebImgInfo>(ConstantUtil.CODE_200, ConstantUtil.MSG_DEL_SUCCESS);
	}


	public ResultEntity<WebImgInfo> webImgInfoSort(String[] imgIds) throws ValidateException, DaoException {
		if(imgIds==null || imgIds.length == 0){
			throw new ValidateException("参数错误！"); 
		}
		int rank = 1;
		for(String imgId: imgIds){
			WebImgInfo webImgInfo = new WebImgInfo();
			webImgInfo.setImgId(imgId);
			webImgInfo.setRank(rank);
			webImgInfoService.updateWebImgInfo(webImgInfo);
			rank++;
		}
		return new ResultEntity<WebImgInfo>(ConstantUtil.CODE_200, "排序成功");
	}

}