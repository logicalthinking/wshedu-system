package com.logicalthinking.manage.system.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logicalthinking.manage.system.annotation.SysLog;
import com.logicalthinking.manage.system.biz.ArticleInfoBiz;
import com.logicalthinking.manage.system.biz.DictInfoBiz;
import com.logicalthinking.manage.system.entity.ArticleInfo;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.DictConstant;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.vo.KeyValue;

/**
 * 文案信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Controller
public class ArticleInfoController extends BaseController {

	@Resource
	private ArticleInfoBiz articleInfoBiz;
	
	@Resource
	private DictInfoBiz dictInfoBiz;
	
	/**
	 * 文章列表页面
	 */
	@RequestMapping(value = "/manage/articleListPage")
	public String articleListPage(HttpServletRequest request, HttpServletResponse response) {
		try {
			//查询字典文案类型
			List<KeyValue> articleTypes = dictInfoBiz.selectDictChidren(DictConstant.DICT_ARTICLE_TYPE);
			request.setAttribute("articleTypes", articleTypes);
		} catch (DaoException e) {
			e.printStackTrace();
		}
		
		return "website/article/articleList";
	}
	
	/**
	 * 查看文章页面
	 */
	@RequestMapping(value = "/manage/articleViewPage")
	public String articleViewPage(String articleId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<ArticleInfo> resultEntity = articleInfoBiz.articleInfoByArticleId(articleId);
			request.setAttribute("articleInfo", resultEntity.getResult());
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "website/article/articleView";
	}
	
	/**
	 * 编辑文章页面
	 */
	@RequestMapping(value = "/manage/articleEditPage")
	public String articleEditPage(String articleId,HttpServletRequest request, HttpServletResponse response) {
		try {
			//查询字典文案类型
			List<KeyValue> articleTypes = dictInfoBiz.selectDictChidren(DictConstant.DICT_ARTICLE_TYPE);
			request.setAttribute("articleTypes", articleTypes);
			
			ResultEntity<ArticleInfo> resultEntity = articleInfoBiz.articleInfoByArticleId(articleId);
			request.setAttribute("articleInfo", resultEntity.getResult());
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "website/article/articleEdit";
	}
	
	/**
	 * 新增文章页面
	 */
	@RequestMapping(value = "/manage/articleAddPage")
	public String articleAddPage(HttpServletRequest request, HttpServletResponse response) {
		try {
			//查询字典文案类型
			List<KeyValue> articleTypes = dictInfoBiz.selectDictChidren(DictConstant.DICT_ARTICLE_TYPE);
			request.setAttribute("articleTypes", articleTypes);
		} catch (DaoException e) {
			e.printStackTrace();
		}
				
		return "website/article/articleAdd";
	}

	/**
	 * 文案排序页面
	 */
	@RequestMapping(value = "/manage/articleSortPage")
	public String articleSortPage(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String,Object> map=initRequestMap(request);
		try {
			map.put("state", ConstantUtil.IS_VALID_Y);
			map.put("sortName", "rank");
			map.put("sortOrder", "asc");
			List<ArticleInfo> articleInfos = articleInfoBiz.selectAllArticleInfoList(map);
			request.setAttribute("articleInfos", articleInfos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "website/article/articleSort";
	}
	
	/**
	 * 排序
	 */
	@ResponseBody
	@SysLog(operateType="文案排序")
	@RequestMapping(value = "/manage/articleSort", produces = "application/json;charset=UTF-8")
	public String articleSort(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<ArticleInfo> resultEntity = null;
		try {
			String [] articleIds = request.getParameterValues("articleIds[]");
			resultEntity = articleInfoBiz.articleSort(articleIds);
		} catch (Exception e) {
			resultEntity = new ResultEntity<ArticleInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}
	
	/**
	 * 查询ArticleInfo集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/articleInfoList", produces = "application/json;charset=UTF-8")
	public String articleInfoList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<ArticleInfo>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = articleInfoBiz.articleInfoList(map);
		} catch (Exception e) {
			resultEntity = new ResultEntity<List<ArticleInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据articleId查询ArticleInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/articleInfoByArticleId", produces = "application/json;charset=UTF-8")
	public String articleInfoByArticleId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<ArticleInfo> resultEntity = null;
		try {
			String articleId = request.getParameter("articleId");
			resultEntity = articleInfoBiz.articleInfoByArticleId(articleId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<ArticleInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增ArticleInfo
	 */
	@ResponseBody
	@SysLog(operateType="新增文案")
	@RequestMapping(value = "/manage/addArticleInfo", produces = "application/json;charset=UTF-8")
	public String addArticleInfo(ArticleInfo articleInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<ArticleInfo> resultEntity = null;
		try {
			resultEntity = articleInfoBiz.insertArticleInfo(articleInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<ArticleInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

	/**
	 * 修改ArticleInfo
	 */
	@ResponseBody
	@SysLog(operateType="修改文案")
	@RequestMapping(value = "/manage/updateArticleInfo", produces = "application/json;charset=UTF-8")
	public String updateArticleInfo(ArticleInfo articleInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<ArticleInfo> resultEntity = null;
		try {
			resultEntity = articleInfoBiz.updateArticleInfo(articleInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<ArticleInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

	/**
	 * 删除ArticleInfo
	 */
	@ResponseBody
	@SysLog(operateType="删除文案")
	@RequestMapping(value = "/manage/deleteArticleInfo", produces = "application/json;charset=UTF-8")
	public String deleteArticleInfo(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<ArticleInfo> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = articleInfoBiz.deleteArticleInfo(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<ArticleInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

}