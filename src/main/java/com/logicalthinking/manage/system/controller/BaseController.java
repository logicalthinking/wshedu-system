package com.logicalthinking.manage.system.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.hellooop.odj.token.manager.TokenManager;
import com.hellooop.odj.token.utils.ValidateException;
import com.logicalthinking.manage.system.biz.SysUsersBiz;
import com.logicalthinking.manage.system.entity.SysUsers;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 基础工具控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-09-18
 */
@Controller
public class BaseController {

	@Autowired
	private SysUsersBiz sysUsersBiz;
	/**
	 * 初始化request
	 */
	protected Map<String, Object> initRequestMap(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String,String[]> params = request.getParameterMap();
		if(params != null) {
			for(Entry<String,String[]> entry:params.entrySet()){
				String key = entry.getKey();
				String [] value = entry.getValue();
				if(StringUtils.isNotBlank(key)
					&& value != null && value.length > 0) {
					if(StringUtils.isNotBlank(value[0])){
						map.put(key, value[0]);
					}
				}
			}
		}
		return map;
	}
	
	/**
	 * 设置日志参数 
	 * @param request
	 * @param error_code
	 * @param userId
	 */
	protected void setLogRequest(HttpServletRequest request,String error_code,String userId) {
		if(ConstantUtil.CODE_200.equals(error_code)){
			request.setAttribute("operateDesc", ConstantUtil.OP_MSG_SUCCESS);
		}else{
			request.setAttribute("operateDesc", ConstantUtil.OP_MSG_ERROR);
		}
		request.setAttribute("userId", userId);
	}
	
	/**
	 * 设置日志参数 
	 * @param request
	 * @param error_code
	 * @param userId
	 */
	protected void setLogRequest(HttpServletRequest request,String error_code) {
		if(ConstantUtil.CODE_200.equals(error_code)){
			request.setAttribute("operateDesc", ConstantUtil.OP_MSG_SUCCESS);
		}else{
			request.setAttribute("operateDesc", ConstantUtil.OP_MSG_ERROR);
		}
	}

	public SysUsers getSysUsers(HttpServletRequest request) throws Exception {
		SysUsers sysUsers = null;
		String token = getToken(request);
		TokenManager tokenManager = new TokenManager();
		try {
			String  userId = tokenManager.getTokenData(token,"userId");
			if(StringUtils.isNotBlank(userId)){
				//查询用户
				ResultEntity<SysUsers> resultEntity = sysUsersBiz.sysUsersByUserId(userId);
				if(ConstantUtil.CODE_200.equals(resultEntity.getError_code())){
					sysUsers = resultEntity.getResult();
				}
			}
		} catch (Exception e) {
			throw new ValidateException(e,e.getMessage());
		}
		return sysUsers;
	}
	
	public static String getToken(HttpServletRequest request){
		String token = "";
		Cookie[] cookies = request.getCookies();
		if(cookies!=null){
			for(int i=0;i<cookies.length;i++){
				if(com.logicalthinking.manage.system.utils.ConstantUtil.SESSION_TOKEN_NAME.equals(cookies[i].getName())){
					token = cookies[i].getValue();
					break;
				}
			}
		}
		return token;
	}
}