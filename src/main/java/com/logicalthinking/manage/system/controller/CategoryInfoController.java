package com.logicalthinking.manage.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logicalthinking.manage.system.annotation.SysLog;
import com.logicalthinking.manage.system.biz.CategoryInfoBiz;
import com.logicalthinking.manage.system.biz.DictInfoBiz;
import com.logicalthinking.manage.system.entity.CategoryInfo;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.DictConstant;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.vo.KeyValue;

/**
 * 分类管理控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Controller
public class CategoryInfoController extends BaseController {

	@Resource
	private CategoryInfoBiz categoryInfoBiz;
	
	@Autowired
	private DictInfoBiz dictInfoBiz;

	/**
	 * 分类列表页面
	 */
	@RequestMapping(value = "/manage/categoryListPage")
	public String categoryListPage(HttpServletRequest request, HttpServletResponse response) {
		try {
			//查询字典分类类型
			List<KeyValue> bigTypes = dictInfoBiz.selectDictChidren(DictConstant.DICT_CATEGORY_TYPE);
			request.setAttribute("bigTypes", bigTypes);
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "website/category/categoryList";
	}
	
	/**
	 * 查看分类页面
	 */
	@RequestMapping(value = "/manage/categoryViewPage")
	public String categoryViewPage(String typeId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<CategoryInfo> resultEntity = categoryInfoBiz.categoryInfoByTypeId(typeId);
			request.setAttribute("categoryInfo", resultEntity.getResult());
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "website/category/categoryView";
	}
	
	/**
	 * 编辑分类页面
	 */
	@RequestMapping(value = "/manage/categoryEditPage")
	public String categoryEditPage(String typeId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<CategoryInfo> resultEntity = categoryInfoBiz.categoryInfoByTypeId(typeId);
			CategoryInfo categoryInfo = resultEntity.getResult();
			request.setAttribute("categoryInfo", categoryInfo);
			
			//查询字典分类类型
			List<KeyValue> bigTypes = dictInfoBiz.selectDictChidren(DictConstant.DICT_CATEGORY_TYPE);
			request.setAttribute("bigTypes", bigTypes);
			
			//查询下级
			if(categoryInfo!=null){
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("bigType", categoryInfo.getBigType());
				List<CategoryInfo> parentTypes = categoryInfoBiz.categoryInfoAllList(map);
				request.setAttribute("parentTypes", parentTypes);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "website/category/categoryEdit";
	}
	
	/**
	 * 新增分类页面
	 */
	@RequestMapping(value = "/manage/categoryAddPage")
	public String categoryAddPage(HttpServletRequest request, HttpServletResponse response) {
		try {
			//查询字典分类类型
			List<KeyValue> bigTypes = dictInfoBiz.selectDictChidren(DictConstant.DICT_CATEGORY_TYPE);
			request.setAttribute("bigTypes", bigTypes);
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "website/category/categoryAdd";
	}
	
	/**
	 * 查询CategoryInfo集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/categoryInfoList", produces = "application/json;charset=UTF-8")
	public String categoryInfoList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<CategoryInfo>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = categoryInfoBiz.categoryInfoList(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<CategoryInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} catch (ValidateException e) {
			resultEntity = new ResultEntity<List<CategoryInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}
	
	@ResponseBody
	@RequestMapping(value = "/manage/categoryInfoAllList", produces = "application/json;charset=UTF-8")
	public String categoryInfoAllList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<CategoryInfo>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			List<CategoryInfo> categoryInfos = categoryInfoBiz.categoryInfoAllList(map);
			resultEntity = new ResultEntity<List<CategoryInfo>>(ConstantUtil.CODE_200,categoryInfos);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<CategoryInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} catch (ValidateException e) {
			resultEntity = new ResultEntity<List<CategoryInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据typeId查询CategoryInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/categoryInfoByTypeId", produces = "application/json;charset=UTF-8")
	public String categoryInfoByTypeId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<CategoryInfo> resultEntity = null;
		try {
			String typeId = request.getParameter("typeId");
			resultEntity = categoryInfoBiz.categoryInfoByTypeId(typeId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<CategoryInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增CategoryInfo
	 */
	@ResponseBody
	@SysLog(operateType="新增分类")
	@RequestMapping(value = "/manage/addCategoryInfo", produces = "application/json;charset=UTF-8")
	public String addCategoryInfo(CategoryInfo categoryInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<CategoryInfo> resultEntity = null;
		try {
			resultEntity = categoryInfoBiz.insertCategoryInfo(categoryInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<CategoryInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

	/**
	 * 修改CategoryInfo
	 */
	@ResponseBody
	@SysLog(operateType="修改分类")
	@RequestMapping(value = "/manage/updateCategoryInfo", produces = "application/json;charset=UTF-8")
	public String updateCategoryInfo(CategoryInfo categoryInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<CategoryInfo> resultEntity = null;
		try {
			resultEntity = categoryInfoBiz.updateCategoryInfo(categoryInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<CategoryInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

	/**
	 * 删除CategoryInfo
	 */
	@ResponseBody
	@SysLog(operateType="删除分类")
	@RequestMapping(value = "/manage/deleteCategoryInfo", produces = "application/json;charset=UTF-8")
	public String deleteCategoryInfo(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<CategoryInfo> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = categoryInfoBiz.deleteCategoryInfo(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<CategoryInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

}