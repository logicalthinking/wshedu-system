package com.logicalthinking.manage.system.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logicalthinking.manage.system.entity.ContactRecord;
import com.logicalthinking.manage.system.biz.ContactRecordBiz;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 学员联系记录控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Controller
public class ContactRecordController extends BaseController {

	@Resource
	private ContactRecordBiz contactRecordBiz;
	
	
	/**
	 * 联系记录列表页面
	 */
	@RequestMapping(value = "/manage/contactRecordListPage")
	public String contactRecordListPage(HttpServletRequest request, HttpServletResponse response) {
		return "students/contact/contactRecordList";
	}
	
	/**
	 * 新增联系记录页面
	 */
	@RequestMapping(value = "/manage/contactRecordAddPage")
	public String contactRecordAddPage(HttpServletRequest request, HttpServletResponse response) {
		return "students/contact/contactRecordAdd";
	}
	
	/**
	 * 修改联系记录页面
	 */
	@RequestMapping(value = "/manage/contactRecordEditPage")
	public String contactRecordEditPage(HttpServletRequest request, HttpServletResponse response) {
		return "students/contact/contactRecordEdit";
	}
	
	/**
	 * 查看联系记录页面
	 */
	@RequestMapping(value = "/manage/contactRecordViewPage")
	public String contactRecordViewPage(HttpServletRequest request, HttpServletResponse response) {
		return "students/contact/contactRecordView";
	}

	/**
	 * 查询ContactRecord集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/contactRecordList", produces = "application/json;charset=UTF-8")
	public String contactRecordList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<ContactRecord>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = contactRecordBiz.contactRecordList(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<ContactRecord>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} catch (ValidateException e) {
			resultEntity = new ResultEntity<List<ContactRecord>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据contactId查询ContactRecord
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/contactRecordByContactId", produces = "application/json;charset=UTF-8")
	public String contactRecordByContactId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<ContactRecord> resultEntity = null;
		try {
			String contactId = request.getParameter("contactId");
			resultEntity = contactRecordBiz.contactRecordByContactId(contactId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<ContactRecord>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增ContactRecord
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/addContactRecord", produces = "application/json;charset=UTF-8")
	public String addContactRecord(ContactRecord contactRecord, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<ContactRecord> resultEntity = null;
		try {
			resultEntity = contactRecordBiz.insertContactRecord(contactRecord);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<ContactRecord>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 修改ContactRecord
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/updateContactRecord", produces = "application/json;charset=UTF-8")
	public String updateContactRecord(ContactRecord contactRecord, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<ContactRecord> resultEntity = null;
		try {
			resultEntity = contactRecordBiz.updateContactRecord(contactRecord);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<ContactRecord>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 删除ContactRecord
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/deleteContactRecord", produces = "application/json;charset=UTF-8")
	public String deleteContactRecord(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<ContactRecord> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = contactRecordBiz.deleteContactRecord(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<ContactRecord>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

}