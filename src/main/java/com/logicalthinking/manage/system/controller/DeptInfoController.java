package com.logicalthinking.manage.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logicalthinking.manage.system.biz.DeptInfoBiz;
import com.logicalthinking.manage.system.entity.DeptInfo;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.vo.DeptTreeVo;

/**
 * 部门信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Controller
public class DeptInfoController extends BaseController {

	@Resource
	private DeptInfoBiz deptInfoBiz;
	
	/**
	 * 部门列表页面
	 */
	@RequestMapping(value = "/manage/deptListPage")
	public String deptListPage(HttpServletRequest request, HttpServletResponse response) {
		return "organize/depts/deptList";
	}
	
	/**
	 * 组织结构页面
	 */
	@RequestMapping(value = "/manage/deptTreeListPage")
	public String deptTreeListPage(HttpServletRequest request, HttpServletResponse response) {
		return "organize/depts/deptTreeList";
	}
	
	/**
	 * 新增部门页面
	 */
	@RequestMapping(value = "/manage/deptAddPage")
	public String deptAddPage(HttpServletRequest request, HttpServletResponse response) {
		try {
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("parentId", "0");
			List<DeptInfo> deptInfos = deptInfoBiz.deptInfoAllList(map);
			request.setAttribute("deptInfos", deptInfos);
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "organize/depts/deptAdd";
	}
	
	/**
	 * 修改部门页面
	 */
	@RequestMapping(value = "/manage/deptEditPage")
	public String deptEditPage(String deptId,HttpServletRequest request, HttpServletResponse response) {
		try {
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("parentId", "0");
			List<DeptInfo> deptInfos = deptInfoBiz.deptInfoAllList(map);
			request.setAttribute("deptInfos", deptInfos);
			
			ResultEntity<DeptInfo> resultEntity = deptInfoBiz.deptInfoByDeptId(deptId);
			request.setAttribute("deptInfo", resultEntity.getResult());
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "organize/depts/deptEdit";
	}
	
	/**
	 * 查看部门页面
	 */
	@RequestMapping(value = "/manage/deptViewPage")
	public String deptViewPage(String deptId,HttpServletRequest request, HttpServletResponse response) {
		try {
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("parentId", "0");
			List<DeptInfo> deptInfos = deptInfoBiz.deptInfoAllList(map);
			request.setAttribute("deptInfos", deptInfos);
			
			ResultEntity<DeptInfo> resultEntity = deptInfoBiz.deptInfoByDeptId(deptId);
			request.setAttribute("deptInfo", resultEntity.getResult());
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "organize/depts/deptView";
	}
	
	/**
	 * 查看部门员工信息页面
	 */
	@RequestMapping(value = "/manage/deptEmpListPage")
	public String deptEmpListPage(String deptId,HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("deptId", deptId);
		return "organize/depts/deptEmpList";
	}

	/**
	 * 查询DeptInfo集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/deptInfoList", produces = "application/json;charset=UTF-8")
	public String deptInfoList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<DeptInfo>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = deptInfoBiz.deptInfoList(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<DeptInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} catch (ValidateException e) {
			resultEntity = new ResultEntity<List<DeptInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	
	@ResponseBody
	@RequestMapping(value = "/manage/deptInfoTreeList", produces = "application/json;charset=UTF-8")
	public String deptInfoTreeList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<DeptTreeVo>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			List<DeptTreeVo> deptTreeVos = deptInfoBiz.selectDeptInfoTreeList(map);
			resultEntity = new ResultEntity<List<DeptTreeVo>>(ConstantUtil.CODE_200,deptTreeVos);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<DeptTreeVo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据deptId查询DeptInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/deptInfoByDeptId", produces = "application/json;charset=UTF-8")
	public String deptInfoByDeptId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<DeptInfo> resultEntity = null;
		try {
			String deptId = request.getParameter("deptId");
			resultEntity = deptInfoBiz.deptInfoByDeptId(deptId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<DeptInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增DeptInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/addDeptInfo", produces = "application/json;charset=UTF-8")
	public String addDeptInfo(DeptInfo deptInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<DeptInfo> resultEntity = null;
		try {
			resultEntity = deptInfoBiz.insertDeptInfo(deptInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<DeptInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 修改DeptInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/updateDeptInfo", produces = "application/json;charset=UTF-8")
	public String updateDeptInfo(DeptInfo deptInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<DeptInfo> resultEntity = null;
		try {
			resultEntity = deptInfoBiz.updateDeptInfo(deptInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<DeptInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 删除DeptInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/deleteDeptInfo", produces = "application/json;charset=UTF-8")
	public String deleteDeptInfo(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<DeptInfo> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = deptInfoBiz.deleteDeptInfo(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<DeptInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

}