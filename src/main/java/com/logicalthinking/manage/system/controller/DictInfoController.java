package com.logicalthinking.manage.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hellooop.odj.token.utils.StringUtil;
import com.logicalthinking.manage.system.annotation.SysLog;
import com.logicalthinking.manage.system.biz.DictInfoBiz;
import com.logicalthinking.manage.system.entity.DictInfo;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.vo.DictTreeVo;
import com.logicalthinking.manage.system.vo.KeyValue;

/**
 * 字典信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Controller
public class DictInfoController extends BaseController {

	@Resource
	private DictInfoBiz dictInfoBiz;
	
	/**
	 * 字典列表页面
	 */
	@RequestMapping(value = "/manage/dictInfoListPage")
	public String dictInfoListPage(HttpServletRequest request, HttpServletResponse response) {
		return "system/dictInfo/dictInfoList";
	}
	
	/**
	 * 字典排序页面
	 */
	@RequestMapping(value = "/manage/dictInfoSortPage")
	public String dictInfoSortPage(String dictPid,HttpServletRequest request, HttpServletResponse response) {
		try {
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("dictPid", dictPid);
			map.put("sortName", "rank");
			map.put("sortOrder", "asc");
			map.put("state", ConstantUtil.IS_VALID_Y);
			List<DictInfo> dictInfos = dictInfoBiz.dictInfoAllList(map);
			request.setAttribute("dictInfos", dictInfos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "system/dictInfo/dictInfoSort";
	}
	
	/**
	 * 查看字典页面
	 */
	@RequestMapping(value = "/manage/dictInfoViewPage")
	public String dictInfoViewPage(String dictId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<DictInfo> resultEntity = dictInfoBiz.dictInfoByDictId(dictId);
			request.setAttribute("dictInfo", resultEntity.getResult());
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "system/dictInfo/dictInfoView";
	}
	
	/**
	 * 编辑字典页面
	 */
	@RequestMapping(value = "/manage/dictInfoEditPage")
	public String dictInfoEditPage(String dictId,HttpServletRequest request, HttpServletResponse response) {
		try {
			if(StringUtil.isNotNull(dictId)){
				DictInfo dictInfo = dictInfoBiz.selectDictInfoByDictId(dictId);
				request.setAttribute("dictInfo", dictInfo);
				
				String dictPid = request.getParameter("dictPid");
				if(StringUtil.isNull(dictPid) && dictInfo!=null){
					dictPid = dictInfo.getDictPid();
				}
				DictInfo parentDictInfo = dictInfoBiz.selectDictInfoByDictId(dictPid);
				request.setAttribute("parentDictInfo", parentDictInfo);
				
				String opType = request.getParameter("opType");
				request.setAttribute("opType", opType);
			}
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "system/dictInfo/dictInfoEdit";
	}
	
	/**
	 * 新增字典页面
	 */
	@RequestMapping(value = "/manage/dictInfoAddPage")
	public String dictInfoAddPage(HttpServletRequest request, HttpServletResponse response) {
		try {
			String dictPid = request.getParameter("dictPid");
			if(StringUtil.isNotNull(dictPid)){
				DictInfo dictInfo = dictInfoBiz.selectDictInfoByDictId(dictPid);
				request.setAttribute("parentDictInfo",dictInfo);
			}
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "system/dictInfo/dictInfoAdd";
	}
	
	/**
	 * 查询DictInfo集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/dictChildren", produces = "application/json;charset=UTF-8")
	public String dictChildren(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<KeyValue>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			map.put("sortName", "rank");
			map.put("sortOrder", "asc");
			map.put("state", ConstantUtil.IS_VALID_Y);
			List<KeyValue> dictInfos = dictInfoBiz.selectDictChidren(map);
			resultEntity = new ResultEntity<List<KeyValue>>(ConstantUtil.CODE_200, dictInfos);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<KeyValue>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}
	
	/**
	 * 清除字典缓存
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/clearDictCache", produces = "application/json;charset=UTF-8")
	public String clearDictCache(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<Object> resultEntity = null;
		try {
			dictInfoBiz.clearDictsCache();
			resultEntity = new ResultEntity<Object>(ConstantUtil.CODE_200, "清除成功");
		} catch (Exception e) {
			resultEntity = new ResultEntity<Object>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}
	
	/**
	 * 查询DictInfo集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/dictInfoList", produces = "application/json;charset=UTF-8")
	public String dictInfoList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<DictInfo>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = dictInfoBiz.dictInfoList(map);
		} catch (Exception e) {
			resultEntity = new ResultEntity<List<DictInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}
	/**
	 * 字典排序
	 */
	@ResponseBody
	@SysLog(operateType="字典排序")
	@RequestMapping(value = "/manage/dictInfoSort", produces = "application/json;charset=UTF-8")
	public String dictInfoSort(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<DictInfo> resultEntity = null;
		try {
			String [] dictIds = request.getParameterValues("dictIds[]");
			resultEntity = dictInfoBiz.sortDictInfos(dictIds);
		} catch (Exception e) {
			resultEntity = new ResultEntity<DictInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}
	
	/**
	 * 查询DictTreeVo集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/dictTreeList", produces = "application/json;charset=UTF-8")
	public String dictTreeList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<DictTreeVo>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			List<DictTreeVo> dictTreeVos = dictInfoBiz.getDictTreeVoList(map);
			if(dictTreeVos!=null && dictTreeVos.size()>0){
				for(DictTreeVo dictTreeVo : dictTreeVos){
					dictTreeVo.setSpread(true);
				}
			}
			resultEntity = new ResultEntity<List<DictTreeVo>>(ConstantUtil.CODE_200, dictTreeVos);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<DictTreeVo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据dictId查询DictInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/dictInfoByDictId", produces = "application/json;charset=UTF-8")
	public String dictInfoByDictId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<DictInfo> resultEntity = null;
		try {
			String dictId = request.getParameter("dictId");
			resultEntity = dictInfoBiz.dictInfoByDictId(dictId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<DictInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增DictInfo
	 */
	@ResponseBody
	@SysLog(operateType="新增字典")
	@RequestMapping(value = "/manage/addDictInfo", produces = "application/json;charset=UTF-8")
	public String addDictInfo(DictInfo dictInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<DictInfo> resultEntity = null;
		try {
			resultEntity = dictInfoBiz.insertDictInfo(dictInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<DictInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}
 
	/**
	 * 修改DictInfo
	 */
	@ResponseBody
	@SysLog(operateType="修改字典")
	@RequestMapping(value = "/manage/updateDictInfo", produces = "application/json;charset=UTF-8")
	public String updateDictInfo(DictInfo dictInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<DictInfo> resultEntity = null;
		try {
			resultEntity = dictInfoBiz.updateDictInfo(dictInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<DictInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

	/**
	 * 删除DictInfo
	 */
	@ResponseBody
	@SysLog(operateType="删除字典")
	@RequestMapping(value = "/manage/deleteDictInfo", produces = "application/json;charset=UTF-8")
	public String deleteDictInfo(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<DictInfo> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = dictInfoBiz.deleteDictInfo(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<DictInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

}