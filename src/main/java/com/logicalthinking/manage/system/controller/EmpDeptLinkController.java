package com.logicalthinking.manage.system.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logicalthinking.manage.system.entity.EmpDeptLink;
import com.logicalthinking.manage.system.biz.EmpDeptLinkBiz;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 员工部门关联信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Controller
public class EmpDeptLinkController extends BaseController {

	@Resource
	private EmpDeptLinkBiz empDeptLinkBiz;

	/**
	 * 查询EmpDeptLink集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/empDeptLinkList", produces = "application/json;charset=UTF-8")
	public String empDeptLinkList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<EmpDeptLink>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = empDeptLinkBiz.empDeptLinkList(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<EmpDeptLink>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} catch (ValidateException e) {
			resultEntity = new ResultEntity<List<EmpDeptLink>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据linkId查询EmpDeptLink
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/empDeptLinkByLinkId", produces = "application/json;charset=UTF-8")
	public String empDeptLinkByLinkId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<EmpDeptLink> resultEntity = null;
		try {
			String linkId = request.getParameter("linkId");
			resultEntity = empDeptLinkBiz.empDeptLinkByLinkId(linkId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<EmpDeptLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增EmpDeptLink
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/addEmpDeptLink", produces = "application/json;charset=UTF-8")
	public String addEmpDeptLink(EmpDeptLink empDeptLink, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<EmpDeptLink> resultEntity = null;
		try {
			resultEntity = empDeptLinkBiz.insertEmpDeptLink(empDeptLink);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<EmpDeptLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 修改EmpDeptLink
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/updateEmpDeptLink", produces = "application/json;charset=UTF-8")
	public String updateEmpDeptLink(EmpDeptLink empDeptLink, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<EmpDeptLink> resultEntity = null;
		try {
			resultEntity = empDeptLinkBiz.updateEmpDeptLink(empDeptLink);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<EmpDeptLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 删除EmpDeptLink
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/deleteEmpDeptLink", produces = "application/json;charset=UTF-8")
	public String deleteEmpDeptLink(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<EmpDeptLink> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = empDeptLinkBiz.deleteEmpDeptLink(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<EmpDeptLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

}