package com.logicalthinking.manage.system.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logicalthinking.manage.system.entity.EmpGroupLink;
import com.logicalthinking.manage.system.biz.EmpGroupLinkBiz;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 员工分组关联信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Controller
public class EmpGroupLinkController extends BaseController {

	@Resource
	private EmpGroupLinkBiz empGroupLinkBiz;

	/**
	 * 查询EmpGroupLink集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/empGroupLinkList", produces = "application/json;charset=UTF-8")
	public String empGroupLinkList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<EmpGroupLink>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = empGroupLinkBiz.empGroupLinkList(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<EmpGroupLink>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} catch (ValidateException e) {
			resultEntity = new ResultEntity<List<EmpGroupLink>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据linkId查询EmpGroupLink
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/empGroupLinkByLinkId", produces = "application/json;charset=UTF-8")
	public String empGroupLinkByLinkId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<EmpGroupLink> resultEntity = null;
		try {
			String linkId = request.getParameter("linkId");
			resultEntity = empGroupLinkBiz.empGroupLinkByLinkId(linkId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<EmpGroupLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增EmpGroupLink
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/addEmpGroupLink", produces = "application/json;charset=UTF-8")
	public String addEmpGroupLink(EmpGroupLink empGroupLink, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<EmpGroupLink> resultEntity = null;
		try {
			resultEntity = empGroupLinkBiz.insertEmpGroupLink(empGroupLink);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<EmpGroupLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 修改EmpGroupLink
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/updateEmpGroupLink", produces = "application/json;charset=UTF-8")
	public String updateEmpGroupLink(EmpGroupLink empGroupLink, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<EmpGroupLink> resultEntity = null;
		try {
			resultEntity = empGroupLinkBiz.updateEmpGroupLink(empGroupLink);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<EmpGroupLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 删除EmpGroupLink
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/deleteEmpGroupLink", produces = "application/json;charset=UTF-8")
	public String deleteEmpGroupLink(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<EmpGroupLink> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = empGroupLinkBiz.deleteEmpGroupLink(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<EmpGroupLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

}