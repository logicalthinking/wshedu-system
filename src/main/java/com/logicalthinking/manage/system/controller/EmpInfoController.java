package com.logicalthinking.manage.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logicalthinking.manage.system.entity.DeptInfo;
import com.logicalthinking.manage.system.entity.EmpDeptLink;
import com.logicalthinking.manage.system.entity.EmpInfo;
import com.logicalthinking.manage.system.entity.SysRole;
import com.logicalthinking.manage.system.entity.SysUsers;
import com.logicalthinking.manage.system.biz.DeptInfoBiz;
import com.logicalthinking.manage.system.biz.DictInfoBiz;
import com.logicalthinking.manage.system.biz.EmpInfoBiz;
import com.logicalthinking.manage.system.biz.SysRoleBiz;
import com.logicalthinking.manage.system.biz.SysUsersBiz;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.DictConstant;
import com.logicalthinking.manage.system.utils.StringUtil;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.vo.KeyValue;

/**
 * 员工信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Controller
public class EmpInfoController extends BaseController {

	@Resource
	private EmpInfoBiz empInfoBiz;
	
	@Autowired
	private DeptInfoBiz deptInfoBiz;
	
	@Autowired
	private DictInfoBiz dictInfoBiz;
	
	@Autowired
	private SysRoleBiz sysRoleBiz;
	
	@Autowired
	private SysUsersBiz sysUsersBiz;
	
	/**
	 * 员工列表页面
	 */
	@RequestMapping(value = "/manage/empListPage")
	public String empListPage(HttpServletRequest request, HttpServletResponse response) {
		return "organize/emps/empList";
	}
	
	/**
	 * 员工开通账号页面
	 */
	@RequestMapping(value = "/manage/empUserAddPage")
	public String empUserAddPage(String empId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<EmpInfo> resultEntity = empInfoBiz.empInfoByEmpId(empId);
			request.setAttribute("empInfo", resultEntity.getResult());
			
			Map<String,Object> map = new HashMap<String, Object>();
			List<SysRole> sysRoles = sysRoleBiz.sysRoleAllList(map);
			request.setAttribute("sysRoles", sysRoles);
		} catch (DaoException e) {
			e.printStackTrace();
		}
		
		return "organize/emps/empUserAdd";
	}
	
	/**
	 * 新增员工页面
	 */
	@RequestMapping(value = "/manage/empAddPage")
	public String empAddPage(HttpServletRequest request, HttpServletResponse response) {
		Map<String,Object> map = new HashMap<String, Object>();
		try {
			map.put("noRootId", ConstantUtil.ROOT_DEPT_ID);
			List<DeptInfo> deptInfos = deptInfoBiz.deptInfoAllList(map);
			request.setAttribute("deptInfos", deptInfos);
			
			//查询字典在职状态
			List<KeyValue> jobStatus = dictInfoBiz.selectDictChidren(DictConstant.DICT_JOB_STATUS);
			request.setAttribute("jobStatus", jobStatus);
		} catch (DaoException e) {
			e.printStackTrace();
		}
		
		return "organize/emps/empAdd";
	}
	
	/**
	 * 修改员工页面
	 */
	@RequestMapping(value = "/manage/empEditPage")
	public String empEditPage(String empId,HttpServletRequest request, HttpServletResponse response) {
		Map<String,Object> map = new HashMap<String, Object>();
		try {
			map.put("noRootId", ConstantUtil.ROOT_DEPT_ID);
			List<DeptInfo> deptInfos = deptInfoBiz.deptInfoAllList(map);
			request.setAttribute("deptInfos", deptInfos);
			
			ResultEntity<EmpInfo> resultEntity = empInfoBiz.empInfoByEmpId(empId);
			EmpInfo result = resultEntity.getResult();
			request.setAttribute("empInfo", result);
			
			String deptIds = "";
			String deptNames = "";
			if(result.getEmpDeptLinks()!=null && result.getEmpDeptLinks().size()>0){
				for(EmpDeptLink empDeptLink:result.getEmpDeptLinks()){
					if(empDeptLink.getDeptInfo()!=null){
						deptIds+=empDeptLink.getDeptInfo().getDeptId()+",";
						deptNames+=empDeptLink.getDeptInfo().getDeptName()+",";
					}
				}
				if(StringUtil.isNotNull(deptIds)){
					deptIds = deptIds.substring(0,deptIds.length()-1);
					deptNames = deptNames.substring(0,deptNames.length()-1);
				}
				request.setAttribute("deptIds", deptIds);
				request.setAttribute("deptNames", deptNames);
			}
			
			//查询字典在职状态
			List<KeyValue> jobStatus = dictInfoBiz.selectDictChidren(DictConstant.DICT_JOB_STATUS);
			request.setAttribute("jobStatus", jobStatus);
			
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "organize/emps/empEdit";
	}
	
	/**
	 * 查看员工信息页面
	 */
	@RequestMapping(value = "/manage/empViewPage")
	public String empViewPage(String empId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<EmpInfo> resultEntity = empInfoBiz.empInfoByEmpId(empId);
			EmpInfo empInfo = resultEntity.getResult();
			if(StringUtil.isNotNull(empInfo.getJobStatus())){
				empInfo.setJobStatus(dictInfoBiz.getDictCacheValue(DictConstant.DICT_JOB_STATUS, empInfo.getJobStatus()));
			}
			request.setAttribute("empInfo", empInfo);
			
			String deptIds = "";
			String deptNames = "";
			if(empInfo.getEmpDeptLinks()!=null && empInfo.getEmpDeptLinks().size()>0){
				for(EmpDeptLink empDeptLink:empInfo.getEmpDeptLinks()){
					if(empDeptLink.getDeptInfo()!=null){
						deptIds+=empDeptLink.getDeptInfo().getDeptId()+",";
						deptNames+=empDeptLink.getDeptInfo().getDeptName()+",";
					}
				}
				if(StringUtil.isNotNull(deptIds)){
					deptIds = deptIds.substring(0,deptIds.length()-1);
					deptNames = deptNames.substring(0,deptNames.length()-1);
				}
				request.setAttribute("deptIds", deptIds);
				request.setAttribute("deptNames", deptNames);
			}
			
			
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "organize/emps/empView";
	}

	/**
	 * 查询EmpInfo集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/empInfoList", produces = "application/json;charset=UTF-8")
	public String empInfoList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<EmpInfo>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = empInfoBiz.empInfoList(map);
			
			if(resultEntity.getResult()!=null && resultEntity.getResult().size()>0){
				for(EmpInfo empInfo:resultEntity.getResult()){
					if(StringUtil.isNotNull(empInfo.getJobStatus())){
						empInfo.setJobStatus(dictInfoBiz.getDictCacheValue(DictConstant.DICT_JOB_STATUS, empInfo.getJobStatus()));
					}
				}
			}
			
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<EmpInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} catch (ValidateException e) {
			resultEntity = new ResultEntity<List<EmpInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据empId查询EmpInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/empInfoByEmpId", produces = "application/json;charset=UTF-8")
	public String empInfoByEmpId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<EmpInfo> resultEntity = null;
		try {
			String empId = request.getParameter("empId");
			resultEntity = empInfoBiz.empInfoByEmpId(empId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<EmpInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增EmpInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/addEmpInfo", produces = "application/json;charset=UTF-8")
	public String addEmpInfo(EmpInfo empInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<EmpInfo> resultEntity = null;
		try {
			String [] deptIds = request.getParameterValues("deptIds[]");
			resultEntity = empInfoBiz.insertEmpInfo(empInfo,deptIds);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<EmpInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}
	
	@ResponseBody
	@RequestMapping(value = "/manage/addEmpSysUsers", produces = "application/json;charset=UTF-8")
	public String addEmpSysUsers(SysUsers sysUsers,String roleIds,String empId, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<Object> resultEntity = null;
		try {
			ResultEntity<SysUsers> entity = sysUsersBiz.insertSysUsers(sysUsers,roleIds);
			if(ConstantUtil.CODE_200.equals(entity.getError_code())){
				//修改员工用户ID
				EmpInfo empInfo = new EmpInfo();
				empInfo.setEmpId(empId);
				empInfo.setUserId(sysUsers.getUserId());
				ResultEntity<EmpInfo> result = empInfoBiz.updateEmpInfo(empInfo);
				resultEntity = new ResultEntity<Object>(result.getError_code(),result.getMsg());
			}else{
				resultEntity = new ResultEntity<Object>(entity.getError_code(),entity.getMsg());
			}
		} catch (DaoException e) {
			resultEntity = new ResultEntity<Object>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 修改EmpInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/updateEmpInfo", produces = "application/json;charset=UTF-8")
	public String updateEmpInfo(EmpInfo empInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<EmpInfo> resultEntity = null;
		try {
			String [] deptIds = request.getParameterValues("deptIds[]");
			resultEntity = empInfoBiz.updateEmpInfo(empInfo,deptIds);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<EmpInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 删除EmpInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/deleteEmpInfo", produces = "application/json;charset=UTF-8")
	public String deleteEmpInfo(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<EmpInfo> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = empInfoBiz.deleteEmpInfo(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<EmpInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

}