package com.logicalthinking.manage.system.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logicalthinking.manage.system.entity.EmpStudentLink;
import com.logicalthinking.manage.system.biz.EmpStudentLinkBiz;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 员工学员关联信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Controller
public class EmpStudentLinkController extends BaseController {

	@Resource
	private EmpStudentLinkBiz empStudentLinkBiz;

	/**
	 * 查询EmpStudentLink集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/empStudentLinkList", produces = "application/json;charset=UTF-8")
	public String empStudentLinkList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<EmpStudentLink>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = empStudentLinkBiz.empStudentLinkList(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<EmpStudentLink>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} catch (ValidateException e) {
			resultEntity = new ResultEntity<List<EmpStudentLink>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据linkId查询EmpStudentLink
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/empStudentLinkByLinkId", produces = "application/json;charset=UTF-8")
	public String empStudentLinkByLinkId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<EmpStudentLink> resultEntity = null;
		try {
			String linkId = request.getParameter("linkId");
			resultEntity = empStudentLinkBiz.empStudentLinkByLinkId(linkId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<EmpStudentLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增EmpStudentLink
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/addEmpStudentLink", produces = "application/json;charset=UTF-8")
	public String addEmpStudentLink(EmpStudentLink empStudentLink, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<EmpStudentLink> resultEntity = null;
		try {
			resultEntity = empStudentLinkBiz.insertEmpStudentLink(empStudentLink);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<EmpStudentLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 修改EmpStudentLink
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/updateEmpStudentLink", produces = "application/json;charset=UTF-8")
	public String updateEmpStudentLink(EmpStudentLink empStudentLink, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<EmpStudentLink> resultEntity = null;
		try {
			resultEntity = empStudentLinkBiz.updateEmpStudentLink(empStudentLink);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<EmpStudentLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 删除EmpStudentLink
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/deleteEmpStudentLink", produces = "application/json;charset=UTF-8")
	public String deleteEmpStudentLink(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<EmpStudentLink> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = empStudentLinkBiz.deleteEmpStudentLink(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<EmpStudentLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

}