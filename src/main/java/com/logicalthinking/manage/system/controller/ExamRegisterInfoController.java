package com.logicalthinking.manage.system.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logicalthinking.manage.system.entity.ExamRegisterInfo;
import com.logicalthinking.manage.system.biz.ExamRegisterInfoBiz;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 报考信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Controller
public class ExamRegisterInfoController extends BaseController {

	@Resource
	private ExamRegisterInfoBiz examRegisterInfoBiz;
	
	/**
	 * 报考记录列表页面
	 */
	@RequestMapping(value = "/manage/examRegistListPage")
	public String examRegistListPage(HttpServletRequest request, HttpServletResponse response) {
		return "students/examRegist/examRegistList";
	}
	
	/**
	 * 新增报考记录页面
	 */
	@RequestMapping(value = "/manage/examRegistAddPage")
	public String examRegistAddPage(HttpServletRequest request, HttpServletResponse response) {
		return "students/examRegist/examRegistAdd";
	}
	
	/**
	 * 修改报考记录页面
	 */
	@RequestMapping(value = "/manage/examRegistEditPage")
	public String examRegistEditPage(HttpServletRequest request, HttpServletResponse response) {
		return "students/examRegist/examRegistEdit";
	}
	
	/**
	 * 查看报考记录页面
	 */
	@RequestMapping(value = "/manage/examRegistViewPage")
	public String examRegistViewPage(HttpServletRequest request, HttpServletResponse response) {
		return "students/examRegist/examRegistView";
	}
	

	/**
	 * 查询ExamRegisterInfo集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/examRegisterInfoList", produces = "application/json;charset=UTF-8")
	public String examRegisterInfoList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<ExamRegisterInfo>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = examRegisterInfoBiz.examRegisterInfoList(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<ExamRegisterInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} catch (ValidateException e) {
			resultEntity = new ResultEntity<List<ExamRegisterInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据regId查询ExamRegisterInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/examRegisterInfoByRegId", produces = "application/json;charset=UTF-8")
	public String examRegisterInfoByRegId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<ExamRegisterInfo> resultEntity = null;
		try {
			String regId = request.getParameter("regId");
			resultEntity = examRegisterInfoBiz.examRegisterInfoByRegId(regId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<ExamRegisterInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增ExamRegisterInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/addExamRegisterInfo", produces = "application/json;charset=UTF-8")
	public String addExamRegisterInfo(ExamRegisterInfo examRegisterInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<ExamRegisterInfo> resultEntity = null;
		try {
			resultEntity = examRegisterInfoBiz.insertExamRegisterInfo(examRegisterInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<ExamRegisterInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 修改ExamRegisterInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/updateExamRegisterInfo", produces = "application/json;charset=UTF-8")
	public String updateExamRegisterInfo(ExamRegisterInfo examRegisterInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<ExamRegisterInfo> resultEntity = null;
		try {
			resultEntity = examRegisterInfoBiz.updateExamRegisterInfo(examRegisterInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<ExamRegisterInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 删除ExamRegisterInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/deleteExamRegisterInfo", produces = "application/json;charset=UTF-8")
	public String deleteExamRegisterInfo(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<ExamRegisterInfo> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = examRegisterInfoBiz.deleteExamRegisterInfo(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<ExamRegisterInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

}