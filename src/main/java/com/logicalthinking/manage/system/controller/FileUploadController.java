package com.logicalthinking.manage.system.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.alibaba.fastjson.JSONObject;
import com.logicalthinking.manage.system.utils.ConfigProperties;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 文件上传控制器
 * @author lanping
 * @version 1.0
 * @date 2019-10-16 下午6:28:34
 */
@Controller
public class FileUploadController extends BaseController{
	
	// 上传文件
	@ResponseBody
	@RequestMapping(value = "/manage/uploadImage", produces = "application/json;charset=UTF-8")
	public String uploadImage(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		JSONObject jsonObject = new JSONObject();
		try {
			//wshedu-system-datas/images
			String savePath = ConfigProperties.getInstance().getProperty("ImageFileURL");
			String fileServerURL = ConfigProperties.getInstance().getProperty("FileServerURL");
			
			String localFileServerURL = ConfigProperties.getInstance().getProperty("localFileServerURL");
			String path =localFileServerURL+savePath.replace("/", "\\\\");
			File pathFile = new File(path);
			if (!pathFile.exists()) {
				pathFile.mkdirs();
			}
			// 创建一个通用的多部分解析器
			CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
					request.getSession().getServletContext());
			// 判断 request 是否有文件上传,即多部分请求
			if (multipartResolver.isMultipart(request)) {
				// 转换成多部分request
				MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
				// 取得request中的所有文件名
				Iterator<String> iter = multiRequest.getFileNames();
				while (iter.hasNext()) {
					// 取得上传文件
					MultipartFile file = multiRequest.getFile(iter.next());
					if (file != null) {
						// 取得当前上传文件的文件名称
						String myFileName = file.getOriginalFilename();
						// 如果名称不为“”,说明该文件存在，否则说明该文件不存在
						if (myFileName.trim() != "") {

							// 拿到输出流，同时重命名上传的文件
							String suffix = file.getOriginalFilename()
									.substring(
											file.getOriginalFilename()
													.lastIndexOf("."));
							if(suffix.toLowerCase().equals(".png")
									|| suffix.toLowerCase().equals(".jpg")){
								
								String fileName = UUID.randomUUID().toString() + suffix;
	
								String filepath = path + "/" + fileName;
	
								File localFile = new File(filepath);
	
								try {
									file.transferTo(localFile);
								} catch (IllegalStateException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								}
								JSONObject data = new JSONObject();
								data.put("src", fileServerURL+savePath + "/"+ fileName);
								data.put("suffx_src", "../"+savePath + "/"+ fileName);
								data.put("title", fileName);
								jsonObject.put("data", data);
								jsonObject.put("code", 0);
								
							}else{
								jsonObject.put("code", 1);
								jsonObject.put("msg", "图片只允许上传jpg、png格式");
							}
						}
					}
				}
			}else{
				jsonObject.put("code", 1);
				jsonObject.put("msg", "上传文件异常");
			}
		} catch (Exception e) {
			e.printStackTrace();
			jsonObject.put("code", 1);
			jsonObject.put("msg", "上传文件异常");
		}
		return jsonObject.toString();
	}
	
	// 上传文件
	@ResponseBody
	@RequestMapping(value = "/manage/fileUpload", produces = "application/json;charset=UTF-8")
	public String uploadFile(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		ResultEntity<JSONObject> resultEntity =null;
		JSONObject jsonObject = new JSONObject();
		try {
			String savePath = ConfigProperties.getInstance().getProperty("ImageFileURL");
			String fileServerURL = ConfigProperties.getInstance().getProperty("FileServerURL");
			String localFileServerURL = ConfigProperties.getInstance().getProperty("localFileServerURL");
			String path =localFileServerURL+savePath;
			File pathFile = new File(path);
			if (!pathFile.exists()) {
				pathFile.mkdirs();
			}
			// 创建一个通用的多部分解析器
			CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
					request.getSession().getServletContext());
			// 判断 request 是否有文件上传,即多部分请求
			if (multipartResolver.isMultipart(request)) {
				// 转换成多部分request
				MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
				// 取得request中的所有文件名
				Iterator<String> iter = multiRequest.getFileNames();
				while (iter.hasNext()) {
					// 取得上传文件
					MultipartFile file = multiRequest.getFile(iter.next());
					if (file != null) {
						// 取得当前上传文件的文件名称
						String myFileName = file.getOriginalFilename();
						// 如果名称不为“”,说明该文件存在，否则说明该文件不存在
						if (myFileName.trim() != "") {

							// 拿到输出流，同时重命名上传的文件
							String suffix = file.getOriginalFilename()
									.substring(
											file.getOriginalFilename()
													.lastIndexOf("."));
							if(suffix.toLowerCase().equals(".png")
									|| suffix.toLowerCase().equals(".jpg")){
								
								String fileName = new Date().getTime() + suffix;
	
								String filepath = path + "/" + fileName;
	
								File localFile = new File(filepath);
	
								try {
									file.transferTo(localFile);
								} catch (IllegalStateException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								}
								jsonObject.put("filePath", savePath+"/"+fileName);
								jsonObject.put("fullFilePath", fileServerURL+savePath + "/"+ fileName);
							}else{
								resultEntity = new ResultEntity<JSONObject>(ConstantUtil.CODE_404, "图片只允许上传jpg、png格式");
							}
						}
					}
				}
			}else{
				resultEntity = new ResultEntity<JSONObject>(ConstantUtil.CODE_500, "上传文件异常");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultEntity =  new ResultEntity<JSONObject>(ConstantUtil.CODE_500, "上传文件异常");
		}
		return resultEntity.toString();
	}

	// 文件上传后删除文件
	@ResponseBody
	@RequestMapping(value = "/manage/deleteFile", produces = "application/json;charset=UTF-8")
	public String deleteFile(HttpServletRequest request,HttpServletResponse response) {
		ResultEntity<JSONObject> resultEntity =null;
		String fpath = request.getParameter("filepath");
		if (StringUtils.isNotBlank(fpath)) {
			String savePath = ConfigProperties.getInstance().getProperty("ImageFileURL");
			String fileServerURL = ConfigProperties.getInstance().getProperty("FileServerURL");
			String path = fileServerURL+savePath;
			// 通过文件路径获得File对象
			String filepath = "";
			if (fpath.indexOf("/") != -1) {
				filepath = path + fpath.substring(fpath.lastIndexOf("/"));
			} else {
				filepath = path + fpath;
			}

			File file = new File(filepath);
			if (file.exists() && file.isFile()) {
				file.delete();
				resultEntity = new ResultEntity<JSONObject>(ConstantUtil.CODE_200, "删除成功");
			} else {
				resultEntity = new ResultEntity<JSONObject>(ConstantUtil.CODE_404, "文件找不到");
			}
		} else {
			resultEntity = new ResultEntity<JSONObject>(ConstantUtil.CODE_404, "参数错误");
		}
		return resultEntity.toString();
	}

	// 文件下载
	@RequestMapping(value = "/manage/fileDownload")
	public void fileload(HttpServletRequest request,
			HttpServletResponse response) {
		String fpath = request.getParameter("fpath");
		String fName = request.getParameter("fName");
		if (StringUtils.isNotBlank(fpath)) {
			String fileServerURL = ConfigProperties.getInstance().getProperty("FileServerURL");
			String path = fileServerURL+fpath;
			// 通过文件路径获得File对象
			File file = new File(path + File.separator + fName);
			if (file.exists()) {
				try {
					InputStream fis = new BufferedInputStream(
							new FileInputStream(file));
					byte[] buffer = new byte[fis.available()];
					fis.read(buffer);
					fis.close();
					response.reset();
					// 先去掉文件名称中的空格,然后转换编码格式为utf-8,保证不出现乱码,这个文件名称用于浏览器的下载框中自动显示的文件名
					response.addHeader(
							"Content-Disposition",
							"attachment;filename="
									+ new String(fName.getBytes("utf-8"),
											"iso8859-1"));
					response.addHeader("Content-Length", "" + file.length());
					OutputStream os = new BufferedOutputStream(
							response.getOutputStream());
					response.setContentType("application/octet-stream");
					os.write(buffer);// 输出文件
					os.flush();
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// 下载模板文件 读取文件服务器文件
	@RequestMapping(value = "/manage/download/file")
	public void download(HttpServletRequest request,
			HttpServletResponse response) {
		String filetype = request.getParameter("filetype");
		String fpath = ConfigProperties.getInstance().getProperty("LinePriceFileURL");
		if (StringUtils.isNotBlank(fpath)) {
			if (StringUtils.isNotBlank(filetype)) {
				if ("1".equals(filetype)) {
					fpath = ConfigProperties.getInstance()
							.getProperty("LinePriceFileURL");
				} else {
					fpath = ConfigProperties.getInstance().getProperty(
							"transportOrderFileURL");
				}
			}
			String fileServerURL = ConfigProperties.getInstance().getProperty("FileServerURL");
			String path = fileServerURL+fpath;
			try {
				URL urlfile = new URL(path);  
				String fileName=urlfile.getFile().substring(urlfile.getFile().lastIndexOf("/")+1);
				HttpURLConnection uc = (HttpURLConnection) urlfile.openConnection(); 
				uc.setDoInput(true);//设置是否要从 URL 连接读取数据,默认为true
				uc.connect();
				int filelength=uc.getContentLength();
				InputStream fis = new BufferedInputStream(urlfile.openStream());
				byte[] buffer = new byte[fis.available()];
				int length=-1;
				response.reset();
				// 先去掉文件名称中的空格,然后转换编码格式为utf-8,保证不出现乱码,这个文件名称用于浏览器的下载框中自动显示的文件名
				response.addHeader(
						"Content-Disposition",
						"attachment;filename="
								+ new String(fileName.getBytes(
										"utf-8"), "iso8859-1"));
				response.addHeader("Content-Length", "" + filelength);
				response.setContentType("application/octet-stream");
				OutputStream os = new BufferedOutputStream(
						response.getOutputStream());
				while((length=fis.read(buffer))!=-1){
					os.write(buffer,0,length);// 输出文件
				}
				os.flush();
				fis.close();
				os.close();
			} catch (FileNotFoundException e) {
				String url = "transport_price";
				if (StringUtils.isNotBlank(filetype)) {
					if ("1".equals(filetype)) {
						url = "transport_price";
					} else {
						url = "list";
					}
				}
				String data = "<script>alert('文件模板不存在');location.href='../"
						+ url + "'</script>";
				output(response, data, "text/html;charset=UTF-8");
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
	}
	
	
	// 下载模板文件 读取项目服务器文件
	@RequestMapping(value = "/manage/download")
	public void downloadFile(HttpServletRequest request,
			HttpServletResponse response) {
		String filetype = request.getParameter("filetype");
		String fpath = ConfigProperties.getInstance().getProperty("LinePriceFileURL");
		if (StringUtils.isNotBlank(fpath)) {
			if (StringUtils.isNotBlank(filetype)) {
				if ("1".equals(filetype)) {
					fpath = ConfigProperties.getInstance()
							.getProperty("LinePriceFileURL");
				} else {
					fpath = ConfigProperties.getInstance().getProperty(
							"transportOrderFileURL");
				}
			}
			String path = request.getSession().getServletContext()
					.getRealPath(fpath);
			// 通过文件路径获得File对象
			File file = new File(path);
			if (file.exists()) {
				try {
					InputStream fis = new BufferedInputStream(new FileInputStream(file));
					byte[] buffer = new byte[fis.available()];
					int length=-1;
					response.reset();
					// 先去掉文件名称中的空格,然后转换编码格式为utf-8,保证不出现乱码,这个文件名称用于浏览器的下载框中自动显示的文件名
					response.addHeader(
							"Content-Disposition",
							"attachment;filename="
									+ new String(file.getName().getBytes(
											"utf-8"), "iso8859-1"));
					response.addHeader("Content-Length", "" + file.length());
					response.setContentType("application/octet-stream");
					OutputStream os = new BufferedOutputStream(
							response.getOutputStream());
					while((length=fis.read(buffer))!=-1){
						os.write(buffer,0,length);// 输出文件
					}
					os.flush();
					fis.close();
					os.close();
				}catch (IOException e) {
					e.printStackTrace();
				} 
			} else {
				String url = "transport_price";
				if (StringUtils.isNotBlank(filetype)) {
					if ("1".equals(filetype)) {
						url = "transport_price";
					} else {
						url = "list";
					}
				}
				String data = "<script>alert('文件模板不存在');location.href='../"
						+ url + "'</script>";
				output(response, data, "text/html;charset=UTF-8");
			}
		}
	}
	
	/**
	 * 
	 * @param response
	 * @param data
	 * @param contentType  
	 * json格式： application/json;charset=UTF-8
	 * html：text/html;charset=UTF-8
	 */
	public void output(HttpServletResponse response,String data,String contentType){
		try {
			response.setCharacterEncoding("utf-8");
			response.setContentType(contentType);
			PrintWriter out=response.getWriter();
			out.print(data);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
