package com.logicalthinking.manage.system.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logicalthinking.manage.system.entity.GroupInfo;
import com.logicalthinking.manage.system.biz.GroupInfoBiz;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 分组信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Controller
public class GroupInfoController extends BaseController {

	@Resource
	private GroupInfoBiz groupInfoBiz;

	/**
	 * 查询GroupInfo集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/groupInfoList", produces = "application/json;charset=UTF-8")
	public String groupInfoList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<GroupInfo>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = groupInfoBiz.groupInfoList(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<GroupInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} catch (ValidateException e) {
			resultEntity = new ResultEntity<List<GroupInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据groupId查询GroupInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/groupInfoByGroupId", produces = "application/json;charset=UTF-8")
	public String groupInfoByGroupId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<GroupInfo> resultEntity = null;
		try {
			String groupId = request.getParameter("groupId");
			resultEntity = groupInfoBiz.groupInfoByGroupId(groupId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<GroupInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增GroupInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/addGroupInfo", produces = "application/json;charset=UTF-8")
	public String addGroupInfo(GroupInfo groupInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<GroupInfo> resultEntity = null;
		try {
			resultEntity = groupInfoBiz.insertGroupInfo(groupInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<GroupInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 修改GroupInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/updateGroupInfo", produces = "application/json;charset=UTF-8")
	public String updateGroupInfo(GroupInfo groupInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<GroupInfo> resultEntity = null;
		try {
			resultEntity = groupInfoBiz.updateGroupInfo(groupInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<GroupInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 删除GroupInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/deleteGroupInfo", produces = "application/json;charset=UTF-8")
	public String deleteGroupInfo(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<GroupInfo> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = groupInfoBiz.deleteGroupInfo(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<GroupInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

}