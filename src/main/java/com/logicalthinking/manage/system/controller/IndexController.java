package com.logicalthinking.manage.system.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.logicalthinking.manage.system.biz.ArticleInfoBiz;
import com.logicalthinking.manage.system.biz.WebImgInfoBiz;
import com.logicalthinking.manage.system.entity.ArticleInfo;
import com.logicalthinking.manage.system.entity.WebImgInfo;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 门户首页
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Controller
public class IndexController extends BaseController {

	@Autowired
	private WebImgInfoBiz webImgInfoBiz;
	
	@Resource
	private ArticleInfoBiz articleInfoBiz;
	
	/**
	 * 获取web图片
	 */
	@ResponseBody
	@RequestMapping(value = "/getWebImgs", produces = "application/json;charset=UTF-8")
	public String getWebImgs(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<JSONObject> resultEntity = null;
		JSONObject jsonObject = new JSONObject();
		try {
			String [] imgTypes= request.getParameterValues("imgTypes[]");
			if(imgTypes!=null && imgTypes.length>0){
				for(int i=0;i<imgTypes.length;i++){
					List<WebImgInfo> webImgInfos = webImgInfoBiz.selectTypeImages(imgTypes[i]);
					jsonObject.put(imgTypes[i], webImgInfos);
				}
			}
			resultEntity = new ResultEntity<JSONObject>(ConstantUtil.CODE_200,jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
			resultEntity = new ResultEntity<JSONObject>(ConstantUtil.CODE_500,e.getMessage());
		}
		return resultEntity.toString();
	}
	
	/**
	 * 首页
	 */
	@RequestMapping(value = "/index.html")
	public String index(HttpServletRequest request, HttpServletResponse response) {
		try {
			baseImg(request);
			
			//首页标题背景
			String imgType = "bg_index_title";	
			WebImgInfo titlebg = webImgInfoBiz.selectBackgroundImage(imgType);
			request.setAttribute("titlebg", titlebg);
			
			//学习系统小图标learn_icon
			String iconType = "learn_icon";
			List<WebImgInfo> iconImgs = webImgInfoBiz.selectTypeImages(iconType);
			request.setAttribute("iconImgs",iconImgs);
			
			//教学模式图解
			/*String modelType = "education_model";	
			WebImgInfo modelbg = webImgInfoBiz.selectBackgroundImage(modelType);
			request.setAttribute("modelbg", modelbg);*/
			
			/*//标题小图标
			String titleIconType = "title_icon";
			List<WebImgInfo> titleIconImgs = webImgInfoBiz.selectTypeImages(titleIconType);
			request.setAttribute("titleIconImgs",titleIconImgs);
			
			//联系我们
			String contactImgType = "contact_us";
			List<WebImgInfo> contactImgs = webImgInfoBiz.selectTypeImages(contactImgType);
			request.setAttribute("contactImgs",contactImgs);*/
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "front/index";
	}
	
	/**
	 * 关于我们
	 */
	@RequestMapping(value = "/aboutUs.html")
	public String aboutUs(HttpServletRequest request, HttpServletResponse response) {
		try {
			baseImg(request);
			
			//关于我们标题背景
			String imgType = "bg_aboutus_title";
			WebImgInfo titlebg = webImgInfoBiz.selectBackgroundImage(imgType);
			request.setAttribute("titlebg", titlebg);
			
			//文案
			String articleType = "about_us";
			ArticleInfo aboutArticle = articleInfoBiz.selectArticleInfoByType(articleType);
			request.setAttribute("aboutArticle", aboutArticle);
			
			//文案图片
			String articleImgType = "article_img";
			WebImgInfo articleImg = webImgInfoBiz.selectBackgroundImage(articleImgType);
			request.setAttribute("articleImg", articleImg);
			
			/*//人工智能测评
			String evaluateImgType = "evaluate_img";
			WebImgInfo evaluateImg = webImgInfoBiz.selectBackgroundImage(evaluateImgType);
			request.setAttribute("evaluateImg", evaluateImg);
			
			//智能教学系统
			String learnSystemImgType = "learn_system";
			WebImgInfo learnSystemImg = webImgInfoBiz.selectBackgroundImage(learnSystemImgType);
			request.setAttribute("learnSystemImg", learnSystemImg);
			
			//专属课程内容
			String courseContentImgType = "course_content_img";
			WebImgInfo courseContentImg = webImgInfoBiz.selectBackgroundImage(courseContentImgType);
			request.setAttribute("courseContentImg", courseContentImg);
			
			//专属课程内容
			String dataAnalysisImgType = "data_analysis_img";
			WebImgInfo dataAnalysisImg = webImgInfoBiz.selectBackgroundImage(dataAnalysisImgType);
			request.setAttribute("dataAnalysisImg", dataAnalysisImg);*/
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "front/aboutUs";
	}
	
	/**
	 * 师资力量
	 */
	@RequestMapping(value = "/teachers.html")
	public String teachers(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			baseImg(request);
			
			//师资力量标题背景
			String imgType = "bg_teachers_title";	
			WebImgInfo titlebg = webImgInfoBiz.selectBackgroundImage(imgType);
			request.setAttribute("titlebg", titlebg);
			
			
			//教师描述背景
			String teacherType = "teacher_bg";	
			WebImgInfo teacherbg = webImgInfoBiz.selectBackgroundImage(teacherType);
			request.setAttribute("teacherbg", teacherbg);
			
			String teacherPoseType = "teacher_pose_img";
			List<WebImgInfo> teacherImgs = webImgInfoBiz.selectTypeImages(teacherPoseType);
			request.setAttribute("teacherImgs",teacherImgs);
			
			//查询教师简介文案
			String articleType = "teacher_introduction";
			List<ArticleInfo> teacherArticles = articleInfoBiz.selectArticleListByType(articleType);
			request.setAttribute("teacherArticles", teacherArticles);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "front/teachers";
	}
	
	/**
	 * 课程中心
	 */
	@RequestMapping(value = "/courses.html")
	public String courses(HttpServletRequest request, HttpServletResponse response) {
		try {
			baseImg(request);
			
			//课程中心标题背景
			String imgType = "bg_subjects_title";	
			WebImgInfo titlebg = webImgInfoBiz.selectBackgroundImage(imgType);
			request.setAttribute("titlebg", titlebg);
			
			
			//教学团队背景
			String teamType = "course_team_bg";	
			WebImgInfo teambg = webImgInfoBiz.selectBackgroundImage(teamType);
			request.setAttribute("teambg", teambg);
			
			//课程优点小图标
			String courseItemType = "course_item_icon";	
			List<WebImgInfo> courseItemIcons = webImgInfoBiz.selectTypeImages(courseItemType);
			request.setAttribute("courseItemIcons", courseItemIcons);
			
			//学科描述图
			String subjectDescType = "subject_desc_bg";	
			List<WebImgInfo> subjectDescImgs = webImgInfoBiz.selectTypeImages(subjectDescType);
			request.setAttribute("subjectDescImgs", subjectDescImgs);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
				
		return "front/courses";
	}
	
	/**
	 * 校区分布
	 */
	@RequestMapping(value = "/schools.html")
	public String schools(HttpServletRequest request, HttpServletResponse response) {
		try {
			baseImg(request);
			
			//校区分布标题背景
			String imgType = "bg_schools_title";	
			WebImgInfo titlebg = webImgInfoBiz.selectBackgroundImage(imgType);
			request.setAttribute("titlebg", titlebg);
			
			//学习中心分布背景
			String learnType = "learn_center";	
			WebImgInfo learnbg = webImgInfoBiz.selectBackgroundImage(learnType);
			request.setAttribute("learnbg", learnbg);
			
			//校区展示图
			String schoolimgType = "school_imgs";	
			List<WebImgInfo> schoolImgs = webImgInfoBiz.selectTypeImages(schoolimgType);
			request.setAttribute("schoolImgs", schoolImgs);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "front/schools";
	}
	
	private void baseImg(HttpServletRequest request) throws DaoException{
		//logo
		String logoType = "header_logo";	
		WebImgInfo logo = webImgInfoBiz.selectBackgroundImage(logoType);
		request.setAttribute("logo", logo);
		
		//底部二维码
		String qrType = "footer_qr_img";
		WebImgInfo footerqr = webImgInfoBiz.selectBackgroundImage(qrType);
		request.setAttribute("footerqr", footerqr);
	}
}