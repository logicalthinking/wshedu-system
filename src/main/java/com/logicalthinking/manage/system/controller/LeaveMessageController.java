package com.logicalthinking.manage.system.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logicalthinking.manage.system.annotation.SysLog;
import com.logicalthinking.manage.system.biz.LeaveMessageBiz;
import com.logicalthinking.manage.system.entity.LeaveMessage;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.utils.ValidateException;

/**
 * 留言信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Controller
public class LeaveMessageController extends BaseController {

	@Resource
	private LeaveMessageBiz leaveMessageBiz;

	/**
	 * 留言列表页面
	 */
	@RequestMapping(value = "/manage/leaveMessageListPage")
	public String leaveMessageListPage(HttpServletRequest request, HttpServletResponse response) {
		return "website/leaveMessage/leaveMessageList";
	}
	
	/**
	 * 新增留言页面
	 */
	@RequestMapping(value = "/leaveMessageAdd")
	public String leaveMessageAdd(HttpServletRequest request, HttpServletResponse response) {
		return "website/leaveMessage/leaveMessageAdd";
	}
	
	/**
	 * 查看留言页面
	 */
	@RequestMapping(value = "/manage/leaveMessageViewPage")
	public String leaveMessageViewPage(String msgId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<LeaveMessage> resultEntity = leaveMessageBiz.leaveMessageByMsgId(msgId);
			request.setAttribute("leaveMessage", resultEntity.getResult());
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "website/leaveMessage/leaveMessageView";
	}
	
	/**
	 * 查询LeaveMessage集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/leaveMessageList", produces = "application/json;charset=UTF-8")
	public String leaveMessageList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<LeaveMessage>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = leaveMessageBiz.leaveMessageList(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<LeaveMessage>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} catch (ValidateException e) {
			resultEntity = new ResultEntity<List<LeaveMessage>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据msgId查询LeaveMessage
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/leaveMessageByMsgId", produces = "application/json;charset=UTF-8")
	public String leaveMessageByMsgId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<LeaveMessage> resultEntity = null;
		try {
			String msgId = request.getParameter("msgId");
			resultEntity = leaveMessageBiz.leaveMessageByMsgId(msgId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<LeaveMessage>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增LeaveMessage
	 */
	@ResponseBody
	@SysLog(operateType="新增留言")
	@RequestMapping(value = "/addLeaveMessage", produces = "application/json;charset=UTF-8")
	public String addLeaveMessage(LeaveMessage leaveMessage, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<LeaveMessage> resultEntity = null;
		try {
			resultEntity = leaveMessageBiz.insertLeaveMessage(leaveMessage);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<LeaveMessage>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

	/**
	 * 修改LeaveMessage
	 */
	@ResponseBody
	@SysLog(operateType="修改留言")
	@RequestMapping(value = "/manage/updateLeaveMessage", produces = "application/json;charset=UTF-8")
	public String updateLeaveMessage(LeaveMessage leaveMessage, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<LeaveMessage> resultEntity = null;
		try {
			resultEntity = leaveMessageBiz.updateLeaveMessage(leaveMessage);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<LeaveMessage>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

	/**
	 * 删除LeaveMessage
	 */
	@ResponseBody
	@SysLog(operateType="删除留言")
	@RequestMapping(value = "/manage/deleteLeaveMessage", produces = "application/json;charset=UTF-8")
	public String deleteLeaveMessage(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<LeaveMessage> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = leaveMessageBiz.deleteLeaveMessage(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<LeaveMessage>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

}