package com.logicalthinking.manage.system.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logicalthinking.manage.system.entity.StudentInfo;
import com.logicalthinking.manage.system.biz.StudentInfoBiz;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 学员信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Controller
public class StudentInfoController extends BaseController {

	@Resource
	private StudentInfoBiz studentInfoBiz;
	
	/**
	 * 内部学员列表页面
	 */
	@RequestMapping(value = "/manage/innerStudentListPage")
	public String innerStudentListPage(HttpServletRequest request, HttpServletResponse response) {
		return "students/student/innerStudentList";
	}
	
	/**
	 * 意向学员列表页面
	 */
	@RequestMapping(value = "/manage/intentStudentListPage")
	public String intentStudentListPage(HttpServletRequest request, HttpServletResponse response) {
		return "students/student/intentStudentList";
	}
	
	/**
	 * 公共学员列表页面
	 */
	@RequestMapping(value = "/manage/publicStudentListPage")
	public String publicStudentListPage(HttpServletRequest request, HttpServletResponse response) {
		return "students/student/publicStudentList";
	}
	
	/**
	 * 新增学员页面
	 */
	@RequestMapping(value = "/manage/studentAddPage")
	public String studentAddPage(HttpServletRequest request, HttpServletResponse response) {
		return "students/student/studentAdd";
	}
	
	/**
	 * 修改学员页面
	 */
	@RequestMapping(value = "/manage/studentEditPage")
	public String studentEditPage(HttpServletRequest request, HttpServletResponse response) {
		return "students/student/studentEdit";
	}
	
	/**
	 * 查看学员页面
	 */
	@RequestMapping(value = "/manage/studentViewPage")
	public String studentViewPage(HttpServletRequest request, HttpServletResponse response) {
		return "students/student/studentView";
	}

	/**
	 * 查询StudentInfo集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/studentInfoList", produces = "application/json;charset=UTF-8")
	public String studentInfoList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<StudentInfo>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = studentInfoBiz.studentInfoList(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<StudentInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} catch (ValidateException e) {
			resultEntity = new ResultEntity<List<StudentInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据stuId查询StudentInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/studentInfoByStuId", produces = "application/json;charset=UTF-8")
	public String studentInfoByStuId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<StudentInfo> resultEntity = null;
		try {
			String stuId = request.getParameter("stuId");
			resultEntity = studentInfoBiz.studentInfoByStuId(stuId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<StudentInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增StudentInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/addStudentInfo", produces = "application/json;charset=UTF-8")
	public String addStudentInfo(StudentInfo studentInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<StudentInfo> resultEntity = null;
		try {
			resultEntity = studentInfoBiz.insertStudentInfo(studentInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<StudentInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 修改StudentInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/updateStudentInfo", produces = "application/json;charset=UTF-8")
	public String updateStudentInfo(StudentInfo studentInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<StudentInfo> resultEntity = null;
		try {
			resultEntity = studentInfoBiz.updateStudentInfo(studentInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<StudentInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 删除StudentInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/deleteStudentInfo", produces = "application/json;charset=UTF-8")
	public String deleteStudentInfo(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<StudentInfo> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = studentInfoBiz.deleteStudentInfo(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<StudentInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

}