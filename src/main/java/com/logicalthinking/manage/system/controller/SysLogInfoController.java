package com.logicalthinking.manage.system.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logicalthinking.manage.system.biz.SysLogInfoBiz;
import com.logicalthinking.manage.system.entity.SysLogInfo;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.utils.ValidateException;

/**
 * 系统日志信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
@Controller
public class SysLogInfoController extends BaseController {

	@Resource
	private SysLogInfoBiz sysLogInfoBiz;
	
	/**
	 * 查看日志页面
	 */
	@RequestMapping(value = "/manage/sysLogViewPage")
	public String sysLogViewPage(String logId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<SysLogInfo> resultEntity = sysLogInfoBiz.sysLogInfoByLogId(logId);
			request.setAttribute("sysLogInfo", resultEntity.getResult());
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "system/sysLogs/sysLogView";
	}
	
	
	/**
	 * 查看日志页面
	 */
	@RequestMapping(value = "/manage/sysLogListPage")
	public String sysLogListPage(String logId,HttpServletRequest request, HttpServletResponse response) {
		return "system/sysLogs/sysLogList";
	}

	/**
	 * 查询SysLogInfo集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/sysLogInfoList", produces = "application/json;charset=UTF-8")
	public String sysLogInfoList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<SysLogInfo>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = sysLogInfoBiz.sysLogInfoList(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<SysLogInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} catch (ValidateException e) {
			resultEntity = new ResultEntity<List<SysLogInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据logId查询SysLogInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/sysLogInfoByLogId", produces = "application/json;charset=UTF-8")
	public String sysLogInfoByLogId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysLogInfo> resultEntity = null;
		try {
			String logId = request.getParameter("logId");
			resultEntity = sysLogInfoBiz.sysLogInfoByLogId(logId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysLogInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增SysLogInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/addSysLogInfo", produces = "application/json;charset=UTF-8")
	public String addSysLogInfo(SysLogInfo sysLogInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysLogInfo> resultEntity = null;
		try {
			resultEntity = sysLogInfoBiz.insertSysLogInfo(sysLogInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysLogInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 修改SysLogInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/updateSysLogInfo", produces = "application/json;charset=UTF-8")
	public String updateSysLogInfo(SysLogInfo sysLogInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysLogInfo> resultEntity = null;
		try {
			resultEntity = sysLogInfoBiz.updateSysLogInfo(sysLogInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysLogInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 删除SysLogInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/deleteSysLogInfo", produces = "application/json;charset=UTF-8")
	public String deleteSysLogInfo(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysLogInfo> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = sysLogInfoBiz.deleteSysLogInfo(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysLogInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

}