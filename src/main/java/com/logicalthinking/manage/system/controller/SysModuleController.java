package com.logicalthinking.manage.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.logicalthinking.manage.system.annotation.SysLog;
import com.logicalthinking.manage.system.biz.DictInfoBiz;
import com.logicalthinking.manage.system.biz.SysModuleBiz;
import com.logicalthinking.manage.system.biz.SysModuleLinkBiz;
import com.logicalthinking.manage.system.biz.SysRoleBiz;
import com.logicalthinking.manage.system.entity.SysModule;
import com.logicalthinking.manage.system.entity.SysModuleLink;
import com.logicalthinking.manage.system.entity.SysRole;
import com.logicalthinking.manage.system.entity.SysUsers;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.DictConstant;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.utils.StringUtil;
import com.logicalthinking.manage.system.vo.KeyValue;
import com.logicalthinking.manage.system.vo.ModuleTreeVo;

/**
 * 系统模块信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
@Controller
public class SysModuleController extends BaseController {

	@Resource
	private SysModuleBiz sysModuleBiz;
	
	@Resource
	private SysModuleLinkBiz sysModuleLinkBiz;
	
	@Resource
	private SysRoleBiz sysRoleBiz;
	
	@Resource
	private DictInfoBiz dictInfoBiz;
	
	/**
	 * 权限管理页面
	 */
	@RequestMapping(value = "/manage/moduleManagePage")
	public String moduleManagePage(HttpServletRequest request, HttpServletResponse response) {
		return "system/module/moduleManage";
	}
	
	/**
	 * 设置模块页面
	 */
	@RequestMapping(value = "/manage/moduleSetPage")
	public String moduleSetPage(String moduleId,HttpServletRequest request, HttpServletResponse response) {
		String roleId = request.getParameter("roleId");
		if(StringUtil.isNotNull(roleId)){
			try {
				ResultEntity<SysRole> resultEntity = sysRoleBiz.sysRoleByRoleId(roleId);
				request.setAttribute("sysRole", resultEntity.getResult());
				if(ConstantUtil.ROLE_CODE_ADMINISTRATOR.equals(resultEntity.getResult().getRoleCode())){
					request.setAttribute("adminFlag", ConstantUtil.IS_VALID_Y);//系统管理员
				}
			} catch (DaoException e) {
				e.printStackTrace();
			}
		}
		return "system/module/moduleSet";
	}
	
	/**
	 * 模块列表页面
	 */
	@RequestMapping(value = "/manage/moduleListPage")
	public String moduleListPage(HttpServletRequest request, HttpServletResponse response) {
		return "system/module/moduleList";
	}
	
	/**
	 * 树形模块管理页面
	 */
	@RequestMapping(value = "/manage/moduleTreeListPage")
	public String moduleTreeListPage(HttpServletRequest request, HttpServletResponse response) {
		return "system/module/moduleTreeList";
	}
	
	/**
	 * 树形模块查看页面
	 */
	@RequestMapping(value = "/manage/moduleTreeViewPage")
	public String moduleTreeViewPage(String moduleId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<SysModule> resultEntity = sysModuleBiz.sysModuleByModuleId(moduleId);
			request.setAttribute("sysModule", resultEntity.getResult());
			
			List<KeyValue> moduleTypes = dictInfoBiz.selectDictChidren(DictConstant.DICT_MODULE_OP_TYPE);
			request.setAttribute("moduleTypes", moduleTypes);
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "system/module/moduleTreeView";
	}
	
	/**
	 * 模块排序页面
	 */
	@RequestMapping(value = "/manage/moduleSortPage")
	public String moduleSortPage(String parentId,HttpServletRequest request, HttpServletResponse response) {
		try {
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("parentId", parentId);
			map.put("sortName", "rank");
			map.put("sortOrder", "asc");
			map.put("state", ConstantUtil.IS_VALID_Y);
			List<SysModule> sysModules = sysModuleBiz.sysModuleAllList(map);
			request.setAttribute("sysModules", sysModules);
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "system/module/moduleSort";
	}
	
	/**
	 * 查看模块页面
	 */
	@RequestMapping(value = "/manage/moduleViewPage")
	public String moduleViewPage(String moduleId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<SysModule> resultEntity = sysModuleBiz.sysModuleByModuleId(moduleId);
			request.setAttribute("sysModule", resultEntity.getResult());
			
			List<KeyValue> moduleTypes = dictInfoBiz.selectDictChidren(DictConstant.DICT_MODULE_OP_TYPE);
			request.setAttribute("moduleTypes", moduleTypes);
			
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "system/module/moduleView";
	}
	
	/**
	 * 编辑模块页面
	 */
	@RequestMapping(value = "/manage/moduleEditPage")
	public String moduleEditPage(String moduleId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<SysModule> resultEntity = sysModuleBiz.sysModuleByModuleId(moduleId);
			request.setAttribute("sysModule", resultEntity.getResult());
			
			List<KeyValue> moduleTypes = dictInfoBiz.selectDictChidren(DictConstant.DICT_MODULE_OP_TYPE);
			request.setAttribute("moduleTypes", moduleTypes);
			
			String opType = request.getParameter("opType");
			request.setAttribute("opType", opType);
			
			String module = request.getParameter("module");
			request.setAttribute("module", module);
			
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "system/module/moduleEdit";
	}
	
	/**
	 * 新增模块页面
	 */
	@RequestMapping(value = "/manage/moduleAddPage")
	public String moduleAddPage(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<KeyValue> moduleTypes = dictInfoBiz.selectDictChidren(DictConstant.DICT_MODULE_OP_TYPE);
			request.setAttribute("moduleTypes", moduleTypes);
			
			String parentId = request.getParameter("parentId");
			if(StringUtil.isNotNull(parentId)){
				ResultEntity<SysModule> resultEntity = sysModuleBiz.sysModuleByModuleId(parentId);
				SysModule sysModule = resultEntity.getResult();
				if(sysModule!=null){
					request.setAttribute("parentModule",sysModule );
					request.setAttribute("parentId", parentId);
				}
			}else{
				request.setAttribute("parentId", "0");
			}
			request.setAttribute("defaultModuleType", DictConstant.DICT_MODULE_LINK);
			
			String opType = request.getParameter("opType");
			request.setAttribute("opType", opType);
			
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "system/module/moduleAdd";
	}
	
	/**
	 * 功能权限设置  查询SysModule集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/moduleTreeList", produces = "application/json;charset=UTF-8")
	public String moduleTreeList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<JSONObject> resultEntity = null;
		JSONObject jsonObject = new JSONObject();
		try {
			Map<String,Object> map=initRequestMap(request);
			List<ModuleTreeVo> moduleTreeVos = sysModuleBiz.selectModuleTreeList(map);
			
			String roleId = request.getParameter("roleId");
			//是否是系统管理员
			String adminFlag = request.getParameter("adminFlag");
			List<SysModuleLink> sysModuleLinks = sysModuleLinkBiz.selectModuleByRoleList(roleId);
			if(sysModuleLinks!=null && sysModuleLinks.size()>0){
				boolean flag = ConstantUtil.IS_VALID_Y.equals(adminFlag);
				int length = parseModuleTrees(moduleTreeVos, sysModuleLinks,flag);
				if(sysModuleLinks.size() >= length){
					jsonObject.put("checkAll", true);
				}
			}
			jsonObject.put("moduleTreeVos", moduleTreeVos);
			resultEntity = new ResultEntity<JSONObject>(ConstantUtil.CODE_200, jsonObject);
		} catch (Exception e) {
			resultEntity = new ResultEntity<JSONObject>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}
	
	public int parseModuleTrees(List<ModuleTreeVo> moduleTreeVos,List<SysModuleLink> sysModuleLinks,boolean disabled){
		int checkLength = 0;
		if(moduleTreeVos!=null && moduleTreeVos.size()>0){
			for(ModuleTreeVo moduleTreeVo : moduleTreeVos){
				checkLength++;
				for(SysModuleLink moduleLink : sysModuleLinks){
					if(moduleLink.getModuleId().equals(moduleTreeVo.getId())){
						moduleTreeVo.setChecked(true);
						moduleTreeVo.setDisabled(disabled);
					}
				}
				if(moduleTreeVo.getChildren()!=null && moduleTreeVo.getChildren().size()>0){
					moduleTreeVo.setChecked(false);
					checkLength += parseModuleTrees(moduleTreeVo.getChildren(), sysModuleLinks,disabled);
				}
			}
		}
		return checkLength;
	}

	
	/**
	 * 模块管理 查询所有的菜单树形数据集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/moduleAllTreeList", produces = "application/json;charset=UTF-8")
	public String moduleAllTreeList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<ModuleTreeVo>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			List<ModuleTreeVo> moduleTreeVos = sysModuleBiz.selectAllModuleMenuTreeList(map);
			resultEntity = new ResultEntity<List<ModuleTreeVo>>(ConstantUtil.CODE_200, moduleTreeVos);
		} catch (Exception e) {
			resultEntity = new ResultEntity<List<ModuleTreeVo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}
	
	/**
	 * 当前用户 树形菜单
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/userModuleTreeList", produces = "application/json;charset=UTF-8")
	public String userModuleTreeList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<ModuleTreeVo>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			
			SysUsers sysUsers = getSysUsers(request);
			List<ModuleTreeVo> moduleTreeVos = sysModuleBiz.selectUserModuleMenuTreeList(sysUsers,map);
			resultEntity = new ResultEntity<List<ModuleTreeVo>>(ConstantUtil.CODE_200, moduleTreeVos);
		} catch (Exception e) {
			resultEntity = new ResultEntity<List<ModuleTreeVo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}
	
	/**
	 * 清除模块缓存
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/clearModuleCache", produces = "application/json;charset=UTF-8")
	public String clearModuleCache(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<Object> resultEntity = null;
		try {
			sysModuleBiz.clearModulesCache();
			resultEntity = new ResultEntity<Object>(ConstantUtil.CODE_200, "清除成功");
		} catch (Exception e) {
			resultEntity = new ResultEntity<Object>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}
	
	/**
	 * 查询特定页面的用户操作模块
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/userPageModuleList", produces = "application/json;charset=UTF-8")
	public String userPageModuleList(String moduleCode,String parentModuleCode,HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<ModuleTreeVo>> resultEntity = null;
		try {
			SysUsers sysUsers = getSysUsers(request);
			List<ModuleTreeVo> moduleTreeVos = sysModuleBiz.selectModuleByOnePage(sysUsers,moduleCode,parentModuleCode);
			resultEntity = new ResultEntity<List<ModuleTreeVo>>(ConstantUtil.CODE_200, moduleTreeVos);
		} catch (Exception e) {
			resultEntity = new ResultEntity<List<ModuleTreeVo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}
	
	/**
	 * 分页查询SysModule集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/sysModuleList", produces = "application/json;charset=UTF-8")
	public String sysModuleList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<SysModule>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = sysModuleBiz.sysModuleList(map);
		} catch (Exception e) {
			resultEntity = new ResultEntity<List<SysModule>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	
	/**
	 * 分页查询SysModule集合
	 */
	@ResponseBody
	@SysLog(operateType="模块排序")
	@RequestMapping(value = "/manage/sysModuleSort", produces = "application/json;charset=UTF-8")
	public String sysModuleSort(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysModule> resultEntity = null;
		
		try {
			String [] moduleIds = request.getParameterValues("moduleIds[]");
			resultEntity = sysModuleBiz.sortModules(moduleIds);
		} catch (Exception e) {
			resultEntity = new ResultEntity<SysModule>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

	
	/**
	 * 根据moduleId查询SysModule
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/sysModuleByModuleId", produces = "application/json;charset=UTF-8")
	public String sysModuleByModuleId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysModule> resultEntity = null;
		try {
			String moduleId = request.getParameter("moduleId");
			resultEntity = sysModuleBiz.sysModuleByModuleId(moduleId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysModule>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增SysModule
	 */
	@ResponseBody
	@SysLog(operateType="新增模块")
	@RequestMapping(value = "/manage/addSysModule", produces = "application/json;charset=UTF-8")
	public String addSysModule(SysModule sysModule, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysModule> resultEntity = null;
		try {
			resultEntity = sysModuleBiz.insertSysModule(sysModule);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysModule>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

	/**
	 * 修改SysModule
	 */
	@ResponseBody
	@SysLog(operateType="修改模块")
	@RequestMapping(value = "/manage/updateSysModule", produces = "application/json;charset=UTF-8")
	public String updateSysModule(SysModule sysModule, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysModule> resultEntity = null;
		try {
			resultEntity = sysModuleBiz.updateSysModule(sysModule);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysModule>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

	/**
	 * 删除SysModule
	 */
	@ResponseBody
	@SysLog(operateType="删除模块")
	@RequestMapping(value = "/manage/deleteSysModule", produces = "application/json;charset=UTF-8")
	public String deleteSysModule(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysModule> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = sysModuleBiz.deleteSysModule(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysModule>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

}