package com.logicalthinking.manage.system.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logicalthinking.manage.system.annotation.SysLog;
import com.logicalthinking.manage.system.biz.SysModuleLinkBiz;
import com.logicalthinking.manage.system.entity.SysModuleLink;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 模块关联业务信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Controller
public class SysModuleLinkController extends BaseController {

	@Resource
	private SysModuleLinkBiz sysModuleLinkBiz;

	/**
	 * 设置权限
	 */
	@ResponseBody
	@SysLog(operateType="设置权限")
	@RequestMapping(value = "/manage/addSysModuleLinkByBusi", produces = "application/json;charset=UTF-8")
	public String addSysModuleLinkByBusi(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysModuleLink> resultEntity = null;
		try {
			String busiId = request.getParameter("busiId");
			String [] moduleIds = request.getParameterValues("moduleIds[]");
			resultEntity = sysModuleLinkBiz.insertSysModuleLink(busiId,moduleIds);
		} catch (Exception e) {
			resultEntity = new ResultEntity<SysModuleLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}
	
	/**
	 * 查询SysModuleLink集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/sysModuleLinkList", produces = "application/json;charset=UTF-8")
	public String sysModuleLinkList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<SysModuleLink>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = sysModuleLinkBiz.sysModuleLinkList(map);
		} catch (Exception e) {
			resultEntity = new ResultEntity<List<SysModuleLink>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据linkId查询SysModuleLink
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/sysModuleLinkByLinkId", produces = "application/json;charset=UTF-8")
	public String sysModuleLinkByLinkId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysModuleLink> resultEntity = null;
		try {
			String linkId = request.getParameter("linkId");
			resultEntity = sysModuleLinkBiz.sysModuleLinkByLinkId(linkId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysModuleLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增SysModuleLink
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/addSysModuleLink", produces = "application/json;charset=UTF-8")
	public String addSysModuleLink(SysModuleLink sysModuleLink, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysModuleLink> resultEntity = null;
		try {
			resultEntity = sysModuleLinkBiz.insertSysModuleLink(sysModuleLink);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysModuleLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 修改SysModuleLink
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/updateSysModuleLink", produces = "application/json;charset=UTF-8")
	public String updateSysModuleLink(SysModuleLink sysModuleLink, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysModuleLink> resultEntity = null;
		try {
			resultEntity = sysModuleLinkBiz.updateSysModuleLink(sysModuleLink);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysModuleLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 删除SysModuleLink
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/deleteSysModuleLink", produces = "application/json;charset=UTF-8")
	public String deleteSysModuleLink(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysModuleLink> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = sysModuleLinkBiz.deleteSysModuleLink(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysModuleLink>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

}