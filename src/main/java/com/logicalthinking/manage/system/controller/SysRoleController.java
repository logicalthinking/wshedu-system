package com.logicalthinking.manage.system.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.logicalthinking.manage.system.annotation.SysLog;
import com.logicalthinking.manage.system.biz.SysRoleBiz;
import com.logicalthinking.manage.system.entity.SysRole;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.utils.ValidateException;

/**
 * 角色信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
@Controller
public class SysRoleController extends BaseController {

	@Resource
	private SysRoleBiz sysRoleBiz;
	
	/**
	 * 角色列表页面
	 */
	@RequestMapping(value = "/manage/roleListPage")
	public String roleListPage(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("adminCode", ConstantUtil.ROLE_CODE_ADMINISTRATOR);
		return "system/roles/roleList";
	}
	
	/**
	 * 查看角色页面
	 */
	@RequestMapping(value = "/manage/roleViewPage")
	public String roleViewPage(String roleId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<SysRole> resultEntity = sysRoleBiz.sysRoleByRoleId(roleId);
			request.setAttribute("sysRole", resultEntity.getResult());
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "system/roles/roleView";
	}
	
	/**
	 * 编辑角色页面
	 */
	@RequestMapping(value = "/manage/roleEditPage")
	public String roleEditPage(String roleId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<SysRole> resultEntity = sysRoleBiz.sysRoleByRoleId(roleId);
			request.setAttribute("adminCode", ConstantUtil.ROLE_CODE_ADMINISTRATOR);
			request.setAttribute("sysRole", resultEntity.getResult());
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "system/roles/roleEdit";
	}
	
	/**
	 * 角色新增页面
	 */
	@RequestMapping(value = "/manage/roleAddPage")
	public String roleAddPage(HttpServletRequest request, HttpServletResponse response) {
		return "system/roles/roleAdd";
	}

	/**
	 * 查询SysRole集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/sysRoleList", produces = "application/json;charset=UTF-8")
	public String sysRoleList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<SysRole>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = sysRoleBiz.sysRoleList(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<SysRole>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} catch (ValidateException e) {
			resultEntity = new ResultEntity<List<SysRole>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 查询所有SysRole集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/sysRoleAllList", produces = "application/json;charset=UTF-8")
	public String sysRoleAllList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<JSONArray> resultEntity = null;
		JSONArray jsonArray = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			List<SysRole> sysRoles = sysRoleBiz.sysRoleAllList(map);
			if(sysRoles!=null && sysRoles.size()>0){
				jsonArray = JSONArray.parseArray(JSONObject.toJSONString(sysRoles));
				JSONObject jsonObject = jsonArray.getJSONObject(0);
				jsonObject.put("LAY_CHECKED", true);
			}
			resultEntity = new ResultEntity<JSONArray>(ConstantUtil.CODE_200, jsonArray);
		} catch (Exception e) {
			resultEntity = new ResultEntity<JSONArray>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}
	/**
	 * 根据roleId查询SysRole
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/sysRoleByRoleId", produces = "application/json;charset=UTF-8")
	public String sysRoleByRoleId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysRole> resultEntity = null;
		try {
			String roleId = request.getParameter("roleId");
			resultEntity = sysRoleBiz.sysRoleByRoleId(roleId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysRole>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增SysRole
	 */
	@ResponseBody
	@SysLog(operateType="新增角色")
	@RequestMapping(value = "/manage/addSysRole", produces = "application/json;charset=UTF-8")
	public String addSysRole(SysRole sysRole, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysRole> resultEntity = null;
		try {
			resultEntity = sysRoleBiz.insertSysRole(sysRole);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysRole>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

	/**
	 * 修改SysRole
	 */
	@ResponseBody
	@SysLog(operateType="修改角色")
	@RequestMapping(value = "/manage/updateSysRole", produces = "application/json;charset=UTF-8")
	public String updateSysRole(SysRole sysRole, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysRole> resultEntity = null;
		try {
			resultEntity = sysRoleBiz.updateSysRole(sysRole);
		} catch (Exception e) {
			resultEntity = new ResultEntity<SysRole>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

	/**
	 * 删除SysRole
	 */
	@ResponseBody
	@SysLog(operateType="删除角色")
	@RequestMapping(value = "/manage/deleteSysRole", produces = "application/json;charset=UTF-8")
	public String deleteSysRole(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysRole> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = sysRoleBiz.deleteSysRole(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysRole>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

}