package com.logicalthinking.manage.system.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logicalthinking.manage.system.biz.SysUserRoleBiz;
import com.logicalthinking.manage.system.entity.SysUserRole;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 用户关联角色信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-16
 */
@Controller
public class SysUserRoleController extends BaseController {

	@Resource
	private SysUserRoleBiz sysUserRoleBiz;

	/**
	 * 查询SysUserRole集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/sysUserRoleList", produces = "application/json;charset=UTF-8")
	public String sysUserRoleList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<SysUserRole>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = sysUserRoleBiz.sysUserRoleList(map);
		} catch (Exception e) {
			resultEntity = new ResultEntity<List<SysUserRole>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据userRoleId查询SysUserRole
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/sysUserRoleByUserRoleId", produces = "application/json;charset=UTF-8")
	public String sysUserRoleByUserRoleId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysUserRole> resultEntity = null;
		try {
			String userRoleId = request.getParameter("userRoleId");
			resultEntity = sysUserRoleBiz.sysUserRoleByUserRoleId(userRoleId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysUserRole>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增SysUserRole
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/addSysUserRole", produces = "application/json;charset=UTF-8")
	public String addSysUserRole(SysUserRole sysUserRole, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysUserRole> resultEntity = null;
		try {
			resultEntity = sysUserRoleBiz.insertSysUserRole(sysUserRole);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysUserRole>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 修改SysUserRole
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/updateSysUserRole", produces = "application/json;charset=UTF-8")
	public String updateSysUserRole(SysUserRole sysUserRole, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysUserRole> resultEntity = null;
		try {
			resultEntity = sysUserRoleBiz.updateSysUserRole(sysUserRole);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysUserRole>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 删除SysUserRole
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/deleteSysUserRole", produces = "application/json;charset=UTF-8")
	public String deleteSysUserRole(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysUserRole> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = sysUserRoleBiz.deleteSysUserRole(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysUserRole>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

}