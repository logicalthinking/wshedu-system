package com.logicalthinking.manage.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.hellooop.odj.token.manager.TokenManager;
import com.logicalthinking.manage.system.annotation.SysLog;
import com.logicalthinking.manage.system.biz.SysRoleBiz;
import com.logicalthinking.manage.system.biz.SysUsersBiz;
import com.logicalthinking.manage.system.entity.SysRole;
import com.logicalthinking.manage.system.entity.SysUsers;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ResultEntity;

/**
 * 用户信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-12
 */
@Controller
public class SysUsersController extends BaseController {

	@Resource
	private SysUsersBiz sysUsersBiz;
	
	@Resource
	private SysRoleBiz sysRoleBiz;

	/**
	 * 用户登录
	 */
	@ResponseBody
	@SysLog(operateType="用户登录")
	@RequestMapping(value = "dologin", produces = "application/json;charset=UTF-8")
	public String dologin(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<JSONObject> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			/**
			 * 登录后会调用auth-token 创建token
			 */
			resultEntity = sysUsersBiz.dologin(map);
			if(com.logicalthinking.manage.system.utils.ConstantUtil.CODE_200.equals(resultEntity.getError_code())){
				request.setAttribute("operateDesc", "登录成功");
				request.setAttribute("userId", resultEntity.getResult().get("userId"));
				//说明成功了，获取token
				JSONObject jsonObject = resultEntity.getResult();
				if(jsonObject!=null){
					String token = jsonObject.getString("token");
					setCookie(response, com.logicalthinking.manage.system.utils.ConstantUtil.SESSION_TOKEN_NAME,token);
				}
			}else{
				request.setAttribute("operateDesc", "登录失败");
			}
		}catch (Exception e) {
			resultEntity = new ResultEntity<JSONObject>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
			request.setAttribute("operateDesc", "登录失败");
		}
		return resultEntity.toString();
	}


	/**
	 * 用户退出登录
	 */
	@ResponseBody
	@SysLog(operateType="退出登录")
	@RequestMapping(value = "loginOut", produces = "application/json;charset=UTF-8")
	public String loginOut(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<JSONObject> resultEntity = null;
		String token = "";
		Cookie[] cookies = request.getCookies();
		if(cookies!=null){
			for(int i=0;i<cookies.length;i++){
				if(com.logicalthinking.manage.system.utils.ConstantUtil.SESSION_TOKEN_NAME.equals(cookies[i].getName())){
					token = cookies[i].getValue();
					break;
				}
			}
		}

		if(StringUtils.isBlank(token)){
			return new ResultEntity<JSONObject>(com.logicalthinking.manage.system.utils.ConstantUtil.CODE_200, "退出成功").toString();
		}

		String userId = "";
		try {
			TokenManager tokenManager = new TokenManager();
			userId = tokenManager.getTokenData(token, "userId");
			resultEntity = sysUsersBiz.loginOut(token);
		} catch (Exception e) {
			e.printStackTrace();
			resultEntity = new ResultEntity<JSONObject>(ConstantUtil.CODE_500, e.getMessage());
		}finally {
			setLogRequest(request, resultEntity.getError_code(),userId);
		}
		return resultEntity.toString();
	}

	private void setCookie(HttpServletResponse response,String key,String value){
		Cookie cookie = new Cookie(key, value);
		cookie.setMaxAge(7*24*3600);
		response.addCookie(cookie);
	}

	/**
	 * 用户列表页面
	 */
	@RequestMapping(value = "/manage/usersListPage")
	public String usersListPage(HttpServletRequest request, HttpServletResponse response) {
		Map<String,Object> map=initRequestMap(request);
		try {
			List<SysRole> sysRoles = sysRoleBiz.sysRoleAllList(map);
			request.setAttribute("sysRoles", sysRoles);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "system/users/usersList";
	}
	
	/**
	 * 查看用户页面
	 */
	@RequestMapping(value = "/manage/userViewPage")
	public String userViewPage(String userId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<SysUsers> resultEntity = sysUsersBiz.sysUsersByUserId(userId);
			request.setAttribute("sysUsers", resultEntity.getResult());
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "system/users/userView";
	}
	
	/**
	 * 编辑用户页面
	 */
	@RequestMapping(value = "/manage/userEditPage")
	public String userEditPage(String userId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<SysUsers> resultEntity = sysUsersBiz.sysUsersByUserId(userId);
			request.setAttribute("sysUsers", resultEntity.getResult());
			
			Map<String,Object> map = new HashMap<String, Object>();
			List<SysRole> sysRoles = sysRoleBiz.sysRoleAllList(map);
			request.setAttribute("sysRoles", sysRoles);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "system/users/userEdit";
	}
	
	
	/**
	 * 个人信息页面
	 */
	@RequestMapping(value = "/manage/userInfoPage")
	public String userInfoPage(HttpServletRequest request, HttpServletResponse response) {
		try {
			SysUsers sysUsers = getSysUsers(request);
			request.setAttribute("sysUsers", sysUsers);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "main/userInfo";
	}
	
	/**
	 * 修改密码页面
	 */
	@RequestMapping(value = "/manage/updatePassPage")
	public String updatePassPage(HttpServletRequest request, HttpServletResponse response) {
		return "main/updatePass";
	}
	
	
	/**
	 * 用户新增页面
	 */
	@RequestMapping(value = "/manage/userAddPage")
	public String userAddPage(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			Map<String,Object> map = new HashMap<String, Object>();
			List<SysRole> sysRoles = sysRoleBiz.sysRoleAllList(map);
			request.setAttribute("sysRoles", sysRoles);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "system/users/userAdd";
	}
	
	/**
	 * 查询SysUsers集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/sysUsersList", produces = "application/json;charset=UTF-8")
	public String sysUsersList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<SysUsers>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = sysUsersBiz.sysUsersList(map);
		} catch (Exception e) {
			resultEntity = new ResultEntity<List<SysUsers>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}
	
	/**
	 * 修改密码
	 */
	@ResponseBody
	@SysLog(operateType="修改密码")
	@RequestMapping(value = "/manage/updateUserPass", produces = "application/json;charset=UTF-8")
	public String updateUserPass(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysUsers> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			
			SysUsers sysUsers = getSysUsers(request);
			
			resultEntity = sysUsersBiz.updateUserPass(map,sysUsers);
		} catch (Exception e) {
			resultEntity = new ResultEntity<SysUsers>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据userId查询SysUsers
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/sysUsersByUserId", produces = "application/json;charset=UTF-8")
	public String sysUsersByUserId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysUsers> resultEntity = null;
		try {
			String userId = request.getParameter("userId");
			resultEntity = sysUsersBiz.sysUsersByUserId(userId);
		} catch (com.logicalthinking.manage.system.utils.DaoException e) {
			resultEntity = new ResultEntity<SysUsers>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增SysUsers
	 */
	@ResponseBody
	@SysLog(operateType="新增用户")
	@RequestMapping(value = "/manage/addSysUsers", produces = "application/json;charset=UTF-8")
	public String addSysUsers(SysUsers sysUsers,String roleIds, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysUsers> resultEntity = null;
		try {
			resultEntity = sysUsersBiz.insertSysUsers(sysUsers,roleIds);
		} catch (com.logicalthinking.manage.system.utils.DaoException e) {
			resultEntity = new ResultEntity<SysUsers>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} finally{
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

	/**
	 * 修改SysUsers
	 */
	@ResponseBody
	@SysLog(operateType="修改用户")
	@RequestMapping(value = "/manage/updateSysUsers", produces = "application/json;charset=UTF-8")
	public String updateSysUsers(SysUsers sysUsers,String roleIds, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysUsers> resultEntity = null;
		try {
			resultEntity = sysUsersBiz.updateSysUsers(sysUsers,roleIds);
		} catch (com.logicalthinking.manage.system.utils.DaoException e) {
			resultEntity = new ResultEntity<SysUsers>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} finally{
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

	/**
	 * 删除SysUsers
	 */
	@ResponseBody
	@SysLog(operateType="删除用户")
	@RequestMapping(value = "/manage/deleteSysUsers", produces = "application/json;charset=UTF-8")
	public String deleteSysUsers(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<SysUsers> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = sysUsersBiz.deleteSysUsers(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<SysUsers>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} finally{
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

}