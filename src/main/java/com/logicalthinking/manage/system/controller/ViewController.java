package com.logicalthinking.manage.system.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logicalthinking.manage.system.biz.WebImgInfoBiz;
import com.logicalthinking.manage.system.entity.SysUsers;
import com.logicalthinking.manage.system.entity.WebImgInfo;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 页面跳转控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-09-18
 */
@Controller
public class ViewController extends BaseController{

	@Autowired
	private WebImgInfoBiz webImgInfoBiz;
	
	/**
	 * 登录页面
	 */
	@RequestMapping(value = "login")
	public String login(HttpServletRequest request, HttpServletResponse response){
		
		try {
			//背景
			String imgType = "bg_back_login";
			WebImgInfo bgImg = webImgInfoBiz.selectBackgroundImage(imgType);
			request.setAttribute("bgImg", bgImg);
		} catch (DaoException e) {
			e.printStackTrace();
		}
		
		return "common/login";
	}
	
	/**
	 * 403页面
	 * @return
	 */
	@RequestMapping(value = "error403")
	public String error403(){
		return "error/error403";
	}
	
	/**
	 * 404页面
	 * @return
	 */
	@RequestMapping(value = "error404")
	public String error404(){
		return "error/error404";
	}
	
	/**
	 * 500页面
	 * @return
	 */
	@RequestMapping(value = "error500")
	public String error500(){
		return "error/error500";
	}

	/**
	 * 用户首页
	 */
	@RequestMapping(value = "manage/mainIndex")
	public String mainIndex(HttpServletRequest request){
		SysUsers sysUsers = null;
		try {
			sysUsers = this.getSysUsers(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		request.setAttribute("sysUsers",sysUsers);
		
		if(ConstantUtil.IS_VALID_N.equals(sysUsers.getState())){
			request.setAttribute("userState",ConstantUtil.IS_VALID_N);
		}
		
		if(sysUsers.getSysUserRoles()!=null && sysUsers.getSysUserRoles().size()>0
				&& sysUsers.getSysUserRoles().get(0).getSysRole()!=null
				&& ConstantUtil.IS_VALID_N.equals(sysUsers.getSysUserRoles().get(0).getSysRole().getState())){
			request.setAttribute("roleState",ConstantUtil.IS_VALID_N);
		}
		
		return "main/index";
	}

	/**
	 * 个人主页
	 */
	@RequestMapping(value = "manage/mainPage")
	public String mainPage(HttpServletRequest request){
		SysUsers sysUsers = null;
		try {
			sysUsers = this.getSysUsers(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		request.setAttribute("sysUsers",sysUsers);
		return "main/mainPage";
	}
	
}