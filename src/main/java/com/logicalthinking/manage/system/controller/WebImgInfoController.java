package com.logicalthinking.manage.system.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logicalthinking.manage.system.annotation.SysLog;
import com.logicalthinking.manage.system.biz.DictInfoBiz;
import com.logicalthinking.manage.system.biz.WebImgInfoBiz;
import com.logicalthinking.manage.system.entity.WebImgInfo;
import com.logicalthinking.manage.system.utils.ConfigProperties;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.DictConstant;
import com.logicalthinking.manage.system.utils.ResultEntity;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.vo.KeyValue;

/**
 * 网站图片信息控制器
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Controller
public class WebImgInfoController extends BaseController {

	@Resource
	private WebImgInfoBiz webImgInfoBiz;
	
	@Resource
	private DictInfoBiz dictInfoBiz;
	
	@RequestMapping(value = "/image/priview/{imgId}")
	public void imagePriview(@PathVariable String imgId,HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String rootDir = ConfigProperties.getInstance().getProperty("localFileServerURL");
			
			ResultEntity<WebImgInfo> resultEntity = webImgInfoBiz.webImgInfoByImgId(imgId);
			if(resultEntity.getResult()!=null){
				String imgPath = resultEntity.getResult().getImgPath();
				String path =rootDir+imgPath.replace("../", "").replace("/", "\\\\");
				File file = new File(path);
				if (file.exists()) {
					InputStream fis = new BufferedInputStream(new FileInputStream(file));
					byte[] buffer = new byte[fis.available()];
		            fis.read(buffer);
		            // 设置response的header
		            response.reset();
		            response.setHeader("Content-Type", "image/jpg");
		            response.addHeader("Content-Length", "" + file.length());
		            BufferedOutputStream fos = new BufferedOutputStream(response.getOutputStream());
		            fos.write(buffer);
		           	response.flushBuffer();
		            fos.close();
		            fis.close();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 轮播
	 */
	@RequestMapping(value = "manage/carouselPage")
	public String carousel(HttpServletRequest request){
		try {
			//获取照片
			String imgType = "back_carousel";	//后台轮播
			List<WebImgInfo> webImgInfos = webImgInfoBiz.selectTypeImages(imgType);
			request.setAttribute("webImgInfos", webImgInfos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "website/carousel/carousel";
	}

	/**
	 * 网站图片列表页面
	 */
	@RequestMapping(value = "/manage/webImgInfoListPage")
	public String webImgInfoListPage(HttpServletRequest request, HttpServletResponse response) {
		try {
			//查询字典图片栏目类型
			List<KeyValue> imgTypes = dictInfoBiz.selectDictChidren(DictConstant.DICT_WEB_IMG_TYPE);
			request.setAttribute("imgTypes", imgTypes);
		} catch (DaoException e) {
			e.printStackTrace();
		}
		
		return "website/webImgInfo/webImgInfoList";
	}
	
	
	/**
	 * 网站图片排序页面
	 */
	@RequestMapping(value = "/manage/webImgInfoSortPage")
	public String webImgInfoSortPage(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String,Object> map=initRequestMap(request);
		try {
			map.put("state", ConstantUtil.IS_VALID_Y);
			map.put("sortName", "rank");
			map.put("sortOrder", "asc");
			List<WebImgInfo> webImgInfos = webImgInfoBiz.webImgInfoAllList(map);
			request.setAttribute("webImgInfos", webImgInfos);
		} catch (DaoException e) {
			e.printStackTrace();
		}
		
		return "website/webImgInfo/webImgInfoSort";
	}
	
	/**
	 * 查看网站图片页面
	 */
	@RequestMapping(value = "/manage/webImgInfoViewPage")
	public String webImgInfoViewPage(String imgId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<WebImgInfo> resultEntity = webImgInfoBiz.webImgInfoByImgId(imgId);
			request.setAttribute("webImgInfo", resultEntity.getResult());
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "website/webImgInfo/webImgInfoView";
	}
	
	/**
	 * 编辑网站图片页面
	 */
	@RequestMapping(value = "/manage/webImgInfoEditPage")
	public String webImgInfoEditPage(String imgId,HttpServletRequest request, HttpServletResponse response) {
		try {
			ResultEntity<WebImgInfo> resultEntity = webImgInfoBiz.webImgInfoByImgId(imgId);
			request.setAttribute("webImgInfo", resultEntity.getResult());
			
			//查询字典图片栏目类型
			List<KeyValue> imgTypes = dictInfoBiz.selectDictChidren(DictConstant.DICT_WEB_IMG_TYPE);
			request.setAttribute("imgTypes", imgTypes);
			
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return "website/webImgInfo/webImgInfoEdit";
	}
	
	/**
	 * 新增网站图片页面
	 */
	@RequestMapping(value = "/manage/webImgInfoAddPage")
	public String webImgInfoAddPage(HttpServletRequest request, HttpServletResponse response) {
		
		String imgBigType = request.getParameter("imgBigType");
		String imgType = request.getParameter("imgType");
		
		request.setAttribute("imgBigType", imgBigType);
		request.setAttribute("imgType", imgType);
		
		try {
			//查询字典图片栏目类型
			List<KeyValue> imgTypes = dictInfoBiz.selectDictChidren(DictConstant.DICT_WEB_IMG_TYPE);
			request.setAttribute("imgTypes", imgTypes);
		} catch (DaoException e) {
			e.printStackTrace();
		}
		
		return "website/webImgInfo/webImgInfoAdd";
	}
	
	/**
	 * 查询WebImgInfo集合
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/webImgInfoList", produces = "application/json;charset=UTF-8")
	public String webImgInfoList(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<List<WebImgInfo>> resultEntity = null;
		try {
			Map<String,Object> map=initRequestMap(request);
			resultEntity = webImgInfoBiz.webImgInfoList(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<List<WebImgInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		} catch (ValidateException e) {
			resultEntity = new ResultEntity<List<WebImgInfo>>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 根据imgId查询WebImgInfo
	 */
	@ResponseBody
	@RequestMapping(value = "/manage/webImgInfoByImgId", produces = "application/json;charset=UTF-8")
	public String webImgInfoByImgId(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<WebImgInfo> resultEntity = null;
		try {
			String imgId = request.getParameter("imgId");
			resultEntity = webImgInfoBiz.webImgInfoByImgId(imgId);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<WebImgInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}
		return resultEntity.toString();
	}

	/**
	 * 新增WebImgInfo
	 */
	@ResponseBody
	@SysLog(operateType="新增图片")
	@RequestMapping(value = "/manage/addWebImgInfo", produces = "application/json;charset=UTF-8")
	public String addWebImgInfo(WebImgInfo webImgInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<WebImgInfo> resultEntity = null;
		try {
			resultEntity = webImgInfoBiz.insertWebImgInfo(webImgInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<WebImgInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}
	
	@ResponseBody
	@SysLog(operateType="图片排序")
	@RequestMapping(value = "/manage/webImgInfoSort", produces = "application/json;charset=UTF-8")
	public String webImgInfoSort(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<WebImgInfo> resultEntity = null;
		try {
			String [] imgIds = request.getParameterValues("imgIds[]");
			resultEntity = webImgInfoBiz.webImgInfoSort(imgIds);
		} catch (Exception e) {
			resultEntity = new ResultEntity<WebImgInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

	/**
	 * 修改WebImgInfo
	 */
	@ResponseBody
	@SysLog(operateType="修改图片")
	@RequestMapping(value = "/manage/updateWebImgInfo", produces = "application/json;charset=UTF-8")
	public String updateWebImgInfo(WebImgInfo webImgInfo, HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<WebImgInfo> resultEntity = null;
		try {
			resultEntity = webImgInfoBiz.updateWebImgInfo(webImgInfo);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<WebImgInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

	/**
	 * 删除WebImgInfo
	 */
	@ResponseBody
	@SysLog(operateType="删除图片")
	@RequestMapping(value = "/manage/deleteWebImgInfo", produces = "application/json;charset=UTF-8")
	public String deleteWebImgInfo(HttpServletRequest request, HttpServletResponse response) {
		ResultEntity<WebImgInfo> resultEntity = null;
		try {
			Map<String,Object> map = this.initRequestMap(request);
			resultEntity = webImgInfoBiz.deleteWebImgInfo(map);
		} catch (DaoException e) {
			resultEntity = new ResultEntity<WebImgInfo>(ConstantUtil.CODE_500, e.getMessage());
			e.printStackTrace();
		}finally {
			setLogRequest(request, resultEntity.getError_code());
		}
		return resultEntity.toString();
	}

}