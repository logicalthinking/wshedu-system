package com.logicalthinking.manage.system.entity;

import java.io.UnsupportedEncodingException;

import org.apache.commons.lang.StringUtils;

/**
 * 文案信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public class ArticleInfo { 

	private String articleId;	//主键ID
	private String articleType;	//类型
	private String title;	//标题
	private byte[] content;	//文章内容
	private String remark;	//备注
	private String createTime;	//创建时间
	private Integer rank;	//排序 默认99
	private String state;	//状态 Y 有效 N 无效
	
	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	/**
	 * 类型名称
	 */
	private String articleTypeName;
	
	/**
	 * 文章内容 字符串
	 */
	private String artContent;
	
	public String getArticleTypeName() {
		return articleTypeName;
	}

	public void setArticleTypeName(String articleTypeName) {
		this.articleTypeName = articleTypeName;
	}

	public String getArtContent() {
		if(this.content!=null && this.content.length>0){
			try {
				this.artContent = new String(this.content,"UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return artContent;
	}

	public void setArtContent(String artContent) {
		this.artContent = artContent;
	}

	public String getArticleId() {
		return articleId;
	}

	public void setArticleId(String articleId) { 
		this.articleId = articleId;
	}

	public String getArticleType() {
		return articleType;
	}

	public void setArticleType(String articleType) { 
		this.articleType = articleType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) { 
		this.title = title;
	}

	public byte[] getContent() {
		if(StringUtils.isNotBlank(this.artContent)){
			try {
				this.content = this.artContent.getBytes("UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return content;
	}

	public void setContent(byte[] content) { 
		this.content = content;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) { 
		this.remark = remark;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) { 
		this.createTime = createTime;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "ArticleInfo [articleId=" + articleId + ",articleType=" + articleType + ",title=" + title + ",content=" + content + ",remark=" + remark + ",createTime=" + createTime + ",state=" + state + "]";
	}
}