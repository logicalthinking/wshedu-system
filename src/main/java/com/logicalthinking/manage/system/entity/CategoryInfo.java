package com.logicalthinking.manage.system.entity;

/**
 * 分类信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-10-18
 */
public class CategoryInfo { 

	private String typeId;	//主键ID
	private String typeName;	//类型名称
	private String typeCode;	//类型编号
	private String bigType;	//所属大类
	private Integer rank;	//序号
	private String imgPath;	//类型图标路径
	private String parentId;	//上级ID
	private String typeUrl;	//跳转链接
	private String remark;	//备注
	private String state;	//状态 Y 有效 N 无效
	
	/**
	 * 上级类型名称
	 */
	private String parentTypeName;
	
	/**
	 * 所属大类名称
	 */
	private String bigTypeName;
	
	public String getParentTypeName() {
		return parentTypeName;
	}

	public void setParentTypeName(String parentTypeName) {
		this.parentTypeName = parentTypeName;
	}

	public String getBigTypeName() {
		return bigTypeName;
	}

	public void setBigTypeName(String bigTypeName) {
		this.bigTypeName = bigTypeName;
	}

	public String getTypeUrl() {
		return typeUrl;
	}

	public void setTypeUrl(String typeUrl) {
		this.typeUrl = typeUrl;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) { 
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) { 
		this.typeName = typeName;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) { 
		this.typeCode = typeCode;
	}

	public String getBigType() {
		return bigType;
	}

	public void setBigType(String bigType) { 
		this.bigType = bigType;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) { 
		this.rank = rank;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) { 
		this.imgPath = imgPath;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) { 
		this.parentId = parentId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) { 
		this.remark = remark;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "CategoryInfo [typeId=" + typeId + ",typeName=" + typeName + ",typeCode=" + typeCode + ",bigType=" + bigType + ",rank=" + rank + ",imgPath=" + imgPath + ",parentId=" + parentId + ",remark=" + remark + ",state=" + state + "]";
	}
}