package com.logicalthinking.manage.system.entity;

/**
 * 学员联系记录实体类
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public class ContactRecord { 

	private String contactId;	//主键ID
	private String empId;	//员工ID
	private String stuId;	//学员ID
	private String contactType;	//联系类型
	private String contactTime;	//联系时间
	private String content;	//内容
	private String createTime;	//创建时间
	private String createById;	//创建人ID
	private String updateById;	//更新人ID
	private String remark;	//备注
	private String state;	//状态 Y有效  N无效

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) { 
		this.contactId = contactId;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) { 
		this.empId = empId;
	}

	public String getStuId() {
		return stuId;
	}

	public void setStuId(String stuId) { 
		this.stuId = stuId;
	}

	public String getContactType() {
		return contactType;
	}

	public void setContactType(String contactType) { 
		this.contactType = contactType;
	}

	public String getContactTime() {
		return contactTime;
	}

	public void setContactTime(String contactTime) { 
		this.contactTime = contactTime;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) { 
		this.content = content;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) { 
		this.createTime = createTime;
	}

	public String getCreateById() {
		return createById;
	}

	public void setCreateById(String createById) { 
		this.createById = createById;
	}

	public String getUpdateById() {
		return updateById;
	}

	public void setUpdateById(String updateById) { 
		this.updateById = updateById;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) { 
		this.remark = remark;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "ContactRecord [contactId=" + contactId + ",empId=" + empId + ",stuId=" + stuId + ",contactType=" + contactType + ",contactTime=" + contactTime + ",content=" + content + ",createTime=" + createTime + ",createById=" + createById + ",updateById=" + updateById + ",remark=" + remark + ",state=" + state + "]";
	}
}