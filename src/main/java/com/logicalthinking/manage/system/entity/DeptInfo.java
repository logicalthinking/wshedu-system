package com.logicalthinking.manage.system.entity;

import java.util.List;

/**
 * 部门信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public class DeptInfo { 

	private String deptId;	//部门ID
	private String deptCode;	//部门编号
	private String deptName;	//部门名称
	private String parentId;	//上级ID
	private String remark;	//备注
	private String state;	//状态  Y有效 N无效
	
	private List<DeptInfo> children;
	
	public List<DeptInfo> getChildren() {
		return children;
	}

	public void setChildren(List<DeptInfo> children) {
		this.children = children;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) { 
		this.deptId = deptId;
	}
	
	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) { 
		this.deptName = deptName;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) { 
		this.parentId = parentId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) { 
		this.remark = remark;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "DeptInfo [deptId=" + deptId + ",deptName=" + deptName + ",parentId=" + parentId + ",remark=" + remark + ",state=" + state + "]";
	}
}