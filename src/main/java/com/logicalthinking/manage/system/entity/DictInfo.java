package com.logicalthinking.manage.system.entity;

/**
 * 字典信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public class DictInfo { 

	private String dictId;	//主键ID
	private String dictType;	//字典类型
	private String dictValue;	//字典值
	private String dictName;	//名称
	private String dictPid;	//上级ID
	private String dictDesc;	//描述
	private String state;	//状态 Y 有效 N 无效
	private Integer rank;	//排序号
	
	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getDictId() {
		return dictId;
	}

	public void setDictId(String dictId) { 
		this.dictId = dictId;
	}

	public String getDictType() {
		return dictType;
	}

	public void setDictType(String dictType) { 
		this.dictType = dictType;
	}

	public String getDictValue() {
		return dictValue;
	}

	public void setDictValue(String dictValue) { 
		this.dictValue = dictValue;
	}

	public String getDictName() {
		return dictName;
	}

	public void setDictName(String dictName) { 
		this.dictName = dictName;
	}

	public String getDictPid() {
		return dictPid;
	}

	public void setDictPid(String dictPid) { 
		this.dictPid = dictPid;
	}

	public String getDictDesc() {
		return dictDesc;
	}

	public void setDictDesc(String dictDesc) { 
		this.dictDesc = dictDesc;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "DictInfo [dictId=" + dictId + ",dictType=" + dictType + ",dictValue=" + dictValue + ",dictName=" + dictName + ",dictPid=" + dictPid + ",dictDesc=" + dictDesc + ",state=" + state + "]";
	}
}