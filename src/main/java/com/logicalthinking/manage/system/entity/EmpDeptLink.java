package com.logicalthinking.manage.system.entity;

/**
 * 员工部门关联信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public class EmpDeptLink { 

	private String linkId;	//关联ID
	private String empId;	//员工ID
	private String deptId;	//部门ID
	private String state;	//状态 Y 有效 N 无效
	
	private DeptInfo deptInfo;

	public DeptInfo getDeptInfo() {
		return deptInfo;
	}

	public void setDeptInfo(DeptInfo deptInfo) {
		this.deptInfo = deptInfo;
	}

	public String getLinkId() {
		return linkId;
	}

	public void setLinkId(String linkId) { 
		this.linkId = linkId;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) { 
		this.empId = empId;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) { 
		this.deptId = deptId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "EmpDeptLink [linkId=" + linkId + ",empId=" + empId + ",deptId=" + deptId + ",state=" + state + "]";
	}
}