package com.logicalthinking.manage.system.entity;

/**
 * 员工分组关联信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public class EmpGroupLink { 

	private String linkId;	//关联ID
	private String empId;	//员工ID
	private String groupId;	//分组ID
	private String state;	//状态 Y 有效 N 无效

	public String getLinkId() {
		return linkId;
	}

	public void setLinkId(String linkId) { 
		this.linkId = linkId;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) { 
		this.empId = empId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) { 
		this.groupId = groupId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "EmpGroupLink [linkId=" + linkId + ",empId=" + empId + ",groupId=" + groupId + ",state=" + state + "]";
	}
}