package com.logicalthinking.manage.system.entity;

import java.util.List;

/**
 * 员工信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public class EmpInfo { 

	private String empId;	//员工ID
	private String empCode;	//员工工号
	private String empName;	//员工姓名
	private String userId;	//绑定的用户ID
	private String joinTime;	//入职时间
	private String outTime;	//离岗时间
	private String job;	//工作岗位
	private String jobStatus;	//在职状态
	private String createTime;	//创建时间
	private String updateTime;	//更新时间
	private String remark;	//备注
	private String state;	//有效状态 Y有效 N无效
	
	private List<EmpDeptLink> empDeptLinks;
	
	public List<EmpDeptLink> getEmpDeptLinks() {
		return empDeptLinks;
	}

	public void setEmpDeptLinks(List<EmpDeptLink> empDeptLinks) {
		this.empDeptLinks = empDeptLinks;
	}

	public String getEmpCode() {
		return empCode;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) { 
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) { 
		this.empName = empName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) { 
		this.userId = userId;
	}

	public String getJoinTime() {
		return joinTime;
	}

	public void setJoinTime(String joinTime) { 
		this.joinTime = joinTime;
	}

	public String getOutTime() {
		return outTime;
	}

	public void setOutTime(String outTime) { 
		this.outTime = outTime;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) { 
		this.job = job;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) { 
		this.jobStatus = jobStatus;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) { 
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) { 
		this.updateTime = updateTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) { 
		this.remark = remark;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "EmpInfo [empId=" + empId + ",empName=" + empName + ",userId=" + userId + ",joinTime=" + joinTime + ",outTime=" + outTime + ",job=" + job + ",jobStatus=" + jobStatus + ",createTime=" + createTime + ",updateTime=" + updateTime + ",remark=" + remark + ",state=" + state + "]";
	}
}