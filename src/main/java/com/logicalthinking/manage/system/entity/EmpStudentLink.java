package com.logicalthinking.manage.system.entity;

/**
 * 员工学员关联信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public class EmpStudentLink { 

	private String linkId;	//关联主键ID
	private String empId;	//员工ID
	private String stuId;	//学员ID
	private String linkStatus;	//员工学员绑定状态
	private String createTime;	//创建时间
	private String updateTime;	//更新时间
	private String remark;	//备注
	private String state;	//状态 Y 有效 N 无效

	public String getLinkId() {
		return linkId;
	}

	public void setLinkId(String linkId) { 
		this.linkId = linkId;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) { 
		this.empId = empId;
	}

	public String getStuId() {
		return stuId;
	}

	public void setStuId(String stuId) { 
		this.stuId = stuId;
	}

	public String getLinkStatus() {
		return linkStatus;
	}

	public void setLinkStatus(String linkStatus) { 
		this.linkStatus = linkStatus;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) { 
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) { 
		this.updateTime = updateTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) { 
		this.remark = remark;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "EmpStudentLink [linkId=" + linkId + ",empId=" + empId + ",stuId=" + stuId + ",linkStatus=" + linkStatus + ",createTime=" + createTime + ",updateTime=" + updateTime + ",remark=" + remark + ",state=" + state + "]";
	}
}