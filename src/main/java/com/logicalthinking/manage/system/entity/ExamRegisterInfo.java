package com.logicalthinking.manage.system.entity;

/**
 * 报考信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public class ExamRegisterInfo { 

	private String regId;	//主键ID
	private String stuId;	//学员ID
	private String school;	//报考学校
	private String level;	//层次
	private String major;	//专业
	private Double allAmount;	//总费用
	private Double handAmount;	//已交费用
	private String joinTime;	//报名时间
	private String feeStatus;	//是否缴费状态
	private String createById;	//创建人ID
	private String updateById;	//更新人ID
	private String remark;	//备注
	private String state;	//状态 Y有效 N无效

	public String getRegId() {
		return regId;
	}

	public void setRegId(String regId) { 
		this.regId = regId;
	}

	public String getStuId() {
		return stuId;
	}

	public void setStuId(String stuId) { 
		this.stuId = stuId;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) { 
		this.school = school;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) { 
		this.level = level;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) { 
		this.major = major;
	}

	public Double getAllAmount() {
		return allAmount;
	}

	public void setAllAmount(Double allAmount) { 
		this.allAmount = allAmount;
	}

	public Double getHandAmount() {
		return handAmount;
	}

	public void setHandAmount(Double handAmount) { 
		this.handAmount = handAmount;
	}

	public String getJoinTime() {
		return joinTime;
	}

	public void setJoinTime(String joinTime) { 
		this.joinTime = joinTime;
	}

	public String getFeeStatus() {
		return feeStatus;
	}

	public void setFeeStatus(String feeStatus) { 
		this.feeStatus = feeStatus;
	}

	public String getCreateById() {
		return createById;
	}

	public void setCreateById(String createById) { 
		this.createById = createById;
	}

	public String getUpdateById() {
		return updateById;
	}

	public void setUpdateById(String updateById) { 
		this.updateById = updateById;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) { 
		this.remark = remark;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "ExamRegisterInfo [regId=" + regId + ",stuId=" + stuId + ",school=" + school + ",level=" + level + ",major=" + major + ",allAmount=" + allAmount + ",handAmount=" + handAmount + ",joinTime=" + joinTime + ",feeStatus=" + feeStatus + ",createById=" + createById + ",updateById=" + updateById + ",remark=" + remark + ",state=" + state + "]";
	}
}