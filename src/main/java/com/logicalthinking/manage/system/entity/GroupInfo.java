package com.logicalthinking.manage.system.entity;

/**
 * 分组信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public class GroupInfo { 

	private String groupId;	//分组ID
	private String groupCode;	//分组编号
	private String groupName;	//分组名称
	private String remark;	//备注
	private String state;	//状态 Y 有效 N 无效

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) { 
		this.groupId = groupId;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) { 
		this.groupCode = groupCode;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) { 
		this.groupName = groupName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) { 
		this.remark = remark;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "GroupInfo [groupId=" + groupId + ",groupCode=" + groupCode + ",groupName=" + groupName + ",remark=" + remark + ",state=" + state + "]";
	}
}