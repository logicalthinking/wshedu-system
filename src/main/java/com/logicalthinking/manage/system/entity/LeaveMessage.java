package com.logicalthinking.manage.system.entity;

/**
 * 留言信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public class LeaveMessage { 

	private String msgId;	//主键ID
	private String userName;	//用户姓名
	private String sex;	//性别
	private String telephone;	//手机号
	private String email;	//邮箱
	private String qqNo;	//qq号
	private String wxNo;	//微信号
	private String content;	//留言内容
	private String createTime;	//创建时间
	private String remark;	//备注
	private String state;	//状态 Y有效 N 无效

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) { 
		this.msgId = msgId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) { 
		this.userName = userName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) { 
		this.sex = sex;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) { 
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) { 
		this.email = email;
	}

	public String getQqNo() {
		return qqNo;
	}

	public void setQqNo(String qqNo) { 
		this.qqNo = qqNo;
	}

	public String getWxNo() {
		return wxNo;
	}

	public void setWxNo(String wxNo) { 
		this.wxNo = wxNo;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) { 
		this.content = content;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) { 
		this.createTime = createTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) { 
		this.remark = remark;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "LeaveMessage [msgId=" + msgId + ",userName=" + userName + ",sex=" + sex + ",telephone=" + telephone + ",email=" + email + ",qqNo=" + qqNo + ",wxNo=" + wxNo + ",content=" + content + ",createTime=" + createTime + ",remark=" + remark + ",state=" + state + "]";
	}
}