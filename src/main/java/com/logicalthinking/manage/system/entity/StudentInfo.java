package com.logicalthinking.manage.system.entity;

/**
 * 学员信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public class StudentInfo { 

	private String stuId;	//学员ID
	private String stuName;	//姓名
	private String stuType;	//类型  意向学员  学员  公共学员
	private String phone;	//电话
	private String email;	//邮箱
	private String qqNo;	//qq号
	private String wxNo;	//微信号
	private String identityId;	//身份证号码
	private String identityType;	//证件类型
	private String sex;	//性别
	private String nation;	//民族
	private String province;	//省份
	private String city;	//城市
	private String area;	//地区
	private String address;	//详细地址
	private String createTime;	//创建时间
	private String updateTime;	//修改时间
	private String remark;	//备注
	private String state;	//有效状态 Y 有效 N 无效

	public String getStuId() {
		return stuId;
	}

	public void setStuId(String stuId) { 
		this.stuId = stuId;
	}

	public String getStuName() {
		return stuName;
	}

	public void setStuName(String stuName) { 
		this.stuName = stuName;
	}

	public String getStuType() {
		return stuType;
	}

	public void setStuType(String stuType) { 
		this.stuType = stuType;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) { 
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) { 
		this.email = email;
	}

	public String getQqNo() {
		return qqNo;
	}

	public void setQqNo(String qqNo) { 
		this.qqNo = qqNo;
	}

	public String getWxNo() {
		return wxNo;
	}

	public void setWxNo(String wxNo) { 
		this.wxNo = wxNo;
	}

	public String getIdentityId() {
		return identityId;
	}

	public void setIdentityId(String identityId) { 
		this.identityId = identityId;
	}

	public String getIdentityType() {
		return identityType;
	}

	public void setIdentityType(String identityType) { 
		this.identityType = identityType;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) { 
		this.sex = sex;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) { 
		this.nation = nation;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) { 
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) { 
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) { 
		this.area = area;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) { 
		this.address = address;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) { 
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) { 
		this.updateTime = updateTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) { 
		this.remark = remark;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "StudentInfo [stuId=" + stuId + ",stuName=" + stuName + ",stuType=" + stuType + ",phone=" + phone + ",email=" + email + ",qqNo=" + qqNo + ",wxNo=" + wxNo + ",identityId=" + identityId + ",identityType=" + identityType + ",sex=" + sex + ",nation=" + nation + ",province=" + province + ",city=" + city + ",area=" + area + ",address=" + address + ",createTime=" + createTime + ",updateTime=" + updateTime + ",remark=" + remark + ",state=" + state + "]";
	}
}