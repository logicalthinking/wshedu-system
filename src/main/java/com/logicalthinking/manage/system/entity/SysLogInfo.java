package com.logicalthinking.manage.system.entity;

/**
 * 系统日志信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
public class SysLogInfo { 

	private String logId;	//主键ID
	private String userId;	//用户ID
	private String operateName;	//操作名称
	private String operateUrl;	//操作接口地址
	private String operateDesc;	//描述
	private String operateParam;	//操作参数
	private String userToken;	//user_token
	private String ip;	//ip地址
	private Integer port;	//端口
	private String logCustType;	//客户端类型pc、android、ios
	private String sysCode;	//系统标识
	private String createTime;	//操作时间
	private String state;	//有效状态 Y有效 N 无效

	/**
	 * 用户对象
	 */
	private SysUsers sysUsers;
	
	public SysUsers getSysUsers() {
		return sysUsers;
	}

	public void setSysUsers(SysUsers sysUsers) {
		this.sysUsers = sysUsers;
	}

	public String getLogId() {
		return logId;
	}

	public void setLogId(String logId) { 
		this.logId = logId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) { 
		this.userId = userId;
	}

	public String getOperateName() {
		return operateName;
	}

	public void setOperateName(String operateName) { 
		this.operateName = operateName;
	}

	public String getOperateUrl() {
		return operateUrl;
	}

	public void setOperateUrl(String operateUrl) { 
		this.operateUrl = operateUrl;
	}

	public String getOperateDesc() {
		return operateDesc;
	}

	public void setOperateDesc(String operateDesc) { 
		this.operateDesc = operateDesc;
	}

	public String getOperateParam() {
		return operateParam;
	}

	public void setOperateParam(String operateParam) { 
		this.operateParam = operateParam;
	}

	public String getUserToken() {
		return userToken;
	}

	public void setUserToken(String userToken) { 
		this.userToken = userToken;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) { 
		this.ip = ip;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) { 
		this.port = port;
	}

	public String getLogCustType() {
		return logCustType;
	}

	public void setLogCustType(String logCustType) { 
		this.logCustType = logCustType;
	}

	public String getSysCode() {
		return sysCode;
	}

	public void setSysCode(String sysCode) { 
		this.sysCode = sysCode;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "SysLogInfo [logId=" + logId + ", userId=" + userId
				+ ", operateName=" + operateName + ", operateUrl=" + operateUrl
				+ ", operateDesc=" + operateDesc + ", operateParam="
				+ operateParam + ", userToken=" + userToken + ", ip=" + ip
				+ ", port=" + port + ", logCustType=" + logCustType
				+ ", sysCode=" + sysCode + ", createTime=" + createTime
				+ ", state=" + state + "]";
	}

}