package com.logicalthinking.manage.system.entity;

/**
 * 系统模块信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
public class SysModule { 

	private String moduleId;	//主键ID
	private String moduleName;	//模块名称
	private String moduleCode;	//模块编码
	private Integer rank;	//排序号
	private String parentId;	//上级模块ID
	private String moduleUrl;	//操作地址
	private String remark;	//备注
	private String moduleType;	//模块类型
	private String icon;	//图标
	private String defaultStatus;	//默认模块状态  Y 是 N 否 
	private String showStatus;	//是否显示状态 Y 是 N 否 
	private String state;	//有效状态Y，有效，N无效
	
	/**
	 * 上级模块名称
	 */
	private String parentModuleName;
	/**
	 * 模块类型名称
	 */
	private String moduleTypeName;
	
	public String getParentModuleName() {
		return parentModuleName;
	}

	public void setParentModuleName(String parentModuleName) {
		this.parentModuleName = parentModuleName;
	}

	public String getModuleTypeName() {
		return moduleTypeName;
	}

	public void setModuleTypeName(String moduleTypeName) {
		this.moduleTypeName = moduleTypeName;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) { 
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) { 
		this.moduleName = moduleName;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) { 
		this.rank = rank;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) { 
		this.parentId = parentId;
	}

	public String getModuleUrl() {
		return moduleUrl;
	}

	public void setModuleUrl(String moduleUrl) { 
		this.moduleUrl = moduleUrl;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) { 
		this.remark = remark;
	}

	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) { 
		this.moduleType = moduleType;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) { 
		this.icon = icon;
	}

	public String getDefaultStatus() {
		return defaultStatus;
	}

	public void setDefaultStatus(String defaultStatus) { 
		this.defaultStatus = defaultStatus;
	}

	public String getShowStatus() {
		return showStatus;
	}

	public void setShowStatus(String showStatus) { 
		this.showStatus = showStatus;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "SysModule [moduleId=" + moduleId + ",moduleName=" + moduleName + ",rank=" + rank + ",parentId=" + parentId + ",moduleUrl=" + moduleUrl + ",remark=" + remark + ",moduleType=" + moduleType + ",icon=" + icon + ",defaultStatus=" + defaultStatus + ",showStatus=" + showStatus + ",state=" + state + "]";
	}
}