package com.logicalthinking.manage.system.entity;

/**
 * 模块关联业务信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public class SysModuleLink { 

	private String linkId;	//主键ID
	private String moduleId;	//模块ID
	private String busiId;	//业务关联ID 角色ID或用户ID
	private String state;	//有效状态Y有效，Y无效

	public String getLinkId() {
		return linkId;
	}

	public void setLinkId(String linkId) { 
		this.linkId = linkId;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) { 
		this.moduleId = moduleId;
	}

	public String getBusiId() {
		return busiId;
	}

	public void setBusiId(String busiId) { 
		this.busiId = busiId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "SysModuleLink [linkId=" + linkId + ",moduleId=" + moduleId + ",busiId=" + busiId + ",state=" + state + "]";
	}
}