package com.logicalthinking.manage.system.entity;

/**
 * 角色信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
public class SysRole { 

	private String roleId;	//角色ID
	private String roleName;	//角色名称
	private String roleCode;	//角色编号
	private String remark;	//备注
	private String state;	//有效状态Y有效，N无效

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) { 
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) { 
		this.roleName = roleName;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) { 
		this.roleCode = roleCode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) { 
		this.remark = remark;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "SysRole [roleId=" + roleId + ",roleName=" + roleName + ",roleCode=" + roleCode + ",remark=" + remark + ",state=" + state + "]";
	}
}