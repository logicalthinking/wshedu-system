package com.logicalthinking.manage.system.entity;

/**
 * 用户关联角色信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-10-16
 */
public class SysUserRole { 

	private String userRoleId;	//主键ID
	private String userId;	//用户ID
	private String roleId;	//角色ID
	private String state;	//状态 Y有效 N 无效
	
	/**
	 * 角色对象
	 */
	private SysRole sysRole;
	
	public SysRole getSysRole() {
		return sysRole;
	}

	public void setSysRole(SysRole sysRole) {
		this.sysRole = sysRole;
	}

	public String getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(String userRoleId) { 
		this.userRoleId = userRoleId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) { 
		this.userId = userId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) { 
		this.roleId = roleId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "SysUserRole [userRoleId=" + userRoleId + ",userId=" + userId + ",roleId=" + roleId + ",state=" + state + "]";
	}
}