package com.logicalthinking.manage.system.entity;

import java.util.List;

/**
 * 用户信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-10-12
 */
public class SysUsers { 

	private String userId;	//主键ID
	private String userName;	//用户名
	private String userPass;	//用户密码
	private String telephone;	//手机号
	private String email;	//邮箱
	private String birthday;	//生日
	private String sex;	//性别
	private Integer age;	//年龄
	private String createTime;	//创建时间
	private String updateTime;	//修改时间
	private String state;	//有效状态 Y 有效 N 无效
	
	/**
	 * 用户关联角色集合
	 */
	private List<SysUserRole> sysUserRoles;
	
	public List<SysUserRole> getSysUserRoles() {
		return sysUserRoles;
	}

	public void setSysUserRoles(List<SysUserRole> sysUserRoles) {
		this.sysUserRoles = sysUserRoles;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) { 
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) { 
		this.userName = userName;
	}

	public String getUserPass() {
		return userPass;
	}

	public void setUserPass(String userPass) { 
		this.userPass = userPass;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) { 
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) { 
		this.email = email;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) { 
		this.birthday = birthday;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) { 
		this.sex = sex;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) { 
		this.age = age;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) { 
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) { 
		this.updateTime = updateTime;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "SysUsers [userId=" + userId + ",userName=" + userName + ",userPass=" + userPass + ",telephone=" + telephone + ",email=" + email + ",birthday=" + birthday + ",sex=" + sex + ",age=" + age + ",createTime=" + createTime + ",updateTime=" + updateTime + ",state=" + state + "]";
	}
}