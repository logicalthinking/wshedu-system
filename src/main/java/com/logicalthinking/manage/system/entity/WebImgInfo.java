package com.logicalthinking.manage.system.entity;

/**
 * 网站图片信息实体类
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public class WebImgInfo { 

	private String imgId;	//主键ID
	private String imgName;	//图片名称
	private String imgPath;	//图片路径
	private String imgUrl;	//图片访问地址
	private String imgBigType;	//所在大类
	private String imgType;	//所在位置类型
	private String suffix;	//图片后缀
	private String remark;	//备注
	private String wordStatus;	//是否显示文字
	private String createTime;	//创建时间
	private String state;	//状态 Y有效N无效
	private Integer rank;	//排序号
	
	/**
	 * 大类名称
	 */
	private String imgBigTypeName;
	
	/**
	 * 所在位置类型名称
	 */
	private String imgTypeName;
	
	public String getWordStatus() {
		return wordStatus;
	}

	public void setWordStatus(String wordStatus) {
		this.wordStatus = wordStatus;
	}

	public String getImgBigTypeName() {
		return imgBigTypeName;
	}

	public void setImgBigTypeName(String imgBigTypeName) {
		this.imgBigTypeName = imgBigTypeName;
	}

	public String getImgBigType() {
		return imgBigType;
	}

	public void setImgBigType(String imgBigType) {
		this.imgBigType = imgBigType;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getImgTypeName() {
		return imgTypeName;
	}

	public void setImgTypeName(String imgTypeName) {
		this.imgTypeName = imgTypeName;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getImgId() {
		return imgId;
	}

	public void setImgId(String imgId) { 
		this.imgId = imgId;
	}

	public String getImgName() {
		return imgName;
	}

	public void setImgName(String imgName) { 
		this.imgName = imgName;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) { 
		this.imgPath = imgPath;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) { 
		this.imgUrl = imgUrl;
	}

	public String getImgType() {
		return imgType;
	}

	public void setImgType(String imgType) { 
		this.imgType = imgType;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) { 
		this.remark = remark;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) { 
		this.createTime = createTime;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) { 
		this.state = state;
	}

	@Override
	public String toString() {
		return "WebImgInfo [imgId=" + imgId + ", imgName=" + imgName
				+ ", imgPath=" + imgPath + ", imgUrl=" + imgUrl + ", imgType="
				+ imgType + ", remark=" + remark + ", createTime=" + createTime
				+ ", state=" + state + ", rank=" + rank + "]";
	}

}