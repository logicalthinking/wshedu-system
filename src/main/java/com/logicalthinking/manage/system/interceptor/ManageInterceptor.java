package com.logicalthinking.manage.system.interceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.hellooop.odj.token.manager.TokenManager;
import com.logicalthinking.manage.system.biz.SysModuleBiz;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 拦截器
 * @author lanping
 * @version 1.0
 * @date 2019/9/18
 */
public class ManageInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object obj) throws Exception {
        boolean flag = false;
        String token = "";
        Cookie [] cookies = request.getCookies();
        if(cookies!=null){
            for(int i=0;i<cookies.length;i++){
                if(ConstantUtil.SESSION_TOKEN_NAME.equals(cookies[i].getName())){
                    token = cookies[i].getValue();
                    break;
                }
            }
        }
        if(StringUtils.isBlank(token)){
            //进入登录页面
            response.sendRedirect("../login");
            return flag;
        }

        TokenManager tokenManager = new TokenManager();
        flag = tokenManager.validateToken(token);
        if(flag==false){
            response.sendRedirect("../login");
            return flag;
        }
        
        String moduleUrl = request.getRequestURI();
        
        WebApplicationContext wac = ContextLoader.getCurrentWebApplicationContext();
        SysModuleBiz sysModuleBiz = (SysModuleBiz) wac.getBean("sysModuleBiz");
        
        //检验访问权限
        boolean validFlag = sysModuleBiz.selectModuleListByValid(moduleUrl, token);
		if(validFlag == false){
		   response.sendRedirect("../error403");
		   return flag;
		}
        
        return flag;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object obj, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object obj, Exception e) throws Exception {

    }
}
