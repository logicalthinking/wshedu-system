package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.ArticleInfo;

/**
 * 文案信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public interface ArticleInfoMapper {

	/**
	 * 新增
	 * 
	 * @param articleInfo
	 * @return
	 * @throws Exception
	 */
	public int insertArticleInfo(ArticleInfo articleInfo) throws Exception;

	/**
	 * 修改
	 * 
	 * @param articleInfo
	 * @return
	 * @throws Exception
	 */
	public int updateArticleInfo(ArticleInfo articleInfo) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteArticleInfo(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param articleId
	 * @return
	 */
	public ArticleInfo selectArticleInfoByArticleId(String articleId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<ArticleInfo> selectArticleInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<ArticleInfo> selectAllArticleInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectArticleInfoListCount(Map<String, Object> map) throws Exception;

}