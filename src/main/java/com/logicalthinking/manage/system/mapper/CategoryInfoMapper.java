package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.CategoryInfo;

/**
 * 分类管理接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public interface CategoryInfoMapper {

	/**
	 * 新增
	 * 
	 * @param categoryInfo
	 * @return
	 * @throws Exception
	 */
	public int insertCategoryInfo(CategoryInfo categoryInfo) throws Exception;

	/**
	 * 修改
	 * 
	 * @param categoryInfo
	 * @return
	 * @throws Exception
	 */
	public int updateCategoryInfo(CategoryInfo categoryInfo) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteCategoryInfo(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param typeId
	 * @return
	 */
	public CategoryInfo selectCategoryInfoByTypeId(String typeId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<CategoryInfo> selectCategoryInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<CategoryInfo> selectAllCategoryInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectCategoryInfoListCount(Map<String, Object> map) throws Exception;

}