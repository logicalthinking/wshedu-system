package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.ContactRecord;

/**
 * 学员联系记录接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface ContactRecordMapper {

	/**
	 * 新增
	 * 
	 * @param contactRecord
	 * @return
	 * @throws Exception
	 */
	public int insertContactRecord(ContactRecord contactRecord) throws Exception;

	/**
	 * 修改
	 * 
	 * @param contactRecord
	 * @return
	 * @throws Exception
	 */
	public int updateContactRecord(ContactRecord contactRecord) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteContactRecord(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param contactId
	 * @return
	 */
	public ContactRecord selectContactRecordByContactId(String contactId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<ContactRecord> selectContactRecordList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<ContactRecord> selectAllContactRecordList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectContactRecordListCount(Map<String, Object> map) throws Exception;

}