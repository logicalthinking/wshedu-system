package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.DeptInfo;
import com.logicalthinking.manage.system.vo.DeptTreeVo;

/**
 * 部门信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface DeptInfoMapper {

	/**
	 * 新增
	 * 
	 * @param deptInfo
	 * @return
	 * @throws Exception
	 */
	public int insertDeptInfo(DeptInfo deptInfo) throws Exception;

	/**
	 * 修改
	 * 
	 * @param deptInfo
	 * @return
	 * @throws Exception
	 */
	public int updateDeptInfo(DeptInfo deptInfo) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteDeptInfo(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param deptId
	 * @return
	 */
	public DeptInfo selectDeptInfoByDeptId(String deptId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<DeptInfo> selectDeptInfoList(Map<String, Object> map) throws Exception;
	
	/**
	 * 查询组织结构树
	 * 
	 * @param map
	 * @return
	 */
	public List<DeptTreeVo> selectDeptInfoTreeList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<DeptInfo> selectAllDeptInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectDeptInfoListCount(Map<String, Object> map) throws Exception;

}