package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.DictInfo;
import com.logicalthinking.manage.system.vo.DictTreeVo;
import com.logicalthinking.manage.system.vo.KeyValue;

/**
 * 字典信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public interface DictInfoMapper {

	/**
	 * 新增
	 * 
	 * @param dictInfo
	 * @return
	 * @throws Exception
	 */
	public int insertDictInfo(DictInfo dictInfo) throws Exception;

	/**
	 * 修改
	 * 
	 * @param dictInfo
	 * @return
	 * @throws Exception
	 */
	public int updateDictInfo(DictInfo dictInfo) throws Exception;
	
	
	/**
	 * 修改
	 * 
	 * @param dictInfo
	 * @return
	 * @throws Exception
	 */
	public int updateDictInfoByMap(Map<String,Object> map) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteDictInfo(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param dictId
	 * @return
	 */
	public DictInfo selectDictInfoByDictId(String dictId) throws Exception;
	
	/**
	 * 查询集合
	 * 
	 * @param dictPid 父级ID
	 * @return
	 */
	public List<DictTreeVo> selectDictTreeVoByDictPid(String dictPid) throws Exception;
	
	/**
	 * 查询下级集合
	 * @param dictType 类型
	 * @param dictValue 字典值
	 * @return
	 * @throws Exception
	 */
	public List<KeyValue> selectDictChildren(Map<String, Object> map) throws Exception;
	
	/**
	 * 查询单个
	 * @param dictType 类型
	 * @param dictValue 字典值
	 * @return
	 * @throws Exception
	 */
	public DictInfo selectDictInfoByTypeValue(Map<String, Object> map) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<DictInfo> selectDictInfoList(Map<String, Object> map) throws Exception;
	
	/**
	 *查询字典数据
	 * 
	 * @param map
	 * @return
	 */
	public List<DictTreeVo> selectDictTreeVoList(Map<String, Object> map) throws Exception;


	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<DictInfo> selectAllDictInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectDictInfoListCount(Map<String, Object> map) throws Exception;

}