package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.EmpDeptLink;

/**
 * 员工部门关联信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface EmpDeptLinkMapper {

	/**
	 * 新增
	 * 
	 * @param empDeptLink
	 * @return
	 * @throws Exception
	 */
	public int insertEmpDeptLink(EmpDeptLink empDeptLink) throws Exception;

	/**
	 * 修改
	 * 
	 * @param empDeptLink
	 * @return
	 * @throws Exception
	 */
	public int updateEmpDeptLink(EmpDeptLink empDeptLink) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteEmpDeptLink(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param linkId
	 * @return
	 */
	public EmpDeptLink selectEmpDeptLinkByLinkId(String linkId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<EmpDeptLink> selectEmpDeptLinkList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<EmpDeptLink> selectAllEmpDeptLinkList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectEmpDeptLinkListCount(Map<String, Object> map) throws Exception;

}