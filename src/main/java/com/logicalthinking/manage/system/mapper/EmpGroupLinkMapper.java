package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.EmpGroupLink;

/**
 * 员工分组关联信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface EmpGroupLinkMapper {

	/**
	 * 新增
	 * 
	 * @param empGroupLink
	 * @return
	 * @throws Exception
	 */
	public int insertEmpGroupLink(EmpGroupLink empGroupLink) throws Exception;

	/**
	 * 修改
	 * 
	 * @param empGroupLink
	 * @return
	 * @throws Exception
	 */
	public int updateEmpGroupLink(EmpGroupLink empGroupLink) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteEmpGroupLink(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param linkId
	 * @return
	 */
	public EmpGroupLink selectEmpGroupLinkByLinkId(String linkId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<EmpGroupLink> selectEmpGroupLinkList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<EmpGroupLink> selectAllEmpGroupLinkList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectEmpGroupLinkListCount(Map<String, Object> map) throws Exception;

}