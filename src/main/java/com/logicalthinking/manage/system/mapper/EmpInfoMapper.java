package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.EmpInfo;

/**
 * 员工信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface EmpInfoMapper {

	/**
	 * 新增
	 * 
	 * @param empInfo
	 * @return
	 * @throws Exception
	 */
	public int insertEmpInfo(EmpInfo empInfo) throws Exception;

	/**
	 * 修改
	 * 
	 * @param empInfo
	 * @return
	 * @throws Exception
	 */
	public int updateEmpInfo(EmpInfo empInfo) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteEmpInfo(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param empId
	 * @return
	 */
	public EmpInfo selectEmpInfoByEmpId(String empId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<EmpInfo> selectEmpInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<EmpInfo> selectAllEmpInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectEmpInfoListCount(Map<String, Object> map) throws Exception;

}