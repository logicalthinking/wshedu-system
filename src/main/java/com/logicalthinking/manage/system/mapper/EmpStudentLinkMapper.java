package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.EmpStudentLink;

/**
 * 员工学员关联信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface EmpStudentLinkMapper {

	/**
	 * 新增
	 * 
	 * @param empStudentLink
	 * @return
	 * @throws Exception
	 */
	public int insertEmpStudentLink(EmpStudentLink empStudentLink) throws Exception;

	/**
	 * 修改
	 * 
	 * @param empStudentLink
	 * @return
	 * @throws Exception
	 */
	public int updateEmpStudentLink(EmpStudentLink empStudentLink) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteEmpStudentLink(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param linkId
	 * @return
	 */
	public EmpStudentLink selectEmpStudentLinkByLinkId(String linkId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<EmpStudentLink> selectEmpStudentLinkList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<EmpStudentLink> selectAllEmpStudentLinkList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectEmpStudentLinkListCount(Map<String, Object> map) throws Exception;

}