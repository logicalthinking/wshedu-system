package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.ExamRegisterInfo;

/**
 * 报考信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface ExamRegisterInfoMapper {

	/**
	 * 新增
	 * 
	 * @param examRegisterInfo
	 * @return
	 * @throws Exception
	 */
	public int insertExamRegisterInfo(ExamRegisterInfo examRegisterInfo) throws Exception;

	/**
	 * 修改
	 * 
	 * @param examRegisterInfo
	 * @return
	 * @throws Exception
	 */
	public int updateExamRegisterInfo(ExamRegisterInfo examRegisterInfo) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteExamRegisterInfo(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param regId
	 * @return
	 */
	public ExamRegisterInfo selectExamRegisterInfoByRegId(String regId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<ExamRegisterInfo> selectExamRegisterInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<ExamRegisterInfo> selectAllExamRegisterInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectExamRegisterInfoListCount(Map<String, Object> map) throws Exception;

}