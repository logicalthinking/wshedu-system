package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.GroupInfo;

/**
 * 分组信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface GroupInfoMapper {

	/**
	 * 新增
	 * 
	 * @param groupInfo
	 * @return
	 * @throws Exception
	 */
	public int insertGroupInfo(GroupInfo groupInfo) throws Exception;

	/**
	 * 修改
	 * 
	 * @param groupInfo
	 * @return
	 * @throws Exception
	 */
	public int updateGroupInfo(GroupInfo groupInfo) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteGroupInfo(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param groupId
	 * @return
	 */
	public GroupInfo selectGroupInfoByGroupId(String groupId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<GroupInfo> selectGroupInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<GroupInfo> selectAllGroupInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectGroupInfoListCount(Map<String, Object> map) throws Exception;

}