package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.LeaveMessage;

/**
 * 留言信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public interface LeaveMessageMapper {

	/**
	 * 新增
	 * 
	 * @param leaveMessage
	 * @return
	 * @throws Exception
	 */
	public int insertLeaveMessage(LeaveMessage leaveMessage) throws Exception;

	/**
	 * 修改
	 * 
	 * @param leaveMessage
	 * @return
	 * @throws Exception
	 */
	public int updateLeaveMessage(LeaveMessage leaveMessage) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteLeaveMessage(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param msgId
	 * @return
	 */
	public LeaveMessage selectLeaveMessageByMsgId(String msgId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<LeaveMessage> selectLeaveMessageList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<LeaveMessage> selectAllLeaveMessageList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectLeaveMessageListCount(Map<String, Object> map) throws Exception;

}