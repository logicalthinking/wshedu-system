package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.StudentInfo;

/**
 * 学员信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface StudentInfoMapper {

	/**
	 * 新增
	 * 
	 * @param studentInfo
	 * @return
	 * @throws Exception
	 */
	public int insertStudentInfo(StudentInfo studentInfo) throws Exception;

	/**
	 * 修改
	 * 
	 * @param studentInfo
	 * @return
	 * @throws Exception
	 */
	public int updateStudentInfo(StudentInfo studentInfo) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteStudentInfo(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param stuId
	 * @return
	 */
	public StudentInfo selectStudentInfoByStuId(String stuId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<StudentInfo> selectStudentInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<StudentInfo> selectAllStudentInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectStudentInfoListCount(Map<String, Object> map) throws Exception;

}