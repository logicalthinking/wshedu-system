package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.SysLogInfo;

/**
 * 系统日志信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
public interface SysLogInfoMapper {

	/**
	 * 新增
	 * 
	 * @param sysLogInfo
	 * @return
	 * @throws Exception
	 */
	public int insertSysLogInfo(SysLogInfo sysLogInfo) throws Exception;

	/**
	 * 修改
	 * 
	 * @param sysLogInfo
	 * @return
	 * @throws Exception
	 */
	public int updateSysLogInfo(SysLogInfo sysLogInfo) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteSysLogInfo(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param logId
	 * @return
	 */
	public SysLogInfo selectSysLogInfoByLogId(String logId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<SysLogInfo> selectSysLogInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<SysLogInfo> selectAllSysLogInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectSysLogInfoListCount(Map<String, Object> map) throws Exception;

}