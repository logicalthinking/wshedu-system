package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.SysModuleLink;

/**
 * 模块关联业务信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public interface SysModuleLinkMapper {

	/**
	 * 新增
	 * 
	 * @param sysModuleLink
	 * @return
	 * @throws Exception
	 */
	public int insertSysModuleLink(SysModuleLink sysModuleLink) throws Exception;

	/**
	 * 修改
	 * 
	 * @param sysModuleLink
	 * @return
	 * @throws Exception
	 */
	public int updateSysModuleLink(SysModuleLink sysModuleLink) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteSysModuleLink(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param linkId
	 * @return
	 */
	public SysModuleLink selectSysModuleLinkByLinkId(String linkId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<SysModuleLink> selectSysModuleLinkList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<SysModuleLink> selectAllSysModuleLinkList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectSysModuleLinkListCount(Map<String, Object> map) throws Exception;

}