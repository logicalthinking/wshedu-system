package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.SysModule;
import com.logicalthinking.manage.system.vo.ModuleTreeVo;

/**
 * 系统模块信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
public interface SysModuleMapper {

	/**
	 * 新增
	 * 
	 * @param sysModule
	 * @return
	 * @throws Exception
	 */
	public int insertSysModule(SysModule sysModule) throws Exception;

	/**
	 * 修改
	 * 
	 * @param sysModule
	 * @return
	 * @throws Exception
	 */
	public int updateSysModule(SysModule sysModule) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteSysModule(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param moduleId
	 * @return
	 */
	public SysModule selectSysModuleByModuleId(String moduleId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<SysModule> selectSysModuleList(Map<String, Object> map) throws Exception;
	
	/**
	 * 查询权限设置 模块树形数据
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public List<ModuleTreeVo> selectModuleTreeVoList(Map<String, Object> map) throws Exception;
	
	/**
	 * 查询当前用户 菜单
	 * 
	 * @param map
	 * @return
	 */
	public List<ModuleTreeVo> selectUserModuleMenuTreeList(Map<String, Object> map) throws Exception;
	
	/**
	 * 查询特定页面的用户操作模块
	 * 
	 * @param map
	 * @return
	 */
	public List<ModuleTreeVo> selectModuleTreeListByOnePage(Map<String, Object> map) throws Exception;
	
	/**
	 * 查询全部模块 树形集合
	 * 
	 * @param map
	 * @return
	 */
	public List<ModuleTreeVo> selectAllModuleMenuTreeList(Map<String, Object> map) throws Exception;
	
	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<SysModule> selectAllSysModuleList(Map<String, Object> map) throws Exception;
	
	/**
	 * 校验访问页面是否有权限
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public List<SysModule> selectModuleListByValid(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectSysModuleListCount(Map<String, Object> map) throws Exception;

}