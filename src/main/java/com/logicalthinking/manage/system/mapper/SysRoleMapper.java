package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.SysRole;

/**
 * 角色信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
public interface SysRoleMapper {

	/**
	 * 新增
	 * 
	 * @param sysRole
	 * @return
	 * @throws Exception
	 */
	public int insertSysRole(SysRole sysRole) throws Exception;

	/**
	 * 修改
	 * 
	 * @param sysRole
	 * @return
	 * @throws Exception
	 */
	public int updateSysRole(SysRole sysRole) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteSysRole(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param roleId
	 * @return
	 */
	public SysRole selectSysRoleByRoleId(String roleId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<SysRole> selectSysRoleList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<SysRole> selectAllSysRoleList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectSysRoleListCount(Map<String, Object> map) throws Exception;

}