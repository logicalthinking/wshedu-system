package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.SysUserRole;

/**
 * 用户关联角色信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-16
 */
public interface SysUserRoleMapper {

	/**
	 * 新增
	 * 
	 * @param sysUserRole
	 * @return
	 * @throws Exception
	 */
	public int insertSysUserRole(SysUserRole sysUserRole) throws Exception;

	/**
	 * 修改
	 * 
	 * @param sysUserRole
	 * @return
	 * @throws Exception
	 */
	public int updateSysUserRole(SysUserRole sysUserRole) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteSysUserRole(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param userRoleId
	 * @return
	 */
	public SysUserRole selectSysUserRoleByUserRoleId(String userRoleId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<SysUserRole> selectSysUserRoleList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<SysUserRole> selectAllSysUserRoleList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectSysUserRoleListCount(Map<String, Object> map) throws Exception;

}