package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.SysUsers;

/**
 * 用户信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-12
 */
public interface SysUsersMapper {

	/**
	 * 新增
	 * 
	 * @param sysUsers
	 * @return
	 * @throws Exception
	 */
	public int insertSysUsers(SysUsers sysUsers) throws Exception;

	/**
	 * 修改
	 * 
	 * @param sysUsers
	 * @return
	 * @throws Exception
	 */
	public int updateSysUsers(SysUsers sysUsers) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteSysUsers(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param userId
	 * @return
	 */
	public SysUsers selectSysUsersByUserId(String userId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<SysUsers> selectSysUsersList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<SysUsers> selectAllSysUsersList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectSysUsersListCount(Map<String, Object> map) throws Exception;

}