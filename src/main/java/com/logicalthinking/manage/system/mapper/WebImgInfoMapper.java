package com.logicalthinking.manage.system.mapper;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.WebImgInfo;

/**
 * 网站图片信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public interface WebImgInfoMapper {

	/**
	 * 新增
	 * 
	 * @param webImgInfo
	 * @return
	 * @throws Exception
	 */
	public int insertWebImgInfo(WebImgInfo webImgInfo) throws Exception;

	/**
	 * 修改
	 * 
	 * @param webImgInfo
	 * @return
	 * @throws Exception
	 */
	public int updateWebImgInfo(WebImgInfo webImgInfo) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteWebImgInfo(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param imgId
	 * @return
	 */
	public WebImgInfo selectWebImgInfoByImgId(String imgId) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<WebImgInfo> selectWebImgInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<WebImgInfo> selectAllWebImgInfoList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectWebImgInfoListCount(Map<String, Object> map) throws Exception;

}