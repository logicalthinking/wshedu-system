package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.ArticleInfo;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 文案信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public interface ArticleInfoService {

	/**
	 * 新增
	 * 
	 * @param articleInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertArticleInfo(ArticleInfo articleInfo) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param articleInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateArticleInfo(ArticleInfo articleInfo) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteArticleInfo(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param articleId
	 * @return
	 * @throws DaoException
	 */
	public ArticleInfo selectArticleInfoByArticleId(String articleId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<ArticleInfo> selectArticleInfoList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<ArticleInfo> selectAllArticleInfoList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectArticleInfoListCount(Map<String, Object> map) throws DaoException;

}