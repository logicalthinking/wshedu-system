package com.logicalthinking.manage.system.service;

/**
 * 缓存操作接口
 * @author lanping
 * @version 1.0
 * @date 2019-10-21 下午7:08:26
 */
public interface CacheService {

	/**
	 * 初始化字典缓存
	 */
	public void initDictCache() throws Exception;
	
	/**
	 * 清除字典缓存
	 */
	public void clearDictCache() throws Exception;
	
	/**
	 * 初始化模块缓存
	 */
	public void initModuleCache() throws Exception;
	
	/**
	 * 清除模块缓存
	 */
	public void clearModuleCache() throws Exception;
}
