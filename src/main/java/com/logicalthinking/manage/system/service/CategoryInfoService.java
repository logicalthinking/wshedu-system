package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.CategoryInfo;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 分类管理接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public interface CategoryInfoService {

	/**
	 * 新增
	 * 
	 * @param categoryInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertCategoryInfo(CategoryInfo categoryInfo) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param categoryInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateCategoryInfo(CategoryInfo categoryInfo) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteCategoryInfo(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param typeId
	 * @return
	 * @throws DaoException
	 */
	public CategoryInfo selectCategoryInfoByTypeId(String typeId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<CategoryInfo> selectCategoryInfoList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<CategoryInfo> selectAllCategoryInfoList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectCategoryInfoListCount(Map<String, Object> map) throws DaoException;

}