package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.ContactRecord;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 学员联系记录接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface ContactRecordService {

	/**
	 * 新增
	 * 
	 * @param contactRecord
	 * @return
	 * @throws DaoException
	 */
	public int insertContactRecord(ContactRecord contactRecord) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param contactRecord
	 * @return
	 * @throws DaoException
	 */
	public int updateContactRecord(ContactRecord contactRecord) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteContactRecord(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param contactId
	 * @return
	 * @throws DaoException
	 */
	public ContactRecord selectContactRecordByContactId(String contactId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<ContactRecord> selectContactRecordList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<ContactRecord> selectAllContactRecordList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectContactRecordListCount(Map<String, Object> map) throws DaoException;

}