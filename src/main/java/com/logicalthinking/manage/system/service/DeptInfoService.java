package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.DeptInfo;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.vo.DeptTreeVo;

/**
 * 部门信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface DeptInfoService {

	/**
	 * 新增
	 * 
	 * @param deptInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertDeptInfo(DeptInfo deptInfo) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param deptInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateDeptInfo(DeptInfo deptInfo) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteDeptInfo(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param deptId
	 * @return
	 * @throws DaoException
	 */
	public DeptInfo selectDeptInfoByDeptId(String deptId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<DeptInfo> selectDeptInfoList(Map<String, Object> map) throws DaoException, ValidateException;
	
	/**
	 * 查询组织结构树
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<DeptTreeVo> selectDeptInfoTreeList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<DeptInfo> selectAllDeptInfoList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectDeptInfoListCount(Map<String, Object> map) throws DaoException;

}