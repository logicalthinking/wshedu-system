package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.DictInfo;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.vo.DictTreeVo;
import com.logicalthinking.manage.system.vo.KeyValue;

/**
 * 字典信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public interface DictInfoService {

	/**
	 * 新增
	 * 
	 * @param dictInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertDictInfo(DictInfo dictInfo) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param dictInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateDictInfo(DictInfo dictInfo) throws DaoException;
	
	/**
	 * 修改
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int updateDictInfo(Map<String,Object> map) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteDictInfo(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param dictId
	 * @return
	 * @throws DaoException
	 */
	public DictInfo selectDictInfoByDictId(String dictId) throws DaoException;
	
	
	/**
	 * 查询集合
	 * 
	 * @param dictPid 父级ID
	 * @return
	 */
	public List<DictTreeVo> selectDictTreeVoByDictPid(String dictPid) throws DaoException;
	
	
	/**
	 * 查询下级集合
	 * @param dictType 类型
	 * @param dictValue 字典值
	 * @return
	 * @throws Exception
	 */
	public List<KeyValue> selectDictChildren(String dictType,String dictValue) throws DaoException;
	
	/**
	 * 查询下级集合 实际还是dictType,dictValue两个参数
	 * @param dictType 类型
	 * @return
	 * @throws Exception
	 */
	public List<KeyValue> selectDictChildren(String dictType) throws DaoException;
	
	/**
	 * 查询下级集合
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<KeyValue> selectDictChildren(Map<String,Object> map) throws DaoException;
	
	/**
	 * 查询单个
	 * @param dictType 类型
	 * @param dictValue 字典值
	 * @return
	 * @throws Exception
	 */
	public DictInfo selectDictInfoByTypeValue(String dictType,String dictValue) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<DictInfo> selectDictInfoList(Map<String, Object> map) throws DaoException, ValidateException;

	
	/**
	 *查询字典数据
	 * 
	 * @param map
	 * @return
	 */
	public List<DictTreeVo> selectDictTreeVoList(Map<String, Object> map) throws DaoException;
	
	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<DictInfo> selectAllDictInfoList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectDictInfoListCount(Map<String, Object> map) throws DaoException;

}