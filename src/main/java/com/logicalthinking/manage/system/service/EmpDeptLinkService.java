package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.EmpDeptLink;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 员工部门关联信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface EmpDeptLinkService {

	/**
	 * 新增
	 * 
	 * @param empDeptLink
	 * @return
	 * @throws DaoException
	 */
	public int insertEmpDeptLink(EmpDeptLink empDeptLink) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param empDeptLink
	 * @return
	 * @throws DaoException
	 */
	public int updateEmpDeptLink(EmpDeptLink empDeptLink) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteEmpDeptLink(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param linkId
	 * @return
	 * @throws DaoException
	 */
	public EmpDeptLink selectEmpDeptLinkByLinkId(String linkId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<EmpDeptLink> selectEmpDeptLinkList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<EmpDeptLink> selectAllEmpDeptLinkList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectEmpDeptLinkListCount(Map<String, Object> map) throws DaoException;

}