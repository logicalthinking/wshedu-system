package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.EmpGroupLink;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 员工分组关联信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface EmpGroupLinkService {

	/**
	 * 新增
	 * 
	 * @param empGroupLink
	 * @return
	 * @throws DaoException
	 */
	public int insertEmpGroupLink(EmpGroupLink empGroupLink) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param empGroupLink
	 * @return
	 * @throws DaoException
	 */
	public int updateEmpGroupLink(EmpGroupLink empGroupLink) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteEmpGroupLink(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param linkId
	 * @return
	 * @throws DaoException
	 */
	public EmpGroupLink selectEmpGroupLinkByLinkId(String linkId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<EmpGroupLink> selectEmpGroupLinkList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<EmpGroupLink> selectAllEmpGroupLinkList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectEmpGroupLinkListCount(Map<String, Object> map) throws DaoException;

}