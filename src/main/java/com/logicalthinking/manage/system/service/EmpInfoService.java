package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.EmpInfo;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 员工信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface EmpInfoService {

	/**
	 * 新增
	 * 
	 * @param empInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertEmpInfo(EmpInfo empInfo) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param empInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateEmpInfo(EmpInfo empInfo) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteEmpInfo(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param empId
	 * @return
	 * @throws DaoException
	 */
	public EmpInfo selectEmpInfoByEmpId(String empId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<EmpInfo> selectEmpInfoList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<EmpInfo> selectAllEmpInfoList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectEmpInfoListCount(Map<String, Object> map) throws DaoException;

}