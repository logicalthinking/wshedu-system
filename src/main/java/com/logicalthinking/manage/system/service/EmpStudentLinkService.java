package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.EmpStudentLink;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 员工学员关联信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface EmpStudentLinkService {

	/**
	 * 新增
	 * 
	 * @param empStudentLink
	 * @return
	 * @throws DaoException
	 */
	public int insertEmpStudentLink(EmpStudentLink empStudentLink) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param empStudentLink
	 * @return
	 * @throws DaoException
	 */
	public int updateEmpStudentLink(EmpStudentLink empStudentLink) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteEmpStudentLink(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param linkId
	 * @return
	 * @throws DaoException
	 */
	public EmpStudentLink selectEmpStudentLinkByLinkId(String linkId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<EmpStudentLink> selectEmpStudentLinkList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<EmpStudentLink> selectAllEmpStudentLinkList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectEmpStudentLinkListCount(Map<String, Object> map) throws DaoException;

}