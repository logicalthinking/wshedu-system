package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.ExamRegisterInfo;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 报考信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface ExamRegisterInfoService {

	/**
	 * 新增
	 * 
	 * @param examRegisterInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertExamRegisterInfo(ExamRegisterInfo examRegisterInfo) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param examRegisterInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateExamRegisterInfo(ExamRegisterInfo examRegisterInfo) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteExamRegisterInfo(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param regId
	 * @return
	 * @throws DaoException
	 */
	public ExamRegisterInfo selectExamRegisterInfoByRegId(String regId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<ExamRegisterInfo> selectExamRegisterInfoList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<ExamRegisterInfo> selectAllExamRegisterInfoList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectExamRegisterInfoListCount(Map<String, Object> map) throws DaoException;

}