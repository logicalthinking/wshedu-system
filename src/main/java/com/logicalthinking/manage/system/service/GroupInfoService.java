package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.GroupInfo;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 分组信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface GroupInfoService {

	/**
	 * 新增
	 * 
	 * @param groupInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertGroupInfo(GroupInfo groupInfo) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param groupInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateGroupInfo(GroupInfo groupInfo) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteGroupInfo(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param groupId
	 * @return
	 * @throws DaoException
	 */
	public GroupInfo selectGroupInfoByGroupId(String groupId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<GroupInfo> selectGroupInfoList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<GroupInfo> selectAllGroupInfoList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectGroupInfoListCount(Map<String, Object> map) throws DaoException;

}