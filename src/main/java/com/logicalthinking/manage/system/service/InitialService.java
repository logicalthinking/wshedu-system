package com.logicalthinking.manage.system.service;

/**
 * 初始化service
 * @author lanping
 * @version 1.0
 * @date 2019-10-22 上午8:52:18
 */
public interface InitialService {

	/**
	 * 创建AccessKey
	 */
	public void createAccessKey() throws Exception;
	
	/**
	 * 初始化缓存
	 * @throws Exception 
	 */
	public void initCache() throws Exception;
}
