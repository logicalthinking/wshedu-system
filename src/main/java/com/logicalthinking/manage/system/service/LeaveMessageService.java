package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.LeaveMessage;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 留言信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public interface LeaveMessageService {

	/**
	 * 新增
	 * 
	 * @param leaveMessage
	 * @return
	 * @throws DaoException
	 */
	public int insertLeaveMessage(LeaveMessage leaveMessage) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param leaveMessage
	 * @return
	 * @throws DaoException
	 */
	public int updateLeaveMessage(LeaveMessage leaveMessage) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteLeaveMessage(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param msgId
	 * @return
	 * @throws DaoException
	 */
	public LeaveMessage selectLeaveMessageByMsgId(String msgId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<LeaveMessage> selectLeaveMessageList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<LeaveMessage> selectAllLeaveMessageList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectLeaveMessageListCount(Map<String, Object> map) throws DaoException;

}