package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.StudentInfo;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 学员信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
public interface StudentInfoService {

	/**
	 * 新增
	 * 
	 * @param studentInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertStudentInfo(StudentInfo studentInfo) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param studentInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateStudentInfo(StudentInfo studentInfo) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteStudentInfo(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param stuId
	 * @return
	 * @throws DaoException
	 */
	public StudentInfo selectStudentInfoByStuId(String stuId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<StudentInfo> selectStudentInfoList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<StudentInfo> selectAllStudentInfoList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectStudentInfoListCount(Map<String, Object> map) throws DaoException;

}