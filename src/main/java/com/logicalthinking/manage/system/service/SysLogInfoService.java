package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.SysLogInfo;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 系统日志信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
public interface SysLogInfoService {

	/**
	 * 新增
	 * 
	 * @param sysLogInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertSysLogInfo(SysLogInfo sysLogInfo) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param sysLogInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateSysLogInfo(SysLogInfo sysLogInfo) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysLogInfo(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param logId
	 * @return
	 * @throws DaoException
	 */
	public SysLogInfo selectSysLogInfoByLogId(String logId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<SysLogInfo> selectSysLogInfoList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<SysLogInfo> selectAllSysLogInfoList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectSysLogInfoListCount(Map<String, Object> map) throws DaoException;

}