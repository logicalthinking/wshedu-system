package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.SysModuleLink;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 模块关联业务信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public interface SysModuleLinkService {

	/**
	 * 新增
	 * 
	 * @param sysModuleLink
	 * @return
	 * @throws DaoException
	 */
	public int insertSysModuleLink(SysModuleLink sysModuleLink) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param sysModuleLink
	 * @return
	 * @throws DaoException
	 */
	public int updateSysModuleLink(SysModuleLink sysModuleLink) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysModuleLink(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param linkId
	 * @return
	 * @throws DaoException
	 */
	public SysModuleLink selectSysModuleLinkByLinkId(String linkId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<SysModuleLink> selectSysModuleLinkList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<SysModuleLink> selectAllSysModuleLinkList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectSysModuleLinkListCount(Map<String, Object> map) throws DaoException;

}