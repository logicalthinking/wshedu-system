package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.SysRole;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 角色信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
public interface SysRoleService {

	/**
	 * 新增
	 * 
	 * @param sysRole
	 * @return
	 * @throws DaoException
	 */
	public int insertSysRole(SysRole sysRole) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param sysRole
	 * @return
	 * @throws DaoException
	 */
	public int updateSysRole(SysRole sysRole) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysRole(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param roleId
	 * @return
	 * @throws DaoException
	 */
	public SysRole selectSysRoleByRoleId(String roleId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<SysRole> selectSysRoleList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<SysRole> selectAllSysRoleList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectSysRoleListCount(Map<String, Object> map) throws DaoException;

}