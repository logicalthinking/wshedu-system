package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.SysUserRole;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 用户关联角色信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-16
 */
public interface SysUserRoleService {

	/**
	 * 新增
	 * 
	 * @param sysUserRole
	 * @return
	 * @throws DaoException
	 */
	public int insertSysUserRole(SysUserRole sysUserRole) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param sysUserRole
	 * @return
	 * @throws DaoException
	 */
	public int updateSysUserRole(SysUserRole sysUserRole) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysUserRole(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param userRoleId
	 * @return
	 * @throws DaoException
	 */
	public SysUserRole selectSysUserRoleByUserRoleId(String userRoleId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<SysUserRole> selectSysUserRoleList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<SysUserRole> selectAllSysUserRoleList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectSysUserRoleListCount(Map<String, Object> map) throws DaoException;

}