package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.SysUsers;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 用户信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-12
 */
public interface SysUsersService {

	/**
	 * 新增
	 * 
	 * @param sysUsers
	 * @return
	 * @throws DaoException
	 */
	public int insertSysUsers(SysUsers sysUsers) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param sysUsers
	 * @return
	 * @throws DaoException
	 */
	public int updateSysUsers(SysUsers sysUsers) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysUsers(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param userId
	 * @return
	 * @throws DaoException
	 */
	public SysUsers selectSysUsersByUserId(String userId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<SysUsers> selectSysUsersList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<SysUsers> selectAllSysUsersList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectSysUsersListCount(Map<String, Object> map) throws DaoException;

}