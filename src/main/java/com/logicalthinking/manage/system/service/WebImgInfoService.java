package com.logicalthinking.manage.system.service;

import java.util.List;
import java.util.Map;

import com.logicalthinking.manage.system.entity.WebImgInfo;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 网站图片信息接口
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
public interface WebImgInfoService {

	/**
	 * 新增
	 * 
	 * @param webImgInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertWebImgInfo(WebImgInfo webImgInfo) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param webImgInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateWebImgInfo(WebImgInfo webImgInfo) throws DaoException;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteWebImgInfo(Map<String, Object> map) throws DaoException;

	/**
	 * 查询单个
	 * 
	 * @param imgId
	 * @return
	 * @throws DaoException
	 */
	public WebImgInfo selectWebImgInfoByImgId(String imgId) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<WebImgInfo> selectWebImgInfoList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<WebImgInfo> selectAllWebImgInfoList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectWebImgInfoListCount(Map<String, Object> map) throws DaoException;

}