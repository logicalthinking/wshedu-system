package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.ArticleInfo;
import com.logicalthinking.manage.system.mapper.ArticleInfoMapper;
import com.logicalthinking.manage.system.service.ArticleInfoService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 文案信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Service
public class ArticleInfoServiceImpl implements ArticleInfoService {

	@Autowired
	private ArticleInfoMapper articleInfoMapper;
	/**
	 * 新增
	 * 
	 * @param articleInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertArticleInfo(ArticleInfo articleInfo) throws DaoException {
		int result = 0;
		try {
			result = articleInfoMapper.insertArticleInfo(articleInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param articleInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateArticleInfo(ArticleInfo articleInfo) throws DaoException {
		int result = 0;
		try {
			result = articleInfoMapper.updateArticleInfo(articleInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteArticleInfo(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = articleInfoMapper.deleteArticleInfo(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param articleId
	 * @return
	 * @throws DaoException
	 */
	public ArticleInfo selectArticleInfoByArticleId(String articleId) throws DaoException {
		ArticleInfo articleInfo = null;
		try {
			articleInfo = articleInfoMapper.selectArticleInfoByArticleId(articleId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return articleInfo;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<ArticleInfo> selectArticleInfoList(Map<String, Object> map) throws DaoException, ValidateException {
		List<ArticleInfo> articleInfos = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			articleInfos = articleInfoMapper.selectArticleInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return articleInfos;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<ArticleInfo> selectAllArticleInfoList(Map<String, Object> map) throws DaoException {
		List<ArticleInfo> articleInfos = null;
		try {
			articleInfos = articleInfoMapper.selectAllArticleInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return articleInfos;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectArticleInfoListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = articleInfoMapper.selectArticleInfoListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}