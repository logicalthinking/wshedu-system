package com.logicalthinking.manage.system.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.logicalthinking.manage.system.entity.DictInfo;
import com.logicalthinking.manage.system.entity.SysRole;
import com.logicalthinking.manage.system.mapper.DictInfoMapper;
import com.logicalthinking.manage.system.mapper.SysRoleMapper;
import com.logicalthinking.manage.system.service.CacheService;
import com.logicalthinking.manage.system.service.SysModuleService;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.RedisUtils;
import com.logicalthinking.manage.system.vo.DictTreeVo;
import com.logicalthinking.manage.system.vo.ModuleTreeVo;

/**
 * 缓存操作接口实现类
 * @author lanping
 * @version 1.0
 * @date 2019-10-21 下午7:08:26
 */
@Service
public class CacheServiceImpl implements CacheService{
	
	@Resource
	private RedisUtils redisUtils;
	
	@Autowired
	private DictInfoMapper dictInfoMapper;
	
	@Autowired
	private SysRoleMapper sysRoleMapper;
	
	@Autowired
	private SysModuleService sysModuleService;
	
	/**
	 * 初始化字典缓存
	 * @throws Exception 
	 */
	public void initDictCache() throws Exception{
		//查询字典
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("state", ConstantUtil.IS_VALID_Y);
		List<DictInfo> dictInfos = dictInfoMapper.selectAllDictInfoList(map);
		if(dictInfos!=null && dictInfos.size()>0){
			for(DictInfo dictInfo: dictInfos){
				String key = dictInfo.getDictType()+"_"+dictInfo.getDictValue();
				redisUtils.hset(ConstantUtil.REDIS_KEY_DICT_MAP, key, dictInfo.getDictName());
			}
		}
		
		map.put("dictPid", "-1");
		List<DictTreeVo> dictTreeVos = dictInfoMapper.selectDictTreeVoList(map);
		redisUtils.set(ConstantUtil.REDIS_KEY_DICT_TREE_LIST, JSONArray.toJSONString(dictTreeVos));
	}
	
	/**
	 * 初始化模块缓存
	 */
	public void initModuleCache() throws Exception{
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("state", ConstantUtil.IS_VALID_Y);
		List<SysRole> sysRoles = sysRoleMapper.selectAllSysRoleList(map);
		if(sysRoles!=null && sysRoles.size()>0){
			for(SysRole sysRole: sysRoles){
				String key = sysRole.getRoleId()+ConstantUtil.REDIS_KEY_USER_MODULES_SUFFX;
				List<ModuleTreeVo> moduleTreeVos = null;
				//查询用户角色信息
				if(ConstantUtil.ROLE_CODE_SYS_MANAGER.equals(sysRole.getRoleCode())){
					map.put("parentId", "0");
					//排除公共模块
					map.put("excludeModules", ConstantUtil.public_modules);
					moduleTreeVos = sysModuleService.selectAllModuleMenuTreeList(map);
				}else{
					map.put("parentId", "0");
					map.put("busiIds", sysRole.getRoleId());
					moduleTreeVos = sysModuleService.selectUserModuleMenuTreeList(map);
				}
				redisUtils.set(key, JSONArray.toJSONString(moduleTreeVos));
			}
		}
		
		//查询所有的模块
		map.clear();
		map.put("parentId", "-1");
		List<ModuleTreeVo> allModuleTreeVos = sysModuleService.selectAllModuleMenuTreeList(map);
		redisUtils.set(ConstantUtil.REDIS_KEY_ALL_MODULE_MENUS, JSONArray.toJSONString(allModuleTreeVos));
		
		//查询权限模块
		map.clear();
		map.put("parentId", "0");
		List<ModuleTreeVo> perModuleTreeVos = sysModuleService.selectModuleTreeVoList(map);
		redisUtils.set(ConstantUtil.REDIS_KEY_PERMISSION_MODULE_MENUS, JSONArray.toJSONString(perModuleTreeVos));
	}
	
	/**
	 * 清除字典缓存
	 */
	public void clearDictCache() throws Exception{
		
		redisUtils.hdel(ConstantUtil.REDIS_KEY_DICT_MAP);
		redisUtils.del(ConstantUtil.REDIS_KEY_DICT_TREE_LIST);
	}
	
	/**
	 * 清除模块缓存
	 */
	public void clearModuleCache() throws Exception{
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("state", ConstantUtil.IS_VALID_Y);
		List<SysRole> sysRoles = sysRoleMapper.selectAllSysRoleList(map);
		if(sysRoles!=null && sysRoles.size()>0){
			for(SysRole sysRole: sysRoles){
				String key = sysRole.getRoleId()+ConstantUtil.REDIS_KEY_USER_MODULES_SUFFX;
				redisUtils.del(key);
			}
		}
		
		redisUtils.del(ConstantUtil.REDIS_KEY_ALL_MODULE_MENUS);
		redisUtils.del(ConstantUtil.REDIS_KEY_PERMISSION_MODULE_MENUS);
	}
}
