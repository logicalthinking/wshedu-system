package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.CategoryInfo;
import com.logicalthinking.manage.system.mapper.CategoryInfoMapper;
import com.logicalthinking.manage.system.service.CategoryInfoService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 分类管理实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Service
public class CategoryInfoServiceImpl implements CategoryInfoService {

	@Autowired
	private CategoryInfoMapper categoryInfoMapper;
	/**
	 * 新增
	 * 
	 * @param categoryInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertCategoryInfo(CategoryInfo categoryInfo) throws DaoException {
		int result = 0;
		try {
			result = categoryInfoMapper.insertCategoryInfo(categoryInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param categoryInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateCategoryInfo(CategoryInfo categoryInfo) throws DaoException {
		int result = 0;
		try {
			result = categoryInfoMapper.updateCategoryInfo(categoryInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteCategoryInfo(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = categoryInfoMapper.deleteCategoryInfo(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param typeId
	 * @return
	 * @throws DaoException
	 */
	public CategoryInfo selectCategoryInfoByTypeId(String typeId) throws DaoException {
		CategoryInfo categoryInfo = null;
		try {
			categoryInfo = categoryInfoMapper.selectCategoryInfoByTypeId(typeId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return categoryInfo;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<CategoryInfo> selectCategoryInfoList(Map<String, Object> map) throws DaoException, ValidateException {
		List<CategoryInfo> categoryInfos = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			categoryInfos = categoryInfoMapper.selectCategoryInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return categoryInfos;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<CategoryInfo> selectAllCategoryInfoList(Map<String, Object> map) throws DaoException {
		List<CategoryInfo> categoryInfos = null;
		try {
			categoryInfos = categoryInfoMapper.selectAllCategoryInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return categoryInfos;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectCategoryInfoListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = categoryInfoMapper.selectCategoryInfoListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}