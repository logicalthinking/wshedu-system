package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.ContactRecord;
import com.logicalthinking.manage.system.mapper.ContactRecordMapper;
import com.logicalthinking.manage.system.service.ContactRecordService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 学员联系记录实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class ContactRecordServiceImpl implements ContactRecordService {

	@Autowired
	private ContactRecordMapper contactRecordMapper;
	/**
	 * 新增
	 * 
	 * @param contactRecord
	 * @return
	 * @throws DaoException
	 */
	public int insertContactRecord(ContactRecord contactRecord) throws DaoException {
		int result = 0;
		try {
			result = contactRecordMapper.insertContactRecord(contactRecord);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param contactRecord
	 * @return
	 * @throws DaoException
	 */
	public int updateContactRecord(ContactRecord contactRecord) throws DaoException {
		int result = 0;
		try {
			result = contactRecordMapper.updateContactRecord(contactRecord);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteContactRecord(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = contactRecordMapper.deleteContactRecord(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param contactId
	 * @return
	 * @throws DaoException
	 */
	public ContactRecord selectContactRecordByContactId(String contactId) throws DaoException {
		ContactRecord contactRecord = null;
		try {
			contactRecord = contactRecordMapper.selectContactRecordByContactId(contactId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return contactRecord;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<ContactRecord> selectContactRecordList(Map<String, Object> map) throws DaoException, ValidateException {
		List<ContactRecord> contactRecords = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			contactRecords = contactRecordMapper.selectContactRecordList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return contactRecords;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<ContactRecord> selectAllContactRecordList(Map<String, Object> map) throws DaoException {
		List<ContactRecord> contactRecords = null;
		try {
			contactRecords = contactRecordMapper.selectAllContactRecordList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return contactRecords;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectContactRecordListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = contactRecordMapper.selectContactRecordListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}