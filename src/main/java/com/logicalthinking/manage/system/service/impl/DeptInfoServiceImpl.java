package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.DeptInfo;
import com.logicalthinking.manage.system.mapper.DeptInfoMapper;
import com.logicalthinking.manage.system.service.DeptInfoService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.vo.DeptTreeVo;

/**
 * 部门信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class DeptInfoServiceImpl implements DeptInfoService {

	@Autowired
	private DeptInfoMapper deptInfoMapper;
	/**
	 * 新增
	 * 
	 * @param deptInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertDeptInfo(DeptInfo deptInfo) throws DaoException {
		int result = 0;
		try {
			result = deptInfoMapper.insertDeptInfo(deptInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param deptInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateDeptInfo(DeptInfo deptInfo) throws DaoException {
		int result = 0;
		try {
			result = deptInfoMapper.updateDeptInfo(deptInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteDeptInfo(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = deptInfoMapper.deleteDeptInfo(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param deptId
	 * @return
	 * @throws DaoException
	 */
	public DeptInfo selectDeptInfoByDeptId(String deptId) throws DaoException {
		DeptInfo deptInfo = null;
		try {
			deptInfo = deptInfoMapper.selectDeptInfoByDeptId(deptId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return deptInfo;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<DeptInfo> selectDeptInfoList(Map<String, Object> map) throws DaoException, ValidateException {
		List<DeptInfo> deptInfos = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			deptInfos = deptInfoMapper.selectDeptInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return deptInfos;
	}
	
	/**
	 * 查询组织结构树
	 */
	public List<DeptTreeVo> selectDeptInfoTreeList(Map<String, Object> map) throws DaoException{
		try {
			return deptInfoMapper.selectDeptInfoTreeList(map);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<DeptInfo> selectAllDeptInfoList(Map<String, Object> map) throws DaoException {
		List<DeptInfo> deptInfos = null;
		try {
			deptInfos = deptInfoMapper.selectAllDeptInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return deptInfos;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectDeptInfoListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = deptInfoMapper.selectDeptInfoListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}