package com.logicalthinking.manage.system.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.DictInfo;
import com.logicalthinking.manage.system.mapper.DictInfoMapper;
import com.logicalthinking.manage.system.service.DictInfoService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.vo.DictTreeVo;
import com.logicalthinking.manage.system.vo.KeyValue;

/**
 * 字典信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Service
public class DictInfoServiceImpl implements DictInfoService {

	@Autowired
	private DictInfoMapper dictInfoMapper;
	/**
	 * 新增
	 * 
	 * @param dictInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertDictInfo(DictInfo dictInfo) throws DaoException {
		int result = 0;
		try {
			result = dictInfoMapper.insertDictInfo(dictInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param dictInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateDictInfo(DictInfo dictInfo) throws DaoException {
		int result = 0;
		try {
			result = dictInfoMapper.updateDictInfo(dictInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}
	
	/**
	 * 修改
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int updateDictInfo(Map<String,Object> map) throws DaoException {
		int result = 0;
		try {
			result = dictInfoMapper.updateDictInfoByMap(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteDictInfo(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = dictInfoMapper.deleteDictInfo(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param dictId
	 * @return
	 * @throws DaoException
	 */
	public DictInfo selectDictInfoByDictId(String dictId) throws DaoException {
		DictInfo dictInfo = null;
		try {
			dictInfo = dictInfoMapper.selectDictInfoByDictId(dictId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return dictInfo;
	}
	
	/**
	 * 查询单个
	 * 
	 * @param dictPid 父级ID
	 * @return
	 */
	public List<DictTreeVo> selectDictTreeVoByDictPid(String dictPid) throws DaoException{
		List<DictTreeVo> dictTreeVos = null;
		try {
			dictTreeVos = dictInfoMapper.selectDictTreeVoByDictPid(dictPid);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return dictTreeVos;
	}
	

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<DictInfo> selectDictInfoList(Map<String, Object> map) throws DaoException, ValidateException {
		List<DictInfo> dictInfos = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			dictInfos = dictInfoMapper.selectDictInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return dictInfos;
	}
	
	/**
	 *查询字典数据
	 * 
	 * @param map
	 * @return
	 */
	public List<DictTreeVo> selectDictTreeVoList(Map<String, Object> map) throws DaoException{
		List<DictTreeVo> dictInfos = null;
		try {
			dictInfos = dictInfoMapper.selectDictTreeVoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return dictInfos;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<DictInfo> selectAllDictInfoList(Map<String, Object> map) throws DaoException {
		List<DictInfo> dictInfos = null;
		try {
			dictInfos = dictInfoMapper.selectAllDictInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return dictInfos;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectDictInfoListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = dictInfoMapper.selectDictInfoListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

	@Override
	public List<KeyValue> selectDictChildren(String dictType, String dictValue)
			throws DaoException {
		List<KeyValue> dictInfos = null;
		try {
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("dictType", dictType);
			map.put("dictValue", dictValue);
			map.put("state", ConstantUtil.IS_VALID_Y);
			dictInfos = dictInfoMapper.selectDictChildren(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return dictInfos;
	}
	
	@Override
	public List<KeyValue> selectDictChildren(Map<String,Object> map)
			throws DaoException {
		List<KeyValue> dictInfos = null;
		try {
			dictInfos = dictInfoMapper.selectDictChildren(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return dictInfos;
	}

	@Override
	public List<KeyValue> selectDictChildren(String dictType) throws DaoException {
		return selectDictChildren(dictType, dictType);
	}

	@Override
	public DictInfo selectDictInfoByTypeValue(String dictType, String dictValue)
			throws DaoException {
		DictInfo dictInfo = null;
		try {
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("dictType", dictType);
			map.put("dictValue", dictValue);
			map.put("state", ConstantUtil.IS_VALID_Y);
			dictInfo = dictInfoMapper.selectDictInfoByTypeValue(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return dictInfo;
	}

}