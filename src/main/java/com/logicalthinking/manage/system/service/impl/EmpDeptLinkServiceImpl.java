package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.EmpDeptLink;
import com.logicalthinking.manage.system.mapper.EmpDeptLinkMapper;
import com.logicalthinking.manage.system.service.EmpDeptLinkService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 员工部门关联信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class EmpDeptLinkServiceImpl implements EmpDeptLinkService {

	@Autowired
	private EmpDeptLinkMapper empDeptLinkMapper;
	/**
	 * 新增
	 * 
	 * @param empDeptLink
	 * @return
	 * @throws DaoException
	 */
	public int insertEmpDeptLink(EmpDeptLink empDeptLink) throws DaoException {
		int result = 0;
		try {
			result = empDeptLinkMapper.insertEmpDeptLink(empDeptLink);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param empDeptLink
	 * @return
	 * @throws DaoException
	 */
	public int updateEmpDeptLink(EmpDeptLink empDeptLink) throws DaoException {
		int result = 0;
		try {
			result = empDeptLinkMapper.updateEmpDeptLink(empDeptLink);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteEmpDeptLink(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = empDeptLinkMapper.deleteEmpDeptLink(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param linkId
	 * @return
	 * @throws DaoException
	 */
	public EmpDeptLink selectEmpDeptLinkByLinkId(String linkId) throws DaoException {
		EmpDeptLink empDeptLink = null;
		try {
			empDeptLink = empDeptLinkMapper.selectEmpDeptLinkByLinkId(linkId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return empDeptLink;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<EmpDeptLink> selectEmpDeptLinkList(Map<String, Object> map) throws DaoException, ValidateException {
		List<EmpDeptLink> empDeptLinks = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			empDeptLinks = empDeptLinkMapper.selectEmpDeptLinkList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return empDeptLinks;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<EmpDeptLink> selectAllEmpDeptLinkList(Map<String, Object> map) throws DaoException {
		List<EmpDeptLink> empDeptLinks = null;
		try {
			empDeptLinks = empDeptLinkMapper.selectAllEmpDeptLinkList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return empDeptLinks;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectEmpDeptLinkListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = empDeptLinkMapper.selectEmpDeptLinkListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}