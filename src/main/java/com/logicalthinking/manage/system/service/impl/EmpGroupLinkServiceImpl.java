package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.EmpGroupLink;
import com.logicalthinking.manage.system.mapper.EmpGroupLinkMapper;
import com.logicalthinking.manage.system.service.EmpGroupLinkService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 员工分组关联信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class EmpGroupLinkServiceImpl implements EmpGroupLinkService {

	@Autowired
	private EmpGroupLinkMapper empGroupLinkMapper;
	/**
	 * 新增
	 * 
	 * @param empGroupLink
	 * @return
	 * @throws DaoException
	 */
	public int insertEmpGroupLink(EmpGroupLink empGroupLink) throws DaoException {
		int result = 0;
		try {
			result = empGroupLinkMapper.insertEmpGroupLink(empGroupLink);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param empGroupLink
	 * @return
	 * @throws DaoException
	 */
	public int updateEmpGroupLink(EmpGroupLink empGroupLink) throws DaoException {
		int result = 0;
		try {
			result = empGroupLinkMapper.updateEmpGroupLink(empGroupLink);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteEmpGroupLink(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = empGroupLinkMapper.deleteEmpGroupLink(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param linkId
	 * @return
	 * @throws DaoException
	 */
	public EmpGroupLink selectEmpGroupLinkByLinkId(String linkId) throws DaoException {
		EmpGroupLink empGroupLink = null;
		try {
			empGroupLink = empGroupLinkMapper.selectEmpGroupLinkByLinkId(linkId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return empGroupLink;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<EmpGroupLink> selectEmpGroupLinkList(Map<String, Object> map) throws DaoException, ValidateException {
		List<EmpGroupLink> empGroupLinks = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			empGroupLinks = empGroupLinkMapper.selectEmpGroupLinkList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return empGroupLinks;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<EmpGroupLink> selectAllEmpGroupLinkList(Map<String, Object> map) throws DaoException {
		List<EmpGroupLink> empGroupLinks = null;
		try {
			empGroupLinks = empGroupLinkMapper.selectAllEmpGroupLinkList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return empGroupLinks;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectEmpGroupLinkListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = empGroupLinkMapper.selectEmpGroupLinkListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}