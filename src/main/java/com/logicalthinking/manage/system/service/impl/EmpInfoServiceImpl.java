package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.EmpInfo;
import com.logicalthinking.manage.system.mapper.EmpInfoMapper;
import com.logicalthinking.manage.system.service.EmpInfoService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 员工信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class EmpInfoServiceImpl implements EmpInfoService {

	@Autowired
	private EmpInfoMapper empInfoMapper;
	/**
	 * 新增
	 * 
	 * @param empInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertEmpInfo(EmpInfo empInfo) throws DaoException {
		int result = 0;
		try {
			result = empInfoMapper.insertEmpInfo(empInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param empInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateEmpInfo(EmpInfo empInfo) throws DaoException {
		int result = 0;
		try {
			result = empInfoMapper.updateEmpInfo(empInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteEmpInfo(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = empInfoMapper.deleteEmpInfo(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param empId
	 * @return
	 * @throws DaoException
	 */
	public EmpInfo selectEmpInfoByEmpId(String empId) throws DaoException {
		EmpInfo empInfo = null;
		try {
			empInfo = empInfoMapper.selectEmpInfoByEmpId(empId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return empInfo;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<EmpInfo> selectEmpInfoList(Map<String, Object> map) throws DaoException, ValidateException {
		List<EmpInfo> empInfos = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			empInfos = empInfoMapper.selectEmpInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return empInfos;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<EmpInfo> selectAllEmpInfoList(Map<String, Object> map) throws DaoException {
		List<EmpInfo> empInfos = null;
		try {
			empInfos = empInfoMapper.selectAllEmpInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return empInfos;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectEmpInfoListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = empInfoMapper.selectEmpInfoListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}