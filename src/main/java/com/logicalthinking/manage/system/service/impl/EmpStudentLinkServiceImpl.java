package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.EmpStudentLink;
import com.logicalthinking.manage.system.mapper.EmpStudentLinkMapper;
import com.logicalthinking.manage.system.service.EmpStudentLinkService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 员工学员关联信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class EmpStudentLinkServiceImpl implements EmpStudentLinkService {

	@Autowired
	private EmpStudentLinkMapper empStudentLinkMapper;
	/**
	 * 新增
	 * 
	 * @param empStudentLink
	 * @return
	 * @throws DaoException
	 */
	public int insertEmpStudentLink(EmpStudentLink empStudentLink) throws DaoException {
		int result = 0;
		try {
			result = empStudentLinkMapper.insertEmpStudentLink(empStudentLink);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param empStudentLink
	 * @return
	 * @throws DaoException
	 */
	public int updateEmpStudentLink(EmpStudentLink empStudentLink) throws DaoException {
		int result = 0;
		try {
			result = empStudentLinkMapper.updateEmpStudentLink(empStudentLink);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteEmpStudentLink(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = empStudentLinkMapper.deleteEmpStudentLink(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param linkId
	 * @return
	 * @throws DaoException
	 */
	public EmpStudentLink selectEmpStudentLinkByLinkId(String linkId) throws DaoException {
		EmpStudentLink empStudentLink = null;
		try {
			empStudentLink = empStudentLinkMapper.selectEmpStudentLinkByLinkId(linkId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return empStudentLink;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<EmpStudentLink> selectEmpStudentLinkList(Map<String, Object> map) throws DaoException, ValidateException {
		List<EmpStudentLink> empStudentLinks = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			empStudentLinks = empStudentLinkMapper.selectEmpStudentLinkList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return empStudentLinks;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<EmpStudentLink> selectAllEmpStudentLinkList(Map<String, Object> map) throws DaoException {
		List<EmpStudentLink> empStudentLinks = null;
		try {
			empStudentLinks = empStudentLinkMapper.selectAllEmpStudentLinkList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return empStudentLinks;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectEmpStudentLinkListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = empStudentLinkMapper.selectEmpStudentLinkListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}