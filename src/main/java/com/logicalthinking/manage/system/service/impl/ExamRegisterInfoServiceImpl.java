package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.ExamRegisterInfo;
import com.logicalthinking.manage.system.mapper.ExamRegisterInfoMapper;
import com.logicalthinking.manage.system.service.ExamRegisterInfoService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 报考信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class ExamRegisterInfoServiceImpl implements ExamRegisterInfoService {

	@Autowired
	private ExamRegisterInfoMapper examRegisterInfoMapper;
	/**
	 * 新增
	 * 
	 * @param examRegisterInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertExamRegisterInfo(ExamRegisterInfo examRegisterInfo) throws DaoException {
		int result = 0;
		try {
			result = examRegisterInfoMapper.insertExamRegisterInfo(examRegisterInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param examRegisterInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateExamRegisterInfo(ExamRegisterInfo examRegisterInfo) throws DaoException {
		int result = 0;
		try {
			result = examRegisterInfoMapper.updateExamRegisterInfo(examRegisterInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteExamRegisterInfo(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = examRegisterInfoMapper.deleteExamRegisterInfo(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param regId
	 * @return
	 * @throws DaoException
	 */
	public ExamRegisterInfo selectExamRegisterInfoByRegId(String regId) throws DaoException {
		ExamRegisterInfo examRegisterInfo = null;
		try {
			examRegisterInfo = examRegisterInfoMapper.selectExamRegisterInfoByRegId(regId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return examRegisterInfo;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<ExamRegisterInfo> selectExamRegisterInfoList(Map<String, Object> map) throws DaoException, ValidateException {
		List<ExamRegisterInfo> examRegisterInfos = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			examRegisterInfos = examRegisterInfoMapper.selectExamRegisterInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return examRegisterInfos;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<ExamRegisterInfo> selectAllExamRegisterInfoList(Map<String, Object> map) throws DaoException {
		List<ExamRegisterInfo> examRegisterInfos = null;
		try {
			examRegisterInfos = examRegisterInfoMapper.selectAllExamRegisterInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return examRegisterInfos;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectExamRegisterInfoListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = examRegisterInfoMapper.selectExamRegisterInfoListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}