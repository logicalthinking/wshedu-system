package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.GroupInfo;
import com.logicalthinking.manage.system.mapper.GroupInfoMapper;
import com.logicalthinking.manage.system.service.GroupInfoService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 分组信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class GroupInfoServiceImpl implements GroupInfoService {

	@Autowired
	private GroupInfoMapper groupInfoMapper;
	/**
	 * 新增
	 * 
	 * @param groupInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertGroupInfo(GroupInfo groupInfo) throws DaoException {
		int result = 0;
		try {
			result = groupInfoMapper.insertGroupInfo(groupInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param groupInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateGroupInfo(GroupInfo groupInfo) throws DaoException {
		int result = 0;
		try {
			result = groupInfoMapper.updateGroupInfo(groupInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteGroupInfo(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = groupInfoMapper.deleteGroupInfo(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param groupId
	 * @return
	 * @throws DaoException
	 */
	public GroupInfo selectGroupInfoByGroupId(String groupId) throws DaoException {
		GroupInfo groupInfo = null;
		try {
			groupInfo = groupInfoMapper.selectGroupInfoByGroupId(groupId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return groupInfo;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<GroupInfo> selectGroupInfoList(Map<String, Object> map) throws DaoException, ValidateException {
		List<GroupInfo> groupInfos = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			groupInfos = groupInfoMapper.selectGroupInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return groupInfos;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<GroupInfo> selectAllGroupInfoList(Map<String, Object> map) throws DaoException {
		List<GroupInfo> groupInfos = null;
		try {
			groupInfos = groupInfoMapper.selectAllGroupInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return groupInfos;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectGroupInfoListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = groupInfoMapper.selectGroupInfoListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}