package com.logicalthinking.manage.system.service.impl;

import java.net.InetAddress;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hellooop.odj.token.manager.CustomManager;
import com.logicalthinking.manage.system.service.CacheService;
import com.logicalthinking.manage.system.service.InitialService;
import com.logicalthinking.manage.system.utils.ConfigProperties;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.utils.RedisUtils;

/**
 * 初始化service 实现类
 * @author lanping
 * @version 1.0
 * @date 2019-10-22 上午8:52:18
 */
@Service("initialService")
public class InitialServiceImpl implements InitialService{

	@Autowired
	private RedisUtils redisUtils;
	
	@Autowired
	private CacheService cacheService;
	
	/**
	 * 创建AccessKey
	 * @throws Exception 
	 */
	public void createAccessKey() throws Exception{
		
		//初始化创建accessKey
		String custName = ConfigProperties.getInstance().getProperty(ConstantUtil.ACCESS_KEY_CUST_NAME);
		String custPass = ConfigProperties.getInstance().getProperty(ConstantUtil.ACCESS_KEY_CUST_PASS);

		CustomManager customManager = new CustomManager();
		String ipAddress = InetAddress.getLocalHost().getHostAddress();
		String accessKey = customManager.createAccessKey(custName,custPass,ipAddress);

		if(StringUtils.isNotBlank(accessKey)){
			redisUtils.set(ConstantUtil.REDIS_KEY_ACCESS_KEY,accessKey);
		}
	}
	
	/**
	 * 初始化缓存
	 * @throws Exception 
	 */
	public void initCache() throws Exception{
		
		/**
		 * 初始化字典缓存
		 */
		cacheService.initDictCache();
		
		/**
		 * 初始化角色模块缓存
		 */
		cacheService.initModuleCache();
	}
}
