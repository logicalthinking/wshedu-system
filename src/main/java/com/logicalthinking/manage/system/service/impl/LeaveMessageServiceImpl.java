package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.LeaveMessage;
import com.logicalthinking.manage.system.mapper.LeaveMessageMapper;
import com.logicalthinking.manage.system.service.LeaveMessageService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 留言信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Service
public class LeaveMessageServiceImpl implements LeaveMessageService {

	@Autowired
	private LeaveMessageMapper leaveMessageMapper;
	/**
	 * 新增
	 * 
	 * @param leaveMessage
	 * @return
	 * @throws DaoException
	 */
	public int insertLeaveMessage(LeaveMessage leaveMessage) throws DaoException {
		int result = 0;
		try {
			result = leaveMessageMapper.insertLeaveMessage(leaveMessage);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param leaveMessage
	 * @return
	 * @throws DaoException
	 */
	public int updateLeaveMessage(LeaveMessage leaveMessage) throws DaoException {
		int result = 0;
		try {
			result = leaveMessageMapper.updateLeaveMessage(leaveMessage);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteLeaveMessage(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = leaveMessageMapper.deleteLeaveMessage(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param msgId
	 * @return
	 * @throws DaoException
	 */
	public LeaveMessage selectLeaveMessageByMsgId(String msgId) throws DaoException {
		LeaveMessage leaveMessage = null;
		try {
			leaveMessage = leaveMessageMapper.selectLeaveMessageByMsgId(msgId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return leaveMessage;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<LeaveMessage> selectLeaveMessageList(Map<String, Object> map) throws DaoException, ValidateException {
		List<LeaveMessage> leaveMessages = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			leaveMessages = leaveMessageMapper.selectLeaveMessageList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return leaveMessages;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<LeaveMessage> selectAllLeaveMessageList(Map<String, Object> map) throws DaoException {
		List<LeaveMessage> leaveMessages = null;
		try {
			leaveMessages = leaveMessageMapper.selectAllLeaveMessageList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return leaveMessages;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectLeaveMessageListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = leaveMessageMapper.selectLeaveMessageListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}