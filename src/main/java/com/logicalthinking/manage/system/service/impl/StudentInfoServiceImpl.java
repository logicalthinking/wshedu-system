package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.StudentInfo;
import com.logicalthinking.manage.system.mapper.StudentInfoMapper;
import com.logicalthinking.manage.system.service.StudentInfoService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 学员信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@Service
public class StudentInfoServiceImpl implements StudentInfoService {

	@Autowired
	private StudentInfoMapper studentInfoMapper;
	/**
	 * 新增
	 * 
	 * @param studentInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertStudentInfo(StudentInfo studentInfo) throws DaoException {
		int result = 0;
		try {
			result = studentInfoMapper.insertStudentInfo(studentInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param studentInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateStudentInfo(StudentInfo studentInfo) throws DaoException {
		int result = 0;
		try {
			result = studentInfoMapper.updateStudentInfo(studentInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteStudentInfo(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = studentInfoMapper.deleteStudentInfo(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param stuId
	 * @return
	 * @throws DaoException
	 */
	public StudentInfo selectStudentInfoByStuId(String stuId) throws DaoException {
		StudentInfo studentInfo = null;
		try {
			studentInfo = studentInfoMapper.selectStudentInfoByStuId(stuId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return studentInfo;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<StudentInfo> selectStudentInfoList(Map<String, Object> map) throws DaoException, ValidateException {
		List<StudentInfo> studentInfos = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			studentInfos = studentInfoMapper.selectStudentInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return studentInfos;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<StudentInfo> selectAllStudentInfoList(Map<String, Object> map) throws DaoException {
		List<StudentInfo> studentInfos = null;
		try {
			studentInfos = studentInfoMapper.selectAllStudentInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return studentInfos;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectStudentInfoListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = studentInfoMapper.selectStudentInfoListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}