package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.SysLogInfo;
import com.logicalthinking.manage.system.mapper.SysLogInfoMapper;
import com.logicalthinking.manage.system.service.SysLogInfoService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 系统日志信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
@Service
public class SysLogInfoServiceImpl implements SysLogInfoService {

	@Autowired
	private SysLogInfoMapper sysLogInfoMapper;
	/**
	 * 新增
	 * 
	 * @param sysLogInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertSysLogInfo(SysLogInfo sysLogInfo) throws DaoException {
		int result = 0;
		try {
			result = sysLogInfoMapper.insertSysLogInfo(sysLogInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param sysLogInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateSysLogInfo(SysLogInfo sysLogInfo) throws DaoException {
		int result = 0;
		try {
			result = sysLogInfoMapper.updateSysLogInfo(sysLogInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysLogInfo(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = sysLogInfoMapper.deleteSysLogInfo(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param logId
	 * @return
	 * @throws DaoException
	 */
	public SysLogInfo selectSysLogInfoByLogId(String logId) throws DaoException {
		SysLogInfo sysLogInfo = null;
		try {
			sysLogInfo = sysLogInfoMapper.selectSysLogInfoByLogId(logId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return sysLogInfo;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<SysLogInfo> selectSysLogInfoList(Map<String, Object> map) throws DaoException, ValidateException {
		List<SysLogInfo> sysLogInfos = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			sysLogInfos = sysLogInfoMapper.selectSysLogInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return sysLogInfos;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<SysLogInfo> selectAllSysLogInfoList(Map<String, Object> map) throws DaoException {
		List<SysLogInfo> sysLogInfos = null;
		try {
			sysLogInfos = sysLogInfoMapper.selectAllSysLogInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return sysLogInfos;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectSysLogInfoListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = sysLogInfoMapper.selectSysLogInfoListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}