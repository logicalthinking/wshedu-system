package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.SysModuleLink;
import com.logicalthinking.manage.system.mapper.SysModuleLinkMapper;
import com.logicalthinking.manage.system.service.SysModuleLinkService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 模块关联业务信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Service
public class SysModuleLinkServiceImpl implements SysModuleLinkService {

	@Autowired
	private SysModuleLinkMapper sysModuleLinkMapper;
	/**
	 * 新增
	 * 
	 * @param sysModuleLink
	 * @return
	 * @throws DaoException
	 */
	public int insertSysModuleLink(SysModuleLink sysModuleLink) throws DaoException {
		int result = 0;
		try {
			result = sysModuleLinkMapper.insertSysModuleLink(sysModuleLink);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param sysModuleLink
	 * @return
	 * @throws DaoException
	 */
	public int updateSysModuleLink(SysModuleLink sysModuleLink) throws DaoException {
		int result = 0;
		try {
			result = sysModuleLinkMapper.updateSysModuleLink(sysModuleLink);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysModuleLink(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = sysModuleLinkMapper.deleteSysModuleLink(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param linkId
	 * @return
	 * @throws DaoException
	 */
	public SysModuleLink selectSysModuleLinkByLinkId(String linkId) throws DaoException {
		SysModuleLink sysModuleLink = null;
		try {
			sysModuleLink = sysModuleLinkMapper.selectSysModuleLinkByLinkId(linkId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return sysModuleLink;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<SysModuleLink> selectSysModuleLinkList(Map<String, Object> map) throws DaoException, ValidateException {
		List<SysModuleLink> sysModuleLinks = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			sysModuleLinks = sysModuleLinkMapper.selectSysModuleLinkList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return sysModuleLinks;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<SysModuleLink> selectAllSysModuleLinkList(Map<String, Object> map) throws DaoException {
		List<SysModuleLink> sysModuleLinks = null;
		try {
			sysModuleLinks = sysModuleLinkMapper.selectAllSysModuleLinkList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return sysModuleLinks;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectSysModuleLinkListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = sysModuleLinkMapper.selectSysModuleLinkListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}