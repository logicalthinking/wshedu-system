package com.logicalthinking.manage.system.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.SysModule;
import com.logicalthinking.manage.system.mapper.SysModuleMapper;
import com.logicalthinking.manage.system.service.SysModuleService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.StringUtil;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;
import com.logicalthinking.manage.system.vo.ModuleTreeVo;

/**
 * 系统模块信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
@Service
public class SysModuleServiceImpl implements SysModuleService {

	@Autowired
	private SysModuleMapper sysModuleMapper;
	/**
	 * 新增
	 * 
	 * @param sysModule
	 * @return
	 * @throws DaoException
	 */
	public int insertSysModule(SysModule sysModule) throws DaoException {
		int result = 0;
		try {
			result = sysModuleMapper.insertSysModule(sysModule);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param sysModule
	 * @return
	 * @throws DaoException
	 */
	public int updateSysModule(SysModule sysModule) throws DaoException {
		int result = 0;
		try {
			result = sysModuleMapper.updateSysModule(sysModule);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysModule(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = sysModuleMapper.deleteSysModule(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	
	/**
	 * 查询权限设置模块树形数据
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public List<ModuleTreeVo> selectModuleTreeVoList(Map<String, Object> map) throws DaoException{
		List<ModuleTreeVo> moduleTreeVos = null;
		try {
			moduleTreeVos = sysModuleMapper.selectModuleTreeVoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return moduleTreeVos;
	}
	
	
	/**
	 * 查询当前用户 菜单
	 * 
	 * @param map
	 * @return
	 * @throws ValidateException 
	 */
	public List<ModuleTreeVo> selectUserModuleMenuTreeList(Map<String, Object> map) throws DaoException, ValidateException{
		List<ModuleTreeVo> moduleTreeVos = null;
		if(map!=null && !map.containsKey("busiIds")){
			throw new ValidateException("参数错误");
		}
		
		try {
			moduleTreeVos = sysModuleMapper.selectUserModuleMenuTreeList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return moduleTreeVos;
	}
	
	/**
	 * 查询特定页面的用户操作模块
	 * 
	 * @param map
	 * @return
	 */
	public List<ModuleTreeVo> selectModuleTreeListByOnePage(String moduleCode,String parentModuleCode,String busiIds) 
				throws DaoException{
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("moduleCode", moduleCode);
		map.put("parentModuleCode", parentModuleCode);
		if(StringUtil.isNotNull(busiIds)){
			map.put("busiIds", busiIds);
		}
		List<ModuleTreeVo> moduleTreeVos = null;
		try {
			moduleTreeVos = sysModuleMapper.selectModuleTreeListByOnePage(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return moduleTreeVos;
	}
	
	
	/**
	 * 查询全部模块 树形集合
	 * 
	 * @param map
	 * @return
	 */
	public List<ModuleTreeVo> selectAllModuleMenuTreeList(Map<String, Object> map) throws DaoException{
		List<ModuleTreeVo> moduleTreeVos = null;
		try {
			moduleTreeVos = sysModuleMapper.selectAllModuleMenuTreeList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return moduleTreeVos;
	}
	
	
	/**
	 * 查询单个
	 * 
	 * @param moduleId
	 * @return
	 * @throws DaoException
	 */
	public SysModule selectSysModuleByModuleId(String moduleId) throws DaoException {
		SysModule sysModule = null;
		try {
			sysModule = sysModuleMapper.selectSysModuleByModuleId(moduleId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return sysModule;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<SysModule> selectSysModuleList(Map<String, Object> map) throws DaoException, ValidateException {
		List<SysModule> sysModules = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			sysModules = sysModuleMapper.selectSysModuleList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return sysModules;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<SysModule> selectAllSysModuleList(Map<String, Object> map) throws DaoException {
		List<SysModule> sysModules = null;
		try {
			sysModules = sysModuleMapper.selectAllSysModuleList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return sysModules;
	}
	
	
	/**
	 * 校验访问页面是否有权限
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public List<SysModule> selectModuleListByValid(String moduleUrl,String userId) throws DaoException{
		List<SysModule> sysModules = null;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("moduleUrl", moduleUrl);
			
			if(StringUtil.isNotNull(userId)){
				map.put("userId", userId);
			}
			sysModules = sysModuleMapper.selectModuleListByValid(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return sysModules;
	}
	

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectSysModuleListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = sysModuleMapper.selectSysModuleListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}