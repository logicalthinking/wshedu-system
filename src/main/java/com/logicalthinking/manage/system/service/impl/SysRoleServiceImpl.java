package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.SysRole;
import com.logicalthinking.manage.system.mapper.SysRoleMapper;
import com.logicalthinking.manage.system.service.SysRoleService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 角色信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-14
 */
@Service
public class SysRoleServiceImpl implements SysRoleService {

	@Autowired
	private SysRoleMapper sysRoleMapper;
	/**
	 * 新增
	 * 
	 * @param sysRole
	 * @return
	 * @throws DaoException
	 */
	public int insertSysRole(SysRole sysRole) throws DaoException {
		int result = 0;
		try {
			result = sysRoleMapper.insertSysRole(sysRole);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param sysRole
	 * @return
	 * @throws DaoException
	 */
	public int updateSysRole(SysRole sysRole) throws DaoException {
		int result = 0;
		try {
			result = sysRoleMapper.updateSysRole(sysRole);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysRole(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = sysRoleMapper.deleteSysRole(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param roleId
	 * @return
	 * @throws DaoException
	 */
	public SysRole selectSysRoleByRoleId(String roleId) throws DaoException {
		SysRole sysRole = null;
		try {
			sysRole = sysRoleMapper.selectSysRoleByRoleId(roleId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return sysRole;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<SysRole> selectSysRoleList(Map<String, Object> map) throws DaoException, ValidateException {
		List<SysRole> sysRoles = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			sysRoles = sysRoleMapper.selectSysRoleList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return sysRoles;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<SysRole> selectAllSysRoleList(Map<String, Object> map) throws DaoException {
		List<SysRole> sysRoles = null;
		try {
			sysRoles = sysRoleMapper.selectAllSysRoleList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return sysRoles;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectSysRoleListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = sysRoleMapper.selectSysRoleListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}