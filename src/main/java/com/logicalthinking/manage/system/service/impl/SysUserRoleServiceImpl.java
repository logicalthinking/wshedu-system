package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.SysUserRole;
import com.logicalthinking.manage.system.mapper.SysUserRoleMapper;
import com.logicalthinking.manage.system.service.SysUserRoleService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 用户关联角色信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-16
 */
@Service
public class SysUserRoleServiceImpl implements SysUserRoleService {

	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;
	/**
	 * 新增
	 * 
	 * @param sysUserRole
	 * @return
	 * @throws DaoException
	 */
	public int insertSysUserRole(SysUserRole sysUserRole) throws DaoException {
		int result = 0;
		try {
			result = sysUserRoleMapper.insertSysUserRole(sysUserRole);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param sysUserRole
	 * @return
	 * @throws DaoException
	 */
	public int updateSysUserRole(SysUserRole sysUserRole) throws DaoException {
		int result = 0;
		try {
			result = sysUserRoleMapper.updateSysUserRole(sysUserRole);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysUserRole(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = sysUserRoleMapper.deleteSysUserRole(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param userRoleId
	 * @return
	 * @throws DaoException
	 */
	public SysUserRole selectSysUserRoleByUserRoleId(String userRoleId) throws DaoException {
		SysUserRole sysUserRole = null;
		try {
			sysUserRole = sysUserRoleMapper.selectSysUserRoleByUserRoleId(userRoleId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return sysUserRole;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<SysUserRole> selectSysUserRoleList(Map<String, Object> map) throws DaoException, ValidateException {
		List<SysUserRole> sysUserRoles = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			sysUserRoles = sysUserRoleMapper.selectSysUserRoleList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return sysUserRoles;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<SysUserRole> selectAllSysUserRoleList(Map<String, Object> map) throws DaoException {
		List<SysUserRole> sysUserRoles = null;
		try {
			sysUserRoles = sysUserRoleMapper.selectAllSysUserRoleList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return sysUserRoles;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectSysUserRoleListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = sysUserRoleMapper.selectSysUserRoleListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}