package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.SysUsers;
import com.logicalthinking.manage.system.mapper.SysUsersMapper;
import com.logicalthinking.manage.system.service.SysUsersService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 用户信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-12
 */
@Service
public class SysUsersServiceImpl implements SysUsersService {

	@Autowired
	private SysUsersMapper sysUsersMapper;
	/**
	 * 新增
	 * 
	 * @param sysUsers
	 * @return
	 * @throws DaoException
	 */
	public int insertSysUsers(SysUsers sysUsers) throws DaoException {
		int result = 0;
		try {
			result = sysUsersMapper.insertSysUsers(sysUsers);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param sysUsers
	 * @return
	 * @throws DaoException
	 */
	public int updateSysUsers(SysUsers sysUsers) throws DaoException {
		int result = 0;
		try {
			result = sysUsersMapper.updateSysUsers(sysUsers);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteSysUsers(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = sysUsersMapper.deleteSysUsers(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param userId
	 * @return
	 * @throws DaoException
	 */
	public SysUsers selectSysUsersByUserId(String userId) throws DaoException {
		SysUsers sysUsers = null;
		try {
			sysUsers = sysUsersMapper.selectSysUsersByUserId(userId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return sysUsers;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<SysUsers> selectSysUsersList(Map<String, Object> map) throws DaoException, ValidateException {
		List<SysUsers> sysUserss = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			sysUserss = sysUsersMapper.selectSysUsersList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return sysUserss;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<SysUsers> selectAllSysUsersList(Map<String, Object> map) throws DaoException {
		List<SysUsers> sysUserss = null;
		try {
			sysUserss = sysUsersMapper.selectAllSysUsersList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return sysUserss;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectSysUsersListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = sysUsersMapper.selectSysUsersListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}