package com.logicalthinking.manage.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicalthinking.manage.system.entity.WebImgInfo;
import com.logicalthinking.manage.system.mapper.WebImgInfoMapper;
import com.logicalthinking.manage.system.service.WebImgInfoService;
import com.logicalthinking.manage.system.utils.DaoException;
import com.logicalthinking.manage.system.utils.ValidateException;
import com.logicalthinking.manage.system.utils.ConstantUtil;

/**
 * 网站图片信息实现类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@Service
public class WebImgInfoServiceImpl implements WebImgInfoService {

	@Autowired
	private WebImgInfoMapper webImgInfoMapper;
	/**
	 * 新增
	 * 
	 * @param webImgInfo
	 * @return
	 * @throws DaoException
	 */
	public int insertWebImgInfo(WebImgInfo webImgInfo) throws DaoException {
		int result = 0;
		try {
			result = webImgInfoMapper.insertWebImgInfo(webImgInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param webImgInfo
	 * @return
	 * @throws DaoException
	 */
	public int updateWebImgInfo(WebImgInfo webImgInfo) throws DaoException {
		int result = 0;
		try {
			result = webImgInfoMapper.updateWebImgInfo(webImgInfo);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int deleteWebImgInfo(Map<String, Object> map) throws DaoException {
		int result = 0;
		try {
			result = webImgInfoMapper.deleteWebImgInfo(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}

	/**
	 * 查询单个
	 * 
	 * @param imgId
	 * @return
	 * @throws DaoException
	 */
	public WebImgInfo selectWebImgInfoByImgId(String imgId) throws DaoException {
		WebImgInfo webImgInfo = null;
		try {
			webImgInfo = webImgInfoMapper.selectWebImgInfoByImgId(imgId);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return webImgInfo;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<WebImgInfo> selectWebImgInfoList(Map<String, Object> map) throws DaoException, ValidateException {
		List<WebImgInfo> webImgInfos = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			webImgInfos = webImgInfoMapper.selectWebImgInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return webImgInfos;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<WebImgInfo> selectAllWebImgInfoList(Map<String, Object> map) throws DaoException {
		List<WebImgInfo> webImgInfos = null;
		try {
			webImgInfos = webImgInfoMapper.selectAllWebImgInfoList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return webImgInfos;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectWebImgInfoListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = webImgInfoMapper.selectWebImgInfoListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}