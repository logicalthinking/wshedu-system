package com.logicalthinking.manage.system.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import com.hellooop.odj.token.manager.TokenClock;
import com.logicalthinking.manage.system.service.InitialService;
import com.logicalthinking.manage.system.utils.ConfigProperties;
import com.logicalthinking.manage.system.utils.ConstantUtil;

public class TokenServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1729539637584702611L;

	public TokenServlet() {
		super();
	}

	public void destroy() {
		super.destroy();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	public void init() throws ServletException {
		
		String flag = ConfigProperties.getValue(ConstantUtil.CLEAR_TOKEN_FLAG);
		if("true".equals(flag)){
			TokenClock tokenClock = new TokenClock();
			tokenClock.start();
		}
		
		WebApplicationContext wac = ContextLoader.getCurrentWebApplicationContext();
		InitialService initialService = (InitialService) wac.getBean("initialService");
		
		try {
			//创建AccessKey
			initialService.createAccessKey();
			
			//初始化缓存
			initialService.initCache();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
