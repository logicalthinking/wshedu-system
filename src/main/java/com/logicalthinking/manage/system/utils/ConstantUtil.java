package com.logicalthinking.manage.system.utils;

/**
 * 定义常量类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-09-18
 */
public class ConstantUtil {

	/**
	 * cookie存入token时的键
	 */
	public final static String SESSION_TOKEN_NAME="session_token";

	/**
	 * 创建accessKey的custName
	 */
	public final static String ACCESS_KEY_CUST_NAME="authToken.custName";

	/**
	 * 创建accessKey的custPass
	 */
	public final static String ACCESS_KEY_CUST_PASS="authToken.custPass";

	/**
	 * 系统管理员 编码
	 */
	public final static String ROLE_CODE_ADMINISTRATOR="SUPERADMIN";
	
	/**
	 * 系统开发维护管理员 编码
	 */
	public final static String ROLE_CODE_SYS_MANAGER="SYS_DEVELOP_MANAGER";
	
	
	/**
	 * 公共模块
	 */
	public final static String [] public_modules= {"public_module"};
	
	
	/**
	 * redis缓存的存用户模块 key的后缀
	 */
	public final static String REDIS_KEY_USER_MODULES_SUFFX="_modules";
	
	
	/**
	 * redis缓存所有模块
	 */
	public final static String REDIS_KEY_ALL_MODULE_MENUS="all_module_menus";
	
	
	/**
	 * redis缓存权限模块
	 */
	public final static String REDIS_KEY_PERMISSION_MODULE_MENUS="permission_module_menus";
	
	
	/**
	 * 题目选项默认数组
	 */
	public final static String [] CHOICE_TAG_ARRAY = {"A","B","C","D","E","F","G","H","I","J"};
	
	
	/**
     * 数据有效状态 Y
     */
    public final static String IS_VALID_Y = "Y";

    /**
     * 数据有效状态 N
     */
    public final static String IS_VALID_N = "N";
    
    
    /**
     * 客户端类型PC
     */
    public final static String CUST_TYPE_PC = "PC";

    /**
     * 客户端类型ANDROID
     */
    public final static String CUST_TYPE_ANDROID = "ANDROID";
    
    /**
     * 客户端类型IOS
     */
    public final static String CUST_TYPE_IOS= "IOS";
    
    /**
     * 系统标识 PORTAL_SITE
     */
    public final static String SYS_CODE_PORTAL= "PORTAL_SITE";
    
    /**
	 * redis存入的key 字典map
	 */
    public static final String REDIS_KEY_DICT_MAP="dict_list_map";
    
    /**
   	 * redis存入的key 字典树形集合
   	 */
    public static final String REDIS_KEY_DICT_TREE_LIST="dict_tree_list";
       
    /**
   	 * 是否清除过期token
   	 */
   	public static final String CLEAR_TOKEN_FLAG="clearTokenFlag";
   	
   	/**
	 * redis存入accessKey的键
	 */
	public final static String REDIS_KEY_ACCESS_KEY="wshedu-system-accessKey";
	
	
	/**
	 * 部门根节点ID
	 */
	public final static String ROOT_DEPT_ID="0";
	/**
	 * 错误代号值
	 */
	public final static String CODE_200="200"; //操作成功
	public final static String CODE_401="401"; //验证码错误
	public final static String CODE_402="402"; //用户名或密码错误
	public final static String CODE_403="403"; //没有操作权限
	public final static String CODE_404="404"; //参数错误
	public final static String CODE_406="406"; //账号在另一地点登录
	public final static String CODE_407="407"; //该值已经存在
	public final static String CODE_500="500"; //系统异常

	/**
	 * 错误消息
	 */
	public final static String MSG_ADD_SUCCESS="新增成功";
	public final static String MSG_EDIT_SUCCESS="修改成功";
	public final static String MSG_DEL_SUCCESS="删除成功";
	public final static String MSG_ADD_ERROR="新增失败";
	public final static String MSG_EDIT_ERROR="修改失败";
	public final static String MSG_DEL_ERROR="删除失败";
	public final static String MSG_SELECT_ERROR="查询失败";
	public final static String MSG_PAGE_DATA_ERROR="分页参数错误";

	public final static String MSG_401="验证码错误";
	public final static String MSG_402="用户名或密码错误";
	public final static String MSG_403="没有操作权限";
	public final static String MSG_404="参数错误";
	public final static String MSG_406="您的账号在另一地点登录";
	public final static String MSG_500="系统异常";
	
	/**
	 * 操作成功信息
	 */
	public final static String OP_MSG_SUCCESS="操作成功";
	
	/**
	 * 操作失败信息
	 */
	public final static String OP_MSG_ERROR="操作失败";

}