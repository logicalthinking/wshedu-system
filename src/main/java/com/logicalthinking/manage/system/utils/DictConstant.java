package com.logicalthinking.manage.system.utils;

/**
 * 字典常量类
 * @author lanping
 * @version 1.0
 * @date 2019-10-16 下午5:15:54
 */
public class DictConstant {
	
	/**
	 * 字典根节点 值 root
	 */
	public static final String DICT_ROOT_VALUE="root";

	/**
	 * 文案所属类型
	 */
	public static final String DICT_ARTICLE_TYPE="article_type";
	
	
	/**
	 * 图片所属类型
	 */
	public static final String DICT_WEB_IMG_TYPE="photo_item_type";
	
	
	/**
	 * 分类类型
	 */
	public static final String DICT_CATEGORY_TYPE="category_type";
	
	/**
	 * 模块操作类型
	 */
	public static final String DICT_MODULE_OP_TYPE="module_op_type";
	
	/**
	 * 模块操作类型 默认 链接
	 */
	public static final String DICT_MODULE_LINK="module_link";
	
	/**
	 * 在职状态
	 */
	public static final String DICT_JOB_STATUS="job_status";
}
