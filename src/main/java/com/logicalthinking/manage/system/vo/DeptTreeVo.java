package com.logicalthinking.manage.system.vo;

import java.util.List;

/**
 * 组织结构树
 * @author lanping
 * @version 1.0
 * @date 2019-11-12 下午3:02:08
 */
public class DeptTreeVo {
	
	private String id;	//ID
	private String title; //名称
	private String field; // 字典值
	private boolean checked; //是否选中
	private boolean spread=true;	//是否展开
	private String href;	//跳转地址
	private List<DeptTreeVo> children;	//子集
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public boolean isSpread() {
		return spread;
	}
	public void setSpread(boolean spread) {
		this.spread = spread;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public List<DeptTreeVo> getChildren() {
		return children;
	}
	public void setChildren(List<DeptTreeVo> children) {
		this.children = children;
	}
	
	
}
