package com.logicalthinking.manage.system.vo;

import java.util.List;

/**
 * 字典树形结构 菜单数据
 * @author lanping
 * @version 1.0
 * @date 2019-10-16 上午11:55:10
 */
public class DictTreeVo {
	
	private String id;	//ID
	private String title; //名称
	private String field; // 字典值
	private boolean checked; //是否选中
	private boolean spread;	//是否展开
	private String href;	//跳转地址
	private List<DictTreeVo> children;	//子集
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public boolean isSpread() {
		return spread;
	}
	public void setSpread(boolean spread) {
		this.spread = spread;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public List<DictTreeVo> getChildren() {
		return children;
	}
	public void setChildren(List<DictTreeVo> children) {
		this.children = children;
	}
	
	
}
