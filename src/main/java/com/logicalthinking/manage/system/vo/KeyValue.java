package com.logicalthinking.manage.system.vo;

/**
 * 字典信息
 * @author lanping
 * @version 1.0
 * @date 2019-10-16 下午4:49:45
 */
public class KeyValue {

	private String key;
	private String value;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
