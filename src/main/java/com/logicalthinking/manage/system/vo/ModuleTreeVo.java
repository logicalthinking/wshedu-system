package com.logicalthinking.manage.system.vo;

import java.util.List;

/**
 * 功能模块树形结构 菜单数据
 * @author lanping
 * @version 1.0
 * @date 2019-10-17 下午1:53:08
 */
public class ModuleTreeVo {
	
	private String id;	//ID
	private String title; //名称
	private String field; // 字典值
	private String icon;	//图标
	private boolean checked; //是否选中
	private boolean spread = true;	//是否展开
	private boolean disabled;	//是否禁用
	private String href;	//跳转地址
	private String type;	//类型
	private String defaultStatus;	//默认菜单状态
	private String showStatus;	//是否显示
	private List<ModuleTreeVo> children;	//子集
	
	public String getDefaultStatus() {
		return defaultStatus;
	}
	public void setDefaultStatus(String defaultStatus) {
		this.defaultStatus = defaultStatus;
	}
	public String getShowStatus() {
		return showStatus;
	}
	public void setShowStatus(String showStatus) {
		this.showStatus = showStatus;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public boolean isDisabled() {
		return disabled;
	}
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public boolean isSpread() {
		return spread;
	}
	public void setSpread(boolean spread) {
		this.spread = spread;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public List<ModuleTreeVo> getChildren() {
		return children;
	}
	public void setChildren(List<ModuleTreeVo> children) {
		this.children = children;
	}
	
	
}
