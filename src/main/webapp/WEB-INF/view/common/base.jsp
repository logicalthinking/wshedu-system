<%@page import="com.logicalthinking.manage.system.utils.DateUtil"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.logicalthinking.manage.system.utils.ConfigProperties" %><%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    
    String webColorStyle = ConfigProperties.getInstance().getProperty("WebColorStyle");
    request.setAttribute("colorStyle",webColorStyle);
    String sysName = "文上华人工智能教学系统";
    request.setAttribute("sysName",sysName);
    
    String backSysName = "文上华教育管理系统";
    request.setAttribute("backSysName",backSysName);
    
    String footerName = "&copy;"+DateUtil.getNowDate("yyyy")+" 深圳文上华教育管理咨询有限公司 版权所有";
    request.setAttribute("footerName",footerName);
%>