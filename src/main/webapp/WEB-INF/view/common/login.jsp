<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../common/base.jsp"/>
<!DOCTYPE html>
<html>
<head>
    <base href="<%= basePath%>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="images/logo-icon.png" type="image/x-icon" />
    <title>${backSysName }-登录</title>
    <link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
    <link rel="stylesheet" href="css/login.css" />
    <style type="text/css">
    	.red{
    		color: red;
    	}
    	.mt5{
    		margin-top: 5px;
    	}
    	.msg_error{
    		display: none;
    		color: red;
    	}
    	.mb0{
    		margin-bottom: 0px !important;
    	}
    	.beg-login-box{
    		height: auto;
    	}
    	.beg-login-box .beg-login-main{
    		height: auto;
    	}
    </style>
</head>
<body class="beg-login-bg" style="background:url('image/priview/05384412-eaf3-438f-814a-ae7acc1e91e3.jpg') no-repeat;">
	<div class="beg-login-box">
	    <header>
	        <h1>用户登录</h1>
	    </header>
	    <div class="beg-login-main">
	        <form action="" class="layui-form">
	            <div class="layui-form-item">
	                <label class="beg-login-icon">
	                    <i class="layui-icon">&#xe612;</i>
	                </label>
	                <input type="text" name="userName" autocomplete="off" placeholder="这里输入用户名" class="layui-input">
	            	<p class="msg_error mt5" id="userName_error">请输入用户名</p>
	            </div>
	            <div class="layui-form-item">
	                <label class="beg-login-icon">
	                    <i class="layui-icon">&#xe642;</i>
	                </label>
	                <input type="password" name="userPass" autocomplete="off" placeholder="这里输入密码" class="layui-input">
	           		<p class="msg_error mt5" id="userPass_error">请输入密码</p>
	            </div>
	            
	            <div class="layui-form-item">
				    <div class="layui-input-inline" style="width: 160px;">
				      <input type="text" id="code" name="code" 
				      	placeholder="请输入验证码" 
				      		autocomplete="off" class="layui-input">
			      		<p class="msg_error mt5" id="code_error">请输入验证码</p>
				    </div>
				    <div class="layui-input-inline" style="width: 100px;margin-right: 0px;">
				    	<div id="v_container" style="width:94px;height:38px;float: left;"></div>
				    </div>
			    </div>
	            
	            <div class="layui-form-item mb0">
	                <div class="beg-pull-left beg-login-remember">
	                    <label>记住帐号？</label>
	                    <input type="checkbox" name="rememberMe" lay-skin="switch" title="记住帐号">
	                </div>
	                <div class="beg-pull-right">
	                    <button class="layui-btn layui-btn-primary" lay-submit lay-filter="login">
	                        <i class="layui-icon">&#xe650;</i> 登录
	                    </button>
	                </div>
	                <div class="beg-clear"></div>
	                <p class="msg_error" id="res_msg">用户名或密码错误</p>
	            </div>
	        </form>
	    </div>
	    <footer>
	        <p>
	        	${footerName }
	        </p>
	    </footer>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/GVerify.js"></script>
	<script>
	
		if(top!=self){
			parent.location.href="login";
		}
	    layui.use(['layer', 'form'], function(){
	        var layer = layui.layer
	            ,form = layui.form;
	        var $ = layui.jquery;
	        
	        var verifyCode = new GVerify("v_container");
	        
	        var login_remember = localStorage.getItem("login_remember");
	        if(login_remember && login_remember!="undefined"){
	        	var item = JSON.parse(login_remember);
	        	var userName = item.user_name;
	        	var remember_me = item.remember_me;
	        	if(remember_me == true ){
	        		$("input[name='rememberMe']").prop("checked",true);
		       		$("input[name='userName']").val(userName);
		       		form.render();
	        	}
	        }
	        
	        function isNull(obj,error_obj,msg){
	        	var flag = true;
	        	var val = $(obj).val();
	        	if(!val){
	        		$(obj).css('border','1px solid red');
	 				$(obj).focus();
	 				if(msg){
	 					$(error_obj).html(msg);
	 				}
					$(error_obj).show();
					flag = false;
	        	}else{
	        		$(obj).css('border','1px solid #FFF');
	        		$(error_obj).hide();
	        	}
	        	return flag;
	        }
	        
	        function validNull(){
	        	var flag = true;
	        	flag = isNull("input[name='userName']","#userName_error","");
	        	if(!flag){
	        		return;
	        	}
	        	flag = isNull("input[name='userPass']","#userPass_error","请输入密码");
	        	if(!flag){
	        		return;
	        	}
	        	flag = isNull("input[name='code']","#code_error","请输入验证码");
	        	if(!flag){
	        		return;
	        	}
	        	return flag;
	        }
	        
	        form.on('submit(login)', function(data) {
	        	 var result = data.field;
	        	 
	        	 var flag  = validNull();
	        	 if(!flag){
	        	 	return false;
	        	 }
	        	 
	        	 var valid = verifyCode.validate(result.code);
	        	 if(!valid){
					var obj = $("input[name='code']");
	  				$(obj).css('border','1px solid red');
	  				$(obj).focus();
	  				$("#code_error").html("验证码不正确");
					$("#code_error").show();
	        	 	return false;
	        	 }
	        	 
	        	 $.post("dologin",result,function(res){
	                if(res.error_code=='200'){
	                		
	                	var rememberMe = $("input[name='rememberMe']").prop("checked");
						if(rememberMe==true){
							var user_name = result.userName;
							var remember_me = true;
							var item = {
								user_name:user_name,
								remember_me:remember_me
							};
							localStorage.setItem("login_remember",JSON.stringify(item));
						}else{
							localStorage.removeItem("login_remember");
						}
	                    location.href="manage/mainIndex";
	                }else{
	                	$("#res_msg").html(res.msg);
	                	$("#res_msg").show();
	                }
	            });
	        	return false;
	        });
	    });
	
	
	</script>
</body>
</html>