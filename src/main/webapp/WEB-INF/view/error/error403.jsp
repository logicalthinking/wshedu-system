<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%= basePath%>">
    <meta charset="UTF-8">
    <title>您没有访问权限</title>
    <link rel="shortcut icon" href="images/logo-icon.png" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="css/index/base.css">
	<link rel="stylesheet" type="text/css" href="css/index/index.css">
    <style type="text/css">
    	.bg{
    		background: url("images/403.jpg") no-repeat center center;
    	}
    	.back{
    		margin-top: 375px;
    		display: inline-block;
    		color: #F5F5F5;
    		font-size:16px;
    		background: #E38D14;
    		width: 130px;
    		height: 38px;
    		text-decoration: none;
    		line-height: 38px;
    		display: none;
    	}
    	body{
    		background: #FFFFFF;
    	}
    </style>
</head>
<body>
    <div class="bg h100 w100 mgauto tc">
    	<a href="javascript:;" onclick="window.history.go(-1)" class="back">返  回上一步</a>
    </div>
</body>
</html>