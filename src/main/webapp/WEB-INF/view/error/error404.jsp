<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%= basePath%>">
    <meta charset="UTF-8">
    <title>您访问的页面不存在</title>
    <link rel="shortcut icon" href="images/logo-icon.png" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="css/index/base.css">
	<link rel="stylesheet" type="text/css" href="css/index/index.css">
    <style type="text/css">
    	.bg{
    		margin-top:100px;
    		background: url("images/404.jpg") no-repeat center center;
    	}
    	.back{
    		margin-top: 314px;
    		display: inline-block;
    		color: #36C;
    		font-size:18px;
    	}
    	body{
    		background: #F0EEEF;
    	}
    </style>
</head>
<body>
    <div class="bg ht500 w100 mgauto tc">
    	<a href="index.html" class="back">返回首页</a>
    </div>
</body>
</html>
