<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%= basePath%>">
    <meta charset="UTF-8">
    <title>抱歉，系统异常</title>
    <link rel="shortcut icon" href="images/logo-icon.png" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="css/index/base.css">
	<link rel="stylesheet" type="text/css" href="css/index/index.css">
    <style type="text/css">
    	.bg{
    		background: url("images/500.gif") no-repeat center center;
    		margin-top: 100px;
    		display: inline-block;
    	}
    	.back{
    		color: #36C;
    		font-size:18px;
    		display: inline-block;
    	}
    	.back_bg{
    		width: 200px;
    		height: 40px;
    		margin: auto;
    		background: #F7F7F7;
    		text-align: center;
    		line-height: 40px;
    		margin-top: 380px;
    	}
    	body{
    		background: #F7F7F7;
    	}
    </style>
</head>
<body>
    <div class="bg w100 ht600">
   		<div class="back_bg">
   			<a href="index.html" class="back">返回首页</a>
   		</div>
   	</div>
</body>
</html>