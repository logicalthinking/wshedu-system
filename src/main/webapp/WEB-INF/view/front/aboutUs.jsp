<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	request.setAttribute("active","about");
%>
<jsp:include page="../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>${sysName }-关于我们</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> -->
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="shortcut icon" href="images/logo-icon.png" type="image/x-icon" />
	<script type="text/javascript" src="js/bridge.js"></script>
	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/index/base.css">
	<link rel="stylesheet" type="text/css" href="css/index/index.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<%@include file="base/header.jsp" %>
	<div class="container pt100">
		<div class="banner ht500 w100 flex" style="background:url('image/priview/${titlebg.imgId}${titlebg.suffix}') no-repeat;">
			<c:if test="${titlebg.wordStatus == 'Y'}">
				<div class="big-title mgauto">
					关于我们
				</div>
			</c:if>
		</div>
	</div>
	<div class="container pt50 pb50 bgwhite">
		<div class="layui-container">
			<div class="layui-row">
				<div class="layui-col-md6 fl w50">
					<div class="w95 about-words mt20">
						${aboutArticle.artContent }
					</div>
				</div>
				<div class="layui-col-md6 fl w50">
					<div class="banner ht550 w100 h100 flex">
						<img alt="" class="w95 mgauto" id="article_img"
							src="image/priview/${articleImg.imgId}${articleImg.suffix}">
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container bggray pb100 pt100">
		<div class="layui-container">
			<div class="layui-row layui-col-space10">
				<div class="layui-col-md6 ht500 ot15 fl w50">
					<div class="w100 h100">
						<div class="box-text">
							<p class="first-title orange mt80">人工智能测评</p>
							<p class="second-title mt40">智能测评，定位薄弱知识点；</p>
							<p class="second-title">1000+测评专题，</p>
							<p class="second-title">支持招生教学的多样需求。</p>
							<p class="mt40">
								<a class="layui-btn layui-btn-small apply-btn bgorange">申请试用</a>
							</p>
						</div>
					</div>
				</div>
				<div class="layui-col-md6 ht500 ot15 fl w50">
					<div class="w100 h100 banner flex">
						<%-- <img class="w96 mgauto" id="evaluate_img" src="image/priview/${evaluateImg.imgId}${evaluateImg.suffix}"> --%>
						<img class="w96 mgauto" id="evaluate_img">
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container bgwhite pb100 pt100">
		<div class="layui-container">
			<div class="layui-row layui-col-space10">
				<div class="layui-col-md6 ht500 ot15 fl w50">
					<div class="w100 h100 banner flex">
						<%-- <img alt="" class="w96 mgauto" id="learn_system" src="image/priview/${learnSystemImg.imgId}${learnSystemImg.suffix}"> --%>
						<img alt="" class="w96 mgauto" id="learn_system" >
					</div>
				</div>
				<div class="layui-col-md6 ht500 ot15 fl w50">
					<div class="w100 h100">
						<div class="box-text fr w60">
							<p class="first-title orange mt80">智能教学系统</p>
							<p class="second-title mt40">学习任务智能匹配，</p>
							<p class="second-title">AI助力课堂标准化；</p>
							<p class="second-title">支持线上线下多种教学模式。</p>
							<p class="second-title">(班课、一对一、公开课）</p>
							<p class="mt40">
								<a class="layui-btn layui-btn-small apply-btn bgorange">申请试用</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container bggray pb100 pt100">
		<div class="layui-container">
			<div class="layui-row layui-col-space10">
				<div class="layui-col-md6 ht500 ot15 fl w50">
					<div class="w100 h100">
						<div class="box-text">
							<p class="first-title orange mt80">专属课程内容</p>
							<p class="second-title mt40">标准课程覆盖多教材版本，</p>
							<p class="second-title">支持本地开发定制；</p>
							<p class="second-title">一键备课自动生成教学内容，</p>
							<p class="second-title">节省备课时间。</p>
							<p class="mt40">
								<a class="layui-btn layui-btn-small apply-btn bgorange">申请试用</a>
							</p>
						</div>
					</div>
				</div>
				<div class="layui-col-md6 ht500 ot15 fl w50">
					<div class="w100 h100 banner flex">
						<%-- <img alt="" class="w96 mgauto" id="course_content_img" src="image/priview/${courseContentImg.imgId}${courseContentImg.suffix}"> --%>
						<img alt="" class="w96 mgauto" id="course_content_img">
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container bgwhite pb100 pt100">
		<div class="layui-container">
			<div class="layui-row layui-col-space10">
				<div class="layui-col-md6 ht500 ot15 fl w50">
					<div class="w100 h100 banner flex">
						<%-- <img alt="" class="w96 mgauto" id="data_analysis_img" src="image/priview/${dataAnalysisImg.imgId}${dataAnalysisImg.suffix}"> --%>
						<img alt="" class="w96 mgauto" id="data_analysis_img">
					</div>
				</div>
				<div class="layui-col-md6 ht500 ot15 fl w50">
					<div class="w100 h100">
						<div class="box-text fr w60">
							<p class="first-title orange mt80">学习数据分析</p>
							<p class="second-title mt40">老师上课数据监控，</p>
							<p class="second-title">实时判断学生学习状态；</p>
							<p class="second-title">学习数据直观可见，</p>
							<p class="second-title">招生续费有理有据。</p>
							<p class="mt40">
								<a class="layui-btn layui-btn-small apply-btn bgorange">申请试用</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<%@include file="base/footer.jsp" %>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/base.js"></script>
	<script>
		layui.use(['carousel', 'form'], function(){
	      var carousel = layui.carousel
	      ,form = layui.form
	      ,$ = layui.jquery;
	      
	      
	      	var imgTypes = ['evaluate_img','learn_system',
	      		'course_content_img','data_analysis_img'];
			
			$.post("getWebImgs",{
				imgTypes:imgTypes
			},function(res){
				if(res.error_code == "200"){
					if(res.result!=null && res.result){
						if(res.result.evaluate_img!=null
							&& res.result.evaluate_img.length>0){
							$.each(res.result.evaluate_img,function(index,item){
								$("#evaluate_img").attr("src","image/priview/"+item.imgId+item.suffix);
							});
						}
						
						if(res.result.learn_system!=null
							&& res.result.learn_system.length>0){
							$.each(res.result.learn_system,function(index,item){
								$("#learn_system").attr("src","image/priview/"+item.imgId+item.suffix);
							});
						}
						
						if(res.result.course_content_img!=null
							&& res.result.course_content_img.length>0){
							$.each(res.result.course_content_img,function(index,item){
								$("#course_content_img").attr("src","image/priview/"+item.imgId+item.suffix);
							});
						}
						
						if(res.result.data_analysis_img!=null
							&& res.result.data_analysis_img.length>0){
							$.each(res.result.data_analysis_img,function(index,item){
								$("#data_analysis_img").attr("src","image/priview/"+item.imgId+item.suffix);
							});
						}
					}
				}else{
					//layer.msg(res.msg,{icon:2});
				}
			});
	      
    	});
	</script>
</body>
</html>