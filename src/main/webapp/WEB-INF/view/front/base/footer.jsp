<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="footerbar">
  	<div class="left-item">
  		<div class="logo-word">
  			<span>文上华教育</span>
  		</div>
  		<div class="logo-img">
  			<img alt="" src="image/priview/${footerqr.imgId}${footerqr.suffix}">
  		</div>
  	</div>
  	<div class="right-item">
  		<div class="right-item-list">
  			<ul>
		      <li>
		        <a href="aboutUs.html">关于我们</a>
		      </li>
		      <li>
		        <a href="courses.html">课程中心</a>
		      </li>
		      <li>
		        <a href="teachers.html">师资力量</a>
		      </li>
		      <li class="noborder">
		        <span>服务热线：0755-863079161</span>
		      </li>
		    </ul>
  		</div>
  		<div class="right-item-list">
			<div class="inner-item">
				<p>深圳总部：深圳市南山区桃园路田厦金牛广场A座1710室</p>
				<p>宝安校区：深圳市宝安区固戌华丰国际机器人产业园F栋208-209</p>
				<p>东莞分部：东莞市南城街道胜和路华凯大厦213室</p>
				<p>广州校区：广州市从化区广从北路468号（广州南方学校内）</p>
				<p>联系电话：0755-86307916；0755-86307953</p>
				<p>电子邮箱：13091464@qq.com</p>
				<p>控股公司：深圳文上华教育管理咨询有限公司</p>
			</div>
  		</div>
  	</div>
  	<div class="foot-bottom-info">
    	<p>
          <span>${footerName }</span>
          <a href="http://www.beian.miit.gov.cn" target="_blank">粤ICP备17120973号</a>
          &nbsp;<img src="images/jinghui.png">&nbsp;
          <a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=44030502001049" 
          target="_blank">粤公网安备44030502001049号</a>
        </p>
	</div>
</div>