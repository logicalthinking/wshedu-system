<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="navbar">
    <div class="indexLogo">
      <a href="index.html">
        <img src="image/priview/${logo.imgId}${logo.suffix}">
      </a>
    </div>
    <div class="indexNav">
        <div class="nav-item">
        	<ul>
	          <li
	          	<c:if test="${active=='index' }">class="active"</c:if> 
	           ><a href="index.html" class="index_a">首页</a></li>
	          <li 
	          	<c:if test="${active=='about' }">class="active"</c:if>
	          ><a href="aboutUs.html" >关于我们</a></li>
	          <li 
	          	<c:if test="${active=='course' }">class="active"</c:if> 
			 ><a href="courses.html" >课程中心</a></li>
	          <li
	          	<c:if test="${active=='teacher' }">class="active"</c:if> 
	          ><a href="teachers.html" >师资力量</a></li>
	          <li
	          	<c:if test="${active=='school' }">class="active"</c:if> 
	          ><a href="schools.html" >校区分布</a></li>
	        </ul>
	        <div class="nav-line"></div>
        </div>
        <div class="nav-btn">
       		<div class="nav-btn-inner">
       			<a href="javascript:;" class="layui-btn layui-btn-small" id="login_btn">系统登录</a>
	       		<ul class="nav-btn-ul">
	          		<li>
	          			<a href="login" target="_blank">后台管理系统</a>
	          		</li>
	          		<li>
	          			<a href=""  target="_blank">在线教学系统</a>
	          		</li>
	          		<li>
	          			<a href=""  target="_blank">在线教学系统</a>
	          		</li>
	          	</ul>
       		</div>
        </div>
    </div>
</div>