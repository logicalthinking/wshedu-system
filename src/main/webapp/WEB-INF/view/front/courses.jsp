<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	request.setAttribute("active","course");
%>
<jsp:include page="../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>${sysName }-课程中心</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> -->
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="shortcut icon" href="images/logo-icon.png" type="image/x-icon" />
	<script type="text/javascript" src="js/bridge.js"></script>
	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/index/base.css">
	<link rel="stylesheet" type="text/css" href="css/index/index.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<%@include file="base/header.jsp" %>
	<div class="container pt100">
		<div class="banner ht500 w100 flex" style="background:url('image/priview/${titlebg.imgId}${titlebg.suffix}') no-repeat;">
			<c:if test="${titlebg.wordStatus == 'Y'}">
				<div class="big-title mgauto">
					课程中心
				</div>
			</c:if>
		</div>
	</div>
	<div class="container bgwhite pb50">
		<div class="layui-container">
			<div class="layui-row mb20">
				<div class="layui-col-md4 ht200 fl w33 course-three-item">
					<div class="w120 h100 bggray mlfw25">
						<div class="fl w40 h100 tc flex">
							<div class="wt120 ht100 mgauto bs100 course_item_icon" style="background:url('image/priview/${courseItemIcons[0].imgId}${courseItemIcons[0].suffix}') no-repeat;"></div>
						</div>
						<div class="fl w60 h100 tc flex">
							<div class="mgauto text-title">
								<p class="first-title">量身打造</p>
								<p class="first-title">自适应个性化学习路径</p>
								<p class="second-title">教学内容聚焦于</p>
								<p class="second-title">每个同学的难点、薄弱点</p>
							</div>
						</div>
					</div>
				</div>
				<div class="layui-col-md4 ht200 fl w33 course-three-item">
					<div class="w120 h100 bggray" >
						<div class="fl w40 h100 tc flex">
							<div class="wt120 ht100 mgauto bs100 course_item_icon" style="background:url('image/priview/${courseItemIcons[1].imgId}${courseItemIcons[1].suffix}') no-repeat;"></div>
						</div>
						<div class="fl w60 h100 tc flex">
							<div class="mgauto text-title">
								<p class="first-title">测学练测</p>
								<p class="first-title">科学飙升学习效率</p>
								<p class="second-title">试验证明，智适应学习系统学习</p>
								<p class="second-title">效率是同类产品的3-5倍。</p>
							</div>
						</div>
					</div>
				</div>
				<div class="layui-col-md4 ht200 fl w33 course-three-item">
					<div class="w120 h100 bggray mlw25" >
						<div class="fl w40 h100 tc flex">
							<div class="wt120 ht100 mgauto bs100 course_item_icon" style="background:url('image/priview/${courseItemIcons[2].imgId}${courseItemIcons[2].suffix}') no-repeat;"></div>
						</div>
						<div class="fl w60 h100 tc flex">
							<div class="mgauto text-title">
								<p class="first-title">教学版本</p>
								<p class="first-title">多版本覆盖全国</p>
								<p class="second-title">优质视频、海量题库，内容覆盖</p>
								<p class="second-title">全国20个以上省市地区</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="layui-row mt50">
				<div class="row_title ht100 w100 flex">
					<div class="big-title mgauto">
						<span class="cl6">各学科课程内容</span>
					</div>
				</div>
			</div>
			<div class="layui-row mt40 mb40">
				<div class="layui-col-md12 ht80">
					<div class="course-items">
						<ul>
							<li class="change-img active" imgPath="image/priview/${subjectDescImgs[0].imgId}${subjectDescImgs[0].suffix}">
								<div class="mgauto ht50 w100">
									<span class="text-logo" img1="chinese1.png" img2="chinese2.png">
										<img alt="" src="images/chinese2.png" class="w100 h100">
									</span>
									<span class="text-word">语文</span>
								</div>
							</li>
							<li class="change-img" imgPath="image/priview/${subjectDescImgs[3].imgId}${subjectDescImgs[3].suffix}">
								<div class="mgauto ht50 w100">
									<span class="text-logo" img1="math1.png" img2="math2.png">
										<img alt="" src="images/math1.png" class="w100 h100">
									</span>
									<span class="text-word">数学</span>
								</div>
							</li>
							<li class="change-img" imgPath="image/priview/${subjectDescImgs[6].imgId}${subjectDescImgs[6].suffix}">
								<div class="mgauto ht50 w100">
									<span class="text-logo" img1="english1.png" img2="english2.png">
										<img alt="" src="images/english1.png" class="w100 h100">
									</span>
									<span class="text-word">英语</span>
								</div>
							</li>
							<li class="change-img" imgPath="image/priview/${subjectDescImgs[9].imgId}${subjectDescImgs[9].suffix}">
								<div class="mgauto ht50 w100">
									<span class="text-logo" img1="physics1.png" img2="physics2.png">
										<img alt="" src="images/physics1.png" class="w100 h100">
									</span>
									<span class="text-word">物理</span>
								</div>
							</li>
							<li class="change-img" imgPath="image/priview/${subjectDescImgs[10].imgId}${subjectDescImgs[10].suffix}">
								<div class="mgauto ht50 w100">
									<span class="text-logo" img1="chemistry1.png" img2="chemistry2.png">
										<img alt="" src="images/chemistry1.png" class="w100 h100">
									</span>
									<span class="text-word">化学</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="layui-row">
				<div class="layui-col-md12">
					<div class="course-items ht80 course-items-grade">
						<ul class="course-items-ul">
							<li class="change-class active" imgPath="image/priview/${subjectDescImgs[0].imgId}${subjectDescImgs[0].suffix}">
								<div class="mgauto w100">
									<span class="text-word">初一</span>
								</div>
							</li>
							<li class="change-class" imgPath="image/priview/${subjectDescImgs[1].imgId}${subjectDescImgs[1].suffix}">
								<div class="mgauto w100">
									<span class="text-word">初二</span>
								</div>
							</li>
							<li class="change-class" imgPath="image/priview/${subjectDescImgs[2].imgId}${subjectDescImgs[2].suffix}">
								<div class="mgauto w100">
									<span class="text-word">初三</span>
								</div>
							</li>
						</ul>
						<ul class="course-items-ul hide">
							<li class="change-class active" imgPath="image/priview/${subjectDescImgs[3].imgId}${subjectDescImgs[3].suffix}">
								<div class="mgauto w100">
									<span class="text-word">初一</span>
								</div>
							</li>
							<li class="change-class" imgPath="image/priview/${subjectDescImgs[4].imgId}${subjectDescImgs[4].suffix}">
								<div class="mgauto w100">
									<span class="text-word">初二</span>
								</div>
							</li>
							<li class="change-class" imgPath="image/priview/${subjectDescImgs[5].imgId}${subjectDescImgs[5].suffix}">
								<div class="mgauto w100">
									<span class="text-word">初三</span>
								</div>
							</li>
						</ul>
						<ul class="course-items-ul hide">
							<li class="change-class active" imgPath="image/priview/${subjectDescImgs[6].imgId}${subjectDescImgs[6].suffix}">
								<div class="mgauto w100">
									<span class="text-word">初一</span>
								</div>
							</li>
							<li class="change-class" imgPath="image/priview/${subjectDescImgs[7].imgId}${subjectDescImgs[7].suffix}">
								<div class="mgauto w100">
									<span class="text-word">初二</span>
								</div>
							</li>
							<li class="change-class" imgPath="image/priview/${subjectDescImgs[8].imgId}${subjectDescImgs[8].suffix}">
								<div class="mgauto w100">
									<span class="text-word">初三</span>
								</div>
							</li>
						</ul>
						<ul class="course-items-ul hide">
							
						</ul>
						<ul class="course-items-ul hide">
							
						</ul>
					</div>
				</div>
			</div>
			<div class="layui-row mt20 mb20">
				<div class="layui-col-md12 ht600">
					<div class="banner w100 h100 flex w80 mgauto ovh">
						<img alt="" class="w98 mgauto" id="course-item-img" src="image/priview/${subjectDescImgs[0].imgId}${subjectDescImgs[0].suffix}">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container bggray3 pb50 pt50">
		<div class="layui-container">
			<div class="layui-row">
				<div class="row_title ht100">
					<div class="big-title">
						<span class="cl6">让每个学生拥有自己专属的教学团队</span>
					</div>
				</div>
			</div>
			<div class="layui-row">
				<div class="layui-col-md12 ht600 mt20">
					<div class="banner w100 h100 flex w60 mgauto">
						<img alt="" class="w98 mgauto" id="course_team_bg" src="image/priview/${teambg.imgId}${teambg.suffix}">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pb50 pt50 bgwhite">
		<div class="layui-container">
			<div class="layui-row">
				<div class="row_title ht100">
					<div class="big-title">
						<span class="cl6">中考代数专题测评效率对比</span>
					</div>
				</div>
			</div>
			<div class="layui-row mt20">
				<div class="layui-col-md4 ht600 fl w33">
					<div class="abs-item-cols abs-item-3">
						<div class="cols-item-2 tc">
							<div class="cols-item-line cl6 bgddd ft22 w100 flex">
								<p class="mgauto">对比项</p>
							</div>
							<div class="cols-item-line cl6 ft16 w100 flex">
								<p class="mgauto">科目</p>
							</div>
							<div class="cols-item-line cl6 ft16 w100 flex">
								<p class="mgauto">主要知识点数量</p>
							</div>
							<div class="cols-item-line cl6 ft16 w100 flex">
								<p class="mgauto">平均测试题数</p>
							</div>
							<div class="cols-item-line cl6 ft16 w100 flex">
								<p class="mgauto">平均测试时间<br><span class="ft14">（每道题3分钟）</span></p>
							</div>
							<div class="cols-item-line cl6 ft16 w100 flex">
								<p class="mgauto">课次数<br><span class="ft14">（每周2小时）</span></p>
							</div>
							<div class="cols-item-line cl6 ft16 w100 flex">
								<p class="mgauto">测评精准度</p>
							</div>
						</div>
					</div>
				</div>
				<div class="layui-col-md4 ht500 fl w33">
					<div class="abs-item-cols abs-item">
						<div class="cols-item-2 tc">
							<div class="cols-item-line white bgorange ft22 w100 flex">
								<p class="mgauto">文上华®人工智能学习系统</p>
							</div>
							<div class="cols-item-line orange ft16 w100 flex">
								<p class="mgauto fwbd">中考代数</p>
							</div>
							<div class="cols-item-line orange ft16 w100 flex">
								<p class="mgauto fwbd">74</p>
							</div>
							<div class="cols-item-line orange ft16 w100 flex">
								<p class="mgauto fwbd">8</p>
							</div>
							<div class="cols-item-line orange ft16 w100 flex">
								<p class="mgauto fwbd">24分钟</p>
							</div>
							<div class="cols-item-line orange ft16 w100 flex">
								<p class="mgauto fwbd">0.4<br><span class="ft14 fwnm">（半次课）</span></p>
							</div>
							<div class="cols-item-line orange ft16 w100 flex">
								<p class="mgauto fwbd">92%</p>
							</div>
						</div>
					</div>
				</div>
				<div class="layui-col-md4 mt10 ht500 fl w33">
					<div class="abs-item-cols abs-item-2">
						<div class="bdgray cols-item-2 tc">
							<div class="cols-item-line cl6 bggray ft22 w100 flex ht50">
								<p class="mgauto">传统学习系统/卷子</p>
							</div>
							<div class="cols-item-line cl6 ft16 w100 flex">
								<p class="mgauto fwbd">中考代数</p>
							</div>
							<div class="cols-item-line cl6 ft16 w100 flex">
								<p class="mgauto fwbd">74</p>
							</div>
							<div class="cols-item-line cl6 ft16 w100 flex">
								<p class="mgauto fwbd">222</p>
							</div>
							<div class="cols-item-line cl6 ft16 w100 flex">
								<p class="mgauto fwbd">666分钟</p>
							</div>
							<div class="cols-item-line cl6 ft16 w100 flex">
								<p class="mgauto fwbd">11<br><span class="ft14 fwnm">（5-6次课/1个半月）</span></p>
							</div>
							<div class="cols-item-line cl6 ft16 w100 flex">
								<p class="mgauto fwbd">90%</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@include file="base/footer.jsp" %>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/base.js"></script>
	<script>
		layui.use(['carousel', 'form'], function(){
	      var carousel = layui.carousel
	      ,form = layui.form
	      ,$ = layui.jquery;
	      
	      
	       $(".change-img").on("click",function(){
	      		var index = $(this).index();
	      		var activeUl= $(".course-items-grade .course-items-ul").eq(index);
	      		if($(activeUl).find("li").length>0){
	      			var grageActive = $(activeUl).find("li.active");
	      			var imgPath = $(grageActive).attr("imgPath");
	      			$("#course-item-img").attr("src",imgPath);
	      		}else{
	      			var imgPath = $(this).attr("imgPath");
	      			$("#course-item-img").attr("src",imgPath);
	      		}
	      });
	      
	      $(".change-img").on("mouseover",function(){
	      		var index = $(this).index();
	      		$(".course-items-grade .course-items-ul").hide();
	      		$(".course-items-grade .course-items-ul").eq(index).show();
	      		
	      		$(this).siblings().removeClass("active");
	      		var boxs= $(this).siblings();
	      		for(var i=0;i<boxs.length;i++){
	      			var imgName = $(boxs[i]).find(".text-logo").attr("img1");
	      			$(boxs[i]).find(".text-logo").find("img").attr("src","images/"+imgName);
	      		}
	      		
	      		$(this).addClass("active");
	      		var imgName = $(this).find(".text-logo").attr("img2");
	      		$(this).find(".text-logo").find("img").attr("src","images/"+imgName);
	      		
	      		var activeUl= $(".course-items-grade .course-items-ul").eq(index);
	      		if($(activeUl).find("li").length>0){
	      			var grageActive = $(activeUl).find("li.active");
	      			var imgPath = $(grageActive).attr("imgPath");
	      			$("#course-item-img").attr("src",imgPath);
	      		}else{
	      			var imgPath = $(this).attr("imgPath");
	      			$("#course-item-img").attr("src",imgPath);
	      		}
	      });
	      
	      $(".change-class").on("click",function(){
	      		$(this).siblings().removeClass("active");
	      		$(this).addClass("active");
	      		
	      		var imgPath = $(this).attr("imgPath");
	      		$("#course-item-img").attr("src",imgPath);
	      });
	    });
	</script>
</body>
</html>