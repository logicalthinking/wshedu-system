<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	request.setAttribute("active","index");
%>
<jsp:include page="../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>${sysName }-首页</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> -->
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="shortcut icon" href="images/logo-icon.png" type="image/x-icon" />
	<script type="text/javascript" src="js/bridge.js"></script>
	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/index/base.css">
	<link rel="stylesheet" type="text/css" href="css/index/index.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- <link rel="stylesheet" type="text/css" href="css/index/star.css">
	<script type="text/javascript" src="js/prefixfree.min.js"></script> -->
	<style type="text/css">
		#newBridge .nb-middle{
			top: 55% !important;
		}
	</style>
	
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script>
		layui.use(['carousel', 'form','layer'], function(){
	      var carousel = layui.carousel
	      ,form = layui.form
	      ,$ = layui.jquery;
	       
		   var ua = navigator.userAgent;

			var ipad = ua.match(/(iPad).*OS\s([\d_]+)/),
			
			isIphone =!ipad && ua.match(/(iPhone\sOS)\s([\d_]+)/),
			
			isAndroid = ua.match(/(Android)\s+([\d.]+)/),
			
			isMobile = isIphone || isAndroid;
			
			//判断
			
			if(isMobile){
				$('#index_banner').height(700);
			}else{
				$(window).on('resize', function() {
					var $content = $('#index_banner');
					$content.height($(this).height());
			    }).resize();
			}
	    });
	</script>
	
</head>
<body>
	<%@include file="base/header.jsp" %>
	<div class="container">
		<div class="banner w100" id="index_banner"
			 style="min-height:700px;background:url('image/priview/${titlebg.imgId}${titlebg.suffix}') no-repeat;perspective: 340px;">
			<!-- <div class="stars"></div> -->
			<canvas id="index-canvas" class="abs w100 h100"></canvas>
			<div class="w100 h100 abs flex">
				<div class="mgauto tc">
					<div class="big-title mt80" style="font-family: '隶书'">
						文上华<sup>&reg;</sup>人工智能学习系统
					</div>
					<div class="small-title mt150">
						AI精准定位薄弱知识点
					</div>
					<div class="normal-title mt30">
						助力中小学生提升学习效率
					</div>
					<div class="title-btn mt30 flex tc">
						<a id="applyBtn" class="layui-btn layui-btn-small apply-btn abs" style="margin-left: 268px;">申请试用</a>
					</div>
				</div>
			</div>
			<a name="index" class="index-tag"></a>
			<div class="down-btn" style="">
				<a href="javascript:;" id="downBtn">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</div>
		</div>
	</div>
	<div class="container pb50 pt50 bggray2">
		<div class="layui-container">
			<div class="layui-row mt20">
				<div class="row_title ht100">
					<div class="big-title">
						文上华<sup>&reg;</sup>人工智能学习系统
					</div>
				</div>
			</div>
			<div class="layui-row mt20">
				<div class="layui-col-md12 ht110 bg6 bdrt5 flex">
					<ul class="row-ul white">
						<li>
							<p>Teacher</p>
							<span>（老师）</span>
						</li>
						<li>
							<p class="ftsz40">+</p>
						</li>
						<li>
							<p>AI</p>
							<span>（人工智能）</span>
						</li>
						<li><p class="ftsz40">+</p></li>
						<li>
							<p>Data</p>
							<span>（大数据）</span>
						</li>
						<li><p class="ftsz40">=</p></li>
						<li>
							<p class="mt5 ftsz28">文上华<sup>&reg;</sup>TAD教学模式</p>
						</li>
					</ul>
				</div>
				<div class="layui-col-md12 ht110 bd1 flex">
					<ul class="row-ul row-ul-new">
						<li>
							<p>新手老师</p>
						</li>
						<li>
							<p class="gray">+</p>
						</li>
						<li>
							<p class="orange">文上华<sup>&reg;</sup>人工智能学习系统</p>
						</li>
						<li><p class="gray">=</p></li>
						<li>
							<p>优秀老师</p>
						</li>
					</ul>
				</div>
				<div class="layui-col-md12 ht110 bd1 bdtn bdrb5 flex">
					<ul class="row-ul row-ul-new">
						<li>
							<p>优秀老师</p>
						</li>
						<li>
							<p class="gray">+</p>
						</li>
						<li>
							<p class="orange">文上华<sup>&reg;</sup>人工智能学习系统</p>
						</li>
						<li><p class="gray">=</p></li>
						<li>
							<p class="pencil-click">
								<a href="javascript:;"><i class="fa fa-pencil"></i></a>
							</p>
						</li>
					</ul>
				</div>
			</div>
			<div class="layui-row mt40 layui-col-space30">
				<div class="layui-col-md4 fl w33">
					<div class="cols-item tc">
						<div class="cols-item-logo mt20">
							<div class="icon_bg banner learn_icon" style="background:url('image/priview/${iconImgs[0].imgId}${iconImgs[0].suffix}') no-repeat;">
							</div>
						</div>
						
						<div class="cols-item-title-1 mt20">AI + 大数据</div>
						<div class="cols-item-title-2 mt15">提升教学效果</div>
						<div class="cols-item-title-3 mt15">实现教学质量规模化提升</div>
					</div>
				</div>
				<div class="layui-col-md4 fl w33">
					<div class="cols-item tc">
						<div class="cols-item-logo mt20">
							<div class="icon_bg banner learn_icon" style="background:url('image/priview/${iconImgs[1].imgId}${iconImgs[1].suffix}') no-repeat;">
							</div>
						</div>
						<div class="cols-item-title-1 mt20">AI + 大数据</div>
						<div class="cols-item-title-2 mt15">提升教学效果</div>
						<div class="cols-item-title-3 mt15">实现教学质量规模化提升</div>
					</div>
				</div>
				<div class="layui-col-md4  fl w33">
					<div class="cols-item tc">
						<div class="cols-item-logo mt20">
							<div class="icon_bg banner learn_icon" style="background:url('image/priview/${iconImgs[2].imgId}${iconImgs[2].suffix}') no-repeat;">
							</div>
						</div>
						<div class="cols-item-title-1 mt20">AI + 大数据</div>
						<div class="cols-item-title-2 mt15">提升教学效果</div>
						<div class="cols-item-title-3 mt15">实现教学质量规模化提升</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pt50 pb50 bgwhite">
		<div class="layui-container">
			<div class="layui-row ht100 mt20">
				<div class="row_title">
					<div class="big-title">
						文上华<sup>&reg;</sup>TAD教学模式图解
					</div>
				</div>
			</div>
			<div class="layui-row">
				<div class="layui-col-md12 ht600 mt20 bgwhite tc">
					<div class="banner w100 h100 flex w60 mgauto">
						<%-- <img alt="" class="w98 mgauto" id="education_model" src="image/priview/${modelbg.imgId}${modelbg.suffix}"> --%>
						<img alt="" class="w98 mgauto" id="education_model">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pb50 pt50 bggray2">
		<div class="layui-container">
			<div class="layui-row">
				<div class="row_title ht100">
					<div class="big-title">
						<span class="orange">文上华<sup>&reg;</sup>TAD教学模式</span><span>和传统教学模式的对比</span>
					</div>
				</div>
			</div>
			<div class="layui-row mt20">
				<div class="layui-col-md6 ht500 fl w50">
					<div class="abs abs-item">
						<div class="cols-item-2 tc">
							<div class="cols-item-line white bgorange ft22 w100 flex">
								<p class="mgauto">文上华®TAD教学模式</p>
							</div>
							<div class="cols-item-line orange ft16 w100 flex">
								<p class="mgauto tl">AI系统找到薄弱知识点针对性教学，少做题，多提分</p>
							</div>
							<div class="cols-item-line orange ft16 w100 flex">
								<p class="mgauto tl">普通老师使用文上华人工智能学习系统教学，也能达到名师教学效果</p>
							</div>
							<div class="cols-item-line orange ft16 w100 flex">
								<p class="mgauto tl">AI系统为每一个学生定制学习路径和学习方式，真正因材施教</p>
							</div>
							<div class="cols-item-line orange ft16 w100 flex">
								<p class="mgauto tl">大数据分析教学过程和教学效果，效果直观，问题客观</p>
							</div>
						</div>
					</div>
				</div>
				<div class="layui-col-md6 mt20 ht500 fl w50">
					<div class="abs abs-item-2">
						<div class="bdgray cols-item-2 tc">
							<div class="cols-item-line cl6 bggray ft22 w100 flex">
								<p class="mgauto">传统教学模式</p>
							</div>
							<div class="cols-item-line cl6 ft16 w100 flex">
								<p class="mgauto">依靠题海战术，学生痛苦，提分慢</p>
							</div>
							<div class="cols-item-line cl6 ft16 w100 flex">
								<p class="mgauto">完全依靠老师经验，优质资源有限</p>
							</div>
							<div class="cols-item-line cl6 ft16 w100 flex">
								<p class="mgauto">学生学习进度不一，无法个性化</p>
							</div>
							<div class="cols-item-line cl6 ft16 w100 flex">
								<p class="mgauto">教学效果不直观，没有提分也找不到原因</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container bgwhite pb50 pt50">
		<div class="layui-container">
			<div class="layui-row ht100">
				<div class="row_title">
					<%-- <div class="title-img-bg" style="background:url('image/priview/${titleIconImgs[0].imgId}${titleIconImgs[0].suffix}') no-repeat;">
						
					</div> --%>
					<div class="title-img-bg title_icon">
						
					</div>
				</div>
			</div>
			<div class="layui-row layui-col-space10">
				<div class="layui-col-md4 ht350 mt20 ot15 fl w33">
					<div class="w100 h100 flex ovh">
						<%-- <img alt="" src="image/priview/${contactImgs[0].imgId}${contactImgs[0].suffix}" 
							class="w95 mgauto contact-imgs"> --%>
							<img alt="" class="w95 mgauto contact-imgs contact_us">
					</div>
				</div>
				<div class="layui-col-md4 ht350 mt20 ot15 fl w33">
					<div class="w100 h100 flex ovh">
						<%-- <img alt="" src="image/priview/${contactImgs[1].imgId}${contactImgs[1].suffix}" 
							class="w95 mgauto contact-imgs"> --%>
						<img alt="" class="w95 mgauto contact-imgs contact_us">
					</div>
				</div>
				<div class="layui-col-md4 ht350 mt20 ot15 fl w33">
					<div class="w100 h100 flex ovh">
						<%-- <img alt="" src="image/priview/${contactImgs[2].imgId}${contactImgs[2].suffix}" 
							class="w95 mgauto contact-imgs"> --%>
						<img alt="" class="w95 mgauto contact-imgs contact_us">
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@include file="base/footer.jsp" %>
	<script type="text/javascript" src="js/base.js"></script>
	<script src="js/TweenLite.min.js"></script>
	<script src="js/EasePack.min.js"></script>
	<script src="js/index-canvas.js"></script>
	
	<script type="text/javascript">
		layui.use(['carousel', 'form','layer'], function(){
	      var carousel = layui.carousel
	      ,form = layui.form
	      ,$ = layui.jquery;
	       
	       $("#downBtn").on('click', function() {
				var height = $('#index_banner').height()-100;
				$(document.documentElement).animate({
					scrollTop:height
				},300);
			});
			
			var imgTypes = ['education_model','title_icon','contact_us'];
			$.post("getWebImgs",{
				imgTypes:imgTypes
			},function(res){
				if(res.error_code == "200"){
					if(res.result!=null && res.result){
						if(res.result.education_model!=null
							&& res.result.education_model.length>0){
							$.each(res.result.education_model,function(index,item){
								$("#education_model").attr("src","image/priview/"+item.imgId+item.suffix);
							});
						}
						
						if(res.result.title_icon!=null
							&& res.result.title_icon.length>0){
							$.each(res.result.title_icon,function(index,item){
								var titleImg= $(".title_icon");
								$(titleImg[index]).css("background","url('image/priview/"+item.imgId+item.suffix+"') no-repeat");
							});
						}
						
						if(res.result.contact_us!=null
							&& res.result.contact_us.length>0){
							$.each(res.result.contact_us,function(index,item){
								var contactImgs= $(".contact-imgs");
								$(contactImgs[index]).attr("src","image/priview/"+item.imgId+item.suffix);
							});
						}
					}
				}else{
					//layer.msg(res.msg,{icon:2});
				}
			});
	    });
	</script>
</body>
</html>
