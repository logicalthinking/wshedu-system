<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	request.setAttribute("active","teacher");
%>
<jsp:include page="../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>${sysName }-师资力量</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> -->
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="shortcut icon" href="images/logo-icon.png" type="image/x-icon" />
	<script type="text/javascript" src="js/bridge.js"></script>
	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/index/base.css">
	<link rel="stylesheet" type="text/css" href="css/index/index.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<%@include file="base/header.jsp" %>
	<div class="container pt100">
		<div class="banner ht500 w100 flex" style="background:url('image/priview/${titlebg.imgId}${titlebg.suffix}') no-repeat;">
			<c:if test="${titlebg.wordStatus == 'Y'}">
				<div class="big-title mgauto">
					师资力量
				</div>
			</c:if>
		</div>
	</div>
	<div class="container pt80 pb80 bgwhite">
		<div class="layui-container">
			<div class="layui-row ht100">
				<div class="row_title">
					<div class="big-title">
						6层严格筛选  万里挑一  的好老师
					</div>
				</div>
			</div>
			<div class="layui-row">
				<div class="layui-col-md12 ht600 mt20 mb20 bgwhite tc">
					<div class="banner w100 h100" style="background:url('image/priview/${teacherbg.imgId}${teacherbg.suffix}') no-repeat;">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pb80 pt80 bggray">
		<div class="layui-container">
			<div class="layui-row">
				<div class="row_title ht100">
					<div class="big-title">
						全明星师资阵容
					</div>
				</div>
			</div>
			<c:if test="${not empty teacherImgs }">
				<div class="layui-row layui-col-space20">
					<c:forEach items="${teacherImgs }" var="item" varStatus="i">
						<c:if test="${i.count<=3 }">
							<div class="layui-col-md4 ht550 fl w33">
								<div class="teacher-item">
									<div class="img-box">
										<div class="imgbg" style="background:url('image/priview/${item.imgId}${item.suffix}') no-repeat;"></div>
									</div>
									<div class="img-title">
										<p>
											${teacherArticles[i.count-1].title }
											<c:if test="${not empty teacherArticles[i.count-1].remark }">
												（${teacherArticles[i.count-1].remark }）
											</c:if>
										</p>
									</div>
									<div class="img-content">
										${teacherArticles[i.count-1].artContent }
									</div>
								</div>
							</div>
						</c:if>
					</c:forEach>
				</div>
				<c:if test="${teacherImgs.size()>3 }">
					<div class="layui-row layui-col-space20 mt40">
						<c:forEach begin="4" end="${teacherImgs.size() }" var="i" step="1">
							<div class="layui-col-md4 ht550 fl w33">
								<div class="teacher-item">
									<div class="img-box">
										<div class="imgbg" style="background:url('image/priview/${teacherImgs[i-1].imgId}${teacherImgs[i-1].suffix}') no-repeat;"></div>
									</div>
									<div class="img-title">
										<p>
											${teacherArticles[i-1].title }
											<c:if test="${not empty teacherArticles[i-1].remark }">
												（${teacherArticles[i-1].remark }）
											</c:if>
										</p>
									</div>
									<div class="img-content">
										${teacherArticles[i-1].artContent }
									</div>
								</div>
							</div>
						</c:forEach>
					</div>
				</c:if>
			</c:if>
		</div>
	</div>
	<%@include file="base/footer.jsp" %>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/base.js"></script>
	<script>
		layui.use(['carousel', 'form'], function(){
	      var carousel = layui.carousel
	      ,form = layui.form
	      ,$ = layui.jquery;
	      
	    });
	</script>
</body>
</html>