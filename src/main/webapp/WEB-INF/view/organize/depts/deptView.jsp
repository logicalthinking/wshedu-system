<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>查看部门</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
	<link rel="stylesheet" href="css/table.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		.layui-select-disabled input{
			color: #333 !important; 
			cursor: text !important;
		}
		.layui-select-disabled input:hover{
			color: #333 !important; 
			cursor: text !important;
		}
	</style>
</head>
<body>
	<div style="margin: 15px;">
		<form class="layui-form" action="">
			<input type="hidden" name="deptId" value="${deptInfo.deptId }">
			<div class="layui-form-item">
				<label class="layui-form-label">上级部门</label>
				<div class="layui-input-block">
					<select name="parentId" disabled="disabled">
						<option value="">--请选择--</option>
						<c:forEach items="${deptInfos }" var="item" varStatus="i">
							<option value="${item.deptId }"
								<c:if test="${deptInfo.parentId == item.deptId }">selected="selected"</c:if>
							>${item.deptName }</option>
						</c:forEach>
						<option value="0"
							<c:if test="${deptInfo.parentId == '0' }">selected="selected"</c:if>
						>无</option>
						<option value="-1"
							<c:if test="${deptInfo.parentId == '-1' }">selected="selected"</c:if>
						>无</option>
					</select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">部门名称</label>
				<div class="layui-input-block">
					<input type="text" name="deptName" readonly="readonly" value="${deptInfo.deptName }" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">部门编号</label>
				<div class="layui-input-block">
					<input type="text" name="deptCode" readonly="readonly" value="${deptInfo.deptCode }"  autocomplete="off" class="layui-input">
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script>
		layui.use(['form','layer'], function() {
			var form = layui.form,
			layer = layui.layer,
			$ = layui.jquery;
			
		});
	</script>
</body>
</html>