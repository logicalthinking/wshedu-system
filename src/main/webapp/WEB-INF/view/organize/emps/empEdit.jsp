<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>编辑员工</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
	<link rel="stylesheet" type="text/css" href="plugins/multiSelect/css/multiSelect.css">
	<link rel="stylesheet" href="css/table.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		#dept_block .layui-form-select{
			display: none !important;
		}
	</style>
</head>
<body>
	<div style="margin: 15px;">
		<form class="layui-form" action="">
			<input type="hidden" id="empId" name="empId" value="${empInfo.empId}">
			<div class="layui-form-item">
				<label class="layui-form-label">姓名<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="text" name="empName" value="${empInfo.empName}" lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">工号<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="text" name="empCode" value="${empInfo.empCode}" lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">所在部门<i class="color-red">*</i></label>
				<div class="layui-input-block" id="dept_block">
					<input type="hidden" id="selectDeptIds" value="${deptIds}">
					<input type="hidden" id="selectDeptNames" value="${deptNames}">
					<select id="deptIds" name="deptIds">
						<c:forEach items="${deptInfos }" var="item" varStatus="i">
							<option value="${item.deptId }">${item.deptName }</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">入职时间</label>
				<div class="layui-input-block">
					<c:if test="${not empty empInfo.joinTime }">
						<input type="text" name="joinTime" value="${empInfo.joinTime.split(' ')[0] }" 
							id="joinTime" autocomplete="off" class="layui-input">
					</c:if>
					<c:if test="${empty empInfo.joinTime }">
						<input type="text" name="joinTime" id="joinTime" autocomplete="off" class="layui-input">
					</c:if>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">职位</label>
				<div class="layui-input-block">
					<input type="text" name="job" value="${empInfo.job}" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">在职状态</label>
				<div class="layui-input-block">
					<select name="jobStatus">
						<option value="">--请选择--</option>
						<c:forEach items="${jobStatus }" var="item" varStatus="i">
							<option value="${item.key }"
								<c:if test="${empInfo.jobStatus == item.key }">selected="selected"</c:if>
							>${item.value }</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="layui-form-item" style="text-align: center;">
				<div class="layui-input-block" style="margin-left:0px;">
					<button class="layui-btn" lay-submit="empInfo" lay-filter="empInfo">保存</button>
					<button type="button" class="layui-btn layui-btn-primary" id="cancerBtn">取消</button>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="plugins/multiSelect/js/jquery.min.js"></script>
	<script type="text/javascript" src="plugins/multiSelect/multiSelect.js"></script>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script>
		layui.use(['form','layer','laydate'], function() {
			var form = layui.form,
			layer = layui.layer,
			$ = layui.jquery;
			
			var names=[];
			var values=[];
			var selectDeptIds = $("#selectDeptIds").val();
			var selectDeptNames = $("#selectDeptNames").val();
			if(selectDeptIds && selectDeptNames){
				values = selectDeptIds.split(",");
				names = selectDeptNames.split(",");
			}
			
			$("#deptIds").multiSelect({
				placeholder:'选择部门', // 可省略
				style:{  // 可省略
					width:'98%',
					height:'30px'
				},data:{
					name:names, //数组
					value:values  //数组
				}
			});
			
			var laydate = layui.laydate;
  
			laydate.render({
			  elem: '#joinTime',
			  type:'date',
			  format:'yyyy-MM-dd'
			});
			
			//监听提交
			form.on('submit(empInfo)', function(data) {
				var result = data.field;
				
				result.deptIds = $("#deptIds").multiVal();
				
				if(result.deptIds==null || result.deptIds.length<=0 ){
	  				$("#multiSelect_deptIds0").css('border','1px solid red');
					layer.msg('请选择部门',{'icon': 5 },function(){
						$("#mt_select_input_deptIds0").focus();
					});
	  				return false;
				}
				$("#multiSelect_deptIds0").css('border','1px solid #e6e6e6');
				
				var load = layer.load(2);
				//提交
				$.post("manage/updateEmpInfo",result,function(res){
					if(res.error_code == "200"){
						layer.msg(res.msg,{icon:1});
						$('#cancerBtn').click();
						parent.layui.table.reload('dataTable');
					}else{
						layer.msg(res.msg,{icon:2});
					}
					layer.close(load);
				});
				
				return false;
			});
			
			$('#cancerBtn').on('click',function(){
				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
				parent.layer.close(index);
			});
			
		});
	</script>
</body>
</html>