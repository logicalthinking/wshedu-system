<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html>
<head>
    <base href="<%= basePath%>">
    <title>员工列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
    <link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
    <link rel="stylesheet" href="css/table.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
	<div class="admin-main">
		<blockquote class="layui-elem-quote">
			<form class="layui-form">
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">员工姓名</label>
						<div class="layui-input-inline">
							<input type="text" id="empName" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">员工编号</label>
						<div class="layui-input-inline">
							<input type="text" id="empCode" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<a href="javascript:;" class="layui-btn layui-btn-small" id="searchBtn">
							<i class="layui-icon">&#xe615;</i> 搜索
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-small layui-btn-danger" id="resetBtn">
							<i class="layui-icon">&#xe639;</i> 清除
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-small" id="addBtn" module-field="employee_manage_add">
							<i class="layui-icon">&#xe608;</i> 新增员工
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-danger layui-btn-small" id="delBtn" 
							module-field="employee_manage_del">
							<i class="layui-icon">&#xe640;</i> 删除员工
						</a>
					</div>
				</div>
			</form>
		</blockquote>
		<fieldset class="layui-elem-field">
			<legend>员工列表</legend>
			<div class="layui-field-box">
				<table class="site-table table-hover" id="dataTable" lay-filter="dataTable">
					
				</table>
			</div>
		</fieldset>
		<div class="admin-table-page">
			<div id="page" class="page">
			</div>
		</div>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/validModule.js"></script>
	<script>
		var pageIndex = 1;
		var pageSize = 10;
		layui.use(['laypage','layer','table','form'], function() {
			var $ = layui.jquery,
				laypage = layui.laypage,
				layer = layui.layer;
			var form = layui.form;
			
			var table = layui.table;
			var dataTable = table.render({
				elem: '#dataTable'
				,url:'manage/empInfoList'
				,where:{
					pageIndex:pageIndex,
					pageSize:pageSize,
					sortName:'create_time',
					sortOrder:'desc'
				}
				,method:'post'
				,request: {
					pageName: 'pageIndex' //页码的参数名称，默认：page
					,limitName: 'pageSize' //每页数据量的参数名，默认：limit
				}
				,parseData: function(res){
					return {
						'code':(res.error_code=='200'?0:-1),
						'msg': res.msg,
					    'count': res.total,
					    'data': res.result
					};
				}
				,autoSort:false
				,cellMinWidth: 100 
			    ,cols: [[
			      {checkbox:true, width:'7%'}
			      ,{field:'empName', title: '员工姓名',sort: true}
			      ,{field:'empCode', title: '员工编号',sort: true,}
			      ,{field:'deptName', title: '所在部门',sort: true,templet:function(row){
			      	var deptName = '';
			      	if(row.empDeptLinks!=null && row.empDeptLinks.length>0){
			      		for(var i=0;i<row.empDeptLinks.length;i++){
			      			if(row.empDeptLinks[i].deptInfo){
			      				deptName+= row.empDeptLinks[i].deptInfo.deptName+' ';
			      			}
			      		}
			      	}
			      	return deptName;
			      }}
			      ,{field:'job', title: '职位',sort: true,}
			      ,{field:'jobStatus', title: '在岗状态',sort: true,}
			      ,{field:'joinTime', title: '入职时间',sort: true,templet:function(row){
			      	if(row.joinTime){
			      		return row.joinTime.split(' ')[0];	
			      	}
			      	return '';
			      }}
			      ,{field:'userId', title: '开通账号', sort: true,templet:function(row){
			      	var btn='<a href="javascript:;" lay-event="openAccount" class="layui-btn layui-btn-mini">开通账号</a>';
			      	var text='<a href="javascript:;" lay-event="findUser">已开通</a>';
			      	return (row.userId?text:btn);
			      }}
			      ,{field:'state', title: '状态', sort: true,templet:function(row){
			      	return row.state == 'Y'?'<span class="green">有效</span>':'<span class="red">无效</span>';
			      }}
			      ,{title: '操作', width:'15%', templet:function(row){
			      	var html ='<a href="javascript:;" lay-event="search" class="layui-btn layui-btn-normal layui-btn-mini" module-field="employee_manage_view">查看</a>';
						html+='<a href="javascript:;" lay-event="edit" class="layui-btn layui-btn-mini" module-field="employee_manage_edit">修改</a>';
						html+= '<a href="javascript:;" lay-event="del" module-field="employee_manage_del"'+
							' class="layui-btn layui-btn-danger layui-btn-mini">删除</a>';
			      	return html;
			      }}
				]],
				done: function(res, curr, count){
					data = res.data;
					laypage.render({
						elem: 'page',
						count: count,
						limit: pageSize,
						curr:pageIndex,
						groups:5,
						limits: [10, 20, 30, 40, 50],
						jump: function(obj, first) {
							var curr = obj.curr;
							pageIndex = curr;
							if(!first) {
								table.reload('dataTable', {
							    where: {
							      empName:$("#empName").val(),
			        			  empCode:$("#empCode").val(),
							      pageIndex:curr
								  ,pageSize:pageSize
							    }
							  });
							}
						}
					});
					
					var parentModuleCode = "sys_manage";
					var moduleCode = "employee_manage";
					validModule(parentModuleCode,moduleCode);
					
				}
			});
			
			
			table.on('rowDouble(dataTable)', function(obj){
				findEmp(obj.data.empId);
			});
			
			var sortNames={
				'empName':'emp_name',
				'empCode':'emp_code',
				'job':'job',
				'jobStatus':'job_status',
				'joinTime':'join_time',
				'state':'state'
			};
			
			table.on('sort(dataTable)', function(obj){
				  pageIndex = 1;
				  table.reload('dataTable', {
				    where: {
				      empName:$("#empName").val(),
			          empCode:$("#empCode").val(),
				      sortName: sortNames[obj.field]
				      ,sortOrder: obj.type
				      ,pageIndex:pageIndex
					  ,pageSize:pageSize
				    }
				  });
			});
			
			table.on('tool(dataTable)', function(obj){
			  switch(obj.event){
			    case 'search':
			      findEmp(obj.data.empId);
			    break;
			    case 'edit':
			      editEmp(obj.data.empId);
			    break;
		      	case 'del':
			      delemp(obj.data.empId,"确认删除该员工吗？");
			    break;
		      	case 'openAccount':
			      openAccount(obj.data.empId);
			    break;
		      	case 'findUser':
			      findUser(obj.data.userId);
			    break;
			  };
			});
			
			$('#delBtn').on('click', function() {
				var checkStatus = table.checkStatus('dataTable');
				if(checkStatus.data.length==0){
					layer.msg("请选择一行！",{icon:0});
					return;
				}
				var empIds = [];
				var flag = false;
				for(var i=0;i<checkStatus.data.length;i++){
					empIds.push(checkStatus.data[i].empId);
				}
				
				delemp(empIds.join(","),"确认删除所选员工吗？");
			});
			
			function delemp(empId,message){
				layer.confirm(message,{icon:3,title:'提示信息'},function(index){
					layer.close(index);
					var load = layer.load(2);
					$.post("manage/deleteEmpInfo",{empIds:empId },function(res){
						if(res.error_code == "200"){
							layer.msg(res.msg,{icon:1});
							pageIndex = 1;
							table.reload('dataTable');
						}else{
							layer.msg(res.msg,{icon:2});
						}
						layer.close(load);
					});
				});
			}

			$('#addBtn').on('click', function() {
				layer.open({
					type:2,
					title:'新增员工',
					area: ['760px', '640px'],
					content:'manage/empAddPage'
				});
			});
			
			function editEmp(empId){
				layer.open({
					type:2,
					title:'编辑员工',
					area: ['760px', '640px'],
					content:'manage/empEditPage?empId='+empId
				});
			}
			
			function openAccount(empId){
				layer.open({
					type:2,
					title:'开通账号',
					area: ['760px', '640px'],
					content:'manage/empUserAddPage?empId='+empId
				});
			}
			
			function findEmp(empId){
				layer.open({
					type:2,
					title:'查看员工',
					area: ['620px', '440px'],
					content:'manage/empViewPage?empId='+empId
				});
			}
			
			function findUser(userId){
				layer.open({
					type:2,
					title:'查看用户',
					area: ['620px', '440px'],
					content:'manage/userViewPage?userId='+userId
				});
			}
			
			form.on('select(empType)', function(data) {
				reloadTable();
			});
			
			function  reloadTable(){
				pageIndex = 1;
				table.reload('dataTable', {
				  where: {
				    empName:$("#empName").val(),
			        empCode:$("#empCode").val(),
				    pageIndex:pageIndex,
				    pageSize:pageSize
				  }
				});
			}
			
			$('#searchBtn').on('click', function() {
				reloadTable();
			});
			
			$('#resetBtn').on('click', function() {
				$("#empName").val("");
				$("#empCode").val("");
				form.render();
			});
		});
	</script>
</body>

</html>
