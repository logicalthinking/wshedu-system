<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>编辑员工</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
	<link rel="stylesheet" type="text/css" href="plugins/multiSelect/css/multiSelect.css">
	<link rel="stylesheet" href="css/table.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div style="margin: 15px;">
		<form class="layui-form" action="">
			<input type="hidden" id="empId" name="empId" value="${empInfo.empId}">
			<div class="layui-form-item">
				<label class="layui-form-label">姓名</label>
				<div class="layui-input-block">
					<input type="text" name="empName" readonly="readonly" value="${empInfo.empName}" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">工号</label>
				<div class="layui-input-block">
					<input type="text" name="empCode" readonly="readonly" value="${empInfo.empCode}" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">所在部门</label>
				<div class="layui-input-block" id="dept_block">
					<input type="text" id="selectDeptNames" readonly="readonly" value="${deptNames}"  autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">入职时间</label>
				<div class="layui-input-block">
					<c:if test="${not empty empInfo.joinTime }">
						<input type="text" name="joinTime" value="${empInfo.joinTime.split(' ')[0] }" 
							id="joinTime" autocomplete="off" class="layui-input">
					</c:if>
					<c:if test="${empty empInfo.joinTime }">
						<input type="text" name="joinTime" readonly="readonly" id="joinTime" autocomplete="off" class="layui-input">
					</c:if>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">职位</label>
				<div class="layui-input-block">
					<input type="text" name="job" readonly="readonly" value="${empInfo.job}" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">在职状态</label>
				<div class="layui-input-block">
					<input type="text" name="jobStatus" readonly="readonly" value="${empInfo.jobStatus}" autocomplete="off" class="layui-input">
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="plugins/multiSelect/js/jquery.min.js"></script>
	<script type="text/javascript" src="plugins/multiSelect/multiSelect.js"></script>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script>
		layui.use(['form','layer','laydate'], function() {
			var form = layui.form,
			layer = layui.layer,
			$ = layui.jquery;
			
		});
	</script>
</body>
</html>