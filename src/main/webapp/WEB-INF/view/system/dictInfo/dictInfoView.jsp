<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>查看字典</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
	<link rel="stylesheet" href="css/table.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		.layui-form-label{
			font-weight: bold;
		}
		.layui-input-block{
			width: 50%;
		}
		.layui-table-tool-temp{
			padding-right: 0px;	
		}
	</style>
</head>
<body>
	<div style="margin: 15px;">
		<fieldset class="layui-elem-field">
			<legend>字典信息</legend>
			<input type="hidden" value="${dictInfo.dictId}" id="dictId">
			<div class="layui-form-item">
				<c:if test="${dictInfo.dictValue != 'root' }">
					<div style="position: absolute;right:40px;">
						<a href="javascript:;" module-field="dict_manage_edit" class="layui-btn layui-btn-small" id="editBtn">
							<i class="layui-icon">&#xe642;</i> 编辑
						</a>
						<a href="javascript:;" module-field="dict_manage_del" class="layui-btn layui-btn-danger layui-btn-small" id="delBtn">
							<i class="layui-icon">&#xe640;</i> 删除
						</a>
					</div>
				</c:if>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">字典名称</label>
				<div class="layui-input-block lineHeight38">
					${dictInfo.dictName}
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">字典值</label>
				<div class="layui-input-block lineHeight38">
					${dictInfo.dictValue}
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">备注</label>
				<div class="layui-input-block lineHeight38">
					${dictInfo.dictDesc}
				</div>
			</div>
		</fieldset>
		<fieldset class="layui-elem-field">
			<legend>子节点列表</legend>
			<div class="layui-field-box">
				<table class="site-table table-hover" id="dataTable" lay-filter="dataTable">
					
				</table>
			</div>
			<div class="admin-table-page">
				<div id="page" class="page">
				</div>
			</div>
		</fieldset>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/validModule.js"></script>
	<script type="text/html" id="barBtn">
  		<a class="layui-btn layui-btn-xs" module-field="dict_manage_add" lay-event="add">新增</a>
 		<a class="layui-btn layui-btn-danger layui-btn-xs" module-field="dict_manage_del" lay-event="del">删除</a>
		<a class="layui-btn layui-btn-xs layui-btn-normal" module-field="dict_manage_sort" style="float:right;margin-top: 5px;" lay-event="sort">排序</a>
	</script>
	<script>
		var pageIndex = 1;
		var pageSize = 10;
		layui.use(['form','layer','table','laypage'], function() {
			var form = layui.form,
				layer = layui.layer,
				$ = layui.jquery;
				var laypage = layui.laypage;
				
				var dictPid = $("#dictId").val();
				
				var table = layui.table;
				var dataTable = table.render({
					elem: '#dataTable'
					,url:'manage/dictInfoList'
					,where:{
						dictPid:dictPid,
						pageIndex:pageIndex,
						pageSize:pageSize,
						sortName:'rank',
						sortOrder:'asc'
					}
					,method:'post'
					,request: {
						pageName: 'pageIndex' //页码的参数名称，默认：page
						,limitName: 'pageSize' //每页数据量的参数名，默认：limit
					}
					,parseData: function(res){
						return {
							'code':(res.error_code=='200'?0:-1),
							'msg': res.msg,
						    'count': res.total,
						    'data': res.result
						};
					}
					,autoSort:false
					,cellMinWidth: 100,
					toolbar: '#barBtn',
					defaultToolbar:[]
					,height: 'full-330'
				    ,cols: [[
				      {checkbox:true, width:'7%'}
				      ,{field:'dictName', title: '字典名称',sort: true}
				      ,{field:'dictValue', title: '字典值',sort: true,}
				      ,{field:'dictDesc', title: '备注', sort: true}
				      ,{title: '操作', width:'20%', templet:function(row){
				      	var html ='<a href="javascript:;" lay-event="edit" module-field="dict_manage_edit" class="layui-btn layui-btn-mini">修改</a>';
							html+= '<a href="javascript:;" lay-event="del"  module-field="dict_manage_del" class="layui-btn layui-btn-danger layui-btn-mini">删除</a>';
				      	return html;
				      }}
					]],
					//page:true,
					done: function(res, curr, count){
						data = res.data;
						laypage.render({
							elem: 'page',
							count: count,
							limit: pageSize,
							curr:pageIndex,
							groups:5,
							limits: [10, 20, 30, 40, 50],
							jump: function(obj, first) {
								var curr = obj.curr;
								pageIndex = curr;
								if(!first) {
									table.reload('dataTable', {
								    where: {
								      dictPid:dictPid,
								      sortName:'rank',
									  sortOrder:'asc',
								      pageIndex:curr
									  ,pageSize:pageSize
								    }
								  });
								}
							}
						});
						
						var parentModuleCode = "sys_manage";
						var moduleCode = "dict_manage";
						validModule(parentModuleCode,moduleCode);
					}
			});
			
			table.on('tool(dataTable)', function(obj){
			  switch(obj.event){
			    case 'edit':
			      editDict(obj.data.dictId);
			    break;
		      	case 'del':
			      delDict(obj.data.dictId,"确认删除该字典项吗？");
			    break;
			  };
			});
			
			table.on('toolbar(dataTable)', function(obj){
			  switch(obj.event){
			    case 'add':
			      addDict();
			    break;
			    case 'del':
			      delDicts();
			    break;
			    case 'sort':
			      sortDict();
			    break;
			  };
			});
			
			function delDicts(){
				var checkStatus = table.checkStatus('dataTable');
				if(checkStatus.data.length==0){
					layer.msg("请选择一行！",{icon:0});
					return;
				}
				var dictIds = [];
				for(var i=0;i<checkStatus.data.length;i++){
					dictIds.push(checkStatus.data[i].dictId);
				}
				delDict(dictIds.join(","),"确认删除所选字典项吗？");
			}
			
			function delDict(dictId,message){
				layer.confirm(message,{icon:3,title:'提示信息'},function(index){
					layer.close(index);
					var load = layer.load(2);
					$.post("manage/deleteDictInfo",{dictIds:dictId },function(res){
						if(res.error_code == "200"){
							layer.msg(res.msg,{icon:1});
							pageIndex = 1;
							table.reload('dataTable');
							loadParentTree();
						}else{
							layer.msg(res.msg,{icon:2});
						}
						layer.close(load);
					});
				});
			}
			
			function sortDict(){
				var dictPid = $("#dictId").val();
				layer.open({
					type:2,
					title:'字典排序',
					area: ['540px', '380px'],
					content:'manage/dictInfoSortPage?dictPid='+dictPid
				});
			}
			
			function addDict(){
				var dictPid = $("#dictId").val();
				layer.open({
					type:2,
					title:'新增字典',
					area: ['400px', '380px'],
					content:'manage/dictInfoAddPage?dictPid='+dictPid
				});
			}
			
			function editDict(dictId){
				var dictPid = $("#dictId").val();
				layer.open({
					type:2,
					title:'编辑字典',
					area: ['400px', '380px'],
					content:'manage/dictInfoEditPage?dictPid='+dictPid+'&dictId='+dictId
				});
			}
			
			$('#editBtn').on('click', function() {
				var dictId = $("#dictId").val();
				layer.open({
					type:2,
					title:'编辑字典',
					area: ['400px', '380px'],
					content:'manage/dictInfoEditPage?dictId='+dictId+'&opType=parent'
				});
			});
			
			$('#delBtn').on('click', function() {
				var dictId = $("#dictId").val();
				layer.confirm("确认删除该字典信息吗？",{icon:3,title:'提示信息'},function(index){
					layer.close(index);
					var load = layer.load(2);
					$.post("manage/deleteDictInfo",{dictIds:dictId },function(res){
						if(res.error_code == "200"){
							layer.msg(res.msg,{icon:1});
							window.parent.location.reload();
						}else{
							layer.msg(res.msg,{icon:2});
						}
						layer.close(load);
					});
				});
			});
			
		});
		
		function loadParentTree(){
			window.parent.reloadTree();
		}
	</script>
</body>
</html>
