<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>编辑模块</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
	<link rel="stylesheet" href="css/table.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		.layui-form-label{
			width: 120px;
		}
		.layui-input-block{
			margin-left: 160px;
		}
	</style>
</head>
<body>
	<div style="margin: 15px;padding-right: 30px;">
		<form class="layui-form" action="">
			<input type="hidden" name="moduleId" value="${sysModule.moduleId }"/>
			<input type="hidden" value="${module}" id="module">
			<input type="hidden" value="${opType}" id="opType">
			<div class="layui-form-item">
				<label class="layui-form-label">上级模块<i class="color-red">*</i></label>
				<div class="layui-input-block lineHeight38">
					<input hidden="parentId" name="parentId" value="${sysModule.parentId }">
					<c:if test="${not empty sysModule.parentModuleName }">${sysModule.parentModuleName }</c:if>
					<c:if test="${empty sysModule.parentModuleName }">无</c:if>
					<!-- <select name="parentId" id="parentId" lay-verify="required">
						<option value="">--请选择--</option>
						<option value="0">无</option>
					</select> -->
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">模块名称<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="text" name="moduleName" value="${sysModule.moduleName }" lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">模块编码<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="text" name="moduleCode" value="${sysModule.moduleCode }" lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">模块类型<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<c:forEach items="${moduleTypes }" var="item" varStatus="i">
						<input type="radio" name="moduleType" value="${item.key }" title="${item.value }"
							<c:if test="${sysModule.moduleType == item.key }">checked="checked"</c:if>
						>
					</c:forEach>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">跳转地址</label>
				<div class="layui-input-block">
					<input type="text" name="moduleUrl" value="${sysModule.moduleUrl}" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">图标</label>
				<div class="layui-input-block">
					<input type="text" name="icon" id="moduleIcon" value="${sysModule.icon}" style="width: 240px;display: inline;margin-right: 10px;" 
						autocomplete="off" class="layui-input">
					<i id="icon-set" style="font-size: 18px;"></i>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">是否默认模块<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="radio" name="defaultStatus" value="Y" title="是"
						<c:if test="${ sysModule.defaultStatus == 'Y' }">checked="checked"</c:if>
					>
					<input type="radio" name="defaultStatus" value="N"  
						<c:if test="${ sysModule.defaultStatus != 'Y' }">checked="checked"</c:if>
					 title="否">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">是否显示<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="radio" name="showStatus" value="Y" 
						<c:if test="${ sysModule.showStatus == 'Y' }">checked="checked"</c:if>
					 title="是">
					<input type="radio" name="showStatus" value="N" 
						<c:if test="${ sysModule.showStatus != 'Y' }">checked="checked"</c:if>
					 title="否">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">序号</label>
				<div class="layui-input-block">
					<input type="text" name="rank" value="${ sysModule.rank}" lay-verify="number" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">备注</label>
				<div class="layui-input-block">
					<input type="text" name="remark" value="${sysModule.remark}" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item" style="text-align: center;">
				<div class="layui-input-block" style="margin-left:0px;">
					<button class="layui-btn" lay-submit="moduleInfo" lay-filter="moduleInfo">保存</button>
					<button type="button" class="layui-btn layui-btn-primary" id="cancerBtn">取消</button>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script>
		layui.use(['form','layer','laydate'], function() {
			var form = layui.form,
				layer = layui.layer,
				$ = layui.jquery;
				
				
			var value = $("#moduleIcon").val();
			if(value){
				if(value.indexOf("layui-icon")!=-1){
					$("#icon-set").html("");
					$("#icon-set").attr("class","layui-icon "+value);
				}else if(value.indexOf("fa fa-")!=-1){
					$("#icon-set").html("");
					$("#icon-set").attr("class",value);
					$("#icon-set").css("font-fimaly","FontAwesome !important");
				}else{
					value = stringToEntity(value,16);
					$("#moduleIcon").val(value);
					$("#icon-set").attr("class","layui-icon");
					$("#icon-set").html(value);
				}
			}
			
			function stringToEntity(str,radix){
			  var arr=[];
			  //返回的字符实体默认10进制，也可以选择16进制
			  radix=radix||0;
			  for(var i=0;i<str.length;i++){                               
			    arr.push((!radix?'&#'+str.charCodeAt(i):'&#x'+str.charCodeAt(i).toString(16))+';');
			  }
			  var tmp=arr.join('');
			  return tmp;
			}
			
		  	function entityToString(entity){
			  var entities=entity.split(';');
			  entities.pop();
			  var tmp='';
			  for(var i=0;i<entities.length;i++){
			    var num=entities[i].trim().slice(2);
			    if(num[0]==='x'){
			    	num=parseInt(num.slice(1),16);
			    }else{
		    		num=parseInt(num);
		    	} 
			    tmp+=String.fromCharCode(num);
			  }
			  return tmp;
			}
			
			$('#moduleIcon').on({
				'keydown':function(){
					var value = this.value;
					if(value.indexOf("&#")!=-1){
						$("#icon-set").attr("class","layui-icon");
						$("#icon-set").html(value);
					}else if(value.indexOf("fa fa-")!=-1){
						$("#icon-set").html("");
						$("#icon-set").attr("class",value);
						$("#icon-set").css("font-fimaly","FontAwesome !important");
					}else{
						$("#icon-set").html("");
						$("#icon-set").attr("class","layui-icon "+value);
					}
				},
				'keyup':function(){
					var value = this.value;
					if(value.indexOf("&#")!=-1){
						$("#icon-set").html(value);
					}else if(value.indexOf("fa fa-")!=-1){
						$("#icon-set").html("");
						$("#icon-set").attr("class",value);
						$("#icon-set").css("font-fimaly","FontAwesome !important");
					}else{
						$("#icon-set").attr("class","layui-icon "+value);
					}
				}
			});
			
			//监听提交
			form.on('submit(moduleInfo)', function(data) {
				var result = data.field;
				var load = layer.load(2);
				//提交
				$.post("manage/updateSysModule",result,function(res){
					if(res.error_code == "200"){
						layer.msg(res.msg,{icon:1});
						$('#cancerBtn').click();
						
						var opType = $("#opType").val();
						var module = $("#module").val();
						if(opType=="tree" && module == 'parent'){
							window.parent.loadParentTree();
							window.parent.location.reload();
						}else if(opType=="tree" && module!= 'parent'){
							window.parent.loadParentTree();
							parent.layui.table.reload('dataTable');
						}else{
							parent.layui.table.reload('dataTable');
						}
					}else{
						layer.msg(res.msg,{icon:2});
					}
					layer.close(load);
				});
				
				return false;
			});
			
			$('#cancerBtn').on('click',function(){
				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
				parent.layer.close(index);
			});
			
		});
	</script>
</body>
</html>