<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>功能模块树结构</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
	<link rel="stylesheet" href="css/table.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		.layui-elem-field{
			height: 100%;
		}
		.tree-content{
			overflow: auto;
			height: 100%;
			margin-top: -20px;
		}
		.layui-form-item{
			border-bottom: 1px solid #CCC;
		}
	</style>
</head>
<body>
	<div class="admin-main">
		<form class="layui-form" action="">
			<input type="hidden" value="${sysRole.roleId}" id="roleId">
			<input type="hidden" value="${sysRole.roleName}" id="roleName">
			<input type="hidden" value="${adminFlag}" id="adminFlag">
			<fieldset class="layui-elem-field">
				<legend>功能模块树结构</legend>
				<div class="layui-form-item">
					<span class="layui-form-label">
						<input type="checkbox" id="checkAll" lay-skin="primary" lay-filter="checkAll" title="全选">
					</span>
					<div class="layui-input-block" style="line-height: 50px;">
						<a href="javascript:;" class="layui-btn layui-btn-small" module-field="permission_manage_set"
							style="margin-left: 30px;" id="saveBtn">保存</a>
						<c:if test="${adminFlag=='Y' }">
							<a href="javascript:;" class="layui-btn layui-btn-small layui-btn-normal" 
								module-field="permission_manage_refresh" id="reloadBtn">
								<i class="layui-icon">&#xe666;</i> 刷新
							</a>
						</c:if>
					</div>
				</div>
				
				<div class="layui-field-box">
					<div id="tree" class="tree-content">
						
					</div>
				</div>
			</fieldset>
		</form>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/validModule.js"></script>
	<script>
		layui.use([ 'tree', 'layer','form' ], function() {
			var tree = layui.tree,
			layer = layui.layer, 
			$ = layui.jquery;
			var form = layui.form;
			
			$('#tree').html('');
			
			var checkAllFlag;
			var resData;
			var roleId = $("#roleId").val();
			var adminFlag = $("#adminFlag").val();
			var load = layer.load(2);
			$.post('manage/moduleTreeList',{
				parentId:'0',
				roleId:roleId,
				adminFlag:adminFlag
				},function(res){
				if(res.error_code == '200'){
					var data = res.result.moduleTreeVos;
					resData = data;
					checkAllFlag = res.result.checkAll;
					if(adminFlag=='Y'){
						layer.msg("系统管理员暂不能操作",{icon:0});
					}else{
						$("#checkAll").prop("checked",checkAllFlag);
						form.render();
					}
					
					layer.close(load);
					tree.render({
						elem : '#tree',
						data : data,
						id : 'treeId',
						onlyIconControl:true,
						showCheckbox: true,
						//isJump : true,
						click : function(obj) {
							//tree.setChecked('treeId', obj.data.id);
						},
						oncheck:function(obj){
							var flag = checkAll();
							if(flag!=null){
								$("#checkAll").prop("checked",flag);
								form.render();
							}
						},
						//edit: ['add', 'update', 'del'],
						operate:function(obj){
							var type = obj.type;
							var data = obj.data;
							var elem = obj.elem;
							if(type=='add'){
								//新增
								elem.addClass("layui-tree-spread");
								elem.find('.layui-tree-pack').show();
								elem.find('.layui-tree-iconClick i').eq(0).removeClass("layui-icon-addition").addClass("layui-icon-subtraction");
								layer.msg("新增");
							}else if(type=='update'){
								//修改
								 layer.msg(elem.find('.layui-tree-txt').html());
							}else if(type=='del'){
								//删除
								layer.msg("删除");
							}
						}
					});
					
					var parentModuleCode = "sys_manage";
					var moduleCode = "permission_manage";
					validModule(parentModuleCode,moduleCode);
				}
			});
			
			function checkAll(){
				var dataLength = parseDataLength(resData);
				try{
					var checkedData = tree.getChecked('treeId');
					var checkLength = parseDataLength(checkedData);
					
					if(checkLength == dataLength){
						return true;
					}else{
						return false;
					}
				}catch(e){
					$("#checkAll").prop("checked",checkAllFlag);
					form.render();
					return null;
				}
			}
			
			function parseData(listData,flag){
				if(listData){
					for(var i=0;i<listData.length;i++){
						listData[i].checked = flag;
						if(listData[i].children!=null && listData[i].children.length>0 ){
							parseData(listData[i].children);
						}
					}
				}
			}
			
			form.on('checkbox(checkAll)', function(data){
			    var check  = $("#checkAll").prop("checked");
			    if(check == true){
			    	//全选
			    	parseData(resData,true);
			    	tree.reload('treeId', {
					  data:resData
					});
			    }else{
			    	//全不选
			    	parseData(resData,false);
			    	tree.reload('treeId', {
					  data:resData
					});
			    }
			});
			
			function parseDataLength(listData){
				var dataLength=0;
				if(listData){
					for(var i=0;i<listData.length;i++){
						dataLength++;
						if(listData[i].children!=null && listData[i].children.length>0 ){
							dataLength += parseDataLength(listData[i].children);
						}
					}
				}
				return dataLength;
			}
			
			$(window).on('resize', function() {
				var $content = $('.admin-main');
				$content.height($(this).height()-80);
				$(".tree-content").height($content.height()-60);
			}).resize();
			
			$('#reloadBtn').on('click', function() {
				var data = resData;
				$("#adminFlag").val("N");
				parseDataDisabled(data,false);
				tree.reload('treeId', {
				  data:data
				});
			});
			
			function parseDataDisabled(listData,flag){
				if(listData){
					for(var i=0;i<listData.length;i++){
						listData[i].disabled = flag;
						if(listData[i].children!=null && listData[i].children.length>0 ){
							parseDataDisabled(listData[i].children,flag);
						}
					}
				}
			}
			
			
			var moduleIds = [];
			function parseModuleIds(listData){
				for(var i=0;i<listData.length;i++){
					moduleIds.push(listData[i].id);
					if(listData[i].children!=null && listData[i].children.length>0 ){
						parseModuleIds(listData[i].children);
					}
				}
			}
			
			$('#saveBtn').on('click', function() {
				var roleId = $("#roleId").val();
				var adminFlag = $("#adminFlag").val();
				var roleName = $("#roleName").val();
				if(adminFlag=='Y'){
					layer.msg("系统管理员暂不能操作",{icon:0});
					return;
				}
				
			    var checkedData = tree.getChecked('treeId'); //获取选中节点的数据
			    if(checkedData!=null && checkedData.length>0){
			    	moduleIds = [];
			    	parseModuleIds(checkedData);
			    	
			    	layer.confirm("确认为角色【"+roleName+"】设置所选功能权限吗？",{icon:3,title:'提示信息'},function(index){
						layer.close(index);
						var load = layer.load(2);
						$.post("manage/addSysModuleLinkByBusi",
						{busiId:roleId,moduleIds:moduleIds
						 },function(res){
							if(res.error_code == "200"){
								layer.msg("设置成功",{icon:1});
								location.reload();
							}else{
								layer.msg(res.msg,{icon:2});
							}
							layer.close(load);
						});
					});
			    }else{
			    	layer.msg("请选择功能",{icon:0});
			    }
			});
			
		});
	</script>
</body>
</html>
