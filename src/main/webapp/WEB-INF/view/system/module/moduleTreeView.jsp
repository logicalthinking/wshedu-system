<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>查看模块</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
	<link rel="stylesheet" href="css/table.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		.layui-form-label{
			font-weight: bold;
		}
		.layui-input-block{
			width: 50%;
		}
		.layui-table-tool-temp{
			padding-right: 0px;	
		}
	</style>
</head>
<body>
	<div style="margin: 15px;">
		<fieldset class="layui-elem-field">
			<legend>模块信息</legend>
			<input type="hidden" value="${sysModule.moduleId}" id="moduleId">
			<div class="layui-form-item">
				<div style="position: absolute;right:40px;">
					<a href="javascript:;" module-field="module_manage_view" class="layui-btn layui-btn-small layui-btn-normal" id="findBtn">
						<i class="layui-icon">&#xe609;</i> 查看详情
					</a>
					<c:if test="${sysModule.moduleCode != 'root' }">
						<a href="javascript:;" module-field="module_manage_edit" class="layui-btn layui-btn-small" id="editBtn">
							<i class="layui-icon">&#xe642;</i> 编辑
						</a>
						<a href="javascript:;" module-field="module_manage_del" class="layui-btn layui-btn-danger layui-btn-small" id="delBtn">
							<i class="layui-icon">&#xe640;</i> 删除
						</a>
					</c:if>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">模块名称</label>
					<div class="layui-input-inline lineHeight38">
						${sysModule.moduleName}
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">是否显示</label>
					<div class="layui-input-inline lineHeight38">
						<c:if test="${ sysModule.showStatus == 'Y' }">是</c:if>
						<c:if test="${ sysModule.showStatus != 'Y' }">否</c:if>
					</div>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">模块编号</label>
					<div class="layui-input-inline lineHeight38">
						${sysModule.moduleCode}
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label" style="width: 110px;margin-left: -30px;">是否默认模块</label>
					<div class="layui-input-inline lineHeight38">
						<c:if test="${ sysModule.defaultStatus == 'Y' }">是</c:if>
						<c:if test="${ sysModule.defaultStatus != 'Y' }">否</c:if>
					</div>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">模块类型</label>
					<div class="layui-input-inline lineHeight38">
						${sysModule.moduleTypeName}
					</div>
				</div>
				<c:if test="${sysModule.moduleType =='module_link' }">
					<div class="layui-inline">
						<label class="layui-form-label">跳转链接</label>
						<div class="layui-input-inline lineHeight38">
							${sysModule.moduleUrl}
						</div>
					</div>
				</c:if>
			</div>
		</fieldset>
		<fieldset class="layui-elem-field">
			<legend>子模块列表</legend>
			<div class="layui-field-box">
				<table class="site-table table-hover" id="dataTable" lay-filter="dataTable">
					
				</table>
			</div>
			<div class="admin-table-page">
				<div id="page" class="page">
				</div>
			</div>
		</fieldset>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/validModule.js"></script>
	<script type="text/html" id="barBtn">
  		<a class="layui-btn layui-btn-xs" module-field="module_manage_add" lay-event="add">新增</a>
 		<a class="layui-btn layui-btn-danger layui-btn-xs" module-field="module_manage_del" lay-event="del">删除</a>
		<a class="layui-btn layui-btn-xs layui-btn-normal" module-field="module_manage_sort" style="float:right;margin-top: 5px;" lay-event="sort">排序</a>
	</script>
	<script>
		var pageIndex = 1;
		var pageSize = 10;
		layui.use(['form','layer','table','laypage'], function() {
			var form = layui.form,
				layer = layui.layer,
				$ = layui.jquery;
				var laypage = layui.laypage;
				
				var parentId = $("#moduleId").val();
				
				var table = layui.table;
				var dataTable = table.render({
					elem: '#dataTable'
					,url:'manage/sysModuleList'
					,where:{
						parentId:parentId,
						pageIndex:pageIndex,
						pageSize:pageSize,
						sortName:'rank',
						sortOrder:'asc'
					}
					,method:'post'
					,request: {
						pageName: 'pageIndex' //页码的参数名称，默认：page
						,limitName: 'pageSize' //每页数据量的参数名，默认：limit
					}
					,parseData: function(res){
						return {
							'code':(res.error_code=='200'?0:-1),
							'msg': res.msg,
						    'count': res.total,
						    'data': res.result
						};
					}
					,autoSort:false
					,cellMinWidth: 100,
					toolbar: '#barBtn',
					defaultToolbar:[]
					,height: 'full-330'
				    ,cols: [[
				      {checkbox:true, width:'7%'}
				      ,{field:'moduleName', width:'14%', title: '模块名称',sort: true}
				      ,{field:'moduleCode', title: '模块编码',sort: true,}
				      ,{field:'moduleTypeName', width:'10%', title: '模块类型', sort: true}
				      ,{field:'moduleUrl', title: '跳转链接', sort: true}
				      ,{field:'showStatus', width:'10%', title: '是否显示', sort: true,templet:function(row){
				      	return row.showStatus == 'Y'?'是':'否';
				      }}
				      ,{title: '操作', width:'20%', templet:function(row){
				      	var html ='<a href="javascript:;" lay-event="search" module-field="module_manage_view" class="layui-btn layui-btn-normal layui-btn-mini">查看</a>';
				      		html+='<a href="javascript:;" lay-event="edit" module-field="module_manage_edit" class="layui-btn layui-btn-mini">修改</a>';
							html+= '<a href="javascript:;" lay-event="del" module-field="module_manage_del" class="layui-btn layui-btn-danger layui-btn-mini">删除</a>';
				      	return html;
				      }}
					]],
					//page:true,
					done: function(res, curr, count){
						data = res.data;
						laypage.render({
							elem: 'page',
							count: count,
							limit: pageSize,
							curr:pageIndex,
							groups:5,
							limits: [10, 20, 30, 40, 50],
							jump: function(obj, first) {
								var curr = obj.curr;
								pageIndex = curr;
								if(!first) {
									table.reload('dataTable', {
								    where: {
								      parentId:parentId,
								      sortName:'rank',
									  sortOrder:'asc',
								      pageIndex:curr
									  ,pageSize:pageSize
								    }
								  });
								}
							}
						});
						
						var parentModuleCode = "sys_manage";
						var moduleCode = "module_manage";
						validModule(parentModuleCode,moduleCode);
					}
			});
			
			
			table.on('rowDouble(dataTable)', function(obj){
				findModule(obj.data.moduleId);
			});
			
			var sortNames={
				'moduleName':'module_name',
				'moduleCode':'module_code',
				'parentModuleName':'parent_id',
				'moduleTypeName':'module_type',
				'moduleUrl':'module_url',
				'defaultStatus':'default_status',
				'showStatus':'show_status',
				'state':'state'
			};
			
			table.on('sort(dataTable)', function(obj){
				  pageIndex = 1;
				  table.reload('dataTable', {
				    where: {
				      parentId:parentId,
				      sortName: sortNames[obj.field]
				      ,sortOrder: obj.type
				      ,pageIndex:pageIndex
					  ,pageSize:pageSize
				    }
				  });
			});
			
			table.on('tool(dataTable)', function(obj){
			  switch(obj.event){
			    case 'search':
			      findModule(obj.data.moduleId);
			    break;
			    case 'edit':
			      editModule(obj.data.moduleId);
			    break;
			    case 'del':
			      delModule(obj.data.moduleId,"确认删除该模块吗？");
			    break;
			  };
			});
			
			function findModule(moduleId){
				layer.open({
					type:2,
					title:'查看模块',
					area: ['680px', '600px'],
					content:'manage/moduleViewPage?moduleId='+moduleId
				});
			}
			
			function editModule(moduleId){
				layer.open({
					type:2,
					title:'编辑模块',
					area: ['740px', '670px'],
					content:'manage/moduleEditPage?moduleId='+moduleId+'&opType=tree'
				});
			}
			
			function delModule(moduleId,message){
				layer.confirm(message,{icon:3,title:'提示信息'},function(index){
					layer.close(index);
					var load = layer.load(2);
					$.post("manage/deleteSysModule",{moduleIds:moduleId },function(res){
						if(res.error_code == "200"){
							layer.msg(res.msg,{icon:1});
							pageIndex = 1;
							table.reload('dataTable');
							loadParentTree();
						}else{
							layer.msg(res.msg,{icon:2});
						}
						layer.close(load);
					});
				});
			}
			
			table.on('toolbar(dataTable)', function(obj){
			  switch(obj.event){
			    case 'add':
			      addModule();
			    break;
			    case 'del':
			      delModules();
			    break;
			    case 'sort':
			      sortModule();
			    break;
			  };
			});
			
			function addModule(){
				layer.open({
					type:2,
					title:'新增模块',
					area: ['740px', '670px'],
					content:'manage/moduleAddPage?parentId='+parentId+'&opType=tree'
				});
			}
			
			function sortModule(){
				layer.open({
					type:2,
					title:'模块排序',
					area: ['540px', '380px'],
					content:'manage/moduleSortPage?parentId='+parentId
				});
			}
			
			function delModules(){
				var checkStatus = table.checkStatus('dataTable');
				if(checkStatus.data.length==0){
					layer.msg("请选择一行！",{icon:0});
					return;
				}
				var moduleIds = [];
				for(var i=0;i<checkStatus.data.length;i++){
					moduleIds.push(checkStatus.data[i].moduleId);
				}
				delModule(moduleIds.join(","),"确认删除所选模块吗？");
			}
			
			$('#findBtn').on('click', function() {
				var moduleId = $("#moduleId").val();
				layer.open({
					type:2,
					title:'查看模块',
					area: ['680px', '600px'],
					content:'manage/moduleViewPage?moduleId='+moduleId
				});
			});
			
			$('#editBtn').on('click', function() {
				var moduleId = $("#moduleId").val();
				layer.open({
					type:2,
					title:'编辑模块',
					area: ['740px', '670px'],
					content:'manage/moduleEditPage?moduleId='+moduleId+'&opType=tree&module=parent'
				});
			});
			
			$('#delBtn').on('click', function() {
				var moduleId = $("#moduleId").val();
				layer.confirm("确认删除该模块信息吗？",{icon:3,title:'提示信息'},function(index){
					layer.close(index);
					var load = layer.load(2);
					$.post("manage/deleteSysModule",{moduleIds:moduleId},function(res){
						if(res.error_code == "200"){
							layer.msg(res.msg,{icon:1});
							window.parent.location.reload();
						}else{
							layer.msg(res.msg,{icon:2});
						}
						layer.close(load);
					});
				});
			});
		});
		
		function loadParentTree(){
			window.parent.reloadTree();
		}
	</script>
</body>
</html>
