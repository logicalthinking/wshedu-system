<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>查看模块</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
	<link rel="stylesheet" href="css/table.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		.layui-form-label{
			width: 120px;
		}
		.layui-input-block{
			margin-left: 160px;
		}
		
		.layui-form-pane .layui-form-label{
			width: 160px;
		}
		.layui-form-pane .layui-input-block{
			margin-left: 160px;
		}
	</style>
</head>
<body>
	<div style="margin: 15px;padding-right: 30px;">
		<form class="layui-form layui-form-pane" action="">
			<input type="hidden" name="moduleId" value="${sysModule.moduleId }"/>
			<div class="layui-form-item">
				<label class="layui-form-label">上级模块<i class="color-red">*</i></label>
				<div class="layui-input-block lineHeight38">
					<input hidden="parentId" name="parentId" value="${sysModule.parentId }">
					<c:if test="${not empty sysModule.parentModuleName }">
						<input type="text" readonly="readonly" class="layui-input" value="${sysModule.parentModuleName }"/>
					</c:if>
					<c:if test="${empty sysModule.parentModuleName }">
						<input type="text"  class="layui-input" readonly="readonly" value="无"/>
					</c:if>
					<!-- <select name="parentId" id="parentId" lay-verify="required">
						<option value="">--请选择--</option>
						<option value="0">无</option>
					</select> -->
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">模块名称<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="text" name="moduleName" readonly="readonly" value="${sysModule.moduleName }" lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">模块编码<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="text" name="moduleCode" readonly="readonly" value="${sysModule.moduleCode }" lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">模块类型<i class="color-red">*</i></label>
				<div class="layui-input-block" style="border: 1px solid #e6e6e6;line-height: 36px;">
					<span style="margin-left: 10px;">
						<c:forEach items="${moduleTypes }" var="item" varStatus="i">
							<c:if test="${sysModule.moduleType == item.key }">${item.value }</c:if>
						</c:forEach>
					</span>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">跳转地址</label>
				<div class="layui-input-block">
					<input type="text" name="moduleUrl" readonly="readonly" value="${sysModule.moduleUrl}" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">图标</label>
				<div class="layui-input-block">
					<input type="text" name="icon" id="moduleIcon" 
						value="${sysModule.icon}" readonly="readonly" style="width: 240px;display: inline;margin-right: 10px;" 
						autocomplete="off" class="layui-input">
					<i class="layui-icon" id="icon-set" style="font-size: 18px;"></i>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">是否默认模块<i class="color-red">*</i></label>
				<div class="layui-input-block" style="border: 1px solid #e6e6e6;line-height: 36px;">
					 <span style="margin-left: 10px;">
						 <c:if test="${ sysModule.defaultStatus == 'Y' }">是</c:if>
						 <c:if test="${ sysModule.defaultStatus != 'Y' }">否</c:if>
					 </span>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">是否显示<i class="color-red">*</i></label>
				<div class="layui-input-block" style="border: 1px solid #e6e6e6;line-height: 36px;">
					<span style="margin-left: 10px;">
						<c:if test="${ sysModule.showStatus == 'Y' }">是</c:if>
						<c:if test="${ sysModule.showStatus != 'Y' }">否</c:if>
					</span>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">备注</label>
				<div class="layui-input-block">
					<input name="remark" class="layui-input" readonly="readonly" value="${ sysModule.remark}">
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script>
		layui.use(['form','layer','laydate'], function() {
			var form = layui.form,
				layer = layui.layer,
				$ = layui.jquery;
			
			var value = $("#moduleIcon").val();
			if(value){
				if(value.indexOf("layui-icon")!=-1){
					$("#icon-set").html("");
					$("#icon-set").attr("class","layui-icon "+value);
				}else if(value.indexOf("fa fa-")!=-1){
					$("#icon-set").html("");
					$("#icon-set").attr("class",value);
					$("#icon-set").css("font-fimaly","FontAwesome !important");
				}else{
					value = stringToEntity(value,16);
					$("#moduleIcon").val(value);
					$("#icon-set").attr("class","layui-icon");
					$("#icon-set").html(value);
				}
			}
			
			
			function stringToEntity(str,radix){
			  var arr=[];
			  //返回的字符实体默认10进制，也可以选择16进制
			  radix=radix||0;
			  for(var i=0;i<str.length;i++){                               
			    arr.push((!radix?'&#'+str.charCodeAt(i):'&#x'+str.charCodeAt(i).toString(16))+';');
			  }
			  var tmp=arr.join('');
			  return tmp;
			}
			
		  	function entityToString(entity){
			  var entities=entity.split(';');
			  entities.pop();
			  var tmp='';
			  for(var i=0;i<entities.length;i++){
			    var num=entities[i].trim().slice(2);
			    if(num[0]==='x'){
			    	num=parseInt(num.slice(1),16);
			    }else{
		    		num=parseInt(num);
		    	} 
			    tmp+=String.fromCharCode(num);
			  }
			  return tmp;
			}
			
		});
	</script>
</body>
</html>