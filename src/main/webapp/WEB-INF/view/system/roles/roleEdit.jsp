<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>编辑角色</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
	<link rel="stylesheet" href="css/table.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div style="margin: 15px;">
		<form class="layui-form" action="">
			<input type="hidden" name="roleId" value="${ sysRole.roleId}">
			<div class="layui-form-item">
				<label class="layui-form-label">角色名称<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="text" name="roleName"
					<c:if test="${ adminCode == sysRole.roleCode }"> readonly="readonly"</c:if>
					value="${sysRole.roleName }" lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">角色编号<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="text" name="roleCode" 
					<c:if test="${ adminCode == sysRole.roleCode }"> readonly="readonly"</c:if>
					value="${sysRole.roleCode }" lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">状态</label>
				<div class="layui-input-block">
					<input type="radio" name="state" title="有效" value="Y"
						<c:if test="${ adminCode == sysRole.roleCode }">disabled="disabled"</c:if>
						<c:if test="${sysRole.state=='Y' }">checked="checked"</c:if>
					>
					<input type="radio" name="state" title="无效" value="N"
						<c:if test="${ adminCode == sysRole.roleCode }">disabled="disabled"</c:if>
						<c:if test="${sysRole.state!='Y' }">checked="checked"</c:if>
					>
				</div>
			</div>
			<c:if test="${ sysRole.roleCode == adminCode }">
				<div class="layui-form-item" style="width: 100%;text-align: center;">
					<label class="red">系统管理员 不能修改！</label>
				</div>
			</c:if>
			
			<c:if test="${ sysRole.roleCode != adminCode  }">
				<div class="layui-form-item" style="text-align: center;">
					<div class="layui-input-block" style="margin-left:0px;">
						<button class="layui-btn" lay-submit="roleInfo" lay-filter="roleInfo">保存</button>
						<button type="button" class="layui-btn layui-btn-primary" id="cancerBtn">取消</button>
					</div>
				</div>
			</c:if>
			
		</form>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script>
		layui.use(['form','layer'], function() {
			var form = layui.form,
				layer = layui.layer,
				$ = layui.jquery;
				
			//监听提交
			form.on('submit(roleInfo)', function(data) {
				var result = data.field;
				
				var load = layer.load(2);
				//提交
				$.post("manage/updateSysRole",result,function(res){
					if(res.error_code == "200"){
						layer.msg(res.msg,{icon:1});
						$('#cancerBtn').click();
						parent.layui.table.reload('dataTable');
					}else{
						layer.msg(res.msg,{icon:2});
					}
					layer.close(load);
				});
				
				return false;
			});
			
			$('#cancerBtn').on('click',function(){
				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
				parent.layer.close(index);
			});
			
		});
	</script>
</body>
</html>
