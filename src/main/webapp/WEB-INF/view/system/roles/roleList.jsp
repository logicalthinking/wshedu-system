<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html>
<head>
    <base href="<%= basePath%>">
    <title>角色列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
    <link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
    <link rel="stylesheet" href="css/table.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
	<div class="admin-main">
		<blockquote class="layui-elem-quote">
			<form class="layui-form">
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">角色名称</label>
						<div class="layui-input-inline">
							<input type="text" id="roleName" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">角色编号</label>
						<div class="layui-input-inline">
							<input type="text" id="roleCode" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<a href="javascript:;" class="layui-btn layui-btn-small" id="searchBtn">
							<i class="layui-icon">&#xe615;</i> 搜索
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-small layui-btn-danger" id="resetBtn">
							<i class="layui-icon">&#xe639;</i> 清除
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-small"  module-field="role_manage_add" id="addBtn">
							<i class="layui-icon">&#xe608;</i> 新增角色
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-danger layui-btn-small"  module-field="role_manage_del" id="delBtn">
							<i class="layui-icon">&#xe640;</i> 删除角色
						</a>
					</div>
				</div>
			</form>
		</blockquote>
		<fieldset class="layui-elem-field">
			<legend>角色列表</legend>
			<div class="layui-field-box">
				<table class="site-table table-hover" id="dataTable" lay-filter="dataTable">
					
				</table>
			</div>
		</fieldset>
		<div class="admin-table-page">
			<div id="page" class="page">
			</div>
		</div>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/validModule.js"></script>
	<script>
		var pageIndex = 1;
		var pageSize = 10;
		var adminCode = '${adminCode}';
		layui.use(['laypage','layer','table','form'], function() {
			var $ = layui.jquery,
				laypage = layui.laypage,
				layer = layui.layer;
			var form = layui.form;
			
			var table = layui.table;
			var dataTable = table.render({
				elem: '#dataTable'
				,url:'manage/sysRoleList'
				,where:{
					pageIndex:pageIndex,
					pageSize:pageSize
				}
				,method:'post'
				,request: {
					pageName: 'pageIndex' //页码的参数名称，默认：page
					,limitName: 'pageSize' //每页数据量的参数名，默认：limit
				}
				,parseData: function(res){
					return {
						'code':(res.error_code=='200'?0:-1),
						'msg': res.msg,
					    'count': res.total,
					    'data': res.result
					};
				}
				,autoSort:false
				,cellMinWidth: 100 
			    ,cols: [[
			      {checkbox:true, width:'7%'}
			      ,{field:'roleName', title: '角色名称',sort: true}
			      ,{field:'roleCode', title: '角色编号',sort: true,}
			      ,{field:'state', title: '状态', sort: true,templet:function(row){
			      	return row.state == 'Y'?'<span class="green">有效</span>':'<span class="red">无效</span>';
			      }}
			      ,{title: '操作', width:'15%', templet:function(row){
			      	var html ='<a href="javascript:;" lay-event="search" module-field="role_manage_view" class="layui-btn layui-btn-normal layui-btn-mini">查看</a>';
					if(adminCode != row.roleCode){
		      			html+='<a href="javascript:;" lay-event="edit" module-field="role_manage_edit"'+
		      			' class="layui-btn layui-btn-mini">修改</a>';
						html+= '<a href="javascript:;" lay-event="del" module-field="role_manage_del"'+
						' class="layui-btn layui-btn-danger layui-btn-mini">删除</a>';
					}
			      	return html;
			      }}
				]],
				done: function(res, curr, count){
					laypage.render({
						elem: 'page',
						count: count,
						limit: pageSize,
						curr:pageIndex,
						groups:5,
						limits: [10, 20, 30, 40, 50],
						jump: function(obj, first) {
							var curr = obj.curr;
							pageIndex = curr;
							if(!first) {
								table.reload('dataTable', {
							    where: {
							      roleName:$("#roleName").val(),
				    			  roleCode:$("#roleCode").val(),
							      pageIndex:curr
								  ,pageSize:pageSize
							    }
							  });
							}
						}
					});
					
					var parentModuleCode = "sys_manage";
					var moduleCode = "role_manage";
					validModule(parentModuleCode,moduleCode);
				}
			});
			
			
			table.on('rowDouble(dataTable)', function(obj){
				findRole(obj.data.roleId);
			});
			
			var sortNames={
				'roleName':'role_name',
				'roleCode':'role_code',
				'state':'state'
			};
			
			table.on('sort(dataTable)', function(obj){
				  pageIndex = 1;
				  table.reload('dataTable', {
				    where: {
				      roleName:$("#roleName").val(),
				      roleCode:$("#roleCode").val(),
				      sortName: sortNames[obj.field]
				      ,sortOrder: obj.type
				      ,pageIndex:pageIndex
					  ,pageSize:pageSize
				    }
				  });
			});
			
			table.on('tool(dataTable)', function(obj){
			  switch(obj.event){
			    case 'search':
			      findRole(obj.data.roleId);
			    break;
			    case 'edit':
			      editRole(obj.data.roleId);
			    break;
		      	case 'del':
			      delRole(obj.data.roleId,"确认删除该角色吗？");
			    break;
			  };
			});
			
			$('#delBtn').on('click', function() {
				var checkStatus = table.checkStatus('dataTable');
				if(checkStatus.data.length==0){
					layer.msg("请选择一行！",{icon:0});
					return;
				}
				var roleIds = [];
				var flag = false;
				for(var i=0;i<checkStatus.data.length;i++){
					var roleCode = checkStatus.data[i].roleCode;
					if(roleCode == adminCode){
						flag = true;
					}else{
						roleIds.push(checkStatus.data[i].roleId);
					}
				}
				
				if(flag == true){
					layer.msg("系统管理员不能删除！",{icon:0});
					return;
				}
				
				if(roleIds.length<=0){
					layer.msg("请选择一行！",{icon:0});
					return;
				}
				delRole(roleIds.join(","),"确认删除所选角色吗？");
			});
			
			function delRole(roleId,message){
				layer.confirm(message,{icon:3,title:'提示信息'},function(index){
					layer.close(index);
					var load = layer.load(2);
					$.post("manage/deleteSysRole",{roleIds:roleId },function(res){
						if(res.error_code == "200"){
							layer.msg(res.msg,{icon:1});
							pageIndex = 1;
							table.reload('dataTable');
						}else{
							layer.msg(res.msg,{icon:2});
						}
						layer.close(load);
					});
				});
			}

			$('#addBtn').on('click', function() {
				layer.open({
					type:2,
					title:'新增角色',
					area: ['500px', '250px'],
					content:'manage/roleAddPage'
				});
			});
			
			function editRole(roleId){
				layer.open({
					type:2,
					title:'编辑角色',
					area: ['500px', '300px'],
					content:'manage/roleEditPage?roleId='+roleId
				});
			}
			
			function findRole(roleId){
				layer.open({
					type:2,
					title:'查看角色',
					area: ['460px', '260px'],
					content:'manage/roleViewPage?roleId='+roleId
				});
			}
			
			$('#searchBtn').on('click', function() {
				pageIndex = 1;
				table.reload('dataTable', {
				  where: {
				    roleName:$("#roleName").val(),
				    roleCode:$("#roleCode").val(),
				    pageIndex:pageIndex,
				    pageSize:pageSize
				  }
				});
			});
			
			$('#resetBtn').on('click', function() {
				$("#roleName").val("");
				$("#roleCode").val("");
				form.render();
			});
		});
	</script>
</body>

</html>
