<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html>
<head>
    <base href="<%= basePath%>">
    <title>日志列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
    <link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
    <link rel="stylesheet" href="css/table.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
	<div class="admin-main">
		<blockquote class="layui-elem-quote">
			<form class="layui-form">
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">用户名</label>
						<div class="layui-input-inline">
							<input type="text" id="userName" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">操作名称</label>
						<div class="layui-input-inline">
							<input type="text" id="operateName" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">操作时间</label>
						<div class="layui-input-inline" style="width: 420px;">
							<input type="text" id="startTime" style="width: 190px;display: inline;" placeholder="YYYY-MM-DD HH:mm:ss" class="layui-input">
							- 
							<input type="text" id="endTime" style="width: 190px;display: inline;" placeholder="YYYY-MM-DD HH:mm:ss" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<a href="javascript:;" class="layui-btn layui-btn-small" id="searchBtn">
							<i class="layui-icon">&#xe615;</i> 搜索
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-small layui-btn-danger" id="resetBtn">
							<i class="layui-icon">&#xe639;</i> 清除
						</a>
					</div>
				</div>
			</form>
		</blockquote>
		<fieldset class="layui-elem-field">
			<legend>日志列表</legend>
			<div class="layui-field-box">
				<table class="site-table table-hover" id="dataTable" lay-filter="dataTable">
					
				</table>
			</div>
		</fieldset>
		<div class="admin-table-page">
			<div id="page" class="page">
			</div>
		</div>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/validModule.js"></script>
	<script>
		var pageIndex = 1;
		var pageSize = 10;
		var data;

		layui.use(['laypage','layer','table','form','laydate'], function() {
			var $ = layui.jquery,
				laypage = layui.laypage,
				layer = layui.layer;
			var form = layui.form;
			
			
			var laydate = layui.laydate;
  
			laydate.render({
			  elem: '#startTime',
			  type:'datetime',
			  format:'yyyy-MM-dd HH:mm:ss'
			});
			laydate.render({
			  elem: '#endTime',
			  type:'datetime',
			  format:'yyyy-MM-dd HH:mm:ss'
			});
			
			var table = layui.table;
			var dataTable = table.render({
				elem: '#dataTable'
				,url:'manage/sysLogInfoList'
				,where:{
					pageIndex:pageIndex,
					pageSize:pageSize,
					sortName:'create_time',
					sortOrder:'desc'
				}
				,method:'post'
				,request: {
					pageName: 'pageIndex' //页码的参数名称，默认：page
					,limitName: 'pageSize' //每页数据量的参数名，默认：limit
				}
				,parseData: function(res){
					return {
						'code':(res.error_code=='200'?0:-1),
						'msg': res.msg,
					    'count': res.total,
					    'data': res.result
					};
				}
				,autoSort:false
				,cellMinWidth: 100 
			    ,cols: [[
			      {checkbox:true, width:'7%'}
			      ,{field:'userId', width:'8%', title: '用户名',templet:function(row){
			      	if(row.sysUsers!=null && row.sysUsers!=''){
			      		return row.sysUsers.userName;
			      	}else{
			      		return '';
			      	}
			      }}
			      ,{field:'operateName', width:'10%', title: '操作名称',sort: true}
			      ,{field:'operateUrl', title: '操作地址',sort: true}
			      ,{field:'operateDesc', width:'10%', title: '描述', sort: true}
			      ,{field:'ip', title: 'IP地址', width: '8%', sort: true}
			      ,{field:'port', title: '端口号',width:'7%', sort: true}
			      ,{field:'createTime', title: '操作时间', sort: true}
			      ,{title: '操作', width:'6%', templet:function(row){
			      	var html ='<a href="javascript:;" lay-event="search" module-field="log_manage_view" class="layui-btn layui-btn-normal layui-btn-mini">查看</a>';
			      	return html;
			      }}
				]],
				done: function(res, curr, count){
					data = res.data;
					laypage.render({
						elem: 'page',
						count: count,
						limit: pageSize,
						curr:pageIndex,
						groups:5,
						limits: [10, 20, 30, 40, 50],
						jump: function(obj, first) {
							var curr = obj.curr;
							pageIndex = curr;
							if(!first) {
								table.reload('dataTable', {
							    where: {
							      userName:$("#userName").val(),
							      operateName:$("#operateName").val(),
							      startTime:$("#startTime").val(),
							      endTime:$("#endTime").val(),
							      pageIndex:curr
								  ,pageSize:pageSize
							    }
							  });
							}
						}
					});
					
					var parentModuleCode = "sys_manage";
					var moduleCode = "log_manage";
					validModule(parentModuleCode,moduleCode);
				}
			});
			
			
			table.on('rowDouble(dataTable)', function(obj){
				findLog(obj.data.logId);
			});
			
			var sortNames={
				'createTime':'create_time',
				'operateName':'operate_name',
				'operateUrl':'operate_url',
				'operateDesc':'operate_desc',
				'ip':'ip',
				'port':'port',
				'state':'state'
			};
			
			table.on('sort(dataTable)', function(obj){
				  pageIndex = 1;
				  table.reload('dataTable', {
				    where: {
				      userName:$("#userName").val(),
				      operateName:$("#operateName").val(),
				      startTime:$("#startTime").val(),
				      endTime:$("#endTime").val(),
				      sortName: sortNames[obj.field]
				      ,sortOrder: obj.type
				      ,pageIndex:pageIndex
					  ,pageSize:pageSize
				    }
				  });
			});
			
			table.on('tool(dataTable)', function(obj){
			  switch(obj.event){
			    case 'search':
			      findLog(obj.data.logId);
			    break;
			  };
			});
			
			function findLog(logId){
				layer.open({
					type:2,
					title:'查看日志',
					area: ['760px', '550px'],
					content:'manage/sysLogViewPage?logId='+logId
				});
			}
			
			$('#searchBtn').on('click', function() {
				pageIndex = 1;
			  	table.reload('dataTable', {
				    where: {
				      userName:$("#userName").val(),
				      operateName:$("#operateName").val(),
				      startTime:$("#startTime").val(),
				      endTime:$("#endTime").val(),
				      sortName: 'create_time'
				      ,sortOrder: 'desc'
				      ,pageIndex:pageIndex
					  ,pageSize:pageSize
				    }
			  	});
			});
			
			$('#resetBtn').on('click', function() {
				$("#userName").val("");
				$("#operateName").val("");
				$("#startTime").val("");
				$("#endTime").val("");
				form.render();
			});
		});
	</script>
</body>

</html>
