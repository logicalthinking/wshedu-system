<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>新增用户</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
	<link rel="stylesheet" href="css/table.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div style="margin: 15px;padding-right: 30px;">
		<form class="layui-form" action="">
			<div class="layui-form-item">
				<label class="layui-form-label">用户名<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="text" name="userName" lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">角色<i class="color-red">*</i></label>
				<div class="layui-input-block lineHeight38">
					<select name="roleIds" lay-verify="required">
						<option value="">--请选择--</option>
						<c:forEach items="${sysRoles }" var="role" varStatus="i">
							<option value="${role.roleId }">${role.roleName }</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">密码<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="password" name="userPass" id="userPass" lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">确认密码<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="password" name="checkUserPass" id="checkUserPass" lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">性别<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="radio" name="sex" value="男" title="男" checked="checked">
					<input type="radio" name="sex" value="女" title="女">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">手机号</label>
				<div class="layui-input-block">
					<input type="text" name="telephone" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">邮箱</label>
				<div class="layui-input-block">
					<input type="text" name="email" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item" style="text-align: center;">
				<div class="layui-input-block" style="margin-left:0px;">
					<button class="layui-btn" lay-submit="userInfo" lay-filter="userInfo">保存</button>
					<button type="button" class="layui-btn layui-btn-primary" id="cancerBtn">取消</button>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/validator.js"></script>
	<script>
		layui.config({
			base: 'js/'
		});
		layui.use(['form','layer','laydate','validator'], function() {
			var form = layui.form,
				layer = layui.layer,
				$ = layui.jquery;
				var validator = layui.validator;
			//监听提交
			form.on('submit(userInfo)', function(data) {
				var result = data.field;
				
				if(result.userPass != result.checkUserPass){
	  				$("#checkUserPass").css('border','1px solid red');
					layer.msg('两次输入密码不一致',{'icon': 5 },function(){
						$("#checkUserPass").focus();
					});
	  				return false;
				}
				
	  			//校验手机号
				var telephone = result.telephone;
	  			if(telephone && !validator.IsMobilePhoneNumber(telephone)){
	  				var obj = $("input[name='telephone']");
	  				$(obj).css('border','1px solid red');
					layer.msg('手机号格式不正确',{'icon': 5 },function(){
						$(obj).focus();
					});
	  				return false;
	  			}
				
				//校验邮箱
				var email = result.email;
	  			if(email && !validator.IsEmail(email)){
	  				var obj = $("input[name='email']");
	  				$(obj).css('border','1px solid red');
					layer.msg('邮箱格式不正确',{'icon': 5 },function(){
						$(obj).focus();
					});
	  				return false;
	  			}
				
				var load = layer.load(2);
				//提交
				$.post("manage/addSysUsers",result,function(res){
					if(res.error_code == "200"){
						layer.msg(res.msg,{icon:1});
						$('#cancerBtn').click();
						parent.layui.table.reload('dataTable');
					}else{
						layer.msg(res.msg,{icon:2});
					}
					layer.close(load);
				});
				
				return false;
			});
			
			$('#cancerBtn').on('click',function(){
				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
				parent.layer.close(index);
			});
			
		});
	</script>
</body>
</html>
