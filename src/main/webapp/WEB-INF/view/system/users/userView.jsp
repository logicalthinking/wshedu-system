<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>查看用户</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
	<link rel="stylesheet" href="css/table.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		.layui-form-label{
			font-weight: bold;
		}
	</style>
</head>
<body>
	<div style="margin: 15px;padding-right: 30px;">
		<form class="layui-form" action="">
			<input type="hidden" name="userId" value="${sysUsers.userId }">
			<div class="layui-form-item">
				<label class="layui-form-label">用户名</label>
				<div class="layui-input-block lineHeight38">
					${sysUsers.userName }
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">角色</label>
				<div class="layui-input-block lineHeight38">
					<c:forEach items="${sysUsers.sysUserRoles }" var="item" varStatus="i">
						<c:if test="${not empty item.sysRole }">
							${item.sysRole.roleName }
						</c:if>
					</c:forEach>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">性别</label>
				<div class="layui-input-block lineHeight38">
					${sysUsers.sex}
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">手机号</label>
				<div class="layui-input-block lineHeight38">
					${sysUsers.telephone }
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">邮箱</label>
				<div class="layui-input-block lineHeight38">
					${sysUsers.email }
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">状态</label>
				<div class="layui-input-block lineHeight38">
					<c:if test="${sysUsers.state=='Y' }"><span class="green">有效</span></c:if>
					<c:if test="${sysUsers.state!='Y' }"><span class="red">无效</span></c:if>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/validator.js"></script>
	<script>
		layui.use(['form','layer'], function() {
			var form = layui.form,
				layer = layui.layer,
				$ = layui.jquery;
		});
	</script>
</body>
</html>
