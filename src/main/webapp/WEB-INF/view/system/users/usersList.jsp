<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html>
<head>
    <base href="<%= basePath%>">
    <title>用户列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
    <link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
    <link rel="stylesheet" href="css/table.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
	<div class="admin-main">
		<blockquote class="layui-elem-quote">
			<form class="layui-form">
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">用户名</label>
						<div class="layui-input-inline">
							<input type="text" id="userName" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">角色</label>
						<div class="layui-input-inline">
							<select id="roleId" lay-filter="roleType">
								<option value="">全部</option>
								<c:forEach items="${sysRoles }" var="role" varStatus="i">
									<option value="${role.roleId }">${role.roleName }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">手机号</label>
						<div class="layui-input-inline">
							<input type="text" id="telephone" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">邮箱</label>
						<div class="layui-input-inline">
							<input type="text" id="email" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<a href="javascript:;" class="layui-btn layui-btn-small" id="searchBtn">
							<i class="layui-icon">&#xe615;</i> 搜索
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-small layui-btn-danger" id="resetBtn">
							<i class="layui-icon">&#xe639;</i> 清除
						</a>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline" style="margin-left: 50px;">
						<a href="javascript:;" class="layui-btn layui-btn-small" module-field="users_manage_add" id="addBtn">
							<i class="layui-icon">&#xe608;</i> 新增用户
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-danger layui-btn-small" module-field="users_manage_del" id="delBtn">
							<i class="layui-icon">&#xe640;</i> 删除用户
						</a>
					</div>
				</div>
			</form>
		</blockquote>
		<fieldset class="layui-elem-field">
			<legend>用户列表</legend>
			<div class="layui-field-box">
				<table class="site-table table-hover" id="dataTable" lay-filter="dataTable">
					
				</table>
			</div>
		</fieldset>
		<div class="admin-table-page">
			<div id="page" class="page">
			</div>
		</div>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/validModule.js"></script>
	<script>
		var pageIndex = 1;
		var pageSize = 10;
		var data;

		layui.use(['laypage','layer','table','form'], function() {
			var $ = layui.jquery,
				laypage = layui.laypage,
				layer = layui.layer;
			var form = layui.form;
			
			var table = layui.table;
			var dataTable = table.render({
				elem: '#dataTable'
				,url:'manage/sysUsersList'
				,where:{
					pageIndex:pageIndex,
					pageSize:pageSize,
					sortName:'create_time',
					sortOrder:'desc'
				}
				,method:'post'
				,request: {
					pageName: 'pageIndex' //页码的参数名称，默认：page
					,limitName: 'pageSize' //每页数据量的参数名，默认：limit
				}
				,parseData: function(res){
					return {
						'code':(res.error_code=='200'?0:-1),
						'msg': res.msg,
					    'count': res.total,
					    'data': res.result
					};
				}
				,autoSort:false
				,cellMinWidth: 100 
			    ,cols: [[
			      {checkbox:true, width:'7%'}
			      ,{field:'userName', title: '用户名',sort: true}
			      ,{field:'sysUserRoles', title: '角色名称',templet:function(row){
			      	var role=[];
			      	if(row.sysUserRoles!=null && row.sysUserRoles.length>0){
			      		for(var i=0;i<row.sysUserRoles.length;i++){
			      			if(row.sysUserRoles[i].sysRole!=null && row.sysUserRoles[i].sysRole!="" ){
			      				role.push(row.sysUserRoles[i].sysRole.roleName);
			      			}
			      		}
			      	}
			      	if(role.length>0){
			      		return role.join(',');
			      	}else{
			      		return '';
			      	}
			      }}
			      ,{field:'sex', width:'7%', title: '性别',sort: true}
			      ,{field:'telephone', title: '手机号', width: '12%', sort: true}
			      ,{field:'email', title: '邮箱',width:'15%', sort: true}
			      ,{field:'state', title: '状态',width:'8%', sort: true,templet:function(row){
			      	return row.state == 'Y'?'<span class="green">有效</span>':'<span class="red">无效</span>';
			      }}
			      ,{title: '操作', width:'15%', templet:function(row){
			      	var html ='<a href="javascript:;" lay-event="search"  module-field="users_manage_view" class="layui-btn layui-btn-normal layui-btn-mini">查看</a>'+
			      			  '<a href="javascript:;" lay-event="edit" module-field="users_manage_edit" '+
			      			  'class="layui-btn layui-btn-mini">编辑</a>'+
							  '<a href="javascript:;" lay-event="del" module-field="users_manage_del" '+
							  ' class="layui-btn layui-btn-danger layui-btn-mini">删除</a>';
			      	return html;
			      }}
				]],
				done: function(res, curr, count){
					data = res.data;
					laypage.render({
						elem: 'page',
						count: count,
						limit: pageSize,
						curr:pageIndex,
						groups:5,
						limits: [10, 20, 30, 40, 50],
						jump: function(obj, first) {
							var curr = obj.curr;
							pageIndex = curr;
							if(!first) {
								table.reload('dataTable', {
							    where: {
							      userName:$("#userName").val(),
				   				  roleId:$("#roleId").val(),
				   				  telephone:$("#telephone").val(),
				   				  email:$("#email").val(),
							      pageIndex:curr
								  ,pageSize:pageSize
							    }
							  });
							}
						}
					});
					
					var parentModuleCode = "sys_manage";
					var moduleCode = "users_manage";
					validModule(parentModuleCode,moduleCode);
				}
			});
			
			
			table.on('rowDouble(dataTable)', function(obj){
				findUser(obj.data.userId);
			});
			
			var sortNames={
				'userName':'user_name',
				'sex':'sex',
				'age':'age',
				'telephone':'telephone',
				'email':'email',
				'state':'state'
			};
			
			table.on('sort(dataTable)', function(obj){
				  pageIndex = 1;
				  table.reload('dataTable', {
				    where: {
				      userName:$("#userName").val(),
	   				  roleId:$("#roleId").val(),
	   				  telephone:$("#telephone").val(),
	   				  email:$("#email").val(),
				      sortName: sortNames[obj.field]
				      ,sortOrder: obj.type
				      ,pageIndex:pageIndex
					  ,pageSize:pageSize
				    }
				  });
			});
			
			form.on('select(roleType)', function(obj){
				console.log(obj);
				  pageIndex = 1;
				  table.reload('dataTable', {
				    where: {
				      userName:$("#userName").val(),
	   				  roleId:$("#roleId").val(),
	   				  telephone:$("#telephone").val(),
	   				  email:$("#email").val(),
				      sortName:'create_time'
				      ,sortOrder: 'desc'
				      ,pageIndex:pageIndex
					  ,pageSize:pageSize
				    }
				  });
			});
			
			table.on('tool(dataTable)', function(obj){
			  switch(obj.event){
			    case 'search':
			      findUser(obj.data.userId);
			    break;
			    case 'edit':
			      editUser(obj.data.userId);
			    break;
			    case 'del':
			      delUser(obj.data.userId,"确认删除该用户吗？");
			    break;
			  };
			});
			
			$('#delBtn').on('click', function() {
				var checkStatus = table.checkStatus('dataTable');
				if(checkStatus.data.length==0){
					layer.msg("请选择一行！",{icon:0});
					return;
				}
				var userIds = "";
				for(var i=0;i<checkStatus.data.length;i++){
					if(i==0){
						userIds += checkStatus.data[i].userId;
					}else{
						userIds += ","+checkStatus.data[i].userId;
					}
				}
				delUser(userIds,"确认删除所选用户吗？");
			});
			
			function delUser(userId,message){
				layer.confirm(message,{icon:3,title:'提示信息'},function(index){
					layer.close(index);
					var load = layer.load(2);
					$.post("manage/deleteSysUsers",{userIds:userId },function(res){
						if(res.error_code == "200"){
							layer.msg(res.msg,{icon:1});
							pageIndex = 1;
							table.reload('dataTable');
						}else{
							layer.msg(res.msg,{icon:2});
						}
						layer.close(load);
					});
				});
			}

			$('#addBtn').on('click', function() {
				layer.open({
					type:2,
					title:'新增用户',
					area: ['460px', '480px'],
					content:'manage/userAddPage'
				});
			});
			
			function findUser(userId){
				layer.open({
					type:2,
					title:'查看用户',
					area: ['340px', '420px'],
					content:'manage/userViewPage?userId='+userId
				});
			}
			
			function editUser(userId){
				layer.open({
					type:2,
					title:'编辑用户',
					area: ['460px', '540px'],
					content:'manage/userEditPage?userId='+userId
				});
			}
			
			$('#searchBtn').on('click', function() {
				pageIndex = 1;
			    table.reload('dataTable', {
				    where: {
				      userName:$("#userName").val(),
	   				  roleId:$("#roleId").val(),
	   				  telephone:$("#telephone").val(),
	   				  email:$("#email").val(),
				      sortName: 'create_time'
				      ,sortOrder: 'desc'
				      ,pageIndex:pageIndex
					  ,pageSize:pageSize
				    }
				});
			});
			
			$('#resetBtn').on('click', function() {
				$("#roleId").val("");
				$("#userName").val("");
				$("#telephone").val("");
				$("#email").val("");
				form.render();
			});
			
		});
	</script>
</body>

</html>
