<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html>
<head>
    <base href="<%= basePath%>">
    <title>文案列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
    <link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
    <link rel="stylesheet" href="css/table.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
	<div class="admin-main">
		<blockquote class="layui-elem-quote">
			<form class="layui-form">
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">文案标题</label>
						<div class="layui-input-inline">
							<input type="text" id="title" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">所属位置</label>
						<div class="layui-input-inline">
							<select id="articleType" lay-filter="articleType">
								<option value="">全部</option>
								<c:forEach items="${articleTypes }" var="item" varStatus="i">
									<option value="${item.key }">${item.value }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="layui-inline">
						<a href="javascript:;" class="layui-btn layui-btn-small" id="searchBtn">
							<i class="layui-icon">&#xe615;</i> 搜索
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-small layui-btn-danger" id="resetBtn">
							<i class="layui-icon">&#xe639;</i> 清除
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-small" id="addBtn" module-field="article_manage_add">
							<i class="layui-icon">&#xe608;</i> 新增文案
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-danger layui-btn-small" id="delBtn" 
							module-field="article_manage_del">
							<i class="layui-icon">&#xe640;</i> 删除文案
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-small" module-field="article_manage_sort" id="sortBtn">
							<i class="fa fa-sort"></i> 排序
						</a>
					</div>
				</div>
			</form>
		</blockquote>
		<fieldset class="layui-elem-field">
			<legend>文案列表</legend>
			<div class="layui-field-box">
				<table class="site-table table-hover" id="dataTable" lay-filter="dataTable">
					
				</table>
			</div>
		</fieldset>
		<div class="admin-table-page">
			<div id="page" class="page">
			</div>
		</div>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/validModule.js"></script>
	<script>
		var pageIndex = 1;
		var pageSize = 10;
		layui.use(['laypage','layer','table','form'], function() {
			var $ = layui.jquery,
				laypage = layui.laypage,
				layer = layui.layer;
			var form = layui.form;
			
			var table = layui.table;
			var dataTable = table.render({
				elem: '#dataTable'
				,url:'manage/articleInfoList'
				,where:{
					pageIndex:pageIndex,
					pageSize:pageSize,
					sortName:'create_time',
					sortOrder:'desc'
				}
				,method:'post'
				,request: {
					pageName: 'pageIndex' //页码的参数名称，默认：page
					,limitName: 'pageSize' //每页数据量的参数名，默认：limit
				}
				,parseData: function(res){
					return {
						'code':(res.error_code=='200'?0:-1),
						'msg': res.msg,
					    'count': res.total,
					    'data': res.result
					};
				}
				,autoSort:false
				,cellMinWidth: 100 
			    ,cols: [[
			      {checkbox:true, width:'7%'}
			      ,{field:'title', title: '标题',sort: true}
			      ,{field:'articleTypeName', title: '所属位置',sort: true,}
			      ,{field:'createTime', title: '添加时间',sort: true,}
			      ,{field:'state', title: '状态', sort: true,templet:function(row){
			      	return row.state == 'Y'?'<span class="green">有效</span>':'<span class="red">无效</span>';
			      }}
			      ,{title: '操作', width:'15%', templet:function(row){
			      	var html ='<a href="javascript:;" lay-event="search" class="layui-btn layui-btn-normal layui-btn-mini" module-field="article_manage_view">查看</a>';
						html+='<a href="javascript:;" lay-event="edit" class="layui-btn layui-btn-mini" module-field="article_manage_edit">修改</a>';
						html+= '<a href="javascript:;" lay-event="del" module-field="article_manage_del"'+
							' class="layui-btn layui-btn-danger layui-btn-mini">删除</a>';
			      	return html;
			      }}
				]],
				done: function(res, curr, count){
					data = res.data;
					laypage.render({
						elem: 'page',
						count: count,
						limit: pageSize,
						curr:pageIndex,
						groups:5,
						limits: [10, 20, 30, 40, 50],
						jump: function(obj, first) {
							var curr = obj.curr;
							pageIndex = curr;
							if(!first) {
								table.reload('dataTable', {
							    where: {
							      articleType:$("#articleType").val(),
				    			  title:$("#title").val(),
							      pageIndex:curr
								  ,pageSize:pageSize
							    }
							  });
							}
						}
					});
					
					var parentModuleCode = "web_manage";
					var moduleCode = "article_manage";
					validModule(parentModuleCode,moduleCode);
					
				}
			});
			
			
			table.on('rowDouble(dataTable)', function(obj){
				findArticle(obj.data.articleId);
			});
			
			var sortNames={
				'title':'title',
				'articleTypeName':'article_type',
				'createTime':'create_time',
				'state':'state'
			};
			
			table.on('sort(dataTable)', function(obj){
				  pageIndex = 1;
				  table.reload('dataTable', {
				    where: {
				      articleType:$("#articleType").val(),
	    			  title:$("#title").val(),
				      sortName: sortNames[obj.field]
				      ,sortOrder: obj.type
				      ,pageIndex:pageIndex
					  ,pageSize:pageSize
				    }
				  });
			});
			
			table.on('tool(dataTable)', function(obj){
			  switch(obj.event){
			    case 'search':
			      findArticle(obj.data.articleId);
			    break;
			    case 'edit':
			      editArticle(obj.data.articleId);
			    break;
		      	case 'del':
			      delArticle(obj.data.articleId,"确认删除该文案吗？");
			    break;
			  };
			});
			
			$('#delBtn').on('click', function() {
				var checkStatus = table.checkStatus('dataTable');
				if(checkStatus.data.length==0){
					layer.msg("请选择一行！",{icon:0});
					return;
				}
				var articleIds = [];
				var flag = false;
				for(var i=0;i<checkStatus.data.length;i++){
					articleIds.push(checkStatus.data[i].articleId);
				}
				
				delArticle(articleIds.join(","),"确认删除所选文案吗？");
			});
			
			function delArticle(articleId,message){
				layer.confirm(message,{icon:3,title:'提示信息'},function(index){
					layer.close(index);
					var load = layer.load(2);
					$.post("manage/deleteArticleInfo",{articleIds:articleId },function(res){
						if(res.error_code == "200"){
							layer.msg(res.msg,{icon:1});
							pageIndex = 1;
							table.reload('dataTable');
						}else{
							layer.msg(res.msg,{icon:2});
						}
						layer.close(load);
					});
				});
			}

			$('#addBtn').on('click', function() {
				layer.open({
					type:2,
					title:'新增文案',
					area: ['960px', '640px'],
					content:'manage/articleAddPage'
				});
			});
			
			function editArticle(articleId){
				layer.open({
					type:2,
					title:'编辑文案',
					area: ['960px', '640px'],
					content:'manage/articleEditPage?articleId='+articleId
				});
			}
			
			function findArticle(articleId){
				layer.open({
					type:2,
					title:'查看文案',
					area: ['960px', '640px'],
					content:'manage/articleViewPage?articleId='+articleId
				});
			}
			
			$('#sortBtn').on('click', function() {
				var title = "";
				var articleType = $("#articleType").val();
				if(articleType==""){
					layer.msg("请选择所属位置",{icon:0});
					return;
				}
				title = $("#articleType option:selected").text();
				layer.open({
					type:2,
					title: title+'排序',
					area: ['800px', '500px'],
					content:'manage/articleSortPage?articleType='+articleType
				});
			});
			
			form.on('select(articleType)', function(data) {
				reloadTable();
			});
			
			function  reloadTable(){
				pageIndex = 1;
				table.reload('dataTable', {
				  where: {
				    articleType:$("#articleType").val(),
    			    title:$("#title").val(),
				    pageIndex:pageIndex,
				    pageSize:pageSize
				  }
				});
			}
			
			$('#searchBtn').on('click', function() {
				reloadTable();
			});
			
			$('#resetBtn').on('click', function() {
				$("#articleType").val("");
				$("#title").val("");
				form.render();
			});
		});
	</script>
</body>

</html>
