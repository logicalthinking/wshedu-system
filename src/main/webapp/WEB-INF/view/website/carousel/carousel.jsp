<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>轮播</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" type="text/css" href="css/index/base.css">
	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<style>
	    .carousel-img{
	      width: 100%;
	      height: 100%;
	      background-size:100% 100%;
	    }
	    .layui-carousel-ind li{
	      width: 15px;
	      height: 15px;
	    }
  </style>
</head>
<body>
	  <div class="container">
	  	<div class="layui-carousel" id="carousel">
	      <div carousel-item="" id="content">
	      	<c:if test="${not empty webImgInfos }">
	      		<c:forEach items="${webImgInfos }" var="item" varStatus="i">
	        		<div class="content-item" style="background:url('image/priview/${item.imgId }${item.imgPath.substring(item.imgPath.lastIndexOf('.'))}') no-repeat center center; background-size:100% 100%;"></div>
	        	</c:forEach>
	      	</c:if>
	      </div>
	    </div>
		<div class="layui-form" style="margin-top: 20px;">
		  <div class="layui-form-item">
		    <div class="layui-inline">
		      <label class="layui-form-label">宽高</label>
		      <div class="layui-input-inline" style="width: 98px;">
		        <input type="number" name="width" value="800" autocomplete="off" placeholder="width" class="layui-input demoSet">
		      </div>
		      <div class="layui-input-inline" style="width: 98px;">
		        <input type="number" name="height" value="640" autocomplete="off" placeholder="height" class="layui-input demoSet">
		      </div>
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label">自动切换</label>
		      <div class="layui-input-block">
		        <input type="checkbox" name="switch" lay-skin="switch" checked="" lay-text="ON|OFF" lay-filter="autoplay">
		      </div>
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label" style="width: auto;">时间间隔</label>
		      <div class="layui-input-inline" style="width: 120px;">
		        <input type="tel" name="interval" value="3000" autocomplete="off" placeholder="毫秒" class="layui-input demoSet">
		      </div>
		    </div>
		  </div>
		  
		  <div class="layui-form-item">
		  	<div class="layui-inline">
				<label class="layui-form-label">动画类型</label>
			    <div class="layui-input-block">
			      <div class="layui-btn-group demoTest" style="margin-top: 5px;">
			        <button class="layui-btn layui-btn-sm" style="background-color: #5FB878;" data-type="set" data-key="anim" data-value="default">左右切换</button>
			        <button class="layui-btn layui-btn-sm" data-type="set" data-key="anim" data-value="updown">上下切换</button>
			        <button class="layui-btn layui-btn-sm" data-type="set" data-key="anim" data-value="fade">渐隐渐显</button>
			      </div> 
			    </div>		    
		    </div>
		  	<div class="layui-inline">
				<label class="layui-form-label">箭头状态</label>
			    <div class="layui-input-block">
			      <div class="layui-btn-group demoTest" style="margin-top: 5px;">
			        <button class="layui-btn layui-btn-sm" style="background-color: #5FB878;" data-type="set" data-key="arrow" data-value="hover">悬停显示</button>
			        <button class="layui-btn layui-btn-sm" data-type="set" data-key="arrow" data-value="always">始终显示</button>
			        <button class="layui-btn layui-btn-sm" data-type="set" data-key="arrow" data-value="none">不显示</button>
			      </div> 
			    </div>    
		    </div>
		    <div class="layui-inline">
		    	<label class="layui-form-label">指示器位置</label>
			    <div class="layui-input-block">
			      <div class="layui-btn-group demoTest" style="margin-top: 5px;">
			        <button class="layui-btn layui-btn-sm" style="background-color: #5FB878;" data-key="indicator" data-type="set" data-value="inside">容器内部</button>
			        <button class="layui-btn layui-btn-sm" data-type="set" data-key="indicator" data-value="outside">容器外部</button>
			        <button class="layui-btn layui-btn-sm" data-type="set" data-key="indicator" data-value="none">不显示</button>
			      </div> 
			    </div>
		    </div>
		  </div>
		</div>
	  </div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script>
		layui.use(['carousel', 'form'], function(){
	      var carousel = layui.carousel
	      ,form = layui.form
	      ,$ = layui.jquery;
	      
	      var carous = carousel.render({
	        elem: '#carousel'
	        ,width: '100%'
	        ,height: '640px'
	      });
	      
	      var $ = layui.$, active = {
		    set: function(othis){
		      var THIS = 'layui-bg-normal'
		      ,key = othis.data('key')
		      ,options = {};
		      
		      othis.css('background-color', '#5FB878').siblings().removeAttr('style'); 
		      options[key] = othis.data('value');
		      carous.reload(options);
		    }
		  };
		  
		  $('.demoSet').on('keyup', function(){
		    var value = this.value
		    ,options = {};
		    if(!/^\d+$/.test(value)) return;
		    
		    options[this.name] = value;
		    carous.reload(options);
		  });
		  
		  
		  form.on('switch(autoplay)', function(){
		    carous.reload({
		      autoplay: this.checked
		    });
		  });
		  
		  //其它示例
		  $('.demoTest .layui-btn').on('click', function(){
		    var othis = $(this), type = othis.data('type');
		    active[type] ? active[type].call(this, othis) : '';
		  });
	      
	
	      var scrollFunc = function (e) {
	            e = e || window.event;
	            if (e.wheelDelta) {//
	                var curr = $(".layui-carousel-ind").find("ul").find("li.layui-this");
	                var length = $(".layui-carousel-ind").find("ul").find("li").length;
	                if (e.wheelDelta > 0) { //当滑轮向上滚动时
	                  var num = $(curr).index();
	                  if (num == 0) return;
	                  $(curr).prev().click();
	                }else if (e.wheelDelta < 0) {//当滑轮向下滚动时
	                  var num = $(curr).index();  
	                  if (num == length-1) return;
	                  $(curr).next().click();
	                }
	            }
	        };
	
	        window.onmousewheel = scrollFunc;
	    });
	</script>
</body>
</html>
