<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>编辑分类</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
	<link rel="stylesheet" href="css/table.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div style="margin: 15px;padding-right: 30px;">
		<form class="layui-form" action="">
			<input type="hidden" name="typeId" value="${categoryInfo.typeId }">
			<div class="layui-form-item">
				<label class="layui-form-label">所属大类<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<select name="bigType" lay-verify="required" lay-filter="bigType">
						<option value="">--请选择--</option>
						<c:forEach items="${bigTypes }" var="item" varStatus="i">
							<option value="${item.key }"
								<c:if test="${categoryInfo.bigType == item.key }">selected="selected"</c:if>
							>${item.value }</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">上级分类<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<select name="parentId" id="parentId" lay-verify="required">
						<option value="">--请选择--</option>
						<c:forEach items="${parentTypes }" var="item" varStatus="i">
							<option value="${item.typeId }"
								<c:if test="${categoryInfo.parentId == item.typeId }">selected="selected"</c:if>
							>${item.typeName }</option>
						</c:forEach>
						<option value="0"
							<c:if test="${categoryInfo.parentId == '0' }">selected="selected"</c:if>
						>无</option>
					</select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">分类名称<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="text" name="typeName" value="${categoryInfo.typeName}" lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">分类编码<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="text" name="typeCode" value="${categoryInfo.typeCode}" lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">跳转地址</label>
				<div class="layui-input-block">
					<input type="text" name="typeUrl" value="${categoryInfo.typeUrl}" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">备注</label>
				<div class="layui-input-block">
					<textarea name="remark" class="layui-textarea">${categoryInfo.remark}</textarea>
				</div>
			</div>
			<div class="layui-form-item" style="text-align: center;">
				<div class="layui-input-block" style="margin-left:0px;">
					<button class="layui-btn" lay-submit="typeInfo" lay-filter="typeInfo">保存</button>
					<button type="button" class="layui-btn layui-btn-primary" id="cancerBtn">取消</button>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script>
		layui.use(['form','layer','laydate'], function() {
			var form = layui.form,
				layer = layui.layer,
				$ = layui.jquery;
			//监听提交
			form.on('submit(typeInfo)', function(data) {
				var result = data.field;
				var load = layer.load(2);
				//提交
				$.post("manage/updateCategoryInfo",result,function(res){
					if(res.error_code == "200"){
						layer.msg(res.msg,{icon:1});
						$('#cancerBtn').click();
						parent.layui.table.reload('dataTable');
					}else{
						layer.msg(res.msg,{icon:2});
					}
					layer.close(load);
				});
				
				return false;
			});
			
			
			form.on('select(bigType)', function(data){
			   var bigType = data.value;
			   
			   $.post("manage/categoryInfoAllList",{
			   	bigType:bigType
			   },function(res){
					if(res.error_code == "200"){
						var options = '<option value="">--请选择--</option>';
						if(res.result && res.result.length>0 ){
							$.each(res.result,function(index,item){
								options+='<option value="'+item.typeId+'">'+item.typeName+'</option>';
							});
						}
						options += '<option value="0">无</option>';
						$("#parentId").html(options);
						form.render();
					}else{
						layer.msg(res.msg,{icon:2});
					}
				});
			});
			
			
			$('#cancerBtn').on('click',function(){
				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
				parent.layer.close(index);
			});
			
		});
	</script>
</body>
</html>
