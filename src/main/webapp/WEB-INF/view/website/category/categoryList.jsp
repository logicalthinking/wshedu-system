<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html>
<head>
    <base href="<%= basePath%>">
    <title>分类列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
    <link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
    <link rel="stylesheet" href="css/table.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
	<div class="admin-main">
		<blockquote class="layui-elem-quote">
			<form class="layui-form">
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">所属大类</label>
						<div class="layui-input-inline">
							<select id="bigType">
								<option value="">--请选择--</option>
								<c:forEach items="${bigTypes }" var="item" varStatus="i">
									<option value="${item.key }">${item.value }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">类型名称</label>
						<div class="layui-input-inline">
							<input type="text" id="typeName" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">类型编号</label>
						<div class="layui-input-inline">
							<input type="text" id="typeCode" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<a href="javascript:;" class="layui-btn layui-btn-small" id="searchBtn">
							<i class="layui-icon">&#xe615;</i> 搜索
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-small layui-btn-danger" id="resetBtn">
							<i class="layui-icon">&#xe639;</i> 清除
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-small" module-field="category_manage_add" id="addBtn">
							<i class="layui-icon">&#xe608;</i> 新增分类
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-danger layui-btn-small"  module-field="category_manage_del" id="delBtn">
							<i class="layui-icon">&#xe640;</i> 删除分类
						</a>
					</div>
				</div>
			</form>
		</blockquote>
		<fieldset class="layui-elem-field">
			<legend>分类列表</legend>
			<div class="layui-field-box">
				<table class="site-table table-hover" id="dataTable" lay-filter="dataTable">
					
				</table>
			</div>
		</fieldset>
		<div class="admin-table-page">
			<div id="page" class="page">
			</div>
		</div>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/validModule.js"></script>
	<script>
		var pageIndex = 1;
		var pageSize = 10;
		var data;

		layui.use(['laypage','layer','table','form'], function() {
			var $ = layui.jquery,
				laypage = layui.laypage,
				layer = layui.layer;
			var form = layui.form;
			
			var table = layui.table;
			var dataTable = table.render({
				elem: '#dataTable'
				,url:'manage/categoryInfoList'
				,where:{
					pageIndex:pageIndex,
					pageSize:pageSize
				}
				,method:'post'
				,request: {
					pageName: 'pageIndex' //页码的参数名称，默认：page
					,limitName: 'pageSize' //每页数据量的参数名，默认：limit
				}
				,parseData: function(res){
					return {
						'code':(res.error_code=='200'?0:-1),
						'msg': res.msg,
					    'count': res.total,
					    'data': res.result
					};
				}
				,autoSort:false
				,cellMinWidth: 100 
			    ,cols: [[
			      {checkbox:true, width:'7%'}
			      ,{field:'bigTypeName', title: '所属大类',sort: true}
			      ,{field:'parentTypeName', title: '上级分类',sort: true}
			      ,{field:'typeName', title: '类型名称',sort: true}
			      ,{field:'typeCode', title: '类别编号',sort: true}
			      ,{field:'state', title: '状态',width:'8%', sort: true,templet:function(row){
			      	return row.state == 'Y'?'<span class="green">有效</span>':'<span class="red">无效</span>';
			      }}
			      ,{title: '操作', width:'15%', templet:function(row){
			      	var html ='<a href="javascript:;" lay-event="search" module-field="category_manage_view" class="layui-btn layui-btn-normal layui-btn-mini">查看</a>'+
			      			  '<a href="javascript:;" lay-event="edit" module-field="category_manage_add" class="layui-btn layui-btn-mini">编辑</a>'+
							  '<a href="javascript:;" lay-event="del" module-field="category_manage_del"'+
							  ' class="layui-btn layui-btn-danger layui-btn-mini">删除</a>';
			      	return html;
			      }}
				]],
				done: function(res, curr, count){
					data = res.data;
					laypage.render({
						elem: 'page',
						count: count,
						limit: pageSize,
						curr:pageIndex,
						groups:5,
						limits: [10, 20, 30, 40, 50],
						jump: function(obj, first) {
							var curr = obj.curr;
							pageIndex = curr;
							if(!first) {
								table.reload('dataTable', {
							    where: {
							      typeName:$("#typeName").val(),
				   				  typeCode:$("#typeCode").val(),
				   				  bigType:$("#bigType").val(),
							      pageIndex:curr
								  ,pageSize:pageSize
							    }
							  });
							}
						}
					});
					
					var parentModuleCode = "web_manage";
					var moduleCode = "category_manage";
					validModule(parentModuleCode,moduleCode);
				}
			});
			
			table.on('rowDouble(dataTable)', function(obj){
				findCategory(obj.data.typeId);
			});
			
			var sortNames={
				'typeName':'type_name',
				'typeCode':'type_code',
				'parentTypeName':'parent_id',
				'bigTypeName':'big_type',
				'state':'state'
			};
			
			table.on('sort(dataTable)', function(obj){
				  pageIndex = 1;
				  table.reload('dataTable', {
				    where: {
				      typeName:$("#typeName").val(),
	   				  typeCode:$("#typeCode").val(),
	   				  bigType:$("#bigType").val(),
				      sortName: sortNames[obj.field]
				      ,sortOrder: obj.type
				      ,pageIndex:pageIndex
					  ,pageSize:pageSize
				    }
				  });
			});
			
			table.on('tool(dataTable)', function(obj){
			  switch(obj.event){
			    case 'search':
			      findCategory(obj.data.typeId);
			    break;
			    case 'edit':
			      editCategory(obj.data.typeId);
			    break;
			    case 'del':
			      delCategory(obj.data.typeId,"确认删除该分类吗？");
			    break;
			  };
			});
			
			$('#delBtn').on('click', function() {
				var checkStatus = table.checkStatus('dataTable');
				if(checkStatus.data.length==0){
					layer.msg("请选择一行！",{icon:0});
					return;
				}
				var typeIds = "";
				for(var i=0;i<checkStatus.data.length;i++){
					if(i==0){
						typeIds += checkStatus.data[i].typeId;
					}else{
						typeIds += ","+checkStatus.data[i].typeId;
					}
				}
				delCategory(typeIds,"确认删除所选分类吗？");
			});
			
			function delCategory(typeId,message){
				layer.confirm(message,{icon:3,title:'提示信息'},function(index){
					layer.close(index);
					var load = layer.load(2);
					$.post("manage/deleteCategoryInfo",{typeIds:typeId },function(res){
						if(res.error_code == "200"){
							layer.msg(res.msg,{icon:1});
							pageIndex = 1;
							table.reload('dataTable');
						}else{
							layer.msg(res.msg,{icon:2});
						}
						layer.close(load);
					});
				});
			}

			$('#addBtn').on('click', function() {
				layer.open({
					type:2,
					title:'新增分类',
					area: ['560px', '540px'],
					content:'manage/categoryAddPage'
				});
			});
			
			function findCategory(typeId){
				layer.open({
					type:2,
					title:'查看分类',
					area: ['560px', '540px'],
					content:'manage/categoryViewPage?typeId='+typeId
				});
			}
			
			function editCategory(typeId){
				layer.open({
					type:2,
					title:'编辑分类',
					area: ['560px', '540px'],
					content:'manage/categoryEditPage?typeId='+typeId
				});
			}
			
			$('#searchBtn').on('click', function() {
				pageIndex = 1;
			    table.reload('dataTable', {
				    where: {
				      typeName:$("#typeName").val(),
	   				  typeCode:$("#typeCode").val(),
	   				  bigType:$("#bigType").val()
				      ,pageIndex:pageIndex
					  ,pageSize:pageSize
				    }
				});
			});
			
			$('#resetBtn').on('click', function() {
				$("#typeName").val("");
				$("#typeCode").val("");
				$("#bigType").val("");
				form.render();
			});
			
		});
	</script>
</body>

</html>
