<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>新增留言</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
	<link rel="stylesheet" href="css/table.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		.label-input-icon {
			position:absolute;
			color: #cccccc;
			margin-top: 10px;
			margin-left: 10px;
		}
		.layui-form .layui-input {
			padding-left: 32px;
		}
		.mt5{
			margin-top: 5px;
		}
		.error-msg{
			display: none;
		}
	</style>
</head>
<body>
	<div style="margin: 15px;">
		<form class="layui-form" action="">
			<div class="layui-form-item">
				<textarea class="layui-textarea" 
					placeholder="请在此输入留言内容，我们会尽快与您联系。（必填）" style="resize:none;" name="content"></textarea>
				<p class="red mt5 error-msg" id="content-error">留言信息必填</p>
			</div>
			<div class="layui-form-item">
				<label class="label-input-icon">
                    <i class="layui-icon">&#xe612;</i>
                </label>
				<input type="text" name="userName" placeholder="姓名" autocomplete="off" class="layui-input">
			</div>
			<div class="layui-form-item">
				<label class="label-input-icon">
                    <i class="fa fa-phone" style="font-size: 17px;"></i>
                </label>
				<input type="text" name="telephone" placeholder="电话（必填）" autocomplete="off" class="layui-input">
				<p class="red mt5 error-msg" id="phone-error">电话号码必填</p>
			</div>
			<div class="layui-form-item">
				<label class="label-input-icon">
                    <i class="fa fa-envelope-o" style="font-size: 17px;"></i>
                </label>
				<input type="text" name="email" placeholder="邮箱" autocomplete="off" class="layui-input">
				<p class="red mt5 error-msg" id="email-error">邮箱格式不正确</p>
			</div>
			<div class="layui-form-item" style="text-align: center;margin-top: 20px;">
				<div class="layui-input-block" style="margin-left:0px;">
					<button class="layui-btn" lay-submit="messageInfo" lay-filter="messageInfo">提交</button>
					<button type="button" class="layui-btn layui-btn-primary" id="cancerBtn">取消</button>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/validator.js"></script>
	<script>
		layui.config({
			base: 'js/'
		});
		layui.use(['form','layer','validator'], function() {
			var form = layui.form,
				layer = layui.layer,
				$ = layui.jquery;
			var validator = layui.validator;
			
			form.verify({
				
			});
				
			//监听提交
			form.on('submit(messageInfo)', function(data) {
				var result = data.field;
				
				if(!result.content){
					var obj = $("textarea[name='content']");
	  				$(obj).css('border','1px solid red');
	  				$(obj).focus();
					$("#content-error").show();
					return false;
				}
				$("#content-error").hide();
				
				//校验手机号
				var telephone = result.telephone;
	  			if(!telephone){
	  				var obj = $("input[name='telephone']");
	  				$(obj).css('border','1px solid red');
	  				$(obj).focus();
	  				$("#phone-error").html("手机号必填");
	  				$("#phone-error").show();
	  				return false;
	  			}
	  			$("#phone-error").hide();
	  			
	  			if(!validator.IsPhoneNumber(telephone)){
	  				var obj = $("input[name='telephone']");
	  				$(obj).css('border','1px solid red');
	  				$(obj).focus();
	  				$("#phone-error").html("电话号码格式不正确");
	  				$("#phone-error").show();
	  				return false;
	  			}
				$("#phone-error").hide();
				
				//校验邮箱
				var email = result.email;
	  			if(email && !validator.IsEmail(email)){
	  				var obj = $("input[name='telephone']");
	  				$(obj).css('border','1px solid red');
	  				$(obj).focus();
	  				$("#email-error").show();
	  				return false;
	  			}
	  			$("#email-error").hide();
				
				//提交
				$.post("addLeaveMessage",result,function(res){
					if(res.error_code == "200"){
						layer.msg("提交成功，感谢您的反馈！",{icon:6,time:1000},function(){
							$('#cancerBtn').click();
						});
					}else{
						layer.msg(res.msg,{icon:2});
					}
				});
				
				return false;
			});
			
			$('#cancerBtn').on('click',function(){
				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
				parent.layer.close(index);
			});
		});
	</script>
</body>
</html>
