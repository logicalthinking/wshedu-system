<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html>
<head>
    <base href="<%= basePath%>">
    <title>留言列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
    <link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
    <link rel="stylesheet" href="css/table.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
	<div class="admin-main">
		<blockquote class="layui-elem-quote">
			<form class="layui-form">
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">姓名</label>
						<div class="layui-input-inline">
							<input type="text" id="userName" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">手机号</label>
						<div class="layui-input-inline">
							<input type="text" id="telephone" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">邮箱</label>
						<div class="layui-input-inline">
							<input type="text" id="email" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">留言时间</label>
						<div class="layui-input-inline" style="width: 420px;">
							<input type="text" id="startTime" style="width: 190px;display: inline;" placeholder="YYYY-MM-DD HH:mm:ss" class="layui-input">
							- 
							<input type="text" id="endTime" style="width: 190px;display: inline;" placeholder="YYYY-MM-DD HH:mm:ss" class="layui-input">
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline" style="margin-left: 50px;">
						<a href="javascript:;" class="layui-btn layui-btn-small" id="searchBtn">
							<i class="layui-icon">&#xe615;</i> 搜索
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-small layui-btn-danger" id="resetBtn">
							<i class="layui-icon">&#xe639;</i> 清除
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-danger layui-btn-small" module-field="leave_message_del" id="delBtn">
							<i class="layui-icon">&#xe640;</i> 删除留言
						</a>
					</div>
				</div>
			</form>
		</blockquote>
		<fieldset class="layui-elem-field">
			<legend>留言列表</legend>
			<div class="layui-field-box">
				<table class="site-table table-hover" id="dataTable" lay-filter="dataTable">
					
				</table>
			</div>
		</fieldset>
		<div class="admin-table-page">
			<div id="page" class="page">
			</div>
		</div>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/validModule.js"></script>
	<script>
		var pageIndex = 1;
		var pageSize = 10;
		var data;
		var adminCode = '${adminCode}';
		layui.use(['laypage','layer','table','form','laydate'], function() {
			var $ = layui.jquery,
				laypage = layui.laypage,
				layer = layui.layer;
			var form = layui.form;
			
			var laydate = layui.laydate;
  
			laydate.render({
			  elem: '#startTime',
			  type:'datetime',
			  format:'yyyy-MM-dd HH:mm:ss'
			});
			laydate.render({
			  elem: '#endTime',
			  type:'datetime',
			  format:'yyyy-MM-dd HH:mm:ss'
			});
			
			var table = layui.table;
			var dataTable = table.render({
				elem: '#dataTable'
				,url:'manage/leaveMessageList'
				,where:{
					pageIndex:pageIndex,
					pageSize:pageSize
				}
				,method:'post'
				,request: {
					pageName: 'pageIndex' //页码的参数名称，默认：page
					,limitName: 'pageSize' //每页数据量的参数名，默认：limit
				}
				,parseData: function(res){
					return {
						'code':(res.error_code=='200'?0:-1),
						'msg': res.msg,
					    'count': res.total,
					    'data': res.result
					};
				}
				,autoSort:false
				,cellMinWidth: 100 
			    ,cols: [[
			      {checkbox:true, width:'7%'}
			      ,{field:'userName', title: '用户姓名',sort: true,width:'10%'}
			      ,{field:'sex', title: '性别',sort: true, width:'7%'}
			      ,{field:'telephone', title: '手机号', sort: true,width:'10%'}
			      ,{field:'email', title: '邮箱', sort: true,width:'10%'}
			      ,{field:'qqNo', title: 'qq号', sort: true,width:'10%'}
			      ,{field:'wxNo', title: '微信号', sort: true,width:'10%'}
			      ,{field:'content', title: '留言内容', sort: true}
			      ,{field:'createTime', title: '留言时间', sort: true,width:'12%'}
			      ,{title: '操作', width:'7%', templet:function(row){
			      	var html ='<a href="javascript:;" lay-event="search" class="layui-btn layui-btn-normal layui-btn-mini" module-field="leave_message_view">查看</a>';
			      	html+= '<a href="javascript:;" lay-event="del" module-field="leave_message_del"'+
			      	' class="layui-btn layui-btn-danger layui-btn-mini">删除</a>';
			      	return html;
			      }}
				]],
				done: function(res, curr, count){
					data = res.data;
					laypage.render({
						elem: 'page',
						count: count,
						limit: pageSize,
						curr:pageIndex,
						groups:5,
						limits: [10, 20, 30, 40, 50],
						jump: function(obj, first) {
							var curr = obj.curr;
							pageIndex = curr;
							if(!first) {
								table.reload('dataTable', {
							    where: {
							      userName:$("#userName").val(),
				    			  telephone:$("#telephone").val(),
				    			  email:$("#email").val(),
				    			  startTime:$("#startTime").val(),
				    			  endTime:$("#endTime").val(),
							      pageIndex:curr
								  ,pageSize:pageSize
							    }
							  });
							}
						}
					});
					
					var parentModuleCode = "web_manage";
					var moduleCode = "leave_message";
					validModule(parentModuleCode,moduleCode);
				}
			});
			
			
			table.on('rowDouble(dataTable)', function(obj){
				findMessage(obj.data.msgId);
			});
			
			var sortNames={
				'userName':'user_name',
				'sex':'sex',
				'telephone':'telephone',
				'email':'email',
				'qqNo':'qq_no',
				'wxNo':'wx_no',
				'content':'content',
				'createTime':'create_time',
				'state':'state'
			};
			
			table.on('sort(dataTable)', function(obj){
				  pageIndex = 1;
				  table.reload('dataTable', {
				    where: {
				      userName:$("#userName").val(),
	    			  telephone:$("#telephone").val(),
	    			  email:$("#email").val(),
	    			  startTime:$("#startTime").val(),
	    			  endTime:$("#endTime").val(),
				      sortName: sortNames[obj.field]
				      ,sortOrder: obj.type
				      ,pageIndex:pageIndex
					  ,pageSize:pageSize
				    }
				  });
			});
			
			table.on('tool(dataTable)', function(obj){
			  switch(obj.event){
			    case 'search':
			      findMessage(obj.data.msgId);
			    break;
		      	case 'del':
			      delMessage(obj.data.msgId,"确认删除该留言吗？");
			    break;
			  };
			});
			
			$('#delBtn').on('click', function() {
				var checkStatus = table.checkStatus('dataTable');
				if(checkStatus.data.length==0){
					layer.msg("请选择一行！",{icon:0});
					return;
				}
				var msgIds = [];
				var flag = false;
				for(var i=0;i<checkStatus.data.length;i++){
					msgIds.push(checkStatus.data[i].msgId);
				}
				
				delMessage(msgIds.join(","),"确认删除所选留言吗？");
			});
			
			function delMessage(msgId,message){
				layer.confirm(message,{icon:3,title:'提示信息'},function(index){
					layer.close(index);
					var load = layer.load(2);
					$.post("manage/deleteLeaveMessage",{msgIds:msgId },function(res){
						if(res.error_code == "200"){
							layer.msg(res.msg,{icon:1});
							pageIndex = 1;
							table.reload('dataTable');
						}else{
							layer.msg(res.msg,{icon:2});
						}
						layer.close(load);
					});
				});
			}

			function findMessage(msgId){
				layer.open({
					type:2,
					title:'查看留言',
					area: ['760px', '550px'],
					content:'manage/leaveMessageViewPage?msgId='+msgId
				});
			}
			
			$('#searchBtn').on('click', function() {
				pageIndex = 1;
				table.reload('dataTable', {
				  where: {
				    userName:$("#userName").val(),
    			    telephone:$("#telephone").val(),
    			    email:$("#email").val(),
    			    startTime:$("#startTime").val(),
    			    endTime:$("#endTime").val(),
				    pageIndex:pageIndex,
				    pageSize:pageSize
				  }
				});
			});
			
			$('#resetBtn').on('click', function() {
				$("#userName").val("");
   			    $("#telephone").val("");
   			    $("#email").val("");
   			    $("#startTime").val("");
   			    $("#endTime").val("");
			});
		});
	</script>
</body>

</html>
