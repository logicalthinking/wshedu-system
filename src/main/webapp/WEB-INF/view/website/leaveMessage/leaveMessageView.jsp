<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>查看留言</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
	<link rel="stylesheet" href="css/table.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		.layui-form-item{
			margin-left: 20px;
			margin-right: 20px;
		}
		.layui-form-label{
			font-weight: bold;
		}
	</style>
</head>
<body>
	<div style="margin: 15px;">
		<form class="layui-form layui-form-pane" action="">	
			<fieldset class="layui-elem-field">
				<legend style="margin-bottom: 30px;">留言信息</legend>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">用户姓名</label>
						<div class="layui-input-block">
							<input type="text" readonly="readonly" class="layui-input" value="${leaveMessage.userName}">
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">性别</label>
						<div class="layui-input-block">
							<input type="text" readonly="readonly" class="layui-input" value="${leaveMessage.sex}">
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">手机号</label>
						<div class="layui-input-block">
							<input type="text" readonly="readonly" class="layui-input" value="${leaveMessage.telephone}">
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">邮箱</label>
						<div class="layui-input-block">
							<input type="text" readonly="readonly" class="layui-input" value="${leaveMessage.email}">
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">QQ号</label>
						<div class="layui-input-block">
							<input type="text" readonly="readonly" class="layui-input" value="${leaveMessage.qqNo}">
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">微信号</label>
						<div class="layui-input-block">
							<input type="text" readonly="readonly" class="layui-input" value="${leaveMessage.wxNo}">
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">留言时间</label>
					<div class="layui-input-block">
						<input type="text" readonly="readonly" class="layui-input" value="${leaveMessage.createTime}">
					</div>
				</div>
				<div class="layui-form-item layui-form-text">
					<label class="layui-form-label">留言内容</label>
					<div class="layui-input-block">
						<textarea class="layui-textarea" readonly="readonly" style="resize:none;">${leaveMessage.content}</textarea>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script>
		layui.use(['form','layer'], function() {
			var form = layui.form,
				layer = layui.layer,
				$ = layui.jquery;
			
		});
	</script>
</body>
</html>
