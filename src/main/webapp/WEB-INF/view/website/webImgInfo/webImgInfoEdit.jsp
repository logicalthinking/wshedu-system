<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>编辑图片</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
	<link rel="stylesheet" href="css/table.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		.preview-item{
			width: 140px;
			height: 140px;
			padding: 10px;
			/* border: 1px solid #CCC; */
		}
		.img-item{
			width: 100%;
		}
	</style>
</head>
<body>
	<div style="margin: 15px;">
		<form class="layui-form" action="">
			<input type="hidden" name="imgId" value="${webImgInfo.imgId }" autocomplete="off" class="layui-input">
			<div class="layui-form-item">
				<label class="layui-form-label">图片名称<i class="color-red">*</i></label>
				<div class="layui-input-block">
					<input type="text" name="imgName" 
					value="${webImgInfo.imgName }"
					lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">所属位置<i class="color-red">*</i></label>
					<div class="layui-input-inline">
						<select name="imgBigType" lay-verify="required" id="imgBigType" lay-filter="imgBigType">
							<option value="">--请选择--</option>
							<c:forEach items="${imgTypes }" var="item" varStatus="i">
								<option value="${item.key }"
									<c:if test="${webImgInfo.imgBigType == item.key }">selected="selected"</c:if>
								>${item.value }</option>
							</c:forEach>
						</select>
					</div>
					<div class="layui-input-inline">
						<input type="hidden" id="imgEditType" value="${webImgInfo.imgType }">
						<select name="imgType" lay-verify="required" id="imgTypeSelect">
							<option value="">--请选择--</option>
						</select>
					</div>
				</div>
			</div>
			
			<div class="layui-form-item">
				<label class="layui-form-label">跳转地址</label>
				<div class="layui-input-block">
					<input type="text" name="imgUrl" 
					value="${webImgInfo.imgUrl }"
					autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">上传图片<i class="color-red">*</i></label>
					<div class="layui-input-inline" style="width: 190px;line-height: 38px;">
						<input type="hidden" name="imgPath" id="imgPath" 
						value="${webImgInfo.imgPath }"
						lay-verify="imgPath" autocomplete="off" class="layui-input">
						<a href="javascript:;" class="layui-btn layui-btn-small" id="uploadBtn">选择图片</a>
						<span id="error_span"></span>
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">排序号<i class="color-red">*</i></label>
					<div class="layui-input-inline">
						<input type="text" name="rank" value="${webImgInfo.rank }" autocomplete="off" class="layui-input">
					</div>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">预览图片</label>
					<div class="layui-input-inline">
						<div class="preview-item">
							<img alt="${webImgInfo.imgName }" src="image/priview/${webImgInfo.imgId }${webImgInfo.suffix}" class="img-item" id="previewImg">
						</div>
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label" style="width: 110px;margin-left: -30px;">文字显示状态<i class="color-red">*</i></label>
					<div class="layui-input-inline">
						<div style="height: 140px;">
							<input name="wordStatus" type="radio" value="Y"
								<c:if test="${webImgInfo.wordStatus=='Y' }">checked="checked"</c:if>
							 title="显示">
							<input name="wordStatus" type="radio" value="N" 
								<c:if test="${webImgInfo.wordStatus=='N' }">checked="checked"</c:if>
							title="不显示">
						</div>
					</div>
				</div>
			</div>
			<div class="layui-form-item" style="text-align: center;margin-top: 20px;">
				<div class="layui-input-block" style="margin-left:0px;">
					<button class="layui-btn" lay-submit="imgInfo" lay-filter="imgInfo">保存</button>
					<button type="button" class="layui-btn layui-btn-primary" id="cancerBtn">取消</button>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script>
		layui.use(['form','layer','upload'], function() {
			var form = layui.form,
				layer = layui.layer,
				$ = layui.jquery;
			var	upload = layui.upload;
				
				var height = $('#previewImg').height();
				if(height>0){
		        	$(".preview-item").css("border","none");
		        }
				
				 var uploadInst = upload.render({
				    elem: '#uploadBtn'
				    ,url: 'manage/uploadImage'
				    ,accept:'images'
				    ,acceptMime: 'image/*'
				    ,before: function(obj){
				      //预读本地文件示例，不支持ie8
				      obj.preview(function(index, file, result){
				        $('#previewImg').attr('src', result); //图片链接（base64）
				        $(".preview-item").css("border","none");
				      });
				    }
				    ,done: function(res){
				      //如果上传失败
				      if(res.code > 0){
				        return layer.msg('上传失败');
				      }
				      //上传成功
				      $("#imgPath").val(res.data.suffx_src);
				    }
				    ,error: function(){
				      //演示失败状态，并实现重传
				      var demoText = $('#error_span');
				      demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
				      demoText.find('.demo-reload').on('click', function(){
				        uploadInst.upload();
				      });
				    }
				  });
				
				
			form.verify({
				imgPath: function(value) {
					if(!value){
						return "请选择图片";
					}
				}
			});
				
			//监听提交
			form.on('submit(imgInfo)', function(data) {
				var result = data.field;
				
				var load = layer.load(2);
				//提交
				$.post("manage/updateWebImgInfo",result,function(res){
					if(res.error_code == "200"){
						layer.msg(res.msg,{icon:1});
						$('#cancerBtn').click();
						parent.layui.table.reload('dataTable');
					}else{
						layer.msg(res.msg,{icon:2});
					}
					layer.close(load);
				});
				
				return false;
			});
			
			var imgBigType = $("#imgBigType").val();
			var imgEditType = $("#imgEditType").val();
			showOptions(imgBigType,imgEditType);
			
			function showOptions(value,selectVal){
				//提交
				$.post("manage/dictChildren",{
					dictType:'photo_item_type',
					dictValue:value
				},function(res){
					if(res.error_code == "200"){
						if(res.result!=null && res.result.length>0){
							var html = '<option value="">--请选择--</option>';
							$.each(res.result,function(index,item){
								var selected = '';
								if(selectVal==item.key){
									selected = ' selected="selected"';
								}
								html+='<option value="'+item.key+'" '+selected+'>'+item.value+'</option>';
							});
							$("#imgTypeSelect").html(html);
							form.render();
						}
					}else{
						layer.msg(res.msg,{icon:2});
					}
				});
			}
			
			
			form.on('select(imgBigType)', function(data) {
				var value = data.value;
				showOptions(value,'');
			});
			
			
			$('#cancerBtn').on('click',function(){
				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
				parent.layer.close(index);
			});
			
		});
	</script>
</body>
</html>