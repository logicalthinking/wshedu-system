<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html>
<head>
    <base href="<%= basePath%>">
    <title>网站图片管理列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
    <link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
    <link rel="stylesheet" href="css/table.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
    	.img-items img.litle-img{
    		cursor: pointer;
    		width: 30px;
    	}
    	.img-view{
    		display: none;
    		position: absolute;
    		z-index: 999999;
    		background: #FFF;
    		padding: 10px;
    		border: 1px solid #DFDFDF;
    		margin-left: 45%;
    		margin-top: 55px;
    		min-width: 100px;
    		max-width: 450px;
    		max-height: 400px;
    		overflow: hidden;
    	}
    	.img-view img{
    		max-width: 100%;
    		max-height: 100%;
    		border: 1px solid #FFFEEE;
    	}
    </style>
</head>

<body>
	<div class="admin-main">
		<blockquote class="layui-elem-quote">
			<form class="layui-form">
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">图片名称</label>
						<div class="layui-input-inline">
							<input type="text" id="imgName" class="layui-input">
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">所属位置</label>
						<div class="layui-input-inline">
							<select id="imgBigType" lay-filter="imgBigType">
								<option value="">全部</option>
								<c:forEach items="${imgTypes }" var="item" varStatus="i">
									<option value="${item.key }">${item.value }</option>
								</c:forEach>
							</select>
						</div>
						<div class="layui-input-inline">
							<select id="imgType" lay-filter="imgType">
								<option value="">全部</option>
							</select>
						</div>
					</div>
					<div class="layui-inline">
						<a href="javascript:;" class="layui-btn layui-btn-small" id="searchBtn">
							<i class="layui-icon">&#xe615;</i> 搜索
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-small layui-btn-danger" id="resetBtn">
							<i class="layui-icon">&#xe639;</i> 清除
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-small" module-field="image_manage_add" id="addBtn">
							<i class="layui-icon">&#xe608;</i> 新增图片
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-danger layui-btn-small" module-field="image_manage_del" id="delBtn">
							<i class="layui-icon">&#xe640;</i> 删除图片
						</a>
						<a href="javascript:;" class="layui-btn layui-btn-small" module-field="image_manage_sort" id="sortBtn">
							<i class="fa fa-sort"></i> 排序
						</a>
					</div>
				</div>
			</form>
		</blockquote>
		<fieldset class="layui-elem-field">
			<legend>图片列表</legend>
			<div class="img-view">
				<img alt="" src="">
			</div>
			<div class="layui-field-box">
				<table class="site-table table-hover" id="dataTable" lay-filter="dataTable">
					
				</table>
			</div>
		</fieldset>
		<div class="admin-table-page">
			<div id="page" class="page">
			</div>
		</div>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="js/validModule.js"></script>
	<script>
		var pageIndex = 1;
		var pageSize = 10;
		layui.use(['laypage','layer','table','form'], function() {
			var $ = layui.jquery,
				laypage = layui.laypage,
				layer = layui.layer;
			var form = layui.form;
			
			var table = layui.table;
			var dataTable = table.render({
				elem: '#dataTable'
				,url:'manage/webImgInfoList'
				,where:{
					pageIndex:pageIndex,
					pageSize:pageSize,
					sortName:'create_time',
					sortOrder:'desc'
				}
				,method:'post'
				,request: {
					pageName: 'pageIndex' //页码的参数名称，默认：page
					,limitName: 'pageSize' //每页数据量的参数名，默认：limit
				}
				,parseData: function(res){
					return {
						'code':(res.error_code=='200'?0:-1),
						'msg': res.msg,
					    'count': res.total,
					    'data': res.result
					};
				}
				,autoSort:false
				,cellMinWidth: 100 
			    ,cols: [[
			      {checkbox:true, width:'7%',unresize:true}
			      ,{field:'imgName', title: '图片名称',sort: true,unresize:true}
			      ,{field:'imgTypeName', title: '所属位置',sort: true,unresize:true,templet:function(row){
			      	return (row.imgBigTypeName?row.imgBigTypeName:'')+row.imgTypeName;
			      }}
			      ,{field:'imgPath', title: '小图显示',unresize:true,templet:function(row){
			      	var html='<div class="img-items">'+
			      			'<img class="litle-img" src="image/priview/'+row.imgId+row.suffix+'" style="width:30px;"/>'+
			      			'</div>';
			      	return html;
			      }}
			      ,{field:'createTime', title: '添加时间',sort: true,unresize:true}
			      ,{field:'rank',width:'7%', title: '排序号',sort: true,unresize:true}
			      ,{field:'state',width:'10%', title: '状态', sort: true,unresize:true,templet:function(row){
			      	return row.state == 'Y'?'<span class="green">有效</span>':'<span class="red">无效</span>';
			      }}
			      ,{title: '操作', width:'15%',unresize:true, templet:function(row){
			      	var html ='<a href="javascript:;" lay-event="search" class="layui-btn layui-btn-normal layui-btn-mini" module-field="image_manage_view">查看</a>';
						html+='<a href="javascript:;" lay-event="edit" class="layui-btn layui-btn-mini" module-field="image_manage_edit">修改</a>';
						html+= '<a href="javascript:;" lay-event="del" module-field="image_manage_del"'
							+' class="layui-btn layui-btn-danger layui-btn-mini">删除</a>';
			      	return html;
			      }}
				]],
				done: function(res, curr, count){
					data = res.data;
					laypage.render({
						elem: 'page',
						count: count,
						limit: pageSize,
						curr:pageIndex,
						groups:5,
						limits: [10, 20, 30, 40, 50],
						jump: function(obj, first) {
							var curr = obj.curr;
							pageIndex = curr;
							if(!first) {
								table.reload('dataTable', {
							    where: {
							      imgBigType:$("#imgBigType").val(),
							      imgType:$("#imgType").val(),
				    			  imgName:$("#imgName").val(),
							      pageIndex:curr
								  ,pageSize:pageSize
							    }
							  });
							}
						}
					});
					
					var parentModuleCode = "web_manage";
					var moduleCode = "image_manage";
					validModule(parentModuleCode,moduleCode);
				}
			});
			
			
			table.on('rowDouble(dataTable)', function(obj){
				findWebImgInfo(obj.data.imgId);
			});
			
			var sortNames={
				'imgName':'img_name',
				'imgTypeName':'img_type',
				'createTime':'create_time',
				'state':'state',
				'rank':'rank'
			};
			
			table.on('sort(dataTable)', function(obj){
				  pageIndex = 1;
				  table.reload('dataTable', {
				    where: {
				      imgBigType:$("#imgBigType").val(),
				      imgType:$("#imgType").val(),
				      imgName:$("#imgName").val(),
				      sortName: sortNames[obj.field]
				      ,sortOrder: obj.type
				      ,pageIndex:pageIndex
					  ,pageSize:pageSize
				    }
				  });
			});
			
			table.on('tool(dataTable)', function(obj){
			  switch(obj.event){
			    case 'search':
			      findWebImgInfo(obj.data.imgId);
			    break;
			    case 'edit':
			      editWebImgInfo(obj.data.imgId);
			    break;
		      	case 'del':
			      delWebImgInfo(obj.data.imgId,"确认删除该图片吗？");
			    break;
			  };
			});
			
			$('#delBtn').on('click', function() {
				var checkStatus = table.checkStatus('dataTable');
				if(checkStatus.data.length==0){
					layer.msg("请选择一行！",{icon:0});
					return;
				}
				var imgIds = [];
				var flag = false;
				for(var i=0;i<checkStatus.data.length;i++){
					imgIds.push(checkStatus.data[i].imgId);
				}
				
				delWebImgInfo(imgIds.join(","),"确认删除所选图片吗？");
			});
			
			$(document).on('mouseover','.img-items .litle-img',function(){
				$(".img-view").find("img").attr("src",this.src);
				$(".img-view").show();
			});
			
			$(document).on('mouseout','.img-items .litle-img',function(){
				$(".img-view").find("img").attr("src","");
				$(".img-view").hide();
			});
			
			function delWebImgInfo(imgId,message){
				layer.confirm(message,{icon:3,title:'提示信息'},function(index){
					layer.close(index);
					var load = layer.load(2);
					$.post("manage/deleteWebImgInfo",{imgIds:imgId },function(res){
						if(res.error_code == "200"){
							layer.msg(res.msg,{icon:1});
							pageIndex = 1;
							table.reload('dataTable');
						}else{
							layer.msg(res.msg,{icon:2});
						}
						layer.close(load);
					});
				});
			}

			$('#addBtn').on('click', function() {
				var imgBigType=$("#imgBigType").val();
				var imgType = $("#imgType").val();
				var param = '';
				if(imgBigType){
					param += '?imgBigType='+imgBigType;
					if(imgType){
						param += '&imgType='+imgType;
						
					}
				}
				
				layer.open({
					type:2,
					title:'新增图片',
					area: ['740px', '540px'],
					content:'manage/webImgInfoAddPage'+param
				});
			});
			
			function editWebImgInfo(imgId){
				layer.open({
					type:2,
					title:'编辑图片',
					area: ['740px', '540px'],
					content:'manage/webImgInfoEditPage?imgId='+imgId
				});
			}
			
			function findWebImgInfo(imgId){
				layer.open({
					type:2,
					title:'查看图片',
					area: ['960px', '640px'],
					content:'manage/webImgInfoViewPage?imgId='+imgId
				});
			}
			
			$('#sortBtn').on('click', function() {
				var title = "";
				var imgBigType = $("#imgBigType").val();
				if(imgBigType==""){
					layer.msg("请选择图片所属位置",{icon:0});
					return;
				}
				title = $("#imgBigType option:selected").text();
				var imgType = $("#imgType").val();
				if(imgType){
					title += $("#imgType option:selected").text();
				}
				layer.open({
					type:2,
					title: title+'排序',
					area: ['800px', '500px'],
					content:'manage/webImgInfoSortPage?imgBigType='+imgBigType+"&imgType="+imgType
				});
			});
			
			$('#searchBtn').on('click', function() {
				reloadTable();
			});
			
			function reloadTable(){
				pageIndex = 1;
				table.reload('dataTable', {
				  where: {
				    imgBigType:$("#imgBigType").val(),
				    imgType:$("#imgType").val(),
				    imgName:$("#imgName").val(),
				    pageIndex:pageIndex,
				    pageSize:pageSize
				  }
				});
			}
			
			
			form.on('select(imgBigType)', function(data) {
				var value = data.value;
				$("#imgType").val("");
				form.render();
				
				reloadTable();
				
				//提交
				$.post("manage/dictChildren",{
					dictType:'photo_item_type',
					dictValue:value
				},function(res){
					if(res.error_code == "200"){
						if(res.result!=null && res.result.length>0){
							var html = '<option value="">全部</option>';
							$.each(res.result,function(index,item){
								html+='<option value="'+item.key+'">'+item.value+'</option>';
							});
							$("#imgType").html(html);
							form.render();
						}
					}else{
						layer.msg(res.msg,{icon:2});
					}
				});
			});
			
			form.on('select(imgType)', function(data) {
				reloadTable();
			});
			
			
			$('#resetBtn').on('click', function() {
				$("#imgBigType").val("");
				$("#imgType").val("");
				$("#imgName").val("");
				form.render();
			});
		});
	</script>
</body>

</html>
