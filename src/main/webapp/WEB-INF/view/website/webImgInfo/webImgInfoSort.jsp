<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html>
<head>
    <base href="<%= basePath%>">
    <title>网站图片排序</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
    <link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
    <link rel="stylesheet" href="css/table.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
		.img-box{
			border:1px solid #DFDFDF;
			width:110px;
			height:135px;
			text-align:center;
			margin-left: 20px;
			margin-top:20px;
			display: inline-block;
			moz-user-select: -moz-none;
			-moz-user-select: none;
			-o-user-select:none;
			-khtml-user-select:none;
			-webkit-user-select:none;
			-ms-user-select:none;
			user-select:none;
		}
		
		.img-box .img-box-inner{
			margin-top:5px;
			margin-left:5px;
			width:100px;
			height: 100px;
			display:flex;
		}
		.img-box img{
			width:100%;
			max-height:100px;
			moz-user-select: -moz-none;
			-moz-user-select: none;
			-o-user-select:none;
			-khtml-user-select:none;
			-webkit-user-select:none;
			-ms-user-select:none;
			user-select:none;
			display: inline-block;
			margin: auto;
		}
		
		.img-box span{
			width: 100%;
			height: 30px;
			line-height: 30px;
			display: inline-block;
		}
		#sortable{
			border: 1px dashed red;	
			padding-bottom: 20px;
		}
	</style>
</head>

<body>
	<div class="admin-main">
		<form class="layui-form" action="" id="labelForm">
			<div class="layui-form-item">
				<h3>请拖拽鼠标进行排序</h3>
			</div>
			<div class="layui-form-item" id="sortable">
				<c:forEach items="${webImgInfos }" var="item" varStatus="i">
					<div class="img-box" imgId="${item.imgId }">
						<div class="img-box-inner">
							<img alt="${item.imgName }" src="image/priview/${item.imgId}${item.suffix}">
						</div>
						<p>${item.imgName }</p>
					</div>
				</c:forEach>
			</div>
			<div class="layui-form-item" style="text-align: center;margin-top: 40px;">
				<div class="layui-input-block" style="margin-left:0px;">
					<button class="layui-btn" type="button" lay-submit="moduleInfo" lay-filter="moduleInfo" id="saveBtn">确定</button>
					<button type="button" class="layui-btn layui-btn-primary" id="cancerBtn">取消</button>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script type="text/javascript" src="plugins/sort/Sortable.js"></script>
	<script>
		layui.use(['form','layer'], function() {
			var form = layui.form,
				layer = layui.layer,
				$ = layui.jquery;
			
			$('#cancerBtn').on('click',function(){
				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
				parent.layer.close(index);
			});
			
			
			$('#saveBtn').on('click',function(){
				var boxs = $("#sortable").find(".img-box");
				if(boxs && boxs!="undefined" && boxs.length>1){
					var imgIds = [];
					for(var i=0;i<boxs.length;i++){
						imgIds.push($(boxs[i]).attr("imgId"));
					}
					var load = layer.load(2);
					$.post("manage/webImgInfoSort",{imgIds:imgIds},function(res){
						if(res.error_code == "200"){
							layer.msg(res.msg,{icon:1});
							$('#cancerBtn').click();
							
							parent.layui.table.reload('dataTable');
						}else{
							layer.msg(res.msg,{icon:2});
						}
						layer.close(load);
					});
				}else if(boxs && boxs!="undefined" && boxs.length==1){
					layer.msg("至少需要两个模块才可以排序",{icon:0});
				}else{
					layer.msg("当前没有模块可以排序",{icon:0});
				}
			});
			
			Sortable.create(document.getElementById('sortable'), {
               animation: 150, //动画参数
               onAdd: function (evt) {   //拖拽时候添加有新的节点的时候发生该事件
                    //console.log('onAdd.foo:', [evt.item, evt.from]);
               },
               onUpdate: function (evt) {  //拖拽更新节点位置发生该事件
                    //console.log('onUpdate.foo:', [evt.item, evt.from]);
               },
               onRemove: function (evt) {   //删除拖拽节点的时候促发该事件
                    //console.log('onRemove.foo:', [evt.item, evt.from]);
               },
               onStart: function (evt) {  //开始拖拽出发该函数
                    //console.log('onStart.foo:', [evt.item, evt.from]);
               },
               onSort: function (evt) {  //发生排序发生该事件
                    //console.log('onSort.foo:', [evt.item, evt.from]);
               },
               onEnd: function (evt) { //拖拽完毕之后发生该事件
                   // console.log('onEnd.foo:', [evt.item, evt.from]);
               }
          });
			
		});
		
	</script>
</body>

</html>
