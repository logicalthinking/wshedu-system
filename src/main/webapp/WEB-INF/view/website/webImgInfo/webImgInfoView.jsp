<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<jsp:include page="../../common/base.jsp"/>
<!DOCTYPE html>
<html >
<head>
	<base href="<%= basePath%>">
    <title>查看图片</title>
    <meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="plugins/layui-v2.5.5/layui/css/${colorStyle}layui.css" media="all" />
	<link rel="stylesheet" href="css/${colorStyle}global.css" media="all">
	<link rel="stylesheet" href="css/table.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		.layui-form-item{
			text-align: center;
		}
		.img-item{
			width: 80%;
			min-height:200px;
			border: 1px solid #DDD;
		}
	</style>
</head>
<body>
	<div style="margin: 15px;">
		<form class="layui-form" action="">
			<div class="layui-form-item img-item-div">
				<c:if test="${not empty webImgInfo.imgUrl }">
					<a href="${webImgInfo.imgUrl }" target="_blank" title="${webImgInfo.imgUrl }">
						<img class="img-item" alt="${webImgInfo.imgName }" src="image/priview/${webImgInfo.imgId}${webImgInfo.suffix}">
					</a>
				</c:if>
				<c:if test="${empty webImgInfo.imgUrl }">
					<img class="img-item" alt="${webImgInfo.imgName }" src="image/priview/${webImgInfo.imgId }${webImgInfo.suffix}">
				</c:if>
			</div>
			<div class="layui-form-item">
				${webImgInfo.imgName }
			</div>
		</form>
	</div>
	<script type="text/javascript" src="plugins/layui-v2.5.5/layui/layui.js"></script>
	<script>
		layui.use(['form','layer'], function() {
			var form = layui.form,
				layer = layui.layer,
				$ = layui.jquery;
				
		});
	</script>
</body>
</html>
