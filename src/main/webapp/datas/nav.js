var navs = [{
	"title": "网站管理",
	"icon": "&#xe7ae;",
	"spread": true,
	"children": [{
		"title": "文案管理",
		"icon": "&#xe655;",
		"href": "manage/articleListPage"
	}, {
		"title": "图片管理",
		"icon": "&#xe64a;",
		"href": "manage/webImgInfoListPage"
	},
	{
		"title": "分类管理",
		"icon": "&#xe656;",
		"href": "manage/categoryListPage"
	},
	{
		"title": "留言管理",
		"icon": "&#xe63c;",
		"href": "manage/leaveMessageListPage"
	}]
},
{
	"title": "考卷管理",
	"icon": "fa fa-th-large",
	"spread": true,
	"children": [{
		"title": "试卷管理",
		"icon": "fa fa-align-justify",
		"href": "manage/examListPage"
	}, {
		"title": "试题管理",
		"icon": "&#xe63c;",
		"href": "manage/subInfoListPage"
	},
	{
		"title": "问卷管理",
		"icon": "&#xe705;",
		"href": "manage/questionExamListPage"
	},
	{
		"title": "我的考卷",
		"icon": "&#xe705;",
		"href": "manage/userExamListPage"
	}]
},
{
	"title": "系统管理",
	"icon": "&#xe614;",
	"spread": true,
	"children": [{
		"title": "用户管理",
		"icon": "&#xe66f;",
		"href": "manage/usersListPage"
	},
	{
		"title": "角色管理",
		"icon": "&#xe770;",
		"href": "manage/roleListPage"
	},
	{
		"title": "权限管理",
		"icon": "&#xe672;",
		"href": "manage/moduleManagePage"
	},
	{
		"title": "模块管理",
		"icon": "&#xe653;",
		"href": "manage/moduleListPage"
	},
	{
		"title": "系统日志",
		"icon": "&#xe60a;",
		"href": "manage/sysLogListPage"
	},
	{
		"title": "数据字典",
		"icon": "&#xe630;",
		"href": "manage/dictInfoListPage"
	}]
}];