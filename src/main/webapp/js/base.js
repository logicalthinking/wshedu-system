layui.use(['form','layer'], function(){
      var carousel = layui.carousel
      ,form = layui.form
      ,$ = layui.jquery;
      var index ;
      $("#leaveMessage").on("click",function(){
      	
      	if(index){
      		layer.close(index);
      	}
      	
      	index = layer.open({
      		type:2,
      		title: '留言反馈',
      		area: ['340px', '460px'],
      		offset: 'rb',
      		shade: 0,
      		anim:2,
      		resize:false,
      		isOutAnim:false,
      		fixed:true,
      		move:false,
      		skin: 'dialog-bg',
      		content:'leaveMessageAdd'
      	});
      });
       
       $(".nav-btn-inner").on({
      	mouseover:function(){
      		$(".nav-btn-ul").show();
      	},
      	mouseout:function(){
      		$(".nav-btn-ul").hide();
      	}
      });
      
      var nav = $(".indexNav .nav-item");
      var navLine = nav.find(".nav-line");
      var defLineW = nav.find("ul").find("li.active").find("a").width();
      var defLineLeft = nav.find("li.active").find("a").position().left;
      navLine.css({left:defLineLeft, width:defLineW});
      
      $(".indexNav .nav-item ul li").find("a").hover(function(){
	    var index = $(this).index();
	    var curLineW = $(this).width();
	    var curLineLeft = $(this).position().left;
	    navLine.stop().animate({
	      left:curLineLeft,
	      width:curLineW
	    },300);
	  },function(){
	    navLine.stop().animate({
	      left:defLineLeft,
	      width:defLineW
	    },300);
	  });
      
});

