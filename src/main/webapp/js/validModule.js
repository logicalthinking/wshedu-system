/**
 * 校验页面操作按钮
 * 
 * @param parentModuleCode
 * @param moduleCode
 * @param btnArray
 */
function validModule(parentModuleCode,moduleCode,$){
	if($ && $!="undefined"){
		validMod(parentModuleCode,moduleCode,btnArray,$);
	}else{
		layui.use(['form','layer'], function() {
			var form = layui.form,
			layer = layui.layer,
			$ = layui.jquery;
			validMod(parentModuleCode,moduleCode,$);
		});
	}
}

function validMod(parentModuleCode,moduleCode,$){
	var btnArray = $("a[module-field],button[module-field]");
	$.post("manage/userPageModuleList",{
		parentModuleCode:parentModuleCode,
		moduleCode:moduleCode
	},function(res){
		if(res.error_code=="200"){
			if(res.result!=null && res.result.length>0){
				for(var i=0;i<btnArray.length;i++){
					var bts = $(btnArray[i]);
					for(var j=0;j<bts.length;j++){
						var field = $(bts[j]).attr("module-field");
						var flag=0;
						$.each(res.result,function(index,item){
							if(field==item.field){
								$(bts[j]).show();
								$(bts[j]).css("display","inline-block");
								flag = 1;
							}
						});
						if(flag == 0){
							$(bts[j]).on("click",function(){disabledClick();});
							$(btnArray[i]).remove();
						}
					}
					
					
				}
			}
		}
	});
}

function disabledClick(){
	layui.use(['form','layer'],function(){
		var form = layui.form;
		var layer = layui.layer;
		layer.msg("您没有操作权限！",{icon:0});
		return;
	});
}