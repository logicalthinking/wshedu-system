package com.logicalthinking.manage.system.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.logicalthinking.manage.system.entity.ArticleInfo;
import com.logicalthinking.manage.system.service.ArticleInfoService;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 文案信息测试类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationContext.xml"})
public class ArticleInfoTest {

	@Resource
	private ArticleInfoService articleInfoService;

	@Test
	public void testArticleInfoListBiz(){
		try {
			Map<String,Object> map=new HashMap<String, Object>();
			List<ArticleInfo> list= articleInfoService.selectAllArticleInfoList(map);
			System.out.println(list);
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}
}