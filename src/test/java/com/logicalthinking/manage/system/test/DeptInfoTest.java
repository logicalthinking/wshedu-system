package com.logicalthinking.manage.system.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.logicalthinking.manage.system.entity.DeptInfo;
import com.logicalthinking.manage.system.service.DeptInfoService;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 部门信息测试类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-11-08
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationContext.xml"})
public class DeptInfoTest {

	@Resource
	private DeptInfoService deptInfoService;

	@Test
	public void testDeptInfoListBiz(){
		try {
			Map<String,Object> map=new HashMap<String, Object>();
			List<DeptInfo> list= deptInfoService.selectAllDeptInfoList(map);
			System.out.println(list);
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}
}