package com.logicalthinking.manage.system.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.logicalthinking.manage.system.entity.SysLogInfo;
import com.logicalthinking.manage.system.service.SysLogInfoService;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 系统日志信息测试类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationContext.xml"})
public class SysLogInfoTest {

	@Resource
	private SysLogInfoService sysLogInfoService;

	@Test
	public void testSysLogInfoListBiz(){
		try {
			Map<String,Object> map=new HashMap<String, Object>();
			List<SysLogInfo> list= sysLogInfoService.selectAllSysLogInfoList(map);
			System.out.println(list);
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}
}