package com.logicalthinking.manage.system.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.logicalthinking.manage.system.entity.SysUserRole;
import com.logicalthinking.manage.system.service.SysUserRoleService;
import com.logicalthinking.manage.system.utils.DaoException;

/**
 * 用户关联角色信息测试类
 * 
 * @author lanping
 * @version 1.0
 * @date 2019-10-16
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationContext.xml"})
public class SysUserRoleTest {

	@Resource
	private SysUserRoleService sysUserRoleService;

	@Test
	public void testSysUserRoleListBiz(){
		try {
			Map<String,Object> map=new HashMap<String, Object>();
			List<SysUserRole> list= sysUserRoleService.selectAllSysUserRoleList(map);
			System.out.println(list);
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}
}